FROM nginx
COPY ./swan /export/files/swan
COPY conf.d/swan.conf /etc/nginx/conf.d/
EXPOSE 50102