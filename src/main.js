import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { Base64 } from "js-base64";
import store from "./store";
import echarts from "echarts";
import { VueAxios } from "./utils/requests";
import "./core/use";
// import './tools/map'
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "vue-orgchart/dist/style.min.css";
import locale from "element-ui/lib/locale/lang/zh-CN";
// import VideoPlayer from 'vue-video-player'
// import 'videojs-flash'

import md5 from "js-md5";
Vue.prototype.$md5 = md5;

import vcolorpicker from "vcolorpicker";

import highcharts from "highcharts";
import VueHighCharts from "vue-highcharts";
import highcharts3d from "highcharts/highcharts-3d";
highcharts3d(highcharts);

// import Videojs from 'video.js'
import "video.js/dist/video-js.css";
// Vue.prototype.$video = Videojs
// import hls from 'videojs-contrib-hls'
// Vue.use(hls)
// Vue.use(VideoPlayer)

Vue.use(ElementUI, { locale });

Vue.config.productionTip = false;
Vue.use(Base64)
Vue.use(ElementUI);
Vue.use(VueAxios);
Vue.use(echarts);
Vue.use(vcolorpicker);
Vue.directive("permission", {
  inserted: function(el, binding) {
    let btnList = Vue.ls.get("btnList");
    if (router.currentRoute.path.indexOf("lqc") > -1) {
      // 说明是在沥青厂项目
      if (btnList) {
        if (!btnList[binding.value]) {
          el.style.display = "none";
        }
      } else {
        Vue.prototype.$message({ message: "获取权限列表失败", type: "error" });
      }
    }
  }
});
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
