import AMap from "AMap";
import store from "@/store/index";
import { selectdeviceIcon } from "./camera";
/**
 * 根据撒点，创建地图并在地图撒点
 * params pointList 点集合
 */
export function createMap(pointList, candidates) {
  var cluster,
    mapList = []; // 所有的撒点marker
  const selectViewList = [];
  const ids = [];
  let icon = {};
  let map = new AMap.Map("map-container", {
    zoom: 15, //级别
    center: [121.508515, 31.14298], //中心点坐标121.508515,  31.142980
    viewMode: "3D" //使用3D视图
  });
  pointList.forEach(item => {
    if ("position" in item && item.position.longitude) {
      if (item.status !== "DAMAGED") {
        let marker = new AMap.Marker({
          resizeEnable: true,
          position: new AMap.LngLat(
            item.position.longitude,
            item.position.latitude
          ), // 经纬度对象，也可以是经纬度构成的一维数组[116.39, 39.9]
          title: "上海",
          icon: selectdeviceIcon("DEFAULT")
        });
        if (item.properties._sys_detectionType) {
          marker = new AMap.Marker({
            resizeEnable: true,
            position: new AMap.LngLat(
              item.position.longitude,
              item.position.latitude
            ), // 经纬度对象，也可以是经纬度构成的一维数组[116.39, 39.9]
            title: "上海",
            icon: selectdeviceIcon(item.properties._sys_detectionType)
          });
        }
        marker.on("click", function(e) {
          if (ids.length > 0 && ids.includes(item.urn.id)) {
            for (let i = 0; i <= ids.length; i++) {
              if (ids[i] === item.urn.id) {
                ids.splice(i, 1);
                selectViewList.splice(i, 1);
                icon = new AMap.Icon({
                  position: new AMap.LngLat(e.lnglat.lng, e.lnglat.lat),
                  image: selectdeviceIcon(item.properties._sys_detectionType) // Icon的图像
                });
              }
            }
          } else {
            icon = new AMap.Icon({
              position: new AMap.LngLat(e.lnglat.lng, e.lnglat.lat),
              image: selectdeviceIcon("SELECT") // Icon的图像
            });
            ids.push(item.urn.id);
            selectViewList.push(item);
          }
          marker.setIcon(icon);
          let selectView = [...new Set(selectViewList)];
          store.commit("SELECT_VIEW", selectView);
          store.commit("SELECT_VIEW_LIST", selectViewList);
        });
        mapList.push(marker);
        map.add(mapList);
        if (candidates) {
          candidates.forEach(camera => {
            if (
              camera ===
              `urn:${item.urn.organization}:${item.urn.group}:${item.urn.id}`
            ) {
              // 有出入
              candidates.forEach(camera => {
                if (
                  camera ===
                  `urn:${item.urn.organization}:${item.urn.group}:${item.urn.id}`
                ) {
                  // 有出入
                  let icon = new AMap.Icon({
                    position: new AMap.LngLat(
                      item.position.longitude,
                      item.position.latitude
                    ),
                    image:
                      "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/jiebangcun-mini-program/hong.png" // Icon的图像
                  });
                  marker.setIcon(icon);
                }
              });
              selectViewList.push(item); // 点击时选中点集合
              let selectView = [...new Set(selectViewList)];
              store.commit("SELECT_VIEW", selectView);
              store.commit("SELECT_VIEW_LIST", selectViewList);
            }
          });
        }
      }
    }
  });
  if (cluster) {
    cluster.setMap(null);
  }
  cluster = new AMap.MarkerClusterer(map, mapList, { gridSize: 20 }); //, {gridSize: 9900}
  // 点击聚合点的回调
  cluster.on("click", function(e) {
    new AMap.InfoWindow({
      content: "模拟事件触发"
    }).open(map, e.lnglat);
    // store.commit('ZOOM_IN_GLOBAL')
    // let lnglatLocalList = e.markers
    // let obj = {
    //     position: lnglatLocalList[0].Ce.position,
    //     markers: lnglatLocalList
    // }
    // store.commit('SHOW_INFO_WINDOW', obj)
  });
}

// 点击聚合点的时候弹出窗
// 构造矢量圆形
// var circle = new AMap.Circle({
//     center: new AMap.LngLat("116.403322", "39.920255"), // 圆心位置
//     radius: 1000,  //半径
//     strokeColor: "#F33",  //线颜色
//     strokeOpacity: 1,  //线透明度
//     strokeWeight: 3,  //线粗细度
//     fillColor: "#ee2200",  //填充颜色
//     fillOpacity: 0.35 //填充透明度
// });
