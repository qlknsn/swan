const deviceIcons = {
    'GENERAL':'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/rockfish-bigscreen/zhoujiadu/office.png',
    'VEHICLE':'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/rockfish-bigscreen/zhoujiadu/road.png',
    'VIOLATION':'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/rockfish-bigscreen/zhoujiadu/car.png',
    'FACIAL':'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/rockfish-bigscreen/zhoujiadu/face.png',
    'DAMAGED':'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/rockfish-bigscreen/zhoujiadu/errIcon.png',
    'SELECT': 'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/jiebangcun-mini-program/huangsedian.png',
    'DEFAULT': 'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/jiebangcun-mini-program/hong.png',
};

export function selectdeviceIcon(statu) {
    return deviceIcons[statu]
}