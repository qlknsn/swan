const safeUkeyRouter = [
  //    添加定时任务
  {
    path: "/homepages",
    name: "homepage",
    component: () => import("@/views/safeUkey/home/homePage.vue"),
    meta: {
      title: "安全U盾-首页"
    }
  },
  {
    path: "/managementList",
    name: "managementList",
    component: () => import("@/views/safeUkey/management/managementList.vue"),
    meta: {
      title: "安全U盾-设备管理"
    }
  },
  {
    path: "/bindManage",
    name: "bindManage",
    component: () => import("@/views/safeUkey/management/bindManage.vue"),
    meta: {
      title: "安全U盾-设备绑定"
    }
  },
  {
    path: "/logManage",
    name: "logManage",
    component: () => import("@/views/safeUkey/log/logManage.vue"),
    meta: {
      title: "安全U盾-日志管理"
    }
  },
  {
    path: "/systemManage",
    name: "systemManage",
    component: () => import("@/views/safeUkey/systemManage/systemManage.vue"),
    meta: {
      title: "安全U盾-系统管理"
    }
  },
  {
    path: "/userManages",
    name: "userManages",
    component: () => import("@/views/safeUkey/userManage/userManage.vue"),
    meta: {
      title: "安全U盾-用户管理"
    }
  },
  {
    path: "/roleManages",
    name: "roleManages",
    component: () => import("@/views/safeUkey/userManage/roleManages.vue"),
    meta: {
      title: "安全U盾-角色管理"
    }
  }
];

export default safeUkeyRouter;
