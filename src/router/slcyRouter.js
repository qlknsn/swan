const slcyRouter = [
  {
    path: "/slcy/department",
    name: "slcydepartment",
    component: () => import("@/views/slcy/department.vue"),
    meta: { title: "部门列表" }
  },
  {
    path: "/slcy/people",
    name: "slcyPeople",
    component: () => import("@/views/slcy/people.vue"),
    meta: { title: "人员列表" }
  },
  {
    path: "/slcy/event",
    name: "slcyEvent",
    component: () => import("@/views/slcy/event.vue"),
    meta: { title: "事件" }
  }
];
export default slcyRouter;
