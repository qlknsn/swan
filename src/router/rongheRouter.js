const zhoupuronghe = [
  {
    path: "/r/shujujiashicang",
    component: () => import("@/views/ronghe/data/shujujiashicang.vue"),
    meta: { title: "数据驾驶舱" }
  },
  {
    path: "/r/discuss",
    component: () => import("@/views/ronghe/discussList.vue"),
    meta: { title: "动态列表" }
  },
  {
    path: "/r/discuss/:disscussID",
    component: () => import("@/views/ronghe/discussDetail.vue"),
    meta: { title: "动态详情" }
  },
  {
    path: "/r/peoplemanage/peoplelist",
    component: () => import("@/views/ronghe/peoplemanage/tabs.vue"),
    meta: { title: "居民管理" }
  },
  {
    path: "/r/reportManage/reportList",
    component: () => import("@/views/ronghe/reportManage/reportList.vue"),
    meta: { title: "公告管理" }
  },
  {
    path: "/r/reportManage/reportDetail",
    component: () => import("@/views/ronghe/reportManage/reportDetail.vue"),
    meta: { title: "公告详情" }
  },
  {
    path: "/r/comeHu/add",
    component: () => import("@/views/ronghe/comeHu/comeAdd.vue"),
    meta: { title: "添加来沪人员" }
  },
  {
    path: "/r/comeHu/comeHuList",
    component: () => import("@/views/ronghe/comeHu/comeHuList.vue"),
    meta: { title: "来沪人员列表" }
  },
  {
    path: "/r/comeHu/comeHuDetail",
    component: () => import("@/views/ronghe/comeHu/comeHuDetail.vue"),
    meta: { title: "来沪人员详情" }
  },
  {
    path: "/r/homeServe/homeServeList",
    component: () => import("@/views/ronghe/homeServe/homeServeList.vue"),
    meta: { title: "家门口服务" }
  },
  {
    path: "/r/homeServe/homeserviceDetail",
    component: () => import("@/views/ronghe/homeServe/homeserviceDetail.vue"),
    meta: { title: "家门口服务详情" }
  },
  {
    path: "/r/peoplemanage/peopledetail",
    component: () => import("@/views/ronghe/peoplemanage/peopleDetail.vue"),
    meta: { title: "居民详情" }
  },
  {
    path: "/r/peoplemanage/forbiddenDetail",
    component: () => import("@/views/ronghe/peoplemanage/forbiddenDetail.vue"),
    meta: { title: "封号详情" }
  },
  {
    path: "/r/systemManage/staffManage",
    component: () => import("@/views/ronghe/systemManage/staffManage.vue"),
    meta: { title: "职工管理" }
  },
  {
    path: "/r/repair/repairList",
    component: () => import("@/views/ronghe/repair/repairList.vue"),
    meta: { title: "物业报修列表" }
  },
  {
    path: "/r/repair/repairDetail",
    component: () => import("@/views/ronghe/repair/repairDetail.vue"),
    meta: { title: "物业报修详情" }
  },
  {
    path: "/r/help/help",
    component: () => import("@/views/ronghe/help/help.vue"),
    meta: { title: "一键求助" }
  },
  {
    path: "/r/lostAndFound",
    component: () => import("@/views/ronghe/lostAndFound/lostAndFound.vue"),
    meta: { title: "失物招领" }
  },
  {
    path: "/r/goodThings",
    component: () => import("@/views/ronghe/goodThings/goodThings.vue"),
    meta: { title: "好人好事" }
  },
  {
    path: "/r/goodThingDetail",
    component: () => import("@/views/ronghe/goodThings/goodThingDetail.vue"),
    meta: { title: "详情" }
  },
  {
    path: "/r/lostAndFoundDetail",
    component: () => import("@/views/ronghe/lostAndFound/lostAndFoundDetail.vue"),
    meta: { title: "详情" }
  }
];

export default zhoupuronghe;
