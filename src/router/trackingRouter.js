const trackingRouter = [
  {
    path: "/t/dashboard",
    component: () => import("@/views/tracking/trackingDashboard.vue"),
    meta: { title: "资源大盘" }
  },
  {
    path: "/t/access",
    component: () => import("@/views/tracking/dataCenter/trackingRecord.vue"),
    meta: { title: "访问列表" }
  }
];

export default trackingRouter;
