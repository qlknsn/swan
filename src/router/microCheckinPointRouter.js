const microCheckinPointRouter = [
  /**
   * dashboard页面
   */
  {
    path: "/mcp/dashboard",
    name: "mcpDashboard",
    component: () => import("@/views/mcp/dashboard.vue"),
    meta: {
      title: "微卡口"
    }
  },
  /**
   * 实时抓拍--工作台
   */
  {
    path: "/mcp/realtime/capture/dashboard",
    name: "mcpRealtimeCaptureDashboard",
    component: () => import("@/views/mcp/realtimeCapture/dashboard.vue"),
    meta: {
      title: "工作台"
    }
  },
  /**
   * 实时抓拍--车辆抓拍
   */
  {
    path: "/mcp/realtime/capture/car",
    name: "mcpRealtimeCaptureCar",
    component: () => import("@/views/mcp/realtimeCapture/carCapture.vue"),
    meta: {
      title: "车辆抓拍"
    }
  },
  /**
   * 实时抓拍--人像抓拍
   */
  {
    path: "/mcp/realtime/capture/people",
    name: "mcpRealtimeCapturePeople",
    component: () => import("@/views/mcp/realtimeCapture/peopleCapture.vue"),
    meta: { title: "人像抓拍" }
  },
  /**
   * 实时抓拍--人像搜索结果
   */
  {
    path: "/mcp/search/people/result",
    name: "mcpSearchPeopleResult",
    component: () =>
      import("@/views/mcp/realtimeCapture/searchPeopleResult.vue"),
    meta: { title: "人像抓拍搜索结果" }
  },
  /**
   * 实时抓拍--车辆搜索结果
   */
  {
    path: "/mcp/search/car/result",
    name: "mcpSearchCarResult",
    component: () => import("@/views/mcp/realtimeCapture/searchCarResult.vue"),
    meta: { title: "车辆抓拍搜索结果" }
  },

  /**
   * 系统管理--组织架构管理
   */
  {
    path: "/mcp/settings/org",
    name: "mcpOrgFrameManage",
    component: () => import("@/views/mcp/settings/orgFrameManage.vue"),
    meta: { title: "组织架构管理" }
  },
  /**
   * 系统管理--角色管理
   */
  {
    path: "/mcp/settings/role",
    name: "mcpRoleManage",
    component: () => import("@/views/mcp/settings/roleManage.vue"),
    meta: { title: "角色管理" }
  },
  /**
   * 系统管理--用户管理
   */
  {
    path: "/mcp/settings/user",
    name: "mcpUserManage",
    component: () => import("@/views/mcp/settings/userManage.vue"),
    meta: { title: "用户管理" }
  },
  /**
   * 系统管理--权限管理
   */
  {
    path: "/mcp/settings/limit",
    name: "mcpLimitManage",
    component: () => import("@/views/mcp/settings/limitManage.vue"),
    meta: { title: "权限管理" }
  },
  /**
   * 系统管理--系统日志
   */
  {
    path: "/mcp/settings/log",
    name: "mcpLogManage",
    component: () => import("@/views/mcp/settings/systemLog.vue"),
    meta: { title: "系统日志" }
  },
  // 预警推送
  {
    path: "/mcp/warning/people",
    name: "mcpWarningPeople",
    component: () => import("@/views/mcp/warning/warningPeople.vue"),
    meta: { title: "人脸预警" }
  },
  {
    path: "/mcp/warning/car",
    name: "mcpWarningCar",
    component: () => import("@/views/mcp/warning/warningCar.vue"),
    meta: { title: "车辆预警" }
  },
  {
    path: "/mcp/warning/singlePeoContrast",
    name: "mcpWarningPeoContrast",
    component: () => import("@/views/mcp/warning/singlePeoContrast.vue"),
    meta: { title: "人像对比" }
  },
  {
    path: "/mcp/warning/singleCarContrast",
    name: "mcpWarningCarContrast",
    component: () => import("@/views/mcp/warning/singleCarContrast.vue"),
    meta: { title: "车辆对比" }
  },
  /**
   * 资源管理--人像库
   */
  {
    path: "/mcp/resourceManagement/peopleLibrary",
    name: "mcpResourceManagePeopleLibrary",
    component: () => import("@/views/mcp/resourceManagement/peopleLibrary.vue"),
    meta: { title: "人像库" }
  },
  /**
   * 资源管理--车辆库
   */
  {
    path: "/mcp/resourceManagement/carLibrary",
    name: "mcpResourceManageCarLibrary",
    component: () => import("@/views/mcp/resourceManagement/carLibrary.vue"),
    meta: { title: "车辆库" }
  },
  /**
   * 资源管理--人像抓拍库
   */
  {
    path: "/mcp/resourceManagement/peopleCaptureLibrary",
    name: "mcpResourceManagePeopleCaptureLibrary",
    component: () =>
      import("@/views/mcp/resourceManagement/peopleCaptureLibrary.vue"),
    meta: { title: "人像抓拍库" }
  },
  /**
   * 资源管理--车辆抓拍库
   */
  {
    path: "/mcp/resourceManagement/carCaptureLibrary",
    name: "mcpResourceManageCarCaptureLibrary",
    component: () =>
      import("@/views/mcp/resourceManagement/carCaptureLibrary.vue"),
    meta: { title: "车辆抓拍库" }
  },
  /**
   * 资源管理--显示栏目
   */
  {
    path: "/mcp/resourceManagement/columns",
    name: "mcpResourceManageColumns",
    component: () => import("@/views/mcp/resourceManagement/columns.vue"),
    meta: { title: "显示栏目" }
  },
  /**
   * 资源管理--人像库列表
   */
  {
    path: "/mcp/resourceManagement/peopleLibraryList",
    name: "mcpResourceManagePeopleLibraryList",
    component: () =>
      import("@/views/mcp/resourceManagement/peopleLibraryList.vue"),
    meta: { title: "人像库列表" }
  },
  /**
   * 资源管理--车辆库列表
   */
  {
    path: "/mcp/resourceManagement/carLibraryList",
    name: "mcpResourceManageCarLibraryList",
    component: () =>
      import("@/views/mcp/resourceManagement/carLibraryList.vue"),
    meta: { title: "车辆库列表" }
  },
  /**
   * 资源管理--人像抓拍库列表
   */
  {
    path: "/mcp/resourceManagement/peopleCaptureLibraryList",
    name: "mcpResourceManagePeopleCaptureLibraryList",
    component: () =>
      import("@/views/mcp/resourceManagement/peopleCaptureLibraryList.vue"),
    meta: { title: "人像抓拍库列表" }
  },
  /**
   * 资源管理--车辆抓拍库列表
   */
  {
    path: "/mcp/resourceManagement/carCaptureLibraryList",
    name: "mcpResourceManageCarCaptureLibraryList",
    component: () =>
      import("@/views/mcp/resourceManagement/carCaptureLibraryList.vue"),
    meta: { title: "车辆抓拍库列表" }
  },
  // 人像检索
  {
    path: "/mcp/retrieval/portraitRetrieval",
    name: "mcpPortraitRetrieval",
    component: () => import("@/views/mcp/retrieval/portraitRetrieval.vue"),
    meta: { title: "人像检索" }
  },
  // 车辆检索
  {
    path: "/mcp/retrieval/carRetrieval",
    name: "mcpCarRetrieval",
    component: () => import("@/views/mcp/retrieval/carRetrieval.vue"),
    meta: { title: "车辆检索" }
  },
  // 应用管理
  {
    path: "/mcp/application/warningManage",
    name: "mcpApplicationWarningManage",
    component: () => import("@/views/mcp/application/warningManage.vue"),
    meta: { title: "预警推送管理" }
  },
  {
    path: "/mcp/application/editPeopleWarning",
    name: "mcpApplicationEditPeopleWarning",
    component: () => import("@/views/mcp/application/editPeopleWarning.vue"),
    meta: { title: "新增人像预警流程" }
  },
  {
    path: "/mcp/application/singlePeopleManageWarning",
    name: "mcpApplicationSinglePeopleManageWarning",
    component: () =>
      import("@/views/mcp/application/singlePeopleManageWarning.vue"),
    meta: { title: "管理人像预警" }
  },
  {
    path: "/mcp/application/editCarWarning",
    name: "mcpApplicationEditCarWarning",
    component: () => import("@/views/mcp/application/editCarWarning.vue"),
    meta: { title: "新增车辆预警流程" }
  },
  {
    path: "/mcp/application/singleCarManageWarning",
    name: "mcpApplicationSingleCarManageWarning",
    component: () =>
      import("@/views/mcp/application/singleCarManageWarning.vue"),
    meta: { title: "管理车辆预警" }
  },
  {
    path: "/mcp/application/thresholdManage",
    name: "mcpApplicationHhresholdManage",
    component: () => import("@/views/mcp/application/thresholdManage.vue"),
    meta: { title: "对比阈值管理" }
  },
  {
    path: "/mcp/application/editThresholdManage",
    name: "mcpApplicationEditThresholdManage",
    component: () => import("@/views/mcp/application/editThresholdManage.vue"),
    meta: { title: "增加对比阈值管理" }
  },
  {
    path: "/mcp/application/singleThresholdManage",
    name: "mcpApplicationSingleThresholdManage",
    component: () =>
      import("@/views/mcp/application/singleThresholdManage.vue"),
    meta: { title: "管理对比阈值" }
  }
];

export default microCheckinPointRouter;
