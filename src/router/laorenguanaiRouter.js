const lrguanaiRouter = [
  {
    path: "/lrga/homepage",
    name: "lrguanaiHomepage",
    meta: { title: "首页" },
    component: () => import("@/views/lrguanai/homepage/homepage.vue")
  },
  {
    path: "/lrga/alarmLogging",
    name: "lrguanaiAlarmLogging",
    meta: { title: "报警记录" },
    component: () => import("@/views/lrguanai/myDevice/alarmLogging.vue")
  },
  {
    path: "/lrga/deviceList",
    name: "lrguanaiDeviceList",
    meta: { title: "设备列表" },
    component: () => import("@/views/lrguanai/myDevice/deviceList.vue")
  },
]
export default lrguanaiRouter
