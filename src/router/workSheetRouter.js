const workSheetRouter = [
  {
    path: "/w/dashboard",
    component: () => import("@/views/workSheet/workSheetDashboard.vue"),
    meta: { title: "数据统计" }
  },
  {
    path: "/w/list",
    component: () => import("@/views/workSheet/workSheetList.vue"),
    meta: { title: "工单列表" }
  },
  {
    path: "/w/detail/:sheetID",
    component: () => import("@/views/workSheet/workSheetDetail.vue"),
    meta: { title: "工单详情" }
  },
  {
    path: "/w/project",
    component: () => import("@/views/workSheet/workSheetProject.vue"),
    meta: { title: "工单详情" }
  }
];

export default workSheetRouter;
