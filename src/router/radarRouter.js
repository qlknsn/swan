const radarRouter = [
  {
    path: "/radar/casedetail",
    name: "radarCaseDetail",
    component: () => import("@/views/radar/caseDetail/caseDetail.vue"),
    meta: { title: "标签管理" }
  },
  {
    path: "/radar/label",
    name: "radarLabelManage",
    component: () => import("@/views/radar/labelManage/labelManage.vue"),
    meta: { title: "标签管理" }
  },
  {
    path: "/radar/rfidinput",
    name: "radarRfidInput",
    component: () => import("@/views/radar/radarUtils/rfidinput.vue"),
    meta: { title: "录入RFID标签" }
  },
  {
    path: "/radar/carlist",
    name: "electricCarList",
    component: () =>
      import("@/views/radar/electricCarList/electricCarList.vue"),
    meta: {
      title: "雷达电动车列表"
    }
  },
  {
    path: "/radar/detail",
    name: "radarDetail",
    component: () => import("@/views/radar/radarDetail.vue"),
    meta: {
      title: "雷达详情"
    }
  },
  {
    path: "/radar/electricCarPath",
    name: "electricCarPath",
    component: () =>
      import("@/views/radar/electricCarPath/electricCarPath.vue"),
    meta: {
      title: "电动车轨迹"
    }
  },
  {
    path: "/radar/radarList",
    name: "radar-list",
    component: () => import("@/views/radar/radarList/radarList.vue"),
    meta: {
      title: "雷达列表"
    }
  },
  {
    path: "/radar/usermanage",
    name: "radarUserManage",
    component: () => import("@/views/radar/userManage/userManage.vue"),
    meta: {
      title: "用户管理"
    }
  },
  {
    path: "/radar/usermanageDetail",
    name: "radarUserMangeDetail",
    component: () => import("@/views/radar/userManage/userManageDetail.vue"),
    meta: {
      title: "用户详情"
    }
  },
  {
    path: "/electricCar/detail",
    name: "electricCarDetail",
    component: () =>
      import("@/views/radar/electricCarList/electricCarDetail.vue"),
    meta: {
      title: "电动车详情"
    }
  },
  {
    path: "/radar/radarOnline",
    name: "radarOnline",
    component: () => import("@/views/radar/radarOnline/radarOnline.vue"),
    meta: {
      title: "雷达在线率"
    }
  },
  {
    path: "/alarmManage/alarmManage",
    name: "alarmManage",
    component: () => import("@/views/radar/alarmManage/alarmManage.vue"),
    meta: {
      title: "接警管理"
    }
  },
  {
    path: "/alarmManage/caseManage",
    name: "caseManage",
    component: () => import("@/views/radar/alarmManage/caseManage.vue"),
    meta: {
      title: "案件管理"
    }
  }
];

export default radarRouter;
