const starfishRouter = [
  {
    path: "/sf/decrypt",
    name: "starfishDecrypt",
    component: () => import("@/views/starfish/decrypt.vue"),
    meta: {
      title: ""
    }
  },
  {
    path: "/sf/homepage",
    name: "starfishHomepage",
    component: () => import("@/views/starfish/starfishHomepage.vue"),
    meta: {
      title: "联勤联动管理后台"
    }
  },
  // ================= 管理要素router =================
  // 任务列表
  {
    path: "/tasklist",
    name: "tasklist",
    component: () => import("@/views/starfish/admin/tasklist.vue"),
    meta: {
      title: "处置任务统计"
    }
  },

  // 流程配置
  {
    path: "/taskadmin",
    name: "taskadmin",
    component: () => import("@/views/starfish/admin/taskflowadmin.vue"),
    meta: {
      title: "工作流程管理"
    }
  },

  // 新建流程配置
  {
    path: "/newbuildflow",
    name: "newbuildflow",
    component: () => import("@/views/starfish/admin/newBuildFlow.vue"),
    meta: {
      title: "新建流程配置"
    }
  },

  // 流程配置
  {
    path: "/flowconfiguration",
    name: "flowconfiguration",
    component: () => import("@/views/starfish/admin/flowconfiguration.vue"),
    meta: {
      title: "工作流程配置项管理"
    }
  },

  // 任务详情
  {
    path: "/taskdetail",
    name: "taskdetail",
    component: () => import("@/views/starfish/admin/taskdetail.vue"),
    meta: {
      title: "任务详情"
    }
  },
  // ================= 基础数据router =================
  // 房屋类型==>房屋列表router
  {
    path: "/houseList",
    name: "house",
    component: () => import("@/views/starfish/baseData/house/houseList.vue"),
    meta: {
      title: "房屋列表"
    }
  },
  // 房屋类型==>房屋详情router
  {
    path: "/houseDetail",
    name: "houseDetail",
    component: () => import("@/views/starfish/baseData/house/houseDetail.vue"),
    meta: {
      title: "房屋详情"
    }
  },
  // 房屋类型==>添加、编辑房屋router
  {
    path: "/addHouse",
    name: "addHouse",
    component: () => import("@/views/starfish/baseData/house/addHouse.vue"),
    meta: {
      title: "编辑房屋"
    }
  },
  // 人口==>实有人口信息详情页
  {
    path: "/personaldetails",
    name: "personaldetails",
    component: () => import("@/views/starfish/baseData/people/detail.vue"),
    meta: {
      title: "实有人口详情"
    }
  },

  // 人口==>人口tabs标签页
  {
    path: "/tabs",
    name: "tabs",
    component: () => import("@/views/starfish/baseData/people/tabs.vue"),
    meta: {
      title: "人口数据列表"
    }
  },

  // 人口==>党员列表页
  {
    path: "/partymemberPage",
    name: "partymemberPage",
    component: () =>
      import("@/views/starfish/baseData/people/partymemberPage.vue"),
    meta: {
      title: "党员列表页"
    }
  },

  // 人口==>重点人员列表
  {
    path: "/importantPeople",
    name: "importantPeople",
    component: () =>
      import("@/views/starfish/baseData/people/importantPeople.vue"),
    meta: {
      title: "重点人员列表"
    }
  },

  // 人口==>关爱人员列表
  {
    path: "/carePeople",
    name: "carePeople",
    component: () => import("@/views/starfish/baseData/people/carePeople.vue"),
    meta: {
      title: "关爱人员列表"
    }
  },
  // 添加企业
  {
    path: "/addCompanyForm",
    name: "addCompanyForm",
    component: () =>
      import("@/views/starfish/baseData/people/baseCompany/addCompanyForm.vue"),
    meta: {
      title: "添加企业"
    }
  },
  // 企业详情
  {
    path: "/singleCompany",
    name: "singleCompany",
    component: () =>
      import("@/views/starfish/baseData/people/baseCompany/singleCompany.vue"),
    meta: {
      title: "企业详情"
    }
  },
  // 添加商铺
  {
    path: "/addshopForm",
    name: "addshopForm",
    component: () =>
      import("@/views/starfish/baseData/people/baseCompany/addshopForm.vue"),
    meta: {
      title: "添加商铺"
    }
  },
  // 商铺详情
  {
    path: "/singleShop",
    name: "singleShop",
    component: () =>
      import("@/views/starfish/baseData/people/baseCompany/singleShop.vue"),
    meta: {
      title: "商铺详情"
    }
  },
  // 添加旅馆
  {
    path: "/addHotelForm",
    name: "addHotelForm",
    component: () =>
      import("@/views/starfish/baseData/people/baseCompany/addHotelForm.vue"),
    meta: {
      title: "添加旅馆"
    }
  },
  // 旅馆详情
  {
    path: "/singleHotel",
    name: "singleHotel",
    component: () =>
      import("@/views/starfish/baseData/people/baseCompany/singleHotel.vue"),
    meta: {
      title: "旅馆详情"
    }
  },
  // 添加民宿
  {
    path: "/addMinsuForm",
    name: "addMinsuForm",
    component: () =>
      import("@/views/starfish/baseData/people/baseCompany/addMinsuForm.vue"),
    meta: {
      title: "添加民宿"
    }
  },
  // 民宿详情
  {
    path: "/singleMinsu",
    name: "singleMinsu",
    component: () =>
      import("@/views/starfish/baseData/people/baseCompany/singleMinsu.vue"),
    meta: {
      title: "民宿详情"
    }
  },
  // 人口==>出租人员列表                  注:新版本里面未没有出租人员与添加出租人员页面,此页面为老版本页面未删除
  {
    path: "/rentoutPeople",
    name: "rentoutPeople",
    component: () =>
      import("@/views/starfish/baseData/people/rentoutPeople.vue"),
    meta: {
      title: "出租人员列表"
    }
  },

  // 人口==>添加实有人口
  {
    path: "/addPeopleBasicInfo",
    name: "addPeopleBasicInfo",
    component: () =>
      import("@/views/starfish/baseData/people/addPeopleBasicInfo.vue"),
    meta: {
      title: "新增人口"
    }
  },

  // 人口==>添加党员
  {
    path: "/addPartyMember",
    name: "addPartyMember",
    component: () =>
      import("@/views/starfish/baseData/people/addPartyMember.vue"),
    meta: {
      title: "添加党员"
    }
  },

  // 人口==>添加出租人员                              注:新版本里面未没有出租人员与添加出租人员页面,此页面为老版本页面未删除
  {
    path: "/addRentoutPeople",
    name: "addRentoutPeople",
    component: () =>
      import("@/views/starfish/baseData/people/addRentoutPeople.vue"),
    meta: {
      title: "添加出租人员"
    }
  },

  // 人口==>添加关爱人员
  {
    path: "/addCarePeople",
    name: "addCarePeople",
    component: () =>
      import("@/views/starfish/baseData/people/addCarePeople.vue"),
    meta: {
      title: "添加关爱人员"
    }
  },

  // 人口==>添加重点人员
  {
    path: "/addImportantPeople",
    name: "addImportantPeople",
    component: () =>
      import("@/views/starfish/baseData/people/addImportantPeople.vue"),
    meta: {
      title: "添加重点人员"
    }
  },

  // 企业列表
  {
    path: "/baseCompany",
    name: "baseCompany",
    component: () => import("@/views/starfish/baseData/people/baseCompany.vue"),
    meta: {
      title: "企业列表"
    }
  },

  // 人口==>实有人口列表
  {
    title: "猎熊座安防管理后台",
    name: "yjxyHome",
    path: "/registerPage",
    component: () =>
      import("@/views/starfish/baseData/people/registerPage.vue"),
    meta: {
      title: "实有人口列表"
    }
  },

  //编辑实有人口
  {
    name: "editBasicPeople",
    path: "/editBasicPeople",
    component: () =>
      import("@/views/starfish/baseData/people/editBasicPeople.vue"),
    meta: {
      title: "编辑实有人口"
    }
  },

  //编辑重点人员
  {
    path: "/editImportantPeople",
    name: "editImportantPeople",
    component: () =>
      import("@/views/starfish/baseData/people/editImportantPeople.vue"),
    meta: {
      title: "编辑重点人员"
    }
  },

  //编辑关爱人员
  //编辑重点人员
  {
    path: "/editCarePeople",
    name: "editCarePeople",
    component: () =>
      import("@/views/starfish/baseData/people/editCarePeople.vue"),
    meta: {
      title: "编辑关爱人员"
    }
  },
  //编辑党员
  {
    path: "/editPartyPeople",
    name: "editPartyPeople",
    component: () =>
      import("@/views/starfish/baseData/people/editPartyPeople.vue"),
    meta: {
      title: "编辑党员"
    }
  },

  // ================= 管理要素router =================
  // 任务类型管理router
  {
    path: "/task/type/manage",
    name: "taskTypeManage",
    component: () => import("@/views/starfish/manageFactor/taskTypeManage.vue"),
    meta: {
      title: "管理任务"
    }
  },
  {
    path: "/task/type/manage/detail/:policyId",
    name: "taskTypeManageDetail",
    component: () =>
      import("@/views/starfish/manageFactor/taskTypeManageDetail.vue"),
    meta: {
      title: "管理任务详情"
    }
  },
  {
    path: "/task/type/manage/add",
    name: "taskTypeManageAdd",
    component: () =>
      import("@/views/starfish/manageFactor/taskTypeManageAdd.vue"),
    meta: {
      title: "添加管理任务"
    }
  },
  {
    path: "/yaosu/manage",
    name: "yaosuManage",
    component: () =>
      import("@/views/starfish/manageFactor/yaosuManageList.vue"),
    meta: {
      title: "治理要素"
    }
  },
  {
    path: "/yaosu/manage/add",
    name: "yaosuManageAdd",
    component: () => import("@/views/starfish/manageFactor/yaosuManageAdd.vue"),
    meta: {
      title: "新增治理要素"
    }
  },
  {
    path: "/yaosu/detail/:detailID",
    name: "yaosuDetail",
    component: () => import("@/views/starfish/manageFactor/yaosuDetail.vue"),
    meta: {
      title: "治理要素"
    }
  },
  {
    path: "/manage/feature/",
    name: "manageFeature",
    component: () =>
      import("@/views/starfish/manageFactor/manageFeatureList.vue"),
    meta: {
      title: "治理要素"
    }
  },
  {
    path: "/manage/feature/detail/:detailID",
    name: "manageFeatureDetail",
    component: () =>
      import("@/views/starfish/manageFactor/manageFeatureDetail.vue"),
    meta: {
      title: "治理要素详情"
    }
  },
  // ================= 走访记录router =================
  // 走访列表
  {
    path: "/visit/record/list",
    name: "visitRecordList",
    component: () => import("@/views/starfish/visitRecord/visitRecordList.vue"),
    meta: {
      title: "走访列表"
    }
  },
  // 走访详情
  {
    path: "/visit/record/detail",
    name: "visitRecordDetail",
    component: () =>
      import("@/views/starfish/visitRecord/visitRecordDetail.vue"),
    meta: {
      title: "走访详情"
    }
  },
  // 用户管理
  {
    path: "/system/organization",
    name: "organization",
    component: () =>
      import("@/views/starfish/systemManagement/organization.vue"),
    meta: {
      title: "用户管理"
    }
  },
  // 组织机构
  {
    path: "/system/userManage",
    name: "starfish-userManage",
    component: () => import("@/views/starfish/systemManagement/userManage.vue"),
    meta: {
      title: "组织机构"
    }
  },
  // 添加用户
  {
    path: "/system/adduserManage",
    name: "adduserManage",
    component: () =>
      import("@/views/starfish/systemManagement/addUsermanage.vue"),
    meta: {
      title: "添加用户"
    }
  },
  // 编辑用户，用户详情
  {
    path: "/system/userManageDetail",
    name: "userManageDetail",
    component: () =>
      import("@/views/starfish/systemManagement/userManageDetail.vue"),
    meta: {
      title: "用户信息"
    }
  },
  // 两委成员
  {
    path: "/system/twoCommittees",
    name: "liangwei",
    component: () =>
      import("@/views/starfish/systemManagement/friendPower.vue"),
    meta: {
      title: "两委成员"
    }
  },
  // 自有力量
  {
    path: "/system/ownPower",
    name: "selfPower",
    component: () =>
      import("@/views/starfish/systemManagement/friendPower.vue"),
    meta: {
      title: "自有力量"
    }
  },
  // 协同力量
  {
    path: "/system/friendPower",
    name: "togetherPower",
    component: () =>
      import("@/views/starfish/systemManagement/friendPower.vue"),
    meta: {
      title: "协同力量"
    }
  },
  // 辖区管理
  {
    path: "/system/areaManage",
    name: "areaManage",
    component: () => import("@/views/starfish/systemManagement/areaManage.vue"),
    meta: {
      title: "辖区管理"
    }
  },
  // ================= 随手报router =================
  // 随手报列表router
  {
    path: "/reportList",
    name: "reportList",
    component: () =>
      import("@/views/starfish/quickReport/reportList/reportList.vue"),
    meta: {
      title: "随手报列表"
    }
  },
  // 随手报详情router
  {
    path: "/reportDetail",
    name: "reportDetail",
    component: () =>
      import("@/views/starfish/quickReport/reportDetail/reportDetail.vue"),
    meta: {
      title: "随手报详情"
    }
  },
  // 随手报审查router
  {
    path: "/reportCheck",
    name: "reportCheck",
    component: () =>
      import("@/views/starfish/quickReport/reportList/reportDetail.vue"),
    meta: {
      title: "随手报审查"
    }
  },
  {
    path: "/sf/role/manage",
    name: "sfRoleManage",
    component: () => import("@/views/starfish/systemManagement/roleManage.vue"),
    meta: {
      title: "角色"
    }
  }
];

export default starfishRouter;
