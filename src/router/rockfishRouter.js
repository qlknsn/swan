const lxzRouter = [
  {
    path: "/liexiongzuo/srs",
    name: "lxzSRS",
    component: () => import("@/views/rockfish/srsMonitor.vue"),
    meta: { title: "猎熊座SRS" }
  },
  //    添加定时任务
  {
    path: "/lxz/addTimeTask",
    name: "lxzaddTimeTask",
    component: () => import("@/views/rockfish/timeTask/addTimeTask.vue"),
    meta: {
      title: "添加定时任务"
    }
  },
  //石斑鱼项目的文件
  {
    path: "/lxz/videoPollingList",
    name: "lxzvideoPollingList",
    component: () => import("@/views/rockfish/videoPollingList.vue"),
    meta: {
      title: "视频轮屏"
    }
  },
  {
    title: "首页",
    name: "lxzhomePage",
    path: "/lxz/homepage",
    component: () => import("@/views/Home.vue")
  }
];
const zhoujiaduRouter = [
  //    添加定时任务
  {
    path: "/addTimeTask",
    name: "addTimeTask",
    component: () => import("@/views/rockfish/timeTask/addTimeTask.vue"),
    meta: {
      title: "添加定时任务"
    }
  },
  //石斑鱼项目的文件
  {
    path: "/videoPollingList",
    name: "videoPollingList",
    component: () => import("@/views/rockfish/videoPollingList.vue"),
    meta: {
      title: "轮巡任务列表"
    }
  },
  {
    title: "首页",
    name: "rockfishHomePage",
    path: "/homepage",
    component: () => import("@/views/Home.vue")
  },
  {
    meta: { title: "屏幕区域管理" },
    title: "屏幕区域管理",
    name: "screenManage",
    path: "/screen/manage",
    component: () => import("@/views/rockfish/screenManage.vue")
  },
  {
    title: "标签管理",
    name: "labelManage",
    path: "/label/manage",
    meta: { title: "标签管理" },
    component: () => import("@/views/rockfish/labelManage.vue")
  },
  {
    title: "导出队列",
    name: "exportcamera",
    path: "/camera/list",
    meta: { title: "导出队列" },
    component: () => import("@/views/rockfish/exportCamera.vue")
  },
  {
    title: "摄像头维修列表",
    name: "damagedCamera",
    path: "/camera/damaged",
    meta: { title: "摄像头维修列表" },
    component: () => import("@/views/rockfish/cameraDamaged.vue")
  },
  {
    title: "摄像头维修列表RTSP",
    name: "damagedCameraFake",
    path: "/camera/damaged/fake",
    meta: { title: "摄像头维修列表RTSP" },
    component: () => import("@/views/rockfish/cameraDamagedFake.vue")
  }
];
const sanlinRouter = [
  {
    path: "/sanlin/department/departmentManagement",
    name: "departmentManagement",
    component: () =>
      import("@/views/rockfish/department/departmentManagement.vue"),
    meta: { title: "部门管理" }
  },
  {
    path: "/sanlin/department/peopleManagement",
    name: "peopleManagement",
    component: () => import("@/views/rockfish/department/peopleManagement.vue"),
    meta: { title: "人员管理" }
  },
  {
    path: "/sanlin/srs",
    name: "sanlinSRS",
    component: () => import("@/views/rockfish/srsMonitor.vue"),
    meta: { title: "三林镇SRS" }
  },
  // 工单分析--满意度
  {
    path: "/sanlin/sheet/satisfy",
    name: "satisfy",
    component: () => import("@/views/rockfish/sheetSatisfy.vue"),
    meta: { title: "满意度" }
  },
  // 实时人群密度-嗅探告警列表
  {
    path: "/sanlin/crow/crowdDensity",
    name: "crowdensity",
    component: () => import("@/views/rockfish/crowdDensity.vue"),
    meta: { title: "嗅探告警" }
  },
  // 工单分析--实际解决率
  {
    path: "/sanlin/sheet/resolved",
    name: "resolved",
    component: () => import("@/views/rockfish/sheetResolved.vue"),
    meta: { title: "实际解决率" }
  },
  // 工单分析--先行联系率
  {
    path: "/sanlin/sheet/contact",
    name: "contact",
    component: () => import("@/views/rockfish/sheetContact.vue"),
    meta: { title: "先行联系率" }
  },
  // 工单分析--饼图统计
  {
    path: "/sanlin/sheet/pieStatistic",
    name: "pieStatistic",
    component: () => import("@/views/rockfish/pieStatistic/pieStatistic.vue"),
    meta: { title: "饼图统计" }
  },

  // 垃圾分类
  {
    path: "/sanlin/trash/classify",
    name: "trashClassify",
    component: () => import("@/views/rockfish/trashClassify.vue"),
    meta: { title: "垃圾分类" }
  },
  {
    path: "/sanlin/trash/garbagecheck",
    name: "garbagecheck",
    component: () => import("@/views/rockfish/garbageWatch/garbageCheck.vue"),
    meta: { title: "检查情况" }
  },
  {
    path: "/sanlin/trash/garbageCensus",
    name: "garbageCensus",
    component: () => import("@/views/rockfish/garbageWatch/garbageCensus.vue"),
    meta: { title: "统计" }
  },
  {
    path: "/sanlin/trash/garbagereCycle",
    name: "garbagereCycle",
    component: () => import("@/views/rockfish/garbageWatch/garbagereCycle.vue"),
    meta: { title: "可回收物服务点" }
  },
  // 违章停车
  {
    path: "/sanlin/parking/violation",
    name: "parkingViolation",
    component: () => import("@/views/rockfish/parkingViolation.vue"),
    meta: { title: "违章停车" }
  },
  //    添加定时任务
  {
    path: "/sanlin/addTimeTask",
    name: "sanlinaddTimeTask",
    component: () => import("@/views/rockfish/timeTask/addTimeTask.vue"),
    meta: {
      title: "添加定时任务"
    }
  },
  //石斑鱼项目的文件
  {
    path: "/sanlin/videoPollingList",
    name: "sanlinvideoPollingList",
    component: () => import("@/views/rockfish/videoPollingList.vue"),
    meta: {
      title: "视频轮屏"
    }
  },
  {
    title: "首页",
    name: "sanlinhomePage",
    path: "/sanlin/homepage",
    component: () => import("@/views/Home.vue")
  },

  {
    name: "noiseList",
    path: "/sanlin/noise/noiseDetectionList",
    component: () => import("@/views/rockfish/noiseList.vue"),
    meta: {
      title: "噪音检测列表"
    }
  },
  {
    name: "noiseDectectionList",
    path: "/sanlin/noise/noiseList",
    component: () => import("@/views/rockfish/noiseDetection.vue"),
    meta: {
      title: "点位管理"
    }
  },
  {
    title: "不达标小区",
    name: "ungetVillage",
    path: "/sanlin/trash/ungetVillage",
    meta: { title: "不达标小区" },
    component: () => import("@/views/rockfish/garbageWatch/ungetVillage.vue")
  },
  {
    title: "用户管理",
    name: "userManage",
    path: "/sanlin/user/userManage",
    component: () => import("@/views/rockfish/user/userManage.vue")
  }
];
const zhuqiaoRouter = [
  //    添加定时任务
  {
    path: "/zhuqiao/addTimeTask",
    name: "zhuqiaoaddTimeTask",
    component: () => import("@/views/rockfish/timeTask/addTimeTask.vue"),
    meta: {
      title: "添加定时任务"
    }
  },
  //石斑鱼项目的文件
  {
    path: "/zhuqiao/videoPollingList",
    name: "zhuqiaovideoPollingList",
    component: () => import("@/views/rockfish/videoPollingList.vue"),
    meta: {
      title: "视频轮屏"
    }
  },
  {
    title: "车辆违停",
    name: "lllegalStop",
    path: "/lllegalStop",
    component: () => import("@/views/rockfish/lllegalStop.vue")
  },
  {
    title: "渣土车管理",
    name: "zhatuManage",
    path: "/zhatuManage",
    component: () => import("@/views/rockfish/zhatuManage.vue")
  }
];

const rockfishRouter = [
  ...lxzRouter,
  ...zhoujiaduRouter,
  ...sanlinRouter,
  ...zhuqiaoRouter
];

export default rockfishRouter;
