const officeRouter = [
  // 新闻中心
  {
    path: "/office/newsCenter",
    meta: { title: "新闻中心" },
    name: "newsCenter",
    component: () => import("@/views/office/newsCenter/newsCenter.vue")
  },
  {
    path: "/office/addNews",
    meta: { title: "添加新闻" },
    name: "addNews",
    component: () => import("@/views/office/newsCenter/addNews.vue")
  },
  {
    path: "/office/editNews",
    meta: { title: "修改新闻" },
    name: "editNews",
    component: () => import("@/views/office/newsCenter/editNews.vue")
  }
];

export default officeRouter;
