import Vue from "vue";
import VueRouter from "vue-router";
import Layout from "../views/Layout.vue";
import rockfishRouter from "./rockfishRouter";
import arRouter from "./arRouter";
import starfishRouter from "./starfishRouter";
import gwRouter from "./gatewayRouter";
import safeUkeyRouter from "./safeUkeyRouter";
import microCheckinPointRouter from "./microCheckinPointRouter";
import office from "./office-website";
import deviceInputRouter from "./deviceInputRouter";
import trackingRouter from "./trackingRouter";
import workSheetRouter from "./workSheetRouter";
import middlewareRouter from "./middlewareRouter";
import radarRouter from "./radarRouter";
import lrguanaiRouter from "./laorenguanaiRouter";
import slcyRouter from "./slcyRouter";
import zhoupuronghe from "./rongheRouter";
import osRouter from "./osRouter";
import lingangRouter from "./lingangRouter";
import lqcRouter from "./lqcRouter";
import gitRouter from "./gitRouter";
import wasteSortRouter from "./wasteSortRouter"
Vue.use(VueRouter);

const routes = [
  {
    path: "*",
    component: () => import("@/views/notFound.vue")
  },
  {
    path: "/lg/login",
    name: "LINGANG-LOGIN",
    meta: { title: "临港综合管理平台" },
    component: () => import("@/views/lingang/lgLogin.vue")
  },
  {
    path: "/gw/login",
    name: "GATEWAY",
    meta: { title: "猎熊座智能网关" },
    component: () => import("@/views/gateway/login/gatewayLogin.vue")
  },
  {
    path: "/rf/liexiongzuo/login",
    name: "ROCKFISH-LXZ",
    component: () => import("@/views/rockfish/login/rockfishLogin.vue"),
    meta: { title: "猎熊座城市运行综合管理平台" }
  },

  {
    path: "/rf/sanlin/login",
    name: "ROCKFISH-SANLIN",
    component: () => import("@/views/rockfish/login/sanlinLogin.vue"),
    meta: { title: "三林镇城市运行综合管理平台" }
  },
  {
    path: "/rf/zhoujiadu/login",
    name: "ROCKFISH-ZHOUJIADU",
    component: () => import("@/views/rockfish/login/zhoujiaduLogin.vue"),
    meta: { title: "周家渡城市运行综合管理平台" }
  },
  {
    path: "/sf/login",
    name: "STARFISH",
    component: () => import("../views/starfish/login/starfishLogin.vue")
  },
  {
    path: "/usbk/login",
    name: "SAFE_UKEY",
    component: () => import("@/views/safeUkey/login/login.vue"),
    meta: {
      title: "安全U盾-登录"
    }
  },
  {
    path: "/mcp/login",
    name: "MICRO_CHECKIN_POINT",
    component: () => import("@/views/mcp/login/login.vue"),
    meta: { title: "浦东新区社区微卡口管理系统" }
  },
  {
    path: "/rf/zhuqiao/login",
    name: "ROCKFISH-ZHUQIAO",
    component: () => import("@/views/rockfish/login/zhuqiaoLogin.vue"),
    meta: {
      title: "祝桥镇城市运行综合管理指挥平台"
    }
  },
  {
    path: "/office/login",
    name: "OFFICE-WEBSITE",
    component: () => import("@/views/office/login/login.vue"),
    meta: {
      title: "猎熊座官网后台管理中心"
    }
  },
  {
    path: "/di/login",
    name: "DEVICE-INPUT",
    component: () => import("@/views/deviceInput/login/login.vue"),
    meta: {
      title: "设备录入系统"
    }
  },
  {
    path: "/w/login",
    name: "WORKSHEET",
    meta: { title: "工单报修系统" },
    component: () => import("@/views/workSheet/workSheetLogin.vue")
  },
  {
    path: "/t/login",
    name: "TRACKING",
    component: () => import("@/views/tracking/login/trackingLogin.vue"),
    meta: {
      title: "埋点后台管理系统"
    }
  },
  {
    path: "/m/login",
    name: "MIDDLEWARE",
    component: () => import("@/views/middleware/middlewareLogin.vue"),
    meta: {
      title: "视频汇聚中间件系统"
    }
  },
  {
    path: "/radar/login",
    name: "RADAR",
    component: () => import("@/views/radar/login.vue"),
    meta: {
      title: "雷达电动车列表"
    }
  },
  {
    path: "/ar/login",
    name: "ARLOGIN",
    component: () => import("@/views/ar/login/arLogin.vue"),
    meta: {
      title: "AR后台管理"
    }
  },
  {
    path: "/slcy/slcylogin",
    name: "SLCYLogin",
    component: () => import("@/views/slcy/slcyLogin.vue"),
    meta: {
      title: "三林城运后台"
    }
  },
  {
    path: "/lrga/login",
    name: "LRGUANAILOGIN",
    component: () => import("@/views/lrguanai/login/lrguanaiLogin.vue"),
    meta: {
      title: "智能居家养老后台管理系统"
    }
  },
  {
    path: "/r/login",
    name: "rongheLogin",
    component: () => import("@/views/ronghe/login"),
    meta: {
      title: "周浦微电子社区"
    }
  },
  {
    path: "/os/login",
    name: "osLogin",
    component: () => import("@/views/oldStreet/login.vue"),
    meta: {
      title: "三林老街后台管理系统"
    }
  },
  {
    path: "/lqc/login",
    name: "lqcLogin",
    component: () => import("@/views/lqc/lqcLogin.vue"),
    meta: {
      title: "南信沥青基地计划量管理平台"
    }
  },
  {
    path: "/lqc/choosePage",
    name: "choosePage",
    component: () => import("@/views/lqc/lqcChoose.vue"),
    meta: {
      title: "沥青厂二级选择页"
    }
  },
  {
    path: "/",
    component: Layout,
    children: [
      ...wasteSortRouter,
      ...rockfishRouter,
      ...starfishRouter,
      ...gwRouter,
      ...safeUkeyRouter,
      ...microCheckinPointRouter,
      ...office,
      ...deviceInputRouter,
      ...trackingRouter,
      ...workSheetRouter,
      ...middlewareRouter,
      ...radarRouter,
      ...arRouter,
      ...lrguanaiRouter,
      ...zhoupuronghe,
      ...slcyRouter,
      ...lingangRouter,
      ...osRouter,
      ...lqcRouter,
      ...gitRouter
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  routes: [...routes]
});
router.beforeEach((to, from, next) => {
  window.document.title = to.meta.title || "";
  next();
});
export default router;
