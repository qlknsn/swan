const lingangRouter = [
  {
    path: "/lg/login",
    name: "lingang-login",
    component: () => import("@/views/lingang/lgLogin.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/zhuangxiu",
    name: "lingang-zhuangxiu",
    component: () => import("@/views/lingang/lgZhuangxiu.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/users",
    name: "lingang-users",
    component: () => import("@/views/lingang/lgUsers.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/roles",
    name: "lingang-roles",
    component: () => import("@/views/lingang/lgRoles.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/menus",
    name: "lingang-menus",
    component: () => import("@/views/lingang/lgMenus.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/person/manage",
    name: "lingang-person-manage",
    component: () => import("@/views/lingang/lgPersonManage.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/depart/manage",
    name: "lingang-depart-manage",
    component: () => import("@/views/lingang/lgDepartManage.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/vip",
    name: "lingang-vip",
    component: () => import("@/views/lingang/lgVip.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/task/manage",
    name: "lingang-task-manage",
    component: () => import("@/views/lingang/lgTaskManage.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/info/capture",
    name: "lingang-info-capture",
    component: () => import("@/views/lingang/personInfoCapture.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/micro/119",
    name: "lingang-micro-119",
    component: () => import("@/views/lingang/micro119.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/store/manage",
    name: "lingang-store-manage",
    component: () => import("@/views/lingang/storeManage.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/elevator/manage",
    name: "lingang-elevator-manage",
    component: () => import("@/views/lingang/elevatorManage.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/house/manage",
    name: "lingang-house-manage",
    component: () => import("@/views/lingang/houseManage.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/oldManEquipMent/manage",
    name: "lingang-oldManEquipMent-manage",
    component: () => import("@/views/lingang/oldManEquipMent.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/video/config",
    name: "lingang-video-config",
    component: () => import("@/views/lingang/lgVideoConfig.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/todo/config",
    name: "lingang-todo-config",
    component: () => import("@/views/lingang/todoConfig.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/todo/tootip",
    name: "lingang-todo-tootip",
    component: () => import("@/views/lingang/tootipManage.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  },
  {
    path: "/lg/label/manage",
    name: "lingang-label-manage",
    component: () => import("@/views/lingang/lgLabelManage.vue"),
    meta: {
      title: "社区管理与服务平台"
    }
  }
];

export default lingangRouter;
