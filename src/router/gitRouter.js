const gitRouter = [
  {
    path: "/git/list",
    name: "gitList",
    component: () => import("@/views/git/gitList.vue"),
    meta: { title: "项目列表" }
  },
  {
    path: "/git/branch/:projectID",
    name: "gitBranch",
    component: () => import("@/views/git/gitBranch.vue"),
    meta: { title: "分支列表" }
  },
  {
    path: "/git/blank",
    name: "gitBlank",
    component: () => import("@/views/git/gitBlank.vue"),
    meta: { title: "分支列表" }
  },
  {
    path: "/git/commit/:breanchName",
    name: "gitCommits",
    component: () => import("@/views/git/gitCommits.vue"),
    meta: { title: "提交列表" }
  }
];
export default gitRouter;
