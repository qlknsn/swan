const gwRouter = [
  {
    path: "/gw/netcard/config",
    name: "netcardConfig",
    component: () => import("../views/gateway/networkConfig/netcardConfig.vue")
  },

  //首页
  {
    path: "/gw/homePage",
    name: "gatewayHomepage",
    meta: { title: "首页" },
    component: () => import("@/views/gateway/homePage/homePage.vue")
  },
  // ==========查看=========
  //主机信息
  {
    path: "/gw/host/info",
    name: "hostInfo",
    meta: { title: "主机信息" },

    component: () => import("@/views/gateway/lookover/hostInfo.vue")
  },

  //网络信息
  {
    path: "/gw/network/info",
    name: "network",
    meta: { title: "网络信息" },

    component: () => import("@/views/gateway/lookover/network.vue")
  },

  // =============== 一机一档 ===============
  {
    path: "/gw/ogom/list",
    name: "ogomList",
    meta: {
      title: "一机一档列表"
    },
    component: () => import("@/views/gateway/ogom/ogomList.vue")
  },
  {
    path: "/gw/ogom/add/:urn",
    name: "ogomAdd",
    meta: { title: "一机一档资料填写" },
    component: () => import("@/views/gateway/ogom/ogomAdd.vue")
  },
  {
    path: "/gw/ogom/ligan",
    name: "ogomLigan",
    meta: { title: "一机一档资料填写" },
    component: () => import("@/views/gateway/ogom/ogomLigan.vue")
  },
  {
    path: "/gw/ogom/ogomAISecurty",
    name: "ogomAISecurty",
    meta: { title: "一机一档资料填写" },

    component: () => import("@/views/gateway/ogom/ogomAISecurty.vue")
  },
  {
    path: "/gw/ogom/ogomCheckIn",
    name: "ogomCheckIn",
    meta: { title: "一机一档资料填写" },

    component: () => import("@/views/gateway/ogom/ogomCheckIn.vue")
  },
  {
    //密码
    path: "/gw/password/reset",
    name: "password",
    component: () => import("@/views/gateway/systemManage/password.vue")
  },
  {
    //手动重启
    path: "/gw/handReset",
    name: "handReset",
    component: () => import("@/views/gateway/systemManage/handReset.vue")
  },
  {
    //定时重启
    path: "/gw/setTimeRestart",
    name: "setTimeRestart",
    component: () => import("@/views/gateway/systemManage/setTimeRestart.vue")
  },
  {
    //定时重启
    path: "/gw/upgrade",
    name: "upgrade",
    component: () => import("@/views/gateway/systemManage/upgrade.vue")
  },
  {
    //备份与恢复
    path: "/gw/backUpdata",
    name: "backUpdata",
    component: () => import("@/views/gateway/systemManage/backUpdata.vue")
  },
  {
    //DNS服务器配置
    path: "/gw/dnsServer",
    name: "dnsServer",
    component: () => import("@/views/gateway/systemManage/dnsServer.vue")
  },
  //系统
  {
    path: "/gw/system",
    name: "system",
    component: () => import("@/views/gateway/systemManage/system.vue")
  },
  //操作日志
  {
    path: "/gw/operationLogTab",
    name: "operationLog",
    component: () => import("@/views/gateway/systemManage/operationLogTab.vue")
  },

  // =====================================连接设备=========================================
  // 设备信息列表
  {
    path: "/gw/contactDeviceInfo",
    meta: { title: "设备连接信息" },
    name: "contactDeviceInfo",
    component: () =>
      import(
        "@/views/gateway/contactDevice/contactDeviceInfo/contactDeviceInfoList.vue"
      )
  },
  // 设备抓拍图片
  {
    path: "/gw/deviceTakePhotos",
    meta: { title: "设备抓拍照片" },
    name: "deviceTakePhotos",
    component: () =>
      import(
        "@/views/gateway/contactDevice/deviceTakePhotos/deviceTakePhotos.vue"
      )
  }
];

export default gwRouter;
