const middlewareRouter = [
  {
    path: "/m/dashboard",
    meta: { title: "首页" },
    name: "m-dashboard",
    component: () => import("@/views/middleware/dashboard/dashboard.vue")
  },
  {
    path: "/m/live/stream",
    meta: { title: "实时预览" },
    name: "m-liev-stream",
    component: () => import("@/views/middleware/liveStream/liveStream.vue")
  },
  {
    path: "/m/usermanage",
    meta: { title: "系统管理/用户管理" },
    name: "m-systemmanage-usermanage",
    component: () =>
      import("@/views/middleware/systemManage/userManage/tabs.vue")
  },
  {
    path: "/m/label",
    meta: { title: "系统管理/点位标签" },
    name: "m-systemmanage-locationlabel",
    component: () =>
      import("@/views/middleware/systemManage/labelManage/labelManage.vue")
  },
  {
    path: "/m/device/manage",
    meta: { title: "设备管理" },
    name: "m-device",
    component: () =>
      import("@/views/middleware/deviceManage/deviceManageList.vue")
  },
  {
    path: "/m/video/replay",
    meta: { title: "录像回放" },
    name: "m-videoReplay",
    component: () => import("@/views/middleware/videoReplay/videoReplay.vue")
  },
  {
    path: "/m/system/role",
    meta: { title: "角色管理" },
    name: "m-roleManage",
    component: () =>
      import("@/views/middleware/systemManage/roleManage/roleManage.vue")
  },
  {
    path: "/m/system/lineUser",
    meta: { title: "关联用户" },
    name: "m-lineUser",
    component: () =>
      import("@/views/middleware/systemManage/roleManage/lineUser.vue")
  },
  {
    path: "/m/device/manageCodeDetail",
    meta: { title: "设备管理-编码设备详情" },
    name: "m-deviceDetail",
    component: () =>
      import("@/views/middleware/deviceManage/deviceManageDetail.vue")
  },
  {
    path: "/m/device/manageOgom",
    meta: { title: "设备管理-一机一档" },
    name: "m-deviceOgom",
    component: () => import("@/views/middleware/deviceManage/manageOgom.vue")
  }
];

export default middlewareRouter;
