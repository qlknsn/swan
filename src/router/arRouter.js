const arRouter = [
  // {
  //   path: "/ar/homePage",
  //   name: "arhomepage",
  //   component: () => import("@/views/ar/homePage/homePage.vue"),
  //   meta: { title: "首页" }
  // },
  {
    path: "/ar/highPoint",
    name: "arhighPoint",
    component: () => import("@/views/ar/highPoint/highPoint.vue"),
    meta: { title: "高点管理" }
  },
  {
    path: "/ar/lowPoint",
    name: "arlowPoint",
    component: () => import("@/views/ar/lowPoint/lowPoint.vue"),
    meta: { title: "低点管理" }
  },
  {
    path: "/ar/labelManage",
    name: "arlabelManage",
    component: () => import("@/views/ar/labelManage/labelManageTabs.vue"),
    meta: { title: "标签管理" }
  },
  {
    path: "/ar/logManage",
    name: "arlogManage",
    component: () => import("@/views/ar/log/logManage.vue"),
    meta: { title: "日志管理" }
  },
  {
    path: "/ar/serviceConfiguration",
    name: "arserviceConfiguration",
    component: () =>
      import("@/views/ar/serviceConfiguration/serviceConfigurationTabs.vue"),
    meta: { title: "服务配置" }
  },
  {
    path: "/ar/poi",
    name: "arpoi",
    component: () => import("@/views/ar/labelManage/poiManageTabs.vue"),
    meta: { title: "POI数据管理" }
  },
  {
    path: "/ar/districtManage",
    name: "",
    component: () => import("@/views/ar/systemManage/districtManage.vue"),
    meta: { title: "区域管理" }
  },
  {
    path: "/ar/userManage",
    name: "aruserManage",
    component: () => import("@/views/ar/systemManage/userManage.vue"),
    meta: { title: "用户管理" }
  },
  {
    path: "/ar/eventCenter",
    name: "areventCenter",
    component: () => import("@/views/ar/systemManage/eventCenterTabs.vue"),
    meta: { title: "事件中心设置" }
  }
];
export default arRouter;
