const wasteSortRouter = [
    {
      path: "/wasteSort",
      name: "wasteSort",
      component: () => import("@/views/wasteSort/wasteSort.vue"),
      meta: { title: "垃圾分类" }
    }
  ];
  export default wasteSortRouter;
  