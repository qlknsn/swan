const osRouter = [
  {
    path: "/os/userManage",
    component: () => import("@/views/oldStreet/userManage.vue"),
    meta: { title: "用户管理" }
  },
  {
    path: "/os/log",
    component: () => import("@/views/oldStreet/log.vue"),
    meta: { title: "审计日志" }
  },
  {
    path: "/os/index",
    component: () => import("@/views/oldStreet/index.vue"),
    meta: { title: "老街首页" }
  },
  {
    path: "/os/noticeToVistor",
    name: "osnotice",
    component: () => import("@/views/oldStreet/noticeToVistor.vue"),
    meta: { title: "游客须知" }
  },
  {
    path: "/os/visitInfo",
    name: "visitInfo",
    component: () => import("@/views/oldStreet/visitInfo.vue"),
    meta: { title: "参观信息" }
  },
  {
    path: "/os/civilizedRules",
    name: "civilizedRules",
    component: () => import("@/views/oldStreet/civilizedRules.vue"),
    meta: { title: "文明公约" }
  },
  {
    path: "/os/bianminxinxi",
    name: "bianminxinxi",
    component: () => import("@/views/oldStreet/bianminxinxi.vue"),
    meta: { title: "便民信息" }
  },
  {
    path: "/os/issueComplain",
    name: "issueComplain",
    component: () => import("@/views/oldStreet/issueComplain.vue"),
    meta: { title: "问题投诉" }
  },
  {
    path: "/os/laojiewenbao",
    name: "laojiewenbao",
    component: () => import("@/views/oldStreet/laojiewenbao.vue"),
    meta: { title: "老街文保" }
  },
  {
    path: "/os/wenhuachangguan",
    name: "wenhuachangguan",
    component: () => import("@/views/oldStreet/wenhuachangguan.vue"),
    meta: { title: "文化场馆" }
  },
  {
    path: "/os/renwujieshao",
    name: "renwujieshao",
    component: () => import("@/views/oldStreet/renwujieshao.vue"),
    meta: { title: "人物介绍" }
  },
  {
    path: "/os/tesexiaochi",
    name: "tesexiaochi",
    component: () => import("@/views/oldStreet/tesexiaochi.vue"),
    meta: { title: "特色小吃" }
  },
  {
    path: "/os/canyinxinxi",
    name: "canyinxinxi",
    component: () => import("@/views/oldStreet/canyinxinxi.vue"),
    meta: { title: "餐饮信息" }
  },
  {
    path: "/os/personManage",
    name: "personManage",
    component: () => import("@/views/oldStreet/personManage.vue"),
    meta: { title: "人员管理" }
  },
  {
    path: "/os/basicInfo",
    name: "basicInfo",
    component: () => import("@/views/oldStreet/basicInfo.vue"),
    meta: { title: "人员管理" }
  },
  {
    path: "/os/personInfo",
    name: "osPersonInfo",
    component: () => import("@/views/oldStreet/personInfo.vue"),
    meta: { title: "人员信息" }
  },
  {
    path: "/os/jingdian",
    name: "osJingdian",
    component: () => import("@/views/oldStreet/jingdian.vue"),
    meta: { title: "老街景点" }
  }
];
export default osRouter;
