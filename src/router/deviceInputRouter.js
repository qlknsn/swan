const deviceInputRouter = [
  // 新闻中心
  {
    path: "/di/areaInfo",
    meta: { title: "区域信息" },
    name: "areaInfo",
    component: () => import("@/views/deviceInput/areaInfo/areaInfo.vue")
  },
  {
    path: "/di/addAreaInfo",
    meta: { title: "添加区域" },
    name: "addAreaInfo",
    component: () => import("@/views/deviceInput/areaInfo/addAreaInfo.vue")
  },
  {
    path: "/di/brandInfo",
    meta: { title: "品牌信息" },
    name: "brandInfo",
    component: () => import("@/views/deviceInput/brandInfo/brandInfo.vue")
  },
  {
    path: "/di/nvrList",
    meta: { title: "nvr列表" },
    name: "nvrList",
    component: () => import("@/views/deviceInput/nvrList/nvrList.vue")
  },
  {
    path: "/di/cameraList",
    meta: { title: "相机列表" },
    name: "diCameraList",
    component: () => import("@/views/deviceInput/cameraList/cameraList.vue")
  },
  {
    path: "/di/editCamera",
    meta: { title: "修改相机信息" },
    name: "editcamera",
    component: () => import("@/views/deviceInput/cameraList/editCamera.vue")
  },
  {
    path: "/di/thirdPlatform",
    meta: { title: "第三方平台" },
    name: "thirdPlatform",
    component: () => import("@/views/deviceInput/thirdPlatform/thirdPlatform.vue")
  },
  {
    path: "/di/thirdPlatformVideo",
    meta: { title: "摄像机" },
    name: "thirdPlatformVideo",
    component: () => import("@/views/deviceInput/thirdPlatform/thirdPlatformVideo.vue")
  }
];

export default deviceInputRouter;
