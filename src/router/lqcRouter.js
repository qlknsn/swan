const lqcRouter = [
  {
    path: "/lqc/permission",
    name: "permission",
    component: () => import("@/views/lqc/permission.vue"),
    meta: { title: "权限管理" }
  },
  {
    path: "/lqc/roleManage",
    name: "roleManage",
    component: () => import("@/views/lqc/roleManage.vue"),
    meta: { title: "角色管理" }
  },
  {
    path: "/lqc/planManage",
    name: "planManage",
    component: () => import("@/views/lqc/planManage.vue"),
    meta: { title: "计划量管理" }
  },
  {
    path: "/lqc/audit",
    name: "lqcAudit",
    component: () => import("@/views/lqc/lqcAudit.vue"),
    meta: { title: "审批管理" }
  },
  {
    path: "/lqc/rtsp",
    name: "lqcrtsp",
    component: () => import("@/views/lqc/lqcRtsp.vue"),
    meta: { title: "rtsp" }
  },
  {
    path: "/lqc/personManage",
    name: "lqcPersonManage",
    component: () => import("@/views/lqc/lqcPersonManage.vue"),
    meta: { title: "人员管理" }
  },
  {
    path: "/lqc/inventoryManager",
    name: "lqcInventoryManage",
    component: () => import("@/views/lqc/lqcInventoryManage.vue"),
    meta: { title: "库存量管理" }
  }
];
export default lqcRouter;
