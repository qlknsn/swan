import { axios } from "@/utils/requests";
import api from "../../api/rockfish/rockfishUrls";

export function exportNoise(params) {
  window.open(api.exportNoise + "?uuid=" + params);
}
export function saveNoise(data) {
  return axios({
    method: "POST",
    data: data,
    url: api.saveNoise
  });
}
export function getNoiseList(data) {
  return axios({
    method: "POST",
    data: data,
    url: api.getNoiseList
  });
}
export function getDevices(data) {
  return axios({
    method: "GET",
    params: data,
    url: api.getDevices
  });
}
export function delPerson(data) {
  return axios({
    method: "GET",
    params: data,
    url: api.delPerson
  });
}
// 新增部门
export function addPerson(data) {
    return axios({
      method: "POST",
      data: data,
      url: api.addPerson
    });
  }
export function getPersonList(data) {
    return axios({
      method: "POST",
      data: data,
      url: api.getPersonList
    });
  }
export function delDept(data) {
    return axios({
      method: "GET",
      params: data,
      url: api.delDept
    });
  }
export function getPerson(data) {
    return axios({
      method: "GET",
      params: data,
      url: api.getPerson
    });
  }
// 新增部门
export function addDept(data) {
    return axios({
      method: "POST",
      data: data,
      url: api.addDept
    });
  }
//获取任务权限
export function getListDept(data) {
    return axios({
      method: "GET",
      params: data,
      url: api.getListDept
    });
  }
//获取部门列表
export function pageBackend(data) {
    return axios({
      method: "post",
      data: data,
      url: api.pageBackend
    });
  }
  //获取二级部门
export function getPrimaryDepart(data) {
    return axios({
      method: "GET",
      params: data,
      url: api.getPrimaryDepart
    });
  }
export function deleteXiaoqu(data) {
  return axios({
    method: "DELETE",
    data: data,
    params: data,
    url: api.deleteXiaoqu
  });
}
export function searchXiaoqu(data) {
  return axios({ params: data, url: api.searchXiaoqu });
}

export function addXiaoqu(data) {
  return axios({
    method: "POST",
    data: data,
    url: api.addXiaoqu
  });
}
export function updateXiaoqu(data) {
  return axios({
    method: "POST",
    data: data,
    url: api.updateXiaoqu
  });
}

export function markInactive(params) {
  return axios({
    method: "POST",
    data: params,
    url: api.markInactive
  });
}
export function configList(params) {
  return axios({
    url: api.configList,
    params: params
  });
}
export function getVideoPollingList(params) {
  //获取轮屏任务列表
  return axios({
    method: "get",
    url: api.videoPollingList,
    params: params
  });
}

export function deleteVideoPolling(params) {
  //删除某个轮屏任务
  return axios({
    method: "delete",
    url: `${api.delete}/${params.pollingId}`,
    headers: {
      "Content-Type": "application/json"
    }
  });
}

export function runTaskNow(params) {
  return axios({
    method: "post",
    url: `${api.delete}/${params.pollingId}`,
    params: { action: "PLAY_NOW" },
    headers: {
      "Content-Type": "application/json"
    }
  });
}
export function getCameraList(params) {
  // ip地址 相机点名称 所属编码设备 所属编码设备编号 监控点类型 位置标签  接入形式    状态
  //  ip              keywords    parents     tags    tags     tags    statuses
  return axios({
    url: api.getCameraList,
    params: params
  });
}
export function exportsCameralist(params) {
  return axios({
    url: api.exportsCameralist,
    params: params
  });
}

export function createScreen(data) {
  return axios({
    url: api.createScreen,
    data: data,
    method: "post"
  });
}

export function getScreens(data) {
  return axios({
    url: api.createScreen,
    params: data
  });
}

export function deleteScreen(data) {
  let urn =
    "urn:" + data.urn.organization + ":" + data.urn.group + ":" + data.urn.id;
  return axios({
    url: api.deleteScreen + `/${urn}`,
    data: data,
    method: "delete"
  });
}
export function connectVideo(params) {
  return axios({
    method: "post",
    url: api.connectVideo,
    data: params
  });
}
export function disconnectVideo(params) {
  return axios({
    method: "post",
    url: api.disconnectVideo,
    data: params
  });
}
export function listRecord(params) {
  return axios({
    method: "post",
    url: api.listRecord,
    data: params
  });
}
export function udpateState(params) {
  return axios({
    method: "post",
    url: api.udpateState,
    data: params
  });
}
export function vehicleRecordListRecord(params) {
  return axios({
    method: "post",
    url: api.vehicleRecordListRecord,
    data: params
  });
}
export function vehicleRecordUpdateState(params) {
  return axios({
    method: "post",
    url: api.vehicleRecordUpdateState,
    data: params
  });
}

export function parkingViolationList(params) {
  return axios({
    url: api.parkingViolationList,
    method: "post",
    data: params
  });
}
export function getRockfishSelectOptions(params) {
  return axios({
    url: api.getRockfishSelectOptions + params.type
  });
}

export function handleParking(params) {
  return axios({
    url: api.handleParking,
    params: params
  });
}

export function getTrashClassifyList(params) {
  return axios({
    url: api.getTrashClassifyList,
    data: params,
    method: "post"
  });
}

export function getSheetSatisfy(params) {
  return axios({
    url: api.getSheetSatisfy,
    data: params,
    method: "post"
  });
}

export function getSheetResolved(params) {
  return axios({
    url: api.getSheetResolved,
    data: params,
    method: "post"
  });
}

export function getSheetContact(params) {
  return axios({
    url: api.getSheetContact,
    data: params,
    method: "post"
  });
}
// 设定标签
export function setDevicesTag(params) {
  return axios({
    url: api.setDevicesTag,
    data: params,
    method: "post"
  });
}
// 读取标签列表
export function readDevicesTags(params) {
  return axios({
    url: api.readDevicesTags,
    params: params,
    method: "get"
  });
}
// 存储标签
export function addDevicesTags(params) {
  return axios({
    url: api.addDevicesTags,
    data: params,
    method: "post"
    // headers:{
    //   "Content-Type":"application/json;charset=utf-8"
    // }
  });
}
// 删除标签
export function deleteDevicesTags(params) {
  return axios({
    url: api.deleteDevicesTags,
    data: params,
    method: "delete"
  });
}
// 雷达信息列表
export function populationList(params) {
  return axios({
    url: api.populationList,
    data: params,
    method: "post"
  });
}
// 雷达信息修改
export function populationEdit(params) {
  return axios({
    url: api.populationEdit,
    data: params,
    method: "post"
  });
}
// 后台-工单分析实际解决率列表导出
export function resolvedExport(params) {
  window.open(api.resolvedExport + "?" + params);
  // return axios({
  //   url: api.resolvedExport,
  //   params: params,
  //   method:'get'
  // });
}
// 后台-工单分析先行联系率列表导出
export function contactExport(params) {
  window.open(api.contactExport + "?" + params);
  // return axios({
  //   url: api.contactExport,
  //   params: params,
  //   method:'get'
  // });
}
// 后台-工单分析满意度列表导出
export function satisfyExport(params) {
  window.open(api.satisfyExport + "?" + params);
  // return axios({
  //   url: api.satisfyExport,
  //   params: params,
  //   method:'get'
  // });
}

//后台-噪音检测设备列表
export function getNoiseDetectionList(params) {
  return axios({
    method: "post",
    url: api.noiseDectectionList,
    data: params
  });
}

//后台-新增噪音检测设备
export function addNoiseDetectionDevice(params) {
  return axios({
    method: "post",
    url: api.addNoiseDetectionDevice,
    data: params
  });
}

//后台-删除噪音检测设备
export function deleteNoiseDetectionDevice(params) {
  return axios({
    method: "post",
    url: api.deleteNoiseDetectionDevice + "?id=" + params.id
  });
}

//后台-修改设备阈值
export function editNoiseDetectionDevice(params) {
  return axios({
    method: "post",
    url: api.editNoiseDetectionDevice,
    data: params
  });
}
//  垃圾分类检查情况列表
export function garbageChecklist(params) {
  return axios({
    method: "post",
    url: api.garbageChecklist,
    data: params
  });
}
//  垃圾分类检查情况修改
export function garbageCheckedit(params) {
  return axios({
    method: "put",
    url: api.garbageCheckedit,
    data: params
  });
}
//  垃圾分类可回收物列表
export function garbageRecycledList(params) {
  return axios({
    method: "post",
    url: api.garbageRecycledList,
    data: params
  });
}
//  可回收服务点更新
export function garbageRecycledEdit(params) {
  return axios({
    method: "put",
    url: api.garbageRecycledEdit,
    data: params
  });
}
//  垃圾统计静态数据列表
export function staticConfigList(params) {
  return axios({
    method: "post",
    url: api.staticConfigList,
    data: params
  });
}
//  垃圾统计静态数据修改
export function staticConfigEdit(params) {
  return axios({
    method: "put",
    url: api.staticConfigEdit,
    data: params
  });
}
export function pieStatisticList(params) {
  return axios({
    method: "post",
    url: api.pieStatisticList,
    data: params
  });
}

export function pieStatisticAdd(params) {
  return axios({
    method: "post",
    url: api.pieStatisticAdd,
    data: params
  });
}

export function pieStatisticEdit(params) {
  return axios({
    method: "put",
    url: api.pieStatisticEdit,
    data: params
  });
}

export function pieStatisticDelete(params) {
  return axios({
    method: "DELETE",
    url: api.pieStatisticDelete + "?id=" + params.id
    // params:params
  });
}
