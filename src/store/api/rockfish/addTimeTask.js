import { axios } from "@/utils/requests";
/**
 * 获取视频的撒点
 */
export function getPoint(params) {
  return axios({
    method: "get",
    url: `/vis/v1/devices`,
    params: params
  });
}
/**
 * 获取编辑的任务信息
 */
export function getTaskDetail(params) {
  return axios({
    method: "get",
    url: `/vis/v1/streaming/polling/${params.poolingId}`
  });
}
/**
 * 添加/修改任务
 */
export function addTask(params) {
  return axios({
    method: "post",
    url: `/vis/v1/streaming/polling`,
    data: params
  });
}
