import api from "./rockfishUrls";
import { axios } from "@/utils/requests";
import axios2 from "axios";
import Vue from "vue";
/**
 * 用户登录
 */
export function login() {
  return axios({
    method: "post",
    url: api.loginUlr
    // data: params
  });
}
export function loginUser(params) {
  return axios({
    method: "post",
    url: api.loginUser,
    data: params
  });
}

export function getRFmenuList(params) {
  return axios({
    url: api.getRFmenuList,
    params: params
  });
}

export function getSanlinRFmenuList(params) {
  return axios({ url: api.getSanlinRFmenuList, params: params });
}
export function getAdminmenuList(params) {
  return axios({ url: api.getAdminmenuList, params: params });
}

export function getRFmenuListZhuqiao(params) {
  return axios({
    url: api.getRFmenuListZhuqiao,
    params: params,
    
  });
}
export function getusers(params) {
  let token  = Vue.ls.get('sanlintoken')
  return axios({
    url: api.getusers,
    params: params,
    headers:{
      tokenv2:token
    }
  });
}
export function getroles(params) {
  let token  = Vue.ls.get('sanlintoken')
  return axios({
    url: api.getroles,
    params: params,
    headers:{
      tokenv2:token
    }
  });
}
export function addsanlinUser(params) {
  let token  = Vue.ls.get('sanlintoken')
  return axios({
    url: api.addsanlinUser,
    data: params,
    method:'post',
    headers:{
      tokenv2:token
    }
  });
}
export function userlogin(params) {
  let token  = Vue.ls.get('sanlintoken')
  return axios({
    url: api.userlogin,
    data: params,
    method:'post',
    headers:{
      tokenv2:token
    }
  });
}
export function setsanlinRole(params) {
  let token  = Vue.ls.get('sanlintoken')
  return axios({
    url: api.setsanlinRole,
    params: params,
    method:'post',
    headers:{
      tokenv2:token
    }
  });
}
