// rockfish 后台管理所需要用的url
const api = {
  // 登录
  loginUlr: "/vis/v1/account/login",
  loginUser: "/zq/api/user/login",
  //视频轮屏列表
  // videoPollingList: "/vis/v1/streaming/polling",
  videoPollingList: "/vcs/v1/streaming/polling",
  //删除轮屏某个任务
  delete: "/vis/v1/streaming/polling",
  //立即执行某个轮屏任务
  runTaskNow: "/vis/v1/streaming/polling/",
  // 获取菜单
  getRFmenuList: "/rockfish/menu.json",
  getSanlinRFmenuList: "/rockfish/sanlinMenu.json",
  getAdminmenuList: "/rockfish/adminMenu.json",
  getRFmenuListZhuqiao: "/rockfish/subMenu.json",
  getCameraList: "/vis/v1/devices", //大屏撒点以及区域搜索通用接口
  createScreen: "/vis/v1/screens", //  新建屏幕
  deleteScreen: "/vis/v1/screen", //   删除屏幕
  connectVideo: "/vcs/v1/streaming/connect", //连接某个视频
  // connectVideo: "/vis/v1/streaming/connect?codec=HLS", //连接某个视频
  disconnectVideo: "/vcs/v1/streaming/disconnect", //断开视频链接
  listRecord: "/zq/api/car/listRecord", //车辆违停列表
  udpateState: "/zq/api/car/udpateState", //车辆违停列表
  vehicleRecordListRecord: "/zq/api/vehicleRecord/listRecord", //车辆违停列表
  vehicleRecordUpdateState: "/zq/api/vehicleRecord/updateState", //车辆违停列表
  parkingViolationList: "/seaweed/v1/car/list", //三林违章停车列表
  getRockfishSelectOptions: "/seaweed/v1/config/dropdown/", //获取下拉框
  handleParking: "/seaweed/v1/car/check", //处理违章停车
  getTrashClassifyList: "/seaweed/v1/garbage/list", // 垃圾分类列表
  getSheetSatisfy: "/seaweed/v1/case/rate/approve", //获取满意度列表
  getSheetResolved: "/seaweed/v1/case/rate/solution", //获取实际解决率列表
  getSheetContact: "/seaweed/v1/case/rate/contact", //获取先行联系率列表
  markInactive: "/vis/v1/streaming/report/", //标记摄像头损坏
  configList: "/seaweed/v1/config/list", // 配置管理
  setDevicesTag: "/vis/v1/devices/tagging", //设定标签
  readDevicesTags: "/vis/v1/devices/tags", //读取标签列表
  addDevicesTags: "/vis/v1/devices/tags", //添加标签
  deleteDevicesTags: "/vis/v1/devices/tags", //删除标签
  populationList: "/seaweed/v1/population/list", //雷达信息列表
  populationEdit: "/seaweed/v1/population/edit", //雷达信息修改
  resolvedExport: "/seaweed/v1/case/rate/solution/export", // 后台-工单分析实际解决率列表导出
  contactExport: "/seaweed/v1/case/rate/contact/export", // 后台-工单分析先行联系率列表导出
  satisfyExport: "/seaweed/v1/case/rate/approve/export", // 后台-工单分析满意度列表导出
  exportsCameralist: "/vis/v1/exports", // 后台-工单分析满意度列表导出,
  noiseDectectionList: "/seaweed/v1/noise/devices", //噪音检测信息列表
  addNoiseDetectionDevice: "/seaweed/v1/noise/add", //新增噪音检测设备
  editNoiseDetectionDevice: "/seaweed/v1/noise/edit", //修改设备阈值
  deleteNoiseDetectionDevice: "/seaweed/v1/noise/del", //编辑检测设备
  //  垃圾分类检查情况列表
  garbageChecklist: "/seaweed/v1/garbage/plate/backend/list",
  //  垃圾分类检查情况修改
  garbageCheckedit: "/seaweed/v1/garbage/plate/backend/edit",
  //  可回收服务点列表
  garbageRecycledList: "/seaweed/v1/garbage/recycled/backend/list",
  //  可回收服务点更新
  garbageRecycledEdit: "/seaweed/v1/garbage/recycled/backend/edit",
  //  垃圾统计静态数据
  staticConfigList: "/seaweed/v1/static/config/list",
  //  垃圾统计静态数据修改
  staticConfigEdit: "/seaweed/v1/static/config/edit",
  //饼图统计
  pieStatisticList: "/seaweed/v1/static/config/list",
  //饼图统计新增
  pieStatisticAdd: "/seaweed/v1/static/config/add",
  pieStatisticEdit: "/seaweed/v1/static/config/edit",
  pieStatisticDelete: "/seaweed/v1/static/config/del",
  addXiaoqu: "/seaweed/v1/garbage/notToStandard/update/create",
  searchXiaoqu: "/seaweed/v1/garbage/notToStandard/search",
  updateXiaoqu: "/seaweed/v1/garbage/notToStandard/update/update",
  deleteXiaoqu: "/seaweed/v1/garbage/notToStandard/delete",
  // 获取用户列表
  getusers: "/seaweed/v1/user/users",
  // 获取用户角色列表
  getroles: "/seaweed/v1/user/roles",
  // 添加用户
  addsanlinUser: "/seaweed/v1/user",
  // 用户登录
  userlogin: "/seaweed/v1/user/login",
  // 给用户分配角色
setsanlinRole: "/slcyapi/v1/user/setRole",
// 删除人员
delPerson:"/slcyapi/v1/account/backend/deleteUser",
// 新增人员
addPerson:"/slcyapi/v1/account/backend/addUser",
// 获取人员列表
getPersonList:"/slcyapi/v1/account/backend/pageBackend",
// 删除部门
delDept:"/slcyapi/v1/deptment/backend/deleteDept",
//获取推送人
getPerson:"/slcyapi/v1/deptment/backend/personList",
// 新增部门
addDept:"/slcyapi/v1/deptment/backend/saveDepartment",
// 获取任务权限
getListDept:"/slcyapi/v1/deptment/backend/listDeptFunction",
//获取部门列表
pageBackend:"/slcyapi/v1/deptment/backend/pageBackend",
//部门管理获取一级部门
getPrimaryDepart:"/slcyapi/v1/deptment/backend/listDept",
getDevices:"/seaweed/v1/noise/backend/devices",
getNoiseList:"/seaweed/v1/noise/backend/list",
saveNoise:"/seaweed/v1/noise/backend/export/saveQuery",
exportNoise:"/seaweed/v1/noise/backend/export"


};
export default api;
