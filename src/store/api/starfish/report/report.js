import {api} from "../starfishUrls";
import {axios} from "../../../../utils/requests";
// 获取上报类型
export function getReportType(params) {
    return axios({
        data: params,
        url: api.getReportUrl,
        method: 'get'
    })
}
// 获取组别
export function getGroupList(params) {
    return axios({
        data: params,
        url: api.getGroupListUrl,
        method: 'get'
    })
}
// 获取随手报的列表
export function getReportList(params) {
    return axios({
        data: params,
        url: api.getReportListUrl,
        method: 'post',
    })
}
// 获取随手报详情
export function getReportDetail(params) {
    return axios({
        data: params,
        url: api.getReportDetailUrl + '?taskId=' + params.taskId,
        method: 'get'
    })
}