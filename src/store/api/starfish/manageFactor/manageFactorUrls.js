const api = {
    // 获取任务类型列表
    getTaskTypeList: '/lxz/url/task/backend/policy/list',
    // 启用禁用任务
    changeTaskTypeStatus: '/lxz/url/task/backend/policy/disable',
    // 任务对象列表，企业列表
    getCompanyTaskObject: '/lxz/url/basicdata/backend/corporation/dropdown',
    // 任务对象列表，地列表
    getFieldTaskObject: '/lxz/url/basicdata/backend/lands/dropdown',
    // 任务对象列表，房列表
    getHouseTaskObject: '/lxz/url/basicdata/backend/rooms/dropdown',
    // 任务对象列表，人列表
    getPeopleTaskObject: '/lxz/url/basicdata/backend/careresidents/dropdown',
    // 任务对象列表，组列表
    getGroupTaskObject: '/lxz/url/basicdata/backend/district/dropdown',
    // 添加任务
    addTaskType: '/lxz/url/task/backend/policy/save',
    // 获取要素清单
    getYaosuList: '/lxz/url/task/backend/element/getAllElement',
    // 获取任务类型详情
    getTaskTypeDetail: '/lxz/url/task/backend/policy/detail',
    // 删除任务类型
    deleteTaskType: '/lxz/url/task/backend/policy/del',
    // 修改任务
    updateTaskType: '/lxz/url/task/backend/policy/edit',
    // 获取要素列表
    getYaosuManageList: '/lxz/url/task/backend/element/list',
    // 获取治理体征列表
    getManageFeatureList: '/lxz/url/task/backend/sign/list',
    // 获取管理要素部门列表
    getManageFactorDepList: '/lxz/admin/backend/account/getDepts',
    // 添加要素
    addYaosu: '/lxz/url/task/backend/element/save',
    // 获取要素详情
    getYaosuDetail: '/lxz/url/task/backend/element/detail',
    // 编辑要素
    updateYaosuDetail: '/lxz/url/task/backend/element/edit',
    // 删除要素
    deleteYaosuDetail: '/lxz/url/task/backend/element/del',
    // 新增治理体征
    addManageFeature: '/lxz/url/task/backend/sign/save',
    // 获取治理体征详情
    getManageFeatureDetail: '/lxz/url/task/backend/sign/detail',
    // 更新治理体征
    updateManageFeatureDetail: '/lxz/url/task/backend/sign/edit',
    // 删除治理体征
    deleteManageFeatureDetail: '/lxz/url/task/backend/sign/del',
    // 获取大屏已经设置的治理体征
    getExistManageFeatureSettings: '/lxz/url/task/backend/sign/getScreenSet',
    // 设置大屏治理体证位置
    setManageFeaturePosition: '/lxz/url/task/backend/sign/screenSet',
    // 获取关联角色
    getAssociationRole:'/lxz/admin/backend/role/getRoleForPolicy'
}

export default api;