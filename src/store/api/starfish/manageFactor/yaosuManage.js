import api from './manageFactorUrls'
import {
    axios
} from '@/utils/requests'

export function getYaosuManageList(params) {
    return axios({
        url: api.getYaosuManageList,
        method: 'post',
        data: params
    })
}



export function getYaosuDetail(params) {
    return axios({
        url: api.getYaosuDetail,
        params: params
    })
}


export function updateYaosuDetail(params) {
    return axios({
        url: api.updateYaosuDetail,
        data: params,
        method: 'put'
    })
}


export function deleteYaosuDetail(params) {
    return axios({
        url: api.deleteYaosuDetail,
        method: 'delete',
        data: params,
        params: params,
    })
}