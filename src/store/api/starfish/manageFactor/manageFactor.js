import api from './manageFactorUrls'

import {
    axios
} from '@/utils/requests'


export function getTaskTypeList(params) {
    return axios({
        url: api.getTaskTypeList,
        data: params,
        method: 'post'
    })
}

export function changeTaskTypeStatus(params) {
    return axios({
        url: api.changeTaskTypeStatus,
        params: params,
    })
}


export function getCompanyTaskObject(params) {
    return axios({
        url: api.getCompanyTaskObject,
        method: 'post',
        data: params
    })
}
export function getFieldTaskObject(params) {
    return axios({
        url: api.getFieldTaskObject,
        method: 'post',
        data: params
    })
}
export function getHouseTaskObject(params) {
    return axios({
        url: api.getHouseTaskObject,
        method: 'post',
        data: params
    })
}
export function getPeopleTaskObject(params) {
    return axios({
        url: api.getPeopleTaskObject,
        method: 'post',
        data: params
    })
}
export function getGroupTaskObject(params) {
    return axios({
        url: api.getGroupTaskObject,
        method: 'post',
        data: params
    })
}

export function addTaskType(params) {
    return axios({
        url: api.addTaskType,
        method: 'post',
        data: params
    })
}

export function getYaosuList(params) {
    return axios({
        url: api.getYaosuList,
        method: 'get',
        data: params
    })
}

export function getTaskTypeDetail(params) {
    return axios({
        url: api.getTaskTypeDetail,
        params: params
    })
}


export function deleteTaskType(params) {
    return axios({
        url: api.deleteTaskType,
        data: params,
        method: 'delete',
        params: params
    })
}

export function updateTaskType(params) {
    return axios({
        url: api.updateTaskType,
        data: params,
        method: 'put',
    })
}

export function getManageFeatureList(params) {
    return axios({
        url: api.getManageFeatureList,
        data: params,
        method: 'post'
    })
}


export function getManageFactorDepList(params) {
    return axios({
        url: api.getManageFactorDepList,
        params: params,
    })
}

export function addYaosu(params) {
    return axios({
        url: api.addYaosu,
        data: params,
        method: 'post',
    })
}

export function getAssociationRole(params) {
    return axios({
        url: api.getAssociationRole,
        params: params
    })
}