import api from './manageFactorUrls'
import {
    axios
} from '@/utils/requests'

export function addManageFeature(params) {
    return axios({
        url: api.addManageFeature,
        data: params,
        method: 'post'
    })
}

export function getManageFeatureDetail(params) {
    return axios({
        url: api.getManageFeatureDetail,
        params: params,
    })
}

export function updateManageFeatureDetail(params) {
    return axios({
        url: api.updateManageFeatureDetail,
        data: params,
        method: 'put',
    })
}

export function deleteManageFeatureDetail(params) {
    return axios({
        url: api.deleteManageFeatureDetail,
        data: params,
        params: params,
        method: 'delete',
    })
}

export function getExistManageFeatureSettings() {
    return axios({
        url: api.getExistManageFeatureSettings,
    })
}
export function setManageFeaturePosition(params) {
    return axios({
        url: api.setManageFeaturePosition,
        method: 'post',
        data: params
    })
}