import { api } from "../../starfishUrls";
import { axios } from "../../../../../utils/requests";
/**
 * 获取房屋列表
 */
export function getHouseList(params) {
  return axios({
    method: "post",
    data: params,
    url: api.getHouseList
  });
}
/**
 * 添加房屋
 */
export function addHouse(params) {
  return axios({
    method: "post",
    data: params,
    url: api.addHouse
  });
}
/**
 * 删除房屋
 */
export function deleteHouse(params) {
  return axios({
    method: "delete",
    url: api.deleteHouseUrl + `?urn=${params.urn}`
  });
}
/**
 * 获取房屋详情
 */
export function getDetail(params) {
  return axios({
    method: "get",
    url: api.getHouseDetailUrl + `?urn=${params.urn}`
  });
}
/**
 * 获取路名
 */
export function getStreet(params) {
  return axios({
    method: "get",
    url: api.getStreet + `?name=${params.name}`
  });
}
/**
 * 获取辖区，街道派出所
 */
export function getXiaquInfo() {
  return axios({
    method: "get",
    url: api.getXiaquInfoUrl
  });
}
/**
 * 获取门牌号列表
 */
export function getDoorList(params) {
  return axios({
    method: "get",
    url: api.getDoorNumberUrl + `?pid=${params.pid}&name=${params.name}`
  });
}
/**
 * 编辑更改房屋信息
 */
export function changeHouse(params) {
  return axios({
    method: "post",
    data: params,
    url: api.changeHouse
  });
}
/**
 * 导出房屋
 */
export function outputHouse(params) {
  return axios({
    method: "post",
    data: params,
    url: api.outputHouseUrl,
    responseType: "arraybuffer"
  });
}
/**
 * 导入房屋
 */
export function importHouse(params) {
  return axios({
    method: "post",
    data: params,
    url: api.importHouseUrl
  });
}
