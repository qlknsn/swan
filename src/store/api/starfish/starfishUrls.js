// starfish 后台管理所需要用到的url
export const api = {
    getBasicPeople:'/lxz/url/basicdata/backend/resident/list',  //获取实有人口列表数据
    getHouseList:'/lxz/url/basicdata/backend/rooms/list',  //获取房屋列表数据
    addHouse:'/lxz/url/basicdata/backend/rooms/save',  //添加房屋
    changeHouse:'/lxz/url/basicdata/backend/rooms/update',  //添加房屋
    deleteHouseUrl: '/lxz/url/basicdata/backend/rooms/delete', // 删除房屋
    getHouseDetailUrl: '/lxz/url/basicdata/backend/rooms/info', // 获取房屋详情
    getStreet: '/lxz/url/basicdata/backend/address/findStreets', // 获取路名列表
    getDoorNumberUrl: '/lxz/url/basicdata/backend/address/findPlots', // 获取门弄牌号
    getXiaquInfoUrl: '/lxz/url/basicdata/backend/district/info', // 获取辖区信息
    outputHouseUrl: '/lxz/url/basicdata/backend/rooms/export', // 房屋导出
    importHouseUrl: '/lxz/url/basicdata/backend/rooms/import', // 房屋导入
    getReportUrl: '/lxz/url/task/backend/anyRport/getAnyReportTypes', // 获取上报类型
    getGroupListUrl: '/lxz/url/task/backend/anyRport/getDistricts', // 获取组别
    getReportListUrl: '/lxz/url/task/backend/anyRport/list', // 获取随手报的列表
    getReportDetailUrl: '/lxz/url/task/backend/anyRport/detail', // 获取随手报的详情
}