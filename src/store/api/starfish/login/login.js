// starfish login模块
import api from './loginUrls'
import {
    axios
} from '@/utils/requests'

export function sfLoginUser(data) {
    return axios({
        url: api.sfLoginUser,
        method:'post',
        data: data
    })
}
// 获取用户信息
export function getUserName(data) {
    return axios({
        url: api.getUserName,
        method: 'get',
        data: data
    })
}