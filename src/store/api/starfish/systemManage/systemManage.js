import api from './systemManageUrl'
import {
    axios
} from '@/utils/requests'

export function accountList(params) {
    return axios({
        url: api.accountList,
        method:'post',
        data: params
    })
}
export function accountGetDepts(params) {
    return axios({
        url: api.accountGetDepts,
        params: params
    })
}
export function getRoles(params) {
    return axios({
        url: api.getRoles,
        params: params
    })
}
// 获取部门
export function getDept(params) {
    return axios({
        url: api.getDeptUrl,
        method:'post',
        data: params
    })
}
// 获取组别
export function getGroup(params) {
    return axios({
        url: api.getGroupUrl,
        method:'post',
        data: params
    })
}
// 获取角色
export function getCharacter(params) {
    return axios({
        url: api.getCharacterUrl,
        method:'post',
        data: params
    })
}
// 添加用户
export function addUser(params) {
    return axios({
        url: api.addUserUrl,
        method: 'post',
        data: params
    })
}


export function getRoleMenus(params) {
    return axios({
        url: api.getRoleMenus,
        params: params,
    })
}
export function saveRoleMenus(params) {
    return axios({
        url: api.saveRoleMenus,
        data: params,
        method: 'post'
    })
}

export function updateRole(params) {
    return axios({
        url: api.updateRole,
        data: params,
        method: 'put'
    })
}
export function saveRole(params) {
    return axios({
        url: api.saveRole,
        data: params,
        method: 'post'
    })
}
// 社区列表
export function districtList(params) {
    return axios({
        url: api.districtList,
        method: 'post',
        data: params
    })
}
// 增加辖区
export function districtSave(params) {
    return axios({
        url: api.districtSave,
        method: 'post',
        data: params
    })
}
// 辖区详情
export function districtDetail(params) {
    return axios({
        url: api.districtDetail,
        params: params
    })
}
// 获取用户信息详情
export function getUserDetail(params) {
    return axios({
        url: api.getUserDetailUrl,
        params: params
    })
}
export function editUserDetail(params) {
    return axios({
        url: api.editUserDetailUrl,
        data: params,
        method: 'put'
    })
}
// 获取用户列表
export function getUserList(params) {
    return axios({
        url: api.getUserListUrl,
        data: params,
        method: 'post'
    })
}
// 注销用户
export function deleteUser(params) {
    return axios({
        url: api.deleteUserUrl,
        params: params,
    })
}
// 修改密码
export function changePassword(params) {
    return axios({
        url: api.changePasswordUrl,
        params: params,
        method: 'put'
    })
}
// 编辑
export function districtEdit(params) {
    return axios({
        url: api.districtEdit,
        method:'put',
        data: params
    })
}
// 删除
export function districtDel(params) {
    return axios({
        url: api.districtDel,
        params: params
    })
}