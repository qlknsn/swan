const api = {
    // 组织机构
    accountList: '/lxz/admin/backend/account/list',
    accountGetDepts: '/lxz/admin/backend/account/getDepts',
    // 用户管理
    getRoles: '/lxz/admin/backend/account/getRoles',
    addUserUrl: '/lxz/admin/backend/account/save',
    getDeptUrl: '/lxz/admin/backend/account/getDepts', // 获取部门
    getGroupUrl: '/lxz/url/basicdata/backend/district/dropdown', // 获取组
    getCharacterUrl: '/lxz/admin/backend/role/list', // 获取角色
    getUserDetailUrl: '/lxz/admin/backend/account/detail', // 获取用户详情
    editUserDetailUrl: '/lxz/admin/backend/account/edit', // 获取角色
    getUserListUrl: '/lxz/admin/backend/account/list', // 获取用户列表
    deleteUserUrl: '/lxz/admin/backend/account/disable', // 注销用户
    changePasswordUrl: '/lxz/admin/backend/account/resetPassword', // 修改密码
    getRoleMenus: '/lxz/admin/backend/function/getAllMenus', // 获取角色所拥有的菜单权限
    saveRoleMenus: '/lxz/admin/backend/role/saveAuthority', // 保存角色拥有的菜单权限
    updateRole: '/lxz/admin/backend/role/edit', //  更新角色
    saveRole: '/lxz/admin/backend/role/save', //  更新角色
    // 辖区管理
    districtList:'/lxz/url/basicdata/backend/district/list',//辖区列表
    districtSave:'/lxz/url/basicdata/backend/district/save',//增加辖区
    districtDetail:'/lxz/url/basicdata/backend/district/detail',//增加辖区
    districtEdit:'/lxz/url/basicdata/backend/district/edit',//增加辖区
    districtDel:'/lxz/url/basicdata/backend/district/del',
}
export default api;