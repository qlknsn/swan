import api from './companyUrl'
import {
    axios
} from '@/utils/requests'

export function districtCorporationList(data) {
    return axios({
        url: api.districtCorporationList,
        method:'post',
        data: data
    })
}
export function corporationDelete(data) {
    return axios({
        url: api.corporationDelete,
        method:'delete',
        params: data
    })
}
export function findRooms(params) {
    return axios({
        url: api.findRooms,
        params: params
    })
}
export function addCompany(params) {
    return axios({
        url: api.addCompany,
        method:'post',
        data: params
    })
}
export function singleCompany(params) {
    return axios({
        url: api.singleCompany,
        params: params
    })
}
export function updateCompany(params) {
    return axios({
        url: api.updateCompany,
        method:'post',
        data: params
    })
}
export function shopList(params) {
    return axios({
        url: api.shopList,
        method:'post',
        data: params
    })
}
export function deleteShopList(params) {
    return axios({
        url: api.deleteShopList,
        method:'delete',
        params: params
    })
}
export function addShopList(params) {
    return axios({
        url: api.addShopList,
        method:'post',
        data: params
    })
}
export function updateShopList(params) {
    return axios({
        url: api.updateShopList,
        method:'post',
        data: params
    })
}
export function singleShop(params) {
    return axios({
        url: api.singleShop,
        params: params
    })
}
export function lvguanList(params) {
    return axios({
        url: api.lvguanList,
        method:'post',
        data: params
    })
}
export function deleteLvguanList(params) {
    return axios({
        url: api.deleteLvguanList,
        method:'delete',
        params: params
    })
}
export function singleLvguan(params) {
    return axios({
        url: api.singleLvguan,
        params: params
    })
}
export function addLvguanList(params) {
    return axios({
        url: api.addLvguanList,
        method:'post',
        data: params
    })
}
export function updateLvguanList(params) {
    return axios({
        url: api.updateLvguanList,
        method:'post',
        data: params
    })
}
export function minsuList(params) {
    return axios({
        url: api.minsuList,
        method:'post',
        data: params
    })
}
export function deleteMinsuList(params) {
    return axios({
        url: api.deleteMinsuList,
        method:'delete',
        params: params
    })
}
export function singleMinsu(params) {
    return axios({
        url: api.singleMinsu,
        params: params
    })
}
export function addMinsuList(params) {
    return axios({
        url: api.addMinsuList,
        method:'post',
        data: params
    })
}
export function updateMinsuList(params) {
    return axios({
        url: api.updateMinsuList,
        method:'post',
        data: params
    })
}
export function corporationExport(params) {
    return axios({
        url: api.corporationExport,
        method:'post',
        data:params,
        responseType: 'arraybuffer',
    })
}
export function corporationImport(params) {
    return axios({
        url: api.corporationImport,
        method:'post',
        data:params,
    })
}
export function shopExport(params) {
    return axios({
        url: api.shopExport,
        method:'post',
        data:params,
        responseType: 'arraybuffer',
    })
}
export function shopImport(params) {
    return axios({
        url: api.shopImport,
        method:'post',
        data:params,
    })
}
export function hotelExport(params) {
    return axios({
        url: api.hotelExport,
        method:'post',
        data:params,
        responseType: 'arraybuffer',
    })
}
export function hotelImport(params) {
    return axios({
        url: api.hotelImport,
        method:'post',
        data:params,
    })
}
export function homestayExport(params) {
    return axios({
        url: api.homestayExport,
        method:'post',
        data:params,
        responseType: 'arraybuffer',
    })
}
export function homestayImport(params) {
    return axios({
        url: api.homestayImport,
        method:'post',
        data:params,
    })
}