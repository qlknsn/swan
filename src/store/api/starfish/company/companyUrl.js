const api = {
    // 企业
    districtCorporationList: '/lxz/url/basicdata/backend/corporation/list',
    corporationDelete: '/lxz/url/basicdata/backend/corporation/delete',
    findRooms:'/lxz/url/basicdata/backend/rooms/findRooms',
    addCompany:'/lxz/url/basicdata/backend/corporation/save',
    singleCompany:'/lxz/url/basicdata/backend/corporation/info',
    updateCompany:'/lxz/url/basicdata/backend/corporation/update',
    corporationExport:'/lxz/url/basicdata/backend/corporation/export',
    corporationImport:'/lxz/url/basicdata/backend/corporation/import',
    // 商铺
    shopList:'/lxz/url/basicdata/backend/shop/list',
    deleteShopList:'/lxz/url/basicdata/backend/shop/delete',
    addShopList:'/lxz/url/basicdata/backend/shop/save',
    updateShopList:'/lxz/url/basicdata/backend/shop/update',
    singleShop:'/lxz/url/basicdata/backend/shop/info',
    shopExport:'/lxz/url/basicdata/backend/shop/export',
    shopImport:'/lxz/url/basicdata/backend/shop/import', 
    // 旅馆
    lvguanList:'/lxz/url/basicdata/backend/hotel/list',
    deleteLvguanList:'/lxz/url/basicdata/backend/hotel/delete',
    singleLvguan:'/lxz/url/basicdata/backend/hotel/info',
    addLvguanList:'/lxz/url/basicdata/backend/hotel/save',
    updateLvguanList:'/lxz/url/basicdata/backend/hotel/update',
    hotelExport:'/lxz/url/basicdata/backend/hotel/export',
    hotelImport:'/lxz/url/basicdata/backend/hotel/import',
    // 民宿
    minsuList:'/lxz/url/basicdata/backend/homestay/list',
    deleteMinsuList:'/lxz/url/basicdata/backend/homestay/delete',
    singleMinsu:'/lxz/url/basicdata/backend/homestay/info',
    addMinsuList:'/lxz/url/basicdata/backend/homestay/save',
    updateMinsuList:'/lxz/url/basicdata/backend/homestay/update',
    homestayExport:'/lxz/url/basicdata/backend/homestay/export',
    homestayImport:'/lxz/url/basicdata/backend/homestay/import',
}

export default api;