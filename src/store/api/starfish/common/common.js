import api from './commonUrls'
import {
    axios
} from '@/utils/requests'
// 获取所有字典
export function getDicts(data) {
    return axios({
        url: api.getDicts,
        params: data
    })
}
// 获取下级辖区
export function getArea(data) {
    return axios({
        url: api.getArea,
        params: data
    })
}