const api = {
    // 全部字典项查询接口
    getDicts: '/lxz/url/basicdata/backend/dictionary/getMultipleDicts',
    // 获取下级辖区
    getArea: '/lxz/url/basicdata/backend/district/listByParentDistrictId'
}

export default api;