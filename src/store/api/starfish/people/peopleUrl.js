export const api = {
    //实有人口
    getBasicPeople:'/lxz/url/basicdata/backend/resident/list',  //获取实有人口列表数据
    getAllDicts:'/lxz/url/basicdata/backend/dictionary/getAllDicts',   //获取全部的字段项查询接口(下来框)
    addBasicPeople:'/lxz/url/basicdata/backend/resident/save'   ,               //添加实有人口
    deleteBasicPeople:'/lxz/url/basicdata/backend/resident/delete'   ,                  //删除实有人口
    basicPeopleDetail:'/lxz/url/basicdata/backend/resident/info'  ,                 //实有人口详情
    editBasicPeople:'/lxz/url/basicdata/backend/resident/update',                   //更新人口详情
    
    //重点人员
    getImportantPeople:'/lxz/url/basicdata/backend/specialresidents/list'    ,                       //获取重点人员列表
    deleteImportantPeople:'/lxz/url/basicdata/backend/specialresidents/delete'   ,                    //删除重点人员
    addImportantPeople:'/lxz/url/basicdata/backend/specialresidents/save'       ,                     //添加重点人员
    getImportantPeopleDetail:'/lxz/url/basicdata/backend/specialresidents/info'      ,                    //获取重点人员详情
    editImportantPeople:'/lxz/url/basicdata/backend/specialresidents/update',                          //重点人员编辑


    //关爱人员
    getCarePeopleList:'/lxz/url/basicdata/backend/careresidents/list' ,                                   //获取关爱人员列表
    deleteCarePeople:'/lxz/url/basicdata/backend/careresidents/delete',                               //删除关爱人员
    carePeopleDetail:'/lxz/url/basicdata/backend/careresidents/info',                                 //关爱人员详情
    addCarePeople:'/lxz/url/basicdata/backend/careresidents/save',                                    //添加关爱人员
    editCarePeople:'/lxz/url/basicdata/backend/careresidents/update' ,                                //编辑关爱人员


    //党员
    getPartyPeopleList:'/lxz/url/basicdata/backend/party/list',                                     //获取党员列表
    addPartyPeople:'/lxz/url/basicdata/backend/party/save',                                //添加党员
    deletePartyPeople:'/lxz/url/basicdata/backend/party/delete',                           //删除党员
    editPartyPeople:'/lxz/url/basicdata/backend/party/update',                             //编辑党员
    getPartyPeopleDetail:'/lxz/url/basicdata/backend/party/info' ,                            //获取党员详情
    getPartyorganization:'/lxz/url/basicdata/backend/partyorganization/list'                 //获取党支部(下拉框)
} 