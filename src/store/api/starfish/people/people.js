import {axios} from '@/utils/requests'
import {api} from './peopleUrl'

export function getBasicPeople(params){   //获取实有人口数据列表
    return axios({
        method:'post',
        url:api.getBasicPeople,
        data:params,
    })

}


export function getAllDicts(params){      //获取全部字典项
    return axios({
        method:'get',
        url:api.getAllDicts,
        params:params,
        
    })
}


export function addBasicPeople(params){          //添加实有人口
    return axios({
        method:'post',
        url:api.addBasicPeople,
        data:params,
    })
}

export function deleteBasicPeople(params){      //删除实有人口
    return axios({
        method:'delete',
        url:api.deleteBasicPeople,
        params:params
    })
}

export function basicPeopleDetail(params){      //实有人口详情
    return axios({
        method:'get',
        url:api.basicPeopleDetail,
        params:params
    })
}


export function editPeopleDetail(params){       //编辑实有人口
    return axios({
        method:'post',
        url:api.editBasicPeople,
        data:params
    })
}

export function getImportantPeopleList(params){        //获取重点人员列表
    return axios({
        method:'post',
        url:api.getImportantPeople,
        data:params
    })
}

export function deleteImportantPeople(params){             //删除重点人员
    return axios({
        method:'delete',
        url:api.deleteImportantPeople,
        params:params
    })
}

export function addImportantPeople(params){               //添加重点人员
    return axios({
        method:'post',
        url:api.addImportantPeople,
        data:params
    })
}

export function getImportantPeopleDetail(params){        //获取重点人员详情
    return axios({
        method:'get',
        url:api.getImportantPeopleDetail,
        params:params
    })
}


export function editImportantPeople(params){            //编辑重点人员
    return axios({
        method:'post',
        url:api.editImportantPeople,
        data:params
    })
}

export function getCarePeoppleList(params){           //获取关爱人员列表
    return axios({
        method:'post',
        url:api.getCarePeopleList,
        data:params
    })
}

export function deleteCarePeople(params){            //删除关爱人员
    return axios({
        method:'delete',
        url:api.deleteCarePeople,
        params:params
    })
}


export function addCarePeople(params){              //添加关爱人员
    return axios({
        method:'post',
        url:api.addCarePeople,
        data:params
    })
}


export function editCarePeople(params){             //编辑关爱人员
    return axios({
        method:'post',
        url:api.editCarePeople,
        data:params
    })
}


export function getCarePeopleDetail(params){      //获取关爱人员详情
    return axios({
        method:'get',
        url:api.carePeopleDetail,
        params:params
    })
}

export function getPartyPeopleList(params){          //获取党员列表
    return axios({
        method:'post',
        url:api.getPartyPeopleList,
        data:params
    })
}


export function addPartyPeople(params){          //添加党员
    return axios({
        method:'post',
        url:api.addPartyPeople,
        data:params
    })
}

export function deletePartyPeople(params){          //删除党员
    return axios({
        method:'delete',
        url:api.deletePartyPeople,
        params:params
    })
}

export function editPartyPeople(params){          //编辑党员
    return axios({
        method:'post',
        url:api.editPartyPeople,
        data:params
    })
}


export function getPartyPeopleDetail(params){          //获取党员详情
    return axios({
        method:'get',
        url:api.getPartyPeopleDetail,
        params:params
    })
}

export function getPartyorganization(params){        //获取党支部(下拉框)
    return axios({
        method:'get',
        url:api.getPartyorganization,
        params:params
    })
}

