const api = {
  // 获取走访记录列表
  getVisitRecordList: "/lxz/url/task/backend/visit/list",
  // 获取走访记录详情
  getVisitRecordDetail: "/lxz/url/task/backend/visit/detail"
};

export default api;
