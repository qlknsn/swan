import api from "./visitRecordUrls";
import { axios } from "@/utils/requests";

export function getVisitRecordList(params) {
  return axios({
    url: api.getVisitRecordList,
    method: "post",
    data: params
  });
}

export function getVisitRecordDetail(params) {
  return axios({
    url: api.getVisitRecordDetail,
    params: params
  });
}
