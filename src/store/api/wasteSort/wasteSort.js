import api from "./wasteSortUrl";
import { axios } from "@/utils/requests";
export function getWasteList(params){
  return axios({
    data: params,
    url: api.getWasteList,
    method: "post",
  });
}
export function handleWaste(params){
  return axios({
    data: params,
    url: api.handleWaste,
    method: "post",
  });
}
export function deleteEvent(params){
  return axios({
    //  data: params,
    url: api.deleteEvent,
    method: "delete",
    params: params 
  });
}
export function uploadImage(params){
  return axios({
    headers: {"Content-Type":"multipart/form-data"},
    data: params,
    url: api.uploadImage,
    method: "post",
  });
}