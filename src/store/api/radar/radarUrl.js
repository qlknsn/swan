const api = {
  // 获取菜单
  getRadarmenuList: "/radar/menu.json",
  // 获取雷达的列表
  // getRadarList: "/sphyrnidae/v1/backstage/findRadars",
  getRadarList: "/sphyrnidae/v1/radar/search",
  // 获取设备状态
  getRadarStatus: "/sphyrnidae/v1/radar/statuses",
  // 获取雷达详情
  getRadarDetail: "/sphyrnidae/v1/radar/entity/",
  // 修改雷达的信息
  changeRadarDetail: "/sphyrnidae/v1/radar/entities",
  // 获取所有区域
  getAreaList: "/sphyrnidae/v1/backstage/findAllZones",
  // 获取所有的电动车列表
  getBicycleList: "/sphyrnidae/v1/bike/search", // '根据筛选条件查询自行车源数据',
  // 获取用户列表
  getUserList: "/sphyrnidae/v1/subscriber/members", // '通用查询用户列表接口',
  // 获取电动车轨迹信息
  getBicycleTrack: "/sphyrnidae/v1/backstage/findBicycleTrack",
  // 获取区域列表
  districtSearch: "/sphyrnidae/v1/district/search",
  // // 获取电车轨迹
  rfidTracksearch: '/sphyrnidae/v1/rfid/track/search',
  getOnlineRate: '/sphyrnidae/v1/report/radar/onlineRate',
  // 雷达状态报表
  getOnlineList: '/sphyrnidae/v1/report/radar/status',
  // 雷达的获取
  // getAllRadar: '/sphyrnidae/v1/radar/batch/search',
  // 雷达的轨迹
  getRadarPath: '/sphyrnidae/v1/rfid/track/search/{rfid}',
  // 录入rfid标签

  inputRfid: "/sphyrnidae/v1/rfid/save",
  // 获取雷达总数
  getRadarAll: '/sphyrnidae/v1/radar/totalCount',
  //根据身份证号或者手机号查询安装车辆
  search: "/sphyrnidae/v1/bike/search",
  //获取所有颜色类型
  getSimpleColours: "/sphyrnidae/v1/bike/simpleColours",

  // 案件列表
  theftcondition:'/sphyrnidae/v1/theft/search/condition',
  // 案件状态列表
  workflowStatus:'/sphyrnidae/v1/theft/workflowStatus',
  // // 案件状态列表
  // workflowStatus:'/sphyrnidae/v1/theft/workflowStatus',
  //报案
  reportCase: "/sphyrnidae/v1/theft/update",
  // 自行车信息
  getBikeInfo:'/sphyrnidae/v1/bike/entity',
  // 获取用户信息
  getUserInfo:'/sphyrnidae/v1/subscriber/member',
  // 更新案件的信息
  theftUpdate:'/sphyrnidae/v1/theft/update',
  // 电动车盗窃案件的类型
  legalCaseType:'/sphyrnidae/v1/theft/legalCaseConclusionType',
};
export default api;
