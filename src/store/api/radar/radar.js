import api from "./radarUrl";
import { axios } from "@/utils/requests";
export function getRadarmenuList() {
  return axios({
    url: api.getRadarmenuList
  });
}
export function getRadarList(data) {
  return axios({
    url:
      api.getRadarList + `?start=${data.page.start}&count=${data.page.count}`,
    data: data.params,
    method: "post"
  });
}
export function getRadarStatus(data) {
  return axios({
    url: api.getRadarStatus,
    params: data
  });
}
export function inputRfid(data) {
  return axios({
    url: api.inputRfid,
    method: "post",
    data: data
  });
}
export function getRadarDetail(data) {
  return axios({
    url: api.getRadarDetail + data.id
  });
}
export function changeRadarDetail(data) {
  return axios({
    url: `${api.changeRadarDetail}?action=partial_update`,
    data: data,
    method: "post"
  });
}
export function getAreaList(data) {
  return axios({
    url: api.getAreaList,
    params: data
  });
}
export function getBicycleList(data) {
  return axios({
    url:
      api.getBicycleList + `?start=${data.page.start}&count=${data.page.count}`,
    method: "post",
    data: data.params
  });
}
export function getBicycle(data) {
  return axios({
    url: api.getBicycleList,
    method: "post",
    data: data
  });
}
export function getUserList(data) {
  return axios({
    url: api.getUserList,
    method: "post",
    data: data.params,
    params: data.page
  });
}
export function getBicycleTrack(data) {
  return axios({
    url: api.getBicycleTrack,
    params: data
  });
}
export function districtSearch(data) {
    return axios({
      url: api.districtSearch,
      method: "post",
      data: data
    });
}
export function alldistrictSearch(data) {
    return axios({
      url: `${api.districtSearch}?start=${data.start}&count=${data.count}`,
      method: "post",
      data:{}
    })
}
export function rfidTracksearch(data) {
  return axios({
    url: `${api.rfidTracksearch}/${data.urn}`,
    method:"get",
    params:{
      startTime:data.startTime,
      endTime:data.endTime
    }
  });
}
export function getOnlineList(data) {
  return axios({
    url: api.getOnlineList,
    params: data
  });
}
export function getOnlineRate(data) {
  return axios({
    url: api.getOnlineRate,
    params: data
  });
}
// export function getAllRadar(data) {
//   return axios({
//     url: api.getAllRadar,
//     params: data
//   })
// }
export function getRadarPath(data) {
  return axios({
    url: api.getRadarPath,
    params: data
  })
}
export function getRadarAll(data) {
  return axios({
    url: api.getRadarAll,
    params: data
  })
}
export function search(data) {
  return axios({
    url: api.search,
    data: data,
    method: "post"
  })
}
export function getSimpleColours(data) {
  return axios({
    url: api.getSimpleColours,
    method: "get"
  })
}
// 获取案件状态
export function workflowStatus(data) {
  return axios({
    url: api.workflowStatus,
    method: "get"
  })
}
export function theftcondition(data) {
  let params = {
    reporterName: data.reporterName,
    statuses: data.statuses
  }
  return axios({
    url: `${api.theftcondition}?start=${data.start}&count=${data.count}`,
    method: "post",
    data: params
  })
}
export function reportCase(data) {
  return axios({
    url: api.reportCase,
    method:"post",
    data:data
  })
}
export function getBikeInfo(data) {
  return axios({
    url: `${api.getBikeInfo}/${data.urn}`,
  })
}
export function getUserInfo(data) {
  return axios({
    url: `${api.getUserInfo}/${data.urn}`,
  })
}
export function theftUpdate(data) {
  return axios({
    url: api.theftUpdate,
    method:"post",
    data:data
  })
}
export function legalCaseType(data) {
  return axios({
    url: api.legalCaseType,
    params:data
  })
}
