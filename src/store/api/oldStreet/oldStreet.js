import api from "../oldStreet/oldStreetUrl.js";
import { axios } from "@/utils/requests";

export function laojieSetRoles(data) {
  return axios({
    url: api.laojieSetRoles,
    params: data,
    method: "post"
  });
}
export function laojieRoles(data) {
  return axios({
    url: api.laojieRoles,
    params: data
  });
}
export function lajieUsers(data) {
  return axios({
    url: api.lajieUsers,
    params: data
  });
}
export function laojieAddUser(data) {
  return axios({
    url: api.laojieAddUser,
    data: data,
    method: "post"
  });
}
export function administratorLogin(data) {
  return axios({
    url: api.administratorLogin,
    data: data,
    method: "post"
  });
}
export function login2admin(data) {
  return axios({
    url: api.login2admin,
    params: data
  });
}
export function login2normal(data) {
  return axios({
    url: api.login2normal,
    params: data
  });
}
export function login2log(data) {
  return axios({
    url: api.login2log,
    params: data
  });
}
export function getOldStreet() {
  return axios({
    url: api.getOldStreet
  });
}
export function getkeliuInfo(params) {
  return axios({
    url: api.getkeliuInfo,
    method: "get",
    params: params
  });
}
export function getkeliuCount(params) {
  return axios({
    url: api.getkeliuCount,
    method: "get",
    params: params
  });
}
export function getpersonInfo(params) {
  return axios({
    url: api.getpersonInfo,
    method: "post",
    data: params
  });
}
export function deletepersonInfo(params) {
  return axios({
    url: api.deletepersonInfo,
    method: "get",
    params: params
  });
}
export function addpersonInfo(params) {
  return axios({
    url: api.addpersonInfo,
    method: "post",
    data: params
  });
}
export function getpersonWork(params) {
  return axios({
    url: api.getpersonWork,
    method: "get",
    params: params
  });
}
export function getOldStreetJingdian(params) {
  return axios({
    url: api.getOldStreetJingdian,
    method: "post",
    data: params
  });
}
export function getJingdianType(params) {
  return axios({
    url: api.getJingdianType,
    method: "get",
    params: params
  });
}
export function deleteJingdian(params) {
  return axios({
    url: api.deleteJingdian,
    method: "get",
    params: params
  });
}
export function addJingdian(params) {
  return axios({
    url: api.addJingdian,
    method: "post",
    data: params
  });
}
export function zhinengbaojing(params) {
  return axios({
    url: api.getzhinengInfo,
    method: "get",
    params: params
  });
}
export function wentitousu(params) {
  return axios({
    url: api.wentitousu,
    method: "get",
    params: params
  });
}
export function xiaochengxuUse(params) {
  return axios({
    url: api.xiaochengxuUse,
    method: "get",
    params: params
  });
}
export function osLogin(params) {
  return axios({
    url: api.osLogin,
    method: "get",
    params: params
  });
}
export function getTourNotes(params) {
  return axios({
    url: api.getTourNotes,
    method: "post",
    data: params
  });
}
export function saveTourNotes(params) {
  return axios({
    url: api.saveTourNotes,
    method: "post",
    data: params
  });
}
export function osDelToourNote(params) {
  return axios({
    url: api.osDelToourNote,
    method: "get",
    params: params
  });
}
export function getCivilizationBack(params) {
  return axios({
    url: api.getCivilizationBack,
    method: "get",
    params: params
  });
}
export function saveCivilization(params) {
  return axios({
    url: api.saveCivilization,
    method: "post",
    data: params
  });
}
export function deleteCivilization(params) {
  return axios({
    url: api.deleteCivilization,
    method: "get",
    params: params
  });
}
export function getConvenientBack(params) {
  return axios({
    url: api.getConvenientBack,
    method: "get",
    params: params
  });
}
export function saveConvenient(params) {
  return axios({
    url: api.saveConvenient,
    method: "post",
    data: params
  });
}
export function deleteConvenient(params) {
  return axios({
    url: api.deleteConvenient,
    method: "get",
    params: params
  });
}
export function getQuestionBack(params) {
  return axios({
    url: api.getQuestionBack,
    method: "get",
    params: params
  });
}
export function deleteQuestion(params) {
  return axios({
    url: api.deleteQuestion,
    method: "get",
    params: params
  });
}
export function getOldStreetBack(params) {
  return axios({
    url: api.getOldStreetBack,
    method: "post",
    data: params
  });
}
export function saveOldStreet(params) {
  return axios({
    url: api.saveOldStreet,
    method: "post",
    data: params
  });
}
export function deleteOldStreet(params) {
  return axios({
    url: api.deleteOldStreet,
    method: "get",
    params: params
  });
}
export function getXiaochi(params) {
  return axios({
    url: api.getXiaochi,
    method: "get",
    params: params
  });
}
export function addXiaochi(params) {
  return axios({
    url: api.addXiaochi,
    method: "post",
    data: params
  });
}
export function deleteXiaochi(params) {
  return axios({
    url: api.deleteXiaochi,
    method: "get",
    params: params
  });
}
export function updateXiaochi(params) {
  return axios({
    url: api.updateXiaochi,
    method: "post",
    data: params
  });
}
export function addCanyin(params) {
  return axios({
    url: api.addCanyin,
    method: "post",
    data: params
  });
}
export function getCanyin(params) {
  return axios({
    url: api.getCanyin,
    data: params
  });
}
export function updateCanyin(params) {
  return axios({
    url: api.updateCanyin,
    data: params,
    method: "post"
  });
}
export function deleteCanyin(params) {
  return axios({
    url: api.deleteCanyin,
    data: params
  });
}
export function getCultureBack(params) {
  return axios({
    url: api.getCultureBack,
    method: "post",
    data: params
  });
}
export function saveCulture(params) {
  return axios({
    url: api.saveCulture,
    method: "post",
    data: params
  });
}
export function deleteCulture(params) {
  return axios({
    url: api.deleteCulture,
    method: "get",
    params: params
  });
}
export function getPersonBack(params) {
  return axios({
    url: api.getPersonBack,
    method: "get",
    params: params
  });
}
export function savePerson(params) {
  return axios({
    url: api.savePerson,
    method: "post",
    data: params
  });
}
export function deletePerson(params) {
  return axios({
    url: api.deletePerson,
    method: "get",
    params: params
  });
}
export function getRestaurantBack(params) {
  return axios({
    url: api.getRestaurantBack,
    method: "get",
    params: params
  });
}
export function saveRestaurant(params) {
  return axios({
    url: api.saveRestaurant,
    method: "post",
    data: params
  });
}
export function deleteRestaurant(params) {
  return axios({
    url: api.deleteRestaurant,
    method: "get",
    params: params
  });
}
export function getQuestionInfo(params) {
  return axios({
    url: api.getQuestionInfo,
    method: "get",
    params: params
  });
}
export function addQuestionInfo(params) {
  return axios({
    url: api.addQuestionInfo,
    method: "post",
    data: params
  });
}
export function getLogList(params) {
  return axios({
    url: api.getLogList,
    method: "post",
    data: params
  });
}
