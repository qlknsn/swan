import api from "./teambitionUrl";
import { axios } from "@/utils/requests";
export function teambitionMenu() {
  return axios({
    url: api.teambitionMenu,
  });
}