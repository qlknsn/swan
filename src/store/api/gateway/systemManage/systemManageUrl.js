const api = {
    //获取DNS
    getDNS: '/gateway/private/api/viid/v1/gateway/network/dns',
    saveDNS: '/gateway/private/api/viid/v1/gateway/network/dns',
    resetPwd: '/gateway/private/api/viid/v1/gateway/users/updatePassword'
}

export default api