import api from './systemManageUrl'
import {
    axios
} from '@/utils/requests'

export function getDNS(params) {
    return axios({
        url: api.getDNS,
        params: params
    })
}


export function saveDNS(params) {
    return axios({
        url: api.saveDNS,
        data: params,
        method: 'post'
    })
}
export function resetPwd(params) {
    return axios({
        url: api.resetPwd,
        data: params,
        method: 'post'
    })
}