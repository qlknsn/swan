import api from "./deviceManageUrl";
import { axios } from "@/utils/requests";

export function getDeviceList(data) {
  return axios({
    url: api.getDeviceList,
    params: data,
  });
}

export function addDevice(data) {
  return axios({
    url: api.addDevice,
    data: data,
    method: "post",
  });
}

export function getDeviceDetail(data) {
  return axios({
    url: api.getDeviceDetail,
    params: data,
  });
}

export function deleteDevice(data) {
  return axios({
    url: api.deleteDevice,
    params: data,
    method: "delete",
  });
}

export function updateDevice(data) {
  return axios({
    url: api.updateDevice,
    data: data,
    method: "post",
  });
}

export function scanCamera(data) {
  return axios({
    url: api.scanCamera,
    params: data,
  });
}

export function capturePic(data) {
  return axios({
    url: api.capturePic,
    params: data,
  });
}
