const api = {
  getDeviceList: "/gateway/private/api/viid/v1/gateway/devices/nvrList", // 获取设备里列表
  getDeviceDetail: "/gateway/private/api/viid/v1/gateway/devices/camera", //获取设备详情
  addDevice: "/gateway/private/api/viid/v1/gateway/devices/cameraCreate", //添加设备
  deleteDevice: "/gateway/private/api/viid/v1/gateway/devices/camera", // 删除设备
  updateDevice: "/gateway/private/api/viid/v1/gateway/devices/camera", //  更新设备
  scanCamera: "/gateway/private/api/viid/v1/gateway/devices/cameraScanner", //扫描网段内的相机
  capturePic: "/gateway/private/api/viid/v1/gateway/picture/picture",
};
export default api;
