import api from "./loginUrl";
import { axios } from "@/utils/requests";
export function getDefaultDicts(data) {
  return axios({
    url: api.getDefaultDicts,
    params: data
  });
}
export function login(params) {
  return axios({
    method: "post",
    url: api.loginUrl,
    data: params
  });
}

export function getHomepageInfo(params) {
  return axios({
    url: api.getHomepageInfo,
    params: params
  });
}
