const api = {
  //登录
  login: "/gateway/login",
  loginUrl: "/gateway/private/api/viid/v1/gateway/users/login",
  getDefaultDicts: "/gateway/private/api/viid/v1/gateway/paths/dropdownmenu",
  // getHomepageInfo: '/gateway/private/api/viid/v1/gateway/monitor/hostStatus',
  getHomepageInfo: "/gateway/private/api/viid/v1/gateway/monitor/homepage"
};

export default api;
