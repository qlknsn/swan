const api = {
    //获取网卡配置信息
    getInterfaceList: '/gateway/private/api/viid/v1/gateway/network/interface', // 获取网络信息
    getHostInfoUrl: '/gateway/private/api/viid/v1/gateway/gateway/hostInfo' // 获取主机信息
}

export default api