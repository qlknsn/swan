import api from './lookupUrl'
import {
    axios
} from '@/utils/requests'


// 获取列表以及详情
export function getInterfaceList(data) {
    return axios({
        url: api.getInterfaceList,
        params: data
    })
}
// 更新信息
export function updataInterfaceList(data) {
    return axios({
        method:'post',
        url: api.getInterfaceList,
        data: data
    })
}

// 获取主机信息
export function getHostInfo(data) {
    return axios({
        url: api.getHostInfoUrl,
        params: data
    })
}

// 修改主机信息
export function updataHostInfo(data) {
    return axios({
        method:'post',
        url: api.getHostInfoUrl,
        data: data
    })
}