import api from './officeUrl'
;
import {
    axios
} from '@/utils/requests'

export function officeLogin(data) {
    return axios({
        url: api.officeLogin,
        method:'post',
        data: data
    })
}
export function getOfficemenuList(data) {
    return axios({
        url: api.getOfficemenuList,
        params: data
    })
}
// 添加文章
export function addNews(data) {
    console.log(axios)
    return axios({
        url: api.addNews,
        method:'post',
        data: data,
        // headers: {
        //     "Authorization ": Vue.ls.get('token')
        // }
    })
}
export function editNews(data) {
    console.log(axios)
    return axios({
        url: `${api.addNews}/${data._id}` ,
        method:'put',
        data: data,
        // headers: {
        //     "Authorization ": Vue.ls.get('token')
        // }
    })
}
// 获取文章列表
export function getNewsList(data) {
    return axios({
        url: api.addNews,
        params: data,
    })
}
// 删除新闻
export function deleteNews(data) {
    return axios({
        url: `${api.addNews}/${data._id}`,
        method:'delete'
    })
}
// 新闻详情
export function singleNews(data) {
    return axios({
        url: `${api.addNews}/${data._id}`,
        method:'get'
    })
}
