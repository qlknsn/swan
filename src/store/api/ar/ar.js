import api from "./arUrl";
import { axios } from "@/utils/requests";
export function getHighPoint(params){
  return axios({
     data: params,
    url: api.getHighPoint,
    method: "get",
      params: params 
  });
}

export function setHighPoint(params){
  return axios({
     data: params,
    url: api.setHighPoint,
    method: "post",
  });
}
export function searchHighPoint(params){
  return axios({
     data: params,
    url: api.searchHighPoint,
    method: "get",
    params: params
  });
}
export function resetPassWord(params){
  return axios({
     data: params,
    url: api.resetPassWord,
    method: "post",
    params: params
  });
}
export function editUserInf(params){
  return axios({
    data: params,
    url: api.editUserInf,
    method: "put",
  });
}
export function addUserInf(params){
  return axios({
    data: params,
    url: api.addUserInf,
    method: "post",
    params: params
  });
}

export function queryAllUser(params){
  return axios({
    data: params,
    url: api.queryAllUser,
    method: "post",
    params: params
  });
}
export function delTree(params) {
  return axios({
    url: api.delTree,
    method: "delete",
    params: params
  });
}
export function addTree(params) {
  return axios({
    data: params,
    url: api.addTree,
    method: "post"
  });
}
export function editTree(params) {
  return axios({
    data: params,
    url: api.editTree,
    method: "post"
  });
}

export function searchTree(params) {
  return axios({
    data: params,
    url: api.searchTree,
    method: "post"
  });
}

export function getArMemuList(params) {
  return axios({
    params: params,
    url: api.getArMemuList
  });
}

export function setMarkShow(params) {
  return axios({
    data: params,
    url: api.setMarkShow,
    method: "post"
  });
}
export function arlogin(params) {
  return axios({
    params: params,
    url: api.arlogin,
    method: "get"
  });
}

export function hightPointList(params) {
  return axios({
    data: params,
    url: api.hightPointList,
    method: "post"
  });
}

export function addHCamera(params) {
  return axios({
    data: params,
    url: api.addHCamera,
    method: "post"
  });
}

export function delHCamera(params) {
  return axios({
    data: params,
    url: api.delHCamera,
    method: "post"
  });
}

export function getHCameraTmpl(params) {
  return axios({
    params: params,
    url: api.getHCameraTmpl,
    method: "get",
    responseType: "arraybuffer"
  });
}

export function exportHCamera(params) {
  return axios({
    params: params,
    url: api.exportHCamera,
    method: "get",
    responseType: "arraybuffer"
  });
}
export function loginout(params) {
  return axios({
    params: params,
    url: api.loginout,
    method: "get"
  });
}
export function getListEnum(params) {
  return axios({
    params: params,
    url: api.getListEnum,
    method: "get"
  });
}
export function getLabelList(params) {
  return axios({
    data: params,
    url: api.getLabelList,
    method: "post"
  });
}
export function delLabel(params) {
  return axios({
    data: params,
    url: api.delLabel,
    method: "post"
  });
}
export function moreDelLabel(params) {
  return axios({
    data: params,
    url: api.moreDelLabel,
    method: "post"
  });
}
export function modifyLable(params) {
  return axios({
    data: params,
    url: api.modifyLable,
    method: "post"
  });
}
export function queryLableType(params) {
  return axios({
    data: params,
    url: api.queryLableType,
    method: "post"
  });
}
export function addLableType(params) {
  return axios({
    data: params,
    url: api.addLableType,
    method: "post"
  });
}
export function delLableType(params) {
  return axios({
    data: params,
    url: api.delLableType,
    method: "post"
  });
}
export function queryLableIcon(params) {
  return axios({
    data: params,
    url: api.queryLableIcon,
    method: "post"
  });
}
export function delLableIcon(params) {
  return axios({
    data: params,
    url: api.delLableIcon,
    method: "post"
  });
}
export function queryLableLayer(params) {
  return axios({
    data: params,
    url: api.queryLableLayer,
    method: "post"
  });
}
export function addLableLayer(params) {
  return axios({
    data: params,
    url: api.addLableLayer,
    method: "post"
  });
}
export function delLableLayer(params) {
  return axios({
    data: params,
    url: api.delLableLayer,
    method: "post"
  });
}
export function queryLableTmpl(params) {
  return axios({
    data: params,
    url: api.queryLableTmpl,
    method: "post"
  });
}
export function addLableTmpl(params) {
  return axios({
    data: params,
    url: api.addLableTmpl,
    method: "post"
  });
}
export function modifyLableTmpl(params) {
  return axios({
    data: params,
    url: api.modifyLableTmpl,
    method: "post"
  });
}
export function delLableTmpl(params) {
  return axios({
    data: params,
    url: api.delLableTmpl,
    method: "post"
  });
}
export function queryPOI(params) {
  return axios({
    data: params,
    url: api.queryPOI,
    method: "post"
  });
}
export function queryPoiResType(params) {
  return axios({
    params: params,
    url: api.queryPoiResType,
  });
}
export function queryMarkTemplate(params) {
  return axios({
    data: params,
    url: api.queryMarkTemplate,
    method: "post"
  });
}
export function deletePOI(params) {
  return axios({
    data: params,
    url: api.deletePOI,
    method: "post"
  });
}
export function queryPOIFromGD(params) {
  return axios({
    data: params,
    url: api.queryPOIFromGD,
    method: "post"
  });
}
export function addPOI(params) {
  return axios({
    data: params,
    url: api.addPOI,
    method: "post"
  });
}
export function queryPoiTypeMap(params) {
  return axios({
    data: params,
    url: api.queryPoiTypeMap,
    method: "post"
  });
}
export function addPoiTypeMap(params) {
  return axios({
    data: params,
    url: api.addPoiTypeMap,
    method: "post"
  });
}
export function delPoiTypeMap(params) {
  return axios({
    data: params,
    url: api.delPoiTypeMap,
    method: "post"
  });
}
export function getConfig(params) {
  return axios({
    params: params,
    url: api.getConfig,
    method: "get"
  });
}
export function modConfig(params) {
  return axios({
    data: params,
    url: api.modConfig,
    method: "post"
  });
}
export function queryOptLog(params) {
  return axios({
    data: params,
    url: api.queryOptLog,
    method: "post"
  });
}
export function exportOptLog(params) {
  return axios({
    params: params,
    url: api.exportOptLog,
    method: "get",
    responseType: "arraybuffer"
  });
}
export function getSystemConfigurationmenus(params) {
  return axios({
    params: params,
    url: api.getSystemConfigurationmenus,
    method: "get"
  });
}
export function setMenus(params) {
  return axios({
    data: params,
    url: api.setMenus,
    method: "post"
  });
}
export function getArEventList(params) {
  return axios({
    params: params,
    url: api.getArEventList,
  });
}
export function getEventDetail(params) {
  return axios({
    params: params,
    url: api.getEventDetail,
  });
}
export function getEventStatus(params) {
  return axios({
    params: params,
    url: api.getEventStatus,
  });
}
export function editAttributes(params) {
  return axios({
    data: params,
    url: api.editAttributes,
    method: "put"
  });
}
export function editEventStatus(params) {
  return axios({
    data: params,
    url: api.editEventStatus,
    method: "put"
  });
}
export function getMapConfig(params) {
  return axios({
    params: params,
    url: api.getMapConfig,
  });
}
export function saveMapConfig(params) {
  return axios({
    data: params,
    url: api.saveMapConfig,
    method: "post"
  });
}
export function searchLowPoint(params) {
  return axios({
    params: params,
    url: api.searchLowPoint,
  });
}
export function queryLowPoint(params) {
  return axios({
    data: params,
    url: api.queryLowPoint,
    method: "post"
  });
}
export function addCamera(params) {
  return axios({
    data: params,
    url: api.addCamera,
    method: "post"
  });
}
export function getHKTree(params) {
  return axios({
    params: params,
    url: api.getHKTree,
  });
}
export function getHKCamera(params) {
  return axios({
    params: params,
    url: api.getHKCamera,
  });
}
export function copyHkCamera(params) {
  return axios({
    data: params,
    url: api.copyHkCamera,
    method: "post"
  });
}
export function deleteCamera(params) {
  return axios({
    url: api.deleteCamera,
    method: "post",
    data: params
  });
}
export function getTemplate(params) {
  return axios({
    params: params,
    url: api.getTemplate,
  });
}
export function importCamera(params) {
  return axios({
    data: params,
    url: api.importCamera,
    method: "post"
  });
}
export function editCamera(params) {
  return axios({
    data: params,
    url: api.editCamera,
    method: "post"
  });
}