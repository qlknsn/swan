const api = {
  getHighPoint: "/arxz/backend/user/getHighCamera",
  setHighPoint: "/arxz/backend/user/addHighCamera",
  searchHighPoint: "/arxz/frontend/searchTree",
  resetPassWord: "/arxz/backend/resetPass",
  editUserInf: "/arxz/backend/user/edit",
  addUserInf: "/arxz/backend/user/add",
  queryAllUser: "/arxz/backend/user/list",
  getArMemuList: "/ar/arMenu.json",
  arlogin: "/arapi/v1/Login",
  hightPointList: "/arapi/v1/QueryHCamera",
  delHCamera: "/arapi/v1/DelHCamera",
  addHCamera: "/arapi/v1/AddHCamera",
  getHCameraTmpl: "/arapi/v1/GetHCameraTmpl",
  exportHCamera: "/arapi/v1/ExportHCamera",
  importHCamera: "/arapi/v1/ImportHCamera",
  loginout: "/arapi/v1/Logout",
  getListEnum: "/arapi/v1/ListEnum",
  getLabelList: "/arapi/v1/QueryLabel",
  delLabel: "/arapi/v1/DelLabel",
  moreDelLabel: "/arapi/v1/ModifyLabel",
  modifyLable: "/arapi/v1/ModifyLabel",
  queryLableType: "/arapi/v1/QueryLabelType",
  addLableType: "/arapi/v1/AddLabelType",
  delLableType: "/arapi/v1/DelLabelType",
  queryLableIcon: "/arapi/v1/QueryLabelIcon",
  delLableIcon: "/arapi/v1/DelLabelIcon",
  queryLableLayer: "/arapi/v1/QueryLabelLayer",
  addLableLayer: "/arapi/v1/AddLabelLayer",
  delLableLayer: "/arapi/v1/DelLabelLayer",
  queryLableTmpl: "/arapi/v1/QueryLabelTmpl",
  addLableTmpl: "/arapi/v1/AddLabelTmpl",
  modifyLableTmpl: "/arapi/v1/ModifyLabelTmpl",
  delLableTmpl: "/arapi/v1/DelLabelTmpl",
  queryPOIFromGD: "/arxapi/v1/QueryPOIFromGD",
  addPOI: "/arxapi/v1/addPOI",
  queryPOI: "/arxapi/v1/QueryPOI",
  queryPoiResType: "/arxapi/v1/QueryPoiResType",
  queryMarkTemplate: "/arxapi/v1/QueryMarkTemplate",
  deletePOI: "/arxapi/v1/DeletePOI",
  queryPoiTypeMap: "/arxapi/v1/QueryPoiTypeMap/Backend",
  addPoiTypeMap: "/arxapi/v1/AddPoiTypeMap",
  delPoiTypeMap: "/arxapi/v1/DelPoiTypeMap",
  getConfig: "/arapi/v1/GetConfig",
  modConfig: "/arapi/v1/ModConfig",
  queryOptLog: "/arapi/v1/QueryOptLog",
  exportOptLog: "/arapi/v1/ExportOptLog",
  setMarkShow: "/arapi/v1/SetMarkShow",
  getSystemConfigurationmenus: "/arxz/backend/menus",
  setMenus: "/arxz/backend/setMenus",
  searchTree: "/arxz/backend/searchTree",
  editTree: "/arxz/backend/editTree",
  addTree: "/arxz/backend/addTree",
  delTree: "/arxz/backend/delTree",
  getArEventList: "/arxz/backend/attributes?position=list",
  getEventDetail: "/arxz/backend/attributes?position=detail",
  getEventStatus: "/arxz/backend/eventStatus",
  editAttributes: "/arxz/backend/editAttributes",
  editEventStatus: "/arxz/backend/editEventStatus",
  getMapConfig: "/arxz/map/getConfig",
  saveMapConfig: "/arxz/backend/saveMapConfig",
  searchLowPoint:"/arxz/backend/queryTree",
  queryLowPoint:"/arxz/backend/queryCamera",
  addCamera:"/arxz/backend/addCamera",
  getHKTree:"/arxz/backend/queryHkTree",
  // getHKCamera:"/arxz/backend/queryHkCamera?treeId=dc4b7823-db95-4f7d-af1a-a1ccd24fbced",
  getHKCamera:"/arxz/backend/queryHkCamera",
  copyHkCamera:"/arxz/backend/copyHkCamera",
  deleteCamera:"/arxz/backend/delCamera",
  getTemplate:"/arxz/backend/downloadTemplate",
  importCamera:"/arxz/backend/importCamera",
  editCamera:"/arxz/backend/editCamera",
};
export default api;
