import api from "../slcy/slcyUrl";
import { axios } from "@/utils/requests";

import axi from "axios";
import Vue from "vue";

export function getSlcyMemuList(params) {
  return axios({
    params: params,
    url: api.getSlcyMemuList
  });
}
export function slcyLogin(params) {
  return axi({
    params: params,
    url: api.slcyLogin,
    method: "get"
  });
}
export function getPageBackend(params) {
  return axi({
    data: params,
    url: api.getPageBackend,
    method: "post"
  });
}
export function delDepartent(params) {
  axi.defaults.headers.common["token"] = Vue.ls.get("slcytoken");
  return axi({
    params: params,
    url: api.delDepartent,
    method: "get"
  });
}
export function addDepartent(params) {
  axi.defaults.headers.common["token"] = Vue.ls.get("slcytoken");
  return axi({
    data: params,
    url: api.addDepartent,
    method: "post"
  });
}
export function getPermissions(params) {
  axi.defaults.headers.common["token"] = Vue.ls.get("slcytoken");
  return axi({
    data: params,
    url: api.getPermissions,
    method: "post"
  });
}
export function setPermissions(params) {
  axi.defaults.headers.common["token"] = Vue.ls.get("slcytoken");
  return axi({
    data: params,
    url: api.setPermissions,
    method: "post"
  });
}
export function getPeopleList(params) {
  axi.defaults.headers.common["token"] = Vue.ls.get("slcytoken");
  return axi({
    data: params,
    url: api.getPeopleList,
    method: "post"
  });
}
export function getDepartmentlist(params) {
  axi.defaults.headers.common["token"] = Vue.ls.get("slcytoken");
  return axi({
    data: params,
    url: api.getDepartmentlist,
    method: "get"
  });
}
export function slcyAddUser(params) {
  axi.defaults.headers.common["token"] = Vue.ls.get("slcytoken");
  return axi({
    data: params,
    url: api.slcyAddUser,
    method: "post"
  });
}
export function slcyDeleteUser(params) {
  axi.defaults.headers.common["token"] = Vue.ls.get("slcytoken");
  return axi({
    params: params,
    url: api.slcyDeleteUser,
    method: "get"
  });
}
export function getEventList(params) {
  axi.defaults.headers.common["token"] = Vue.ls.get("slcytoken");
  return axi({
    data: params,
    url: api.getEventList,
    method: "post"
  });
}
