const api = {
    getSlcyMemuList: "/slcy/slcyMenu.json",
    slcyLogin: "/slcyapi/v1/account/backend/loginBackend",
    getPageBackend: "/slcyapi/v1/deptment/backend/pageBackend",
    delDepartent: "/slcyapi/v1/deptment/backend/deleteDept",
    addDepartent: "/slcyapi/v1/deptment/backend/saveDepartment",
    getPermissions: "/slcyapi/v1/deptment/backend/getPermissions",
    setPermissions: "/slcyapi/v1/deptment/backend/setPermissions",
    getDepartmentlist: "/slcyapi/v1/deptment/backend/listDept",
    getPeopleList: "/slcyapi/v1/account/backend/pageBackend",
    slcyAddUser: "/slcyapi/v1/account/backend/addUser",
    slcyDeleteUser: "/slcyapi/v1/account/backend/deleteUser",
    getEventList: "/slcyapi/v1/function/backend/pageBackend",
}
export default api