const api = {
  // 获取worksheet system菜单
  getWorkSheetMenu: "/workSheet/menu.json",
  // 提交订单
  createOrder: "/flamingo/api/v1/order",
  // 获取订单列表
  getOrderList: "/flamingo/api/v1/order",
  // 获取订单详情
  getOrderDetail: "/flamingo/api/v1/order",
  // 增加订单备注
  addOrderRemark: "/flamingo/api/v1/remark",
  // 获取备注列表
  getOrderRemark: "/flamingo/api/v1/remark",
  // 用户登录
  workSheetLoginUser: "/flamingo/api/v1/user/login",
  // 获取系统列表
  getProjectList: "/flamingo/api/v1/project",
  // 新建系统
  createProject: "/flamingo/api/v1/project",
  // 删除系统
  updateProject: "/flamingo/api/v1/project",
  // 更新订单状态
  updateOrderStatus: "/flamingo/api/v1/order/status",
  // 导出订单Excel
  exportExcel: "/flamingo/api/v1/order/excel"
};
export default api;
