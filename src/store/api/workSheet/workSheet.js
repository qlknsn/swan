import api from "./workSheetUrl";
import { axios } from "@/utils/requests";
export function exportExcel() {
  return axios({ responseType: "blob", url: api.exportExcel });
}
export function getWorkSheetMenu() {
  return axios({
    url: api.getWorkSheetMenu
  });
}

export function createOrder(data) {
  return axios({
    url: api.createOrder,
    data: data,
    method: "post"
  });
}

export function getOrderList(params) {
  return axios({
    url: api.getOrderList,
    params
  });
}

export function getOrderDetail(params) {
  return axios({ url: api.getOrderDetail + `/${params.orderid}` });
}

export function createProject(data) {
  return axios({
    url: api.createProject,
    data,
    method: "post"
  });
}
export function addOrderRemark(data) {
  return axios({
    url: api.addOrderRemark,
    data,
    method: "post"
  });
}

export function getOrderRemark(params) {
  return axios({
    url: api.getOrderRemark,
    params
  });
}
export function workSheetLoginUser(data) {
  return axios({
    url: api.workSheetLoginUser,
    data,
    method: "post"
  });
}

export function getProjectList(params) {
  return axios({
    url: api.getProjectList,
    params
  });
}

export function updateOrderStatus(data) {
  return axios({
    url: api.updateOrderStatus,
    data,
    method: "post"
  });
}

export function updateProject(data) {
  return axios({
    url: api.updateProject + `/${data.projectID}`,
    method: "post",
    data
  });
}
