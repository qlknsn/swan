import api from "./loginUrl";

import { axios } from "@/utils/requests";
export function login(data) {
  return axios({
    url: api.login,
    // method: "post",
    data: data
  });
}
