import api from './logManageUrl'
import {
    axios
} from '@/utils/requests'

export function listPageByCreateTimeAndIdOrUUID(params) {
    return axios({
        url: api.listPageByCreateTimeAndIdOrUUID,
        params: params
    })
}