const api = {
    listPageByIdOrUuid: '/Ukey/device/listPageByIdOrUuid',
    generateUUIDAndKey: '/Ukey/device/generateUUIDAndKey',
    deviceExistsById: '/Ukey/device/existsById',//根据设备序列号查询是否已经添加到系统
    deviceExistsByUuid: '/Ukey/device/existsByUuid',//根据设备UUID查询是否已经添加到系统
    BindlistPageByIdOrUuid: '/Ukey/deviceBind/listPageByIdOrUuid',//分页查询所有,可以根据设备序列号或者UUID查询
    // listPageByIdOrUuid: '/Ukey/deviceBind/listPageByIdOrUuid',//分页查询所有,可以根据设备序列号或者UUID查询
    BindDeleteById: '/Ukey/deviceBind/deleteById',
    deviceDeleteById: '/Ukey/device/deleteById',
    deviceBindInsertList :'/Ukey/deviceBind/insertList',
    insertIfNotExists:'/Ukey/device/insertIfNotExists',
    deviceBindChange:'/Ukey/deviceBind/change',
    deviceBindListPageUserNameByServerId:'/Ukey/deviceBind/listPageUserNameByServerId',

}

export default api;