import api from './managementUrl'
import {
    axios
} from '@/utils/requests'

export function listPageByIdOrUuid(params) {
    return axios({
        url: api.listPageByIdOrUuid,
        params: params
    })
}
export function generateUUIDAndKey() {
    return axios({
        url: api.generateUUIDAndKey
    })
}
export function deviceExistsById(params) {
    return axios({
        url: api.deviceExistsById,
        params: params
    })
}
export function deviceExistsByUuid(params) {
    return axios({
        url: api.deviceExistsByUuid,
        params: params
    })
}
export function BindlistPageByIdOrUuid(params) {
    return axios({
        url: api.BindlistPageByIdOrUuid,
        params: params
    })
}

export function BindDeleteById(params) {
    return axios({
        url: api.BindDeleteById,
        method:'post',
        data: params
    })
}

export function deviceDeleteById(params) {
    return axios({
        url: api.deviceDeleteById,
        params: params
    })
}
export function deviceBindInsertList(params) {
    return axios({
        url: api.deviceBindInsertList,
        method:'post',
        data: params
    })
}
export function insertIfNotExists(params) {
    return axios({
        url: api.insertIfNotExists,
        method:'post',
        data: params
    })
}
export function deviceBindChange(params) {
    return axios({
        url: api.deviceBindChange,
        params: params
    })
}
export function deviceBindListPageUserNameByServerId(params) {
    return axios({
        url: api.deviceBindListPageUserNameByServerId,
        params: params
    })
}

