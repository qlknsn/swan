const api = {
    userLogin: '/Ukey/user/login',
    userRandom:'/Ukey/user/random',
    userUkLogin:'/Ukey/user/ukLogin',
    userLogout:'/Ukey/user/logout'
}
export default api;