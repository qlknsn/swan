import api from './loginUrl'
import {
    axios
} from '@/utils/requests'

export function userLogin(params) {
    return axios({
        url: api.userLogin,
        params: params
    })
}

export function userRandom(params) {
    return axios({
        url: api.userRandom,
        params: params
    })
}
export function userUkLogin(params) {
    return axios({
        url: api.userUkLogin,
        params: params
    })
}
export function userLogout(params) {
    return axios({
        url: api.userLogout,
        params: params
    })
}