const api = {
    listPageByName: '/Ukey/server/listPageByName',
    insertServer: '/Ukey/server/insert',
    dbTypeList: '/Ukey/dbType/list',
    deleteById: '/Ukey/server/deleteById',
    serverUpdate: '/Ukey/server/update',
    listDetailsPageByKey: '/Ukey/user/listDetailsPageByKey',//用户列表
    userInsert: '/Ukey/user/insert',//添加用户列表
    userDeleteById: '/Ukey/user/deleteById',//删除用户列表
    listDetailsPageByRoleName: '/Ukey/role/listDetailsPageByRoleName',//角色liebiao
    userUpdate: '/Ukey/user/update',//修改y用户liebiao
    roleDeleteById: '/Ukey/role/deleteById',//修改y用户liebiao
    listVO:'/Ukey/permission/listVO',
    roleInsert:'/Ukey/role/insert',
    roleGetDetailsById:'/Ukey/role/getDetailsById',
    roleUpdate:'/Ukey/role/update',
    deviceBindUpdateStatus:'/Ukey/deviceBind/updateStatus'
}

export default api;