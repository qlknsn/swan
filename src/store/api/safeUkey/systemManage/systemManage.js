import api from './systemManageUrl'
import {
    axios
} from '@/utils/requests'

export function listPageByName(params) {
    return axios({
        url: api.listPageByName,
        params: params
    })
}
export function insertServer(params) {
    return axios({
        url: api.insertServer,
        method:'post',
        data: params
    })
}
export function dbTypeList(params) {
    return axios({
        url: api.dbTypeList,
        params: params
    })
}
export function deleteById(params) {
    return axios({
        url: api.deleteById,
        params: params
    })
}
export function serverUpdate(params) {
    return axios({
        url: api.serverUpdate,
        method:'post',
        data: params
    })
}
export function listDetailsPageByKey(params) {//添加用户
    return axios({
        url: api.listDetailsPageByKey,
        params: params
    })
}
export function userInsert(params) {//添加用户
    return axios({
        url: api.userInsert,
        method:'post',
        data: params
    })
}
export function userDeleteById(params) {//删除用户
    return axios({
        url: api.userDeleteById,
        params: params
    })
}
export function listDetailsPageByRoleName(params) {
    return axios({
        url: api.listDetailsPageByRoleName,
        params: params
    })
}
export function userUpdate(params) {//修改用户
    return axios({
        url: api.userUpdate,
        method:'post',
        data: params
    })
}
export function roleDeleteById(params) {//修改用户
    return axios({
        url: api.roleDeleteById,
        params: params
    })
}
export function listVO(params) {//修改用户
    return axios({
        url: api.listVO,
        params: params
    })
}
export function roleInsert(params) {//修改用户
    return axios({
        url: api.roleInsert,
        method:'post',
        data: params
    })
}
export function roleGetDetailsById(params) {//修改用户
    return axios({
        url: api.roleGetDetailsById,
        params: params
    })
}
export function roleUpdate(params) {//修改用户
    return axios({
        url: api.roleUpdate,
        method:'post',
        data: params
    })
}
export function deviceBindUpdateStatus(params) {
    return axios({
        url: api.deviceBindUpdateStatus,
        method:'post',
        data: params
    })
}