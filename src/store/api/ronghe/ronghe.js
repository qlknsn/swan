import api from "./rongheUrl";
import { axios } from "@/utils/requests";
export function loginRonghe(data) {
  return axios({
    url: api.loginRonghe,
    data: data
  });
}

export function loginRonghe1(data) {
  return axios({
    url: api.loginRonghe1,
    data: data
  });
}
export function loginRonghe2(data) {
  return axios({
    url: api.loginRonghe2,
    data: data
  });
}

export function account_assembly_setReleaseStatus(data) {
  return axios({
    url: api.account_assembly_setReleaseStatus,
    params: data,
    method: "get",
    data: data
  });
}

export function account_assembly_setTopStatus(data) {
  return axios({
    url: api.account_assembly_setTopStatus,
    params: data,
    method: "get",
    data: data
  });
}
export function account_assembly_deleteAssembly(data) {
  return axios({
    url: api.account_assembly_deleteAssembly,
    params: data,
    method: "get",
    data: data
  });
}

export function account_assembly_findAssemblyByUniqueId(data) {
  return axios({
    url: api.account_assembly_findAssemblyByUniqueId,
    params: data,
    method: "get",
    data: data
  });
}

export function account_assembly_getAssembly(data) {
  return axios({
    url: api.account_assembly_getAssembly,
    params: data,
    method: "post",
    data: data
  });
}

export function account_assembly_getAssemblyAll(data) {
  return axios({
    url: api.account_assembly_getAssemblyAll,
    params: data,
    method: "get",
    data: data
  });
}

export function account_assembly_saveAssembly(data) {
  return axios({
    url: api.account_assembly_saveAssembly,
    params: data,
    method: "post",
    data: data
  });
}

export function account_assembly_setCommentStatus(data) {
  return axios({
    url: api.account_assembly_setCommentStatus,
    params: data,
    method: "get",
    data: data
  });
}

export function account_info(data) {
  return axios({
    url: api.account_info,
    params: data,
    method: "get",
    data: data
  });
}

export function account_jscode2session(data) {
  return axios({
    url: api.account_jscode2session,
    params: data,
    method: "get",
    data: data
  });
}

export function account_loginByOpenid(data) {
  return axios({
    url: api.account_loginByOpenid,
    params: data,
    method: "get",
    data: data
  });
}

export function account_phone(data) {
  return axios({
    url: api.account_phone,
    params: data,
    method: "get",
    data: data
  });
}

export function assembly_setCommentStatus(data) {
  return axios({
    url: api.assembly_setCommentStatus,
    params: data,
    method: "get",
    data: data
  });
}

export function attachment_fileUpload(data) {
  return axios({
    url: api.attachment_fileUpload,
    params: data,
    method: "post",
    data: data
  });
}

export function booking_deleteByServiceUrnAndAndUserUrn(data) {
  return axios({
    url: api.booking_deleteByServiceUrnAndAndUserUrn,
    params: data,
    method: "get",
    data: data
  });
}

export function booking_save(data) {
  return axios({
    url: api.booking_save,
    params: data,
    method: "post",
    data: data
  });
}

export function comment_delete(data) {
  return axios({
    url: api.comment_delete,
    params: data,
    method: "get",
    data: data
  });
}
export function comment_find(data) {
  return axios({
    url: api.comment_find,
    params: data,
    method: "post",
    data: data
  });
}

export function comment_findTree(data) {
  return axios({
    url: api.comment_findTree,
    params: data,
    method: "get",
    data: data
  });
}

export function comment_makeComment(data) {
  return axios({
    url: api.comment_makeComment,
    params: data,
    method: "post",
    data: data
  });
}

export function comment_page(data) {
  return axios({
    url: api.comment_page,
    params: data,
    method: "post",
    data: data
  });
}

export function comment_setExamineStatus(data) {
  return axios({
    url: api.comment_setExamineStatus,
    params: data,
    method: "get",
    data: data
  });
}

export function dictionary_list(data) {
  return axios({
    url: api.dictionary_list,
    params: data,
    method: "get",
    data: data
  });
}

export function district_getDistrictTree(data) {
  return axios({
    url: api.district_getDistrictTree,
    params: data,
    method: "get",
    data: data
  });
}

export function district_getDistrictGroup(data) {
  return axios({
    url: api.district_getDistrictGroup,
    params: data,
  });
}

export function function_delete(data) {
  return axios({
    url: api.function_delete,
    params: data,
    method: "get",
    data: data
  });
}

export function function_info(data) {
  return axios({
    url: api.function_info,
    params: data,
    method: "get",
    data: data
  });
}

export function function_page(data) {
  return axios({
    url: api.function_page,
    params: data,
    method: "post",
    data: data
  });
}

export function function_save(data) {
  return axios({
    url: api.function_save,
    params: data,
    method: "post",
    data: data
  });
}

export function guarantee_deleteGuaranteeObject(data) {
  return axios({
    url: api.guarantee_deleteGuaranteeObject,
    params: data,
    method: "get",
    data: data
  });
}

export function guarantee_findGuaranteeByUniqueId(data) {
  return axios({
    url: api.guarantee_findGuaranteeByUniqueId,
    params: data,
    method: "get",
    data: data
  });
}

export function guarantee_getGuarantee(data) {
  return axios({
    url: api.guarantee_getGuarantee,
    params: data,
    method: "get",
    data: data
  });
}

export function guarantee_getGuaranteeA(data) {
  return axios({
    url: api.guarantee_getGuaranteeA,
    params: data,
    method: "get",
    data: data
  });
}

export function guarantee_handleGuarantee(data) {
  return axios({
    url: api.guarantee_handleGuarantee,
    params: data,
    method: "post",
    data: data
  });
}

export function guarantee_listByType(data) {
  return axios({
    url: api.guarantee_listByType,
    params: data,
    method: "get",
    data: data
  });
}

export function guarantee_saveGuarantee(data) {
  return axios({
    url: api.guarantee_saveGuarantee,
    params: data,
    method: "post",
    data: data
  });
}

export function handle_deleteHandleObject(data) {
  return axios({
    url: api.handle_deleteHandleObject,
    params: data,
    method: "get",
    data: data
  });
}

export function handle_findHandleByUniqueId(data) {
  return axios({
    url: api.handle_findHandleByUniqueId,
    params: data,
    method: "get",
    data: data
  });
}

export function handle_getHandle(data) {
  return axios({
    url: api.handle_getHandle,
    params: data,
    method: "get",
    data: data
  });
}

export function handle_getHandleA(data) {
  return axios({
    url: api.handle_getHandleA,
    params: data,
    method: "get",
    data: data
  });
}

export function handle_handleRquest(data) {
  return axios({
    url: api.handle_handleRquest,
    params: data,
    method: "post",
    data: data
  });
}

export function handle_listByType(data) {
  return axios({
    url: api.handle_listByType,
    params: data,
    method: "get",
    data: data
  });
}

export function handle_saveHandle(data) {
  return axios({
    url: api.handle_saveHandle,
    params: data,
    method: "post",
    data: data
  });
}

export function help_getHelp(data) {
  return axios({
    url: api.help_getHelp,
    params: data,
    method: "get",
  });
}

export function help_getHelpAll(data) {
  return axios({
    url: api.help_getHelpAll,
    params: data,
    method: "get",
    data: data
  });
}

export function help_saveHelp(data) {
  return axios({
    url: api.help_saveHelp,
    method: "post",
    data: data
  });
}
export function help_deleteHelp(data) {
  return axios({
    url: api.help_deleteHelp,
    method: "get",
    params: data
  });
}

export function homePage_deleteHomepageThr(data) {
  return axios({
    url: api.homePage_deleteHomepageThr,
    params: data,
    method: "get",
    data: data
  });
}

export function homePage_getHomepage(data) {
  return axios({
    url: api.homePage_getHomepage,
    params: data,
    method: "get",
    data: data
  });
}

export function homePage_getNotice(data) {
  return axios({
    url: api.homePage_getNotice,
    method: "post",
    data: data
  });
}

export function homePage_listByType(data) {
  return axios({
    url: api.homePage_listByType,
    params: data,
    method: "get",
    data: data
  });
}

export function homePage_saveHomePage(data) {
  return axios({
    url: api.homePage_saveHomePage,
    params: data,
    method: "post",
    data: data
  });
}

export function homeService_delete(data) {
  return axios({
    url: api.homeService_delete,
    params: data,
    method: "get",
    data: data
  });
}

export function homeService_info(data) {
  return axios({
    url: api.homeService_info,
    params: data,
    method: "get",
    data: data
  });
}

export function homeService_page(data) {
  return axios({
    url: api.homeService_page,
    params: data,
    method: "post",
    data: data
  });
}

export function homeService_save(data) {
  return axios({
    url: api.homeService_save,
    method: "post",
    data: data
  });
}

export function likes_cancel(data) {
  return axios({
    url: api.likes_cancel,
    params: data,
    method: "get",
    data: data
  });
}

export function likes_like(data) {
  return axios({
    url: api.likes_like,
    params: data,
    method: "post",
    data: data
  });
}

export function redident_appeal(data) {
  return axios({
    url: api.redident_appeal,
    params: data,
    method: "post",
    data: data
  });
}

export function redident_delay(data) {
  return axios({
    url: api.redident_delay,
    params: data,
    method: "post",
    data: data
  });
}

export function redident_info(data) {
  return axios({
    url: api.redident_info,
    params: data,
    method: "get",

  });
}

export function redident_page(data) {
  return axios({
    url: api.redident_page,
    data: data,
    method: "post",
  });
}

export function redident_register(data) {
  return axios({
    url: api.redident_register,
    params: data,
    method: "post",
    data: data
  });
}

export function redident_seal(data) {
  return axios({
    url: api.redident_seal,
    method: "post",
    data: data
  });
}

export function redident_unseal(data) {
  return axios({
    url: api.redident_unseal,
    params: data,
    method: "get",
  });
}

export function shanghaiPerson_saveShanghaiPerson(data) {
  return axios({
    url: api.shanghaiPerson_saveShanghaiPerson,
    params: data,
    method: "post",
    data: data
  });
}

export function staff_delete(data) {
  return axios({
    url: api.staff_delete,
    params: data,
    method: "get",
  });
}

export function staff_info(data) {
  return axios({
    url: api.staff_info,
    params: data,
    method: "get",
    data: data
  });
}

export function staff_login(data) {

  return axios({
    url: api.staff_login,
    params: data,
    method: "get",
    data: data,

  });
}

export function staff_page(data) {
  return axios({
    url: api.staff_page,
    method: "post",
    data: data
  });
}

export function staff_save(data) {
  return axios({
    url: api.staff_save,
    params: data,
    method: "post",
    data: data
  });
}
export function homeServiceList(data) {
  return axios({
    url: api.homeServiceList,
    params: data,
  });
}
export function gethomeServicepage(data) {
  return axios({
    url: api.gethomeServicepage,
    method: "post",
    data: data
  });
}
// 新增家门口服务类型
export function saveHomeServiceType(data) {
  return axios({
    url: api.saveHomeServiceType,
    method: "post",
    data: data
  });
}
// 设置时间
export function saveServiceTime(data) {
  return axios({
    url: api.saveServiceTime,
    method: "post",
    data: data
  });
}

export function get_repair_list(data) {
  return axios({
    url: api.repair_list,
    method: "post",
    data: data
  });
}

export function repair_list_alr(data) {
  return axios({
    url: api.repair_list_alr,
    method: "post",
    data: data
  });
}

export function repair_detail(data) {
  return axios({
    url: api.repair_detail,
    params: data
  });
}

export function come_hu_list(data) {
  return axios({
    url: api.come_hu_list,
    method: "post",
    data: data
  })
}

export function come_hu_detail(data) {
  return axios({
    url: api.come_hu_detail,
    params: data
  })
}
export function getsomeDatas(data) {
  return axios({
    url: api.getsomeDatas,
    params: data
  })
}

export function savecomehuPerson(data) {
  return axios({
    url: api.savecomehuPerson,
    method: "post",
    data: data
  })
}
export function getYuyuePeople(data) {
  console.log('123')
  return axios({
    url: api.getYuyuePeople,
    method: "post",
    data: data
  })
}
export function voteTableSave(data) {
  return axios({
    url: api.voteTableSave,
    method: "post",
    data: data
  })
}
export function setvoteStatus(data) {
  return axios({
    url: api.setvoteStatus,
    method: "get",
    params: data
  })
}
export function voteDetail(data) {
  return axios({
    url: `${api.voteDetail}/${data.id}`,
    method: "get"
  })
}
