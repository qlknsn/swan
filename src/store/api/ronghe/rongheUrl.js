const api = {
  loginRonghe: "/ronghe/menu.json",
  loginRonghe1: "/ronghe/menu1.json",

  loginRonghe2: "/ronghe/menu2.json",

  account_assembly_setTopStatus: "/hip/v1/account/assembly/setTopStatus",
  account_assembly_deleteAssembly: "/hip/v1/account/assembly/deleteAssembly",
  account_assembly_setReleaseStatus:
    "/hip/v1/account/assembly/setReleaseStatus",
  account_assembly_findAssemblyByUniqueId:
    "/hip/v1/account/assembly/findAssemblyByUniqueId",

  account_assembly_getAssembly: "/hip/v1/account/assembly/getAssembly",

  account_assembly_getAssemblyAll: "/hip/v1/account/assembly/getAssemblyAll",

  account_assembly_saveAssembly: "/hip/v1/account/assembly/saveAssembly",

  account_assembly_setCommentStatus:
    "/hip/v1/account/assembly/setCommentStatus",

  account_info: "/hip/v1/account/info",

  account_jscode2session: "/hip/v1/account/jscode2session",

  account_loginByOpenid: "/hip/v1/account/loginByOpenid",

  account_phone: "/hip/v1/account/phone",

  assembly_setCommentStatus: "/hip/v1/assembly/setCommentStatus",

  attachment_fileUpload: "/hip/v1/attachment/fileUpload",

  booking_deleteByServiceUrnAndAndUserUrn:
    "/hip/v1/booking/deleteByServiceUrnAndAndUserUrn",

  booking_save: "/hip/v1/booking/save",

  comment_delete: "/hip/v1/comment/delete",
  comment_find: "/hip/v1/comment/find",

  comment_findTree: "/hip/v1/comment/findTree",

  comment_makeComment: "/hip/v1/comment/makeComment",

  comment_page: "/hip/v1/comment/page",

  comment_setExamineStatus: "/hip/v1/comment/setExamineStatus",

  dictionary_list: "/hip/v1/dictionary/list",

  district_getDistrictTree: "/hip/v1/district/getDistrictTreeV2",

  district_getDistrictGroup: "/hip/v1/dictionary/list?dictGroup=DISTRICT",

  function_delete: "/hip/v1/function/delete",

  function_info: "/hip/v1/function/info",

  function_page: "/hip/v1/function/page",

  function_save: "/hip/v1/function/save",

  guarantee_deleteGuaranteeObject: "/hip/v1/guarantee/deleteGuaranteeObject",

  guarantee_findGuaranteeByUniqueId:
    "/hip/v1/guarantee/findGuaranteeByUniqueId",

  guarantee_getGuarantee: "/hip/v1/guarantee/getGuarantee",

  guarantee_getGuaranteeA: "/hip/v1/guarantee/getGuaranteeA",

  guarantee_handleGuarantee: "/hip/v1/guarantee/handleGuarantee",

  guarantee_listByType: "/hip/v1/guarantee/listByType",

  guarantee_saveGuarantee: "/hip/v1/guarantee/saveGuarantee",

  handle_deleteHandleObject: "/hip/v1/handle/deleteHandleObject",

  handle_findHandleByUniqueId: "/hip/v1/handle/findHandleByUniqueId",

  handle_getHandle: "/hip/v1/handle/getHandle",

  handle_getHandleA: "/hip/v1/handle/getHandleA",

  handle_handleRquest: "/hip/v1/handle/handleRquest",

  handle_listByType: "/hip/v1/handle/listByType",

  handle_saveHandle: "/hip/v1/handle/saveHandle",

  help_getHelp: "/hip/v1/help/getHelp",

  help_getHelpAll: "/hip/v1/help/getHelpAll",

  help_saveHelp: "/hip/v1/help/saveHelp",
  help_deleteHelp: "/hip/v1/help/deleteHelp",

  homePage_deleteHomepageThr: "/hip/v1/homePage/deleteHomepageThr",

  homePage_getHomepage: "/hip/v1/homePage/getHomepage",

  homePage_getNotice: "/hip/v1/homePage/getNotice",

  homePage_listByType: "/hip/v1/homePage/listByType",

  homePage_saveHomePage: "/hip/v1/homePage/saveHomePage",

  homeService_delete: "/hip/v1/homeService/delete",

  homeService_info: "/hip/v1/homeService/info",

  homeService_page: "/hip/v1/homeService/page",

  homeService_save: "/hip/v1/homeService/save",

  likes_cancel: "/hip/v1/likes/cancel",

  likes_like: "/hip/v1/likes/like",

  redident_appeal: "/hip/v1/redident/appeal",

  redident_delay: "/hip/v1/redident/delay",

  redident_info: "/hip/v1/redident/info",

  redident_page: "/hip/v1/redident/page",

  redident_register: "/hip/v1/redident/register",

  redident_seal: "/hip/v1/redident/seal",

  redident_unseal: "/hip/v1/redident/unseal",

  shanghaiPerson_saveShanghaiPerson:
    "/hip/v1/shanghaiPerson/saveShanghaiPerson",

  staff_delete: "/hip/v1/staff/delete",

  staff_info: "/hip/v1/staff/info",

  staff_login: "/hip/v1/staff/login",

  staff_page: "/hip/v1/staff/page",

  staff_save: "/hip/v1/staff/save",
  homeServiceList: "/hip/v1/dictionary/homeServiceList",
  gethomeServicepage: "/hip/v1/homeService/page",
  saveHomeServiceType: "/hip/v1/dictionary/saveHomeServiceType",
  saveServiceTime: "/hip/v1/homeService/saveServiceTime",

  repair_list: "/hip/v1/guarantee/getGuarantee",

  repair_list_alr: "/hip/v1/guarantee/getGuaranteeA",

  repair_detail: "/hip/v1/guarantee/findGuaranteeByUniqueId",

  come_hu_list: "/hip/v1/shanghaiPerson/getShanghaiPerson",

  come_hu_detail: "/hip/v1/shanghaiPerson/findShanghaiPerson",

  getsomeDatas: "/hip/v1/dictionary/list",

  savecomehuPerson: "/hip/v1/shanghaiPerson/saveShanghaiPerson",

  getYuyuePeople: "/hip/v1/booking/page",
  // 创建投票
  voteTableSave: "/hip/v1/voteTable/save",
  // 修改是否启用投票
  setvoteStatus: "/hip/v1/homePage/voteStatus",
  // 获取公告的投票详情
  voteDetail: "/hip/v1/voteTable/detail",

  
};
export default api;
