import api from './dataCenterUrl'
import { axios } from "../../../../utils/requests.js";
export function getUserBehavior(params) {
    return axios({
        url: api.getUserBehavior,
        params: params
    });
}