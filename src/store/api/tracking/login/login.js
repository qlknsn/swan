import api from "./loginUrl";
import { axios } from "@/utils/requests";
export function getTrackingMenuList() {
  return axios({
    url: api.getTrackingMenuList
  });
}
