import api from "../lqc/lqcUrl";
import { axios } from "@/utils/requests";
// 根据项目编号完结项目
export function finish(params) {
  return axios({
    url: api.finish + `/${params.id}`,
    method: "get"
  });
}
export function roleFind(params) {
  return axios({ url: api.roleFind + `/${params.id}` });
}
export function getPermissionList(params) {
  return axios({ url: api.getPermissionList + `/${params.id}` });
}
export function menuDelete(params) {
  return axios({ url: api.menuDelete + `/${params.id}`, method: "delete" });
}
export function deletePermission(params) {
  return axios({
    url: api.deletePermission + `/${params.id}`,
    method: "delete"
  });
}

export function savePermission(data) {
  return axios({
    url: api.savePermission,
    method: "post",
    data: data
  });
}
export function blendPlanApprovalType() {
  return axios({
    url: api.blendPlanApprovalType
  });
}
export function getAuditStatusList() {
  return axios({
    url: api.getAuditStatusList
  });
}

export function getLqcMenus() {
  return axios({
    url: api.getLqcMenus
  });
}
export function userPermission() {
  return axios({
    url: api.userPermission
  });
}
export function getLQCProjectList(params) {
  return axios({
    url: api.getLQCProjectList,
    method: "post",
    params: params
  });
}
export function getProjectDetail(params) {
  return axios({
    url: api.getProjectDetail + params.id,
    method: "get"
  });
}
export function getProjectPlanList(params) {
  return axios({
    url: api.getProjectPlanList,
    method: "get",
    params: params
  });
}
export function getBlendType() {
  return axios({
    url: api.getBlendType,
    method: "get"
  });
}
export function addBlendPlan(params) {
  return axios({
    url: api.addBlendPlan,
    method: "post",
    data: params
  });
}
export function addProductionPlan(params) {
  return axios({
    url: api.addProductionPlan,
    method: "post",
    data: params
  });
}
export function addLqcProject(params) {
  return axios({
    url: api.addLqcProject,
    method: "post",
    data: params
  });
}
export function lqcLogin(params) {
  return axios({
    url: api.lqcLogin,
    method: "post",
    data: params
  });
}
export function getTorromowPlan(params) {
  return axios({
    url: api.getTorromowPlan,
    method: "get",
    params: params
  });
}
export function addApprove(params) {   //添加审批
  return axios({
    url: api.addApprove,
    method: "post",
    data: params
  });
}
export function deleteLqcPlan(params) {
  return axios({
    url: api.deleteLqcPlan + params.id,
    method: "delete"
  });
}
export function getCheckType() {
  //获取所有审批类型
  return axios({
    url: api.getCheckType,
    method: "get"
  });
}
export function blendPlanApproveList(params) {
  return axios({
    url: api.blendPlanApproveList,
    params: params
  });
}
// 查找模型树结构
export function menuFind(params) {
  return axios({
    url: api.menuFind,
    method: "get",
    params: params
  });
}
// 保存树结构
export function menuSave(params) {
  return axios({
    url: api.menuSave,
    method: "post",
    data: params
  });
}
export function getProjectBlendType(params) {
  return axios({
    url: api.getProjectBlendType + params.id,
    method: "get"
  });
}
// 更新树结构
export function menuUpdate(params) {
  return axios({
    url: api.menuUpdate,
    method: "post",
    data: params
  });
}
// 角色列表
export function roleList(params) {
  return axios({
    url: api.roleList,
    method: "get",
    params: params
  });
}
// 添加角色
export function roleSave(params) {
  return axios({
    url: api.roleSave,
    method: "post",
    data: params
  });
}
// 删除角色
export function roleDelete(params) {
  return axios({
    url: `${api.roleDelete}/${params.id}`,
    method: "delete"
  });
}
export function getlqcPersonList(params) {
  return axios({
    url: api.getlqcPersonList,
    method: "get",
    params: params
  });
}
export function getlqcRoleType(params) {
  return axios({
    url: api.getlqcRoleType,
    method: "get",
    params: params
  });
}
export function addlqcRole(params) {
  return axios({
    url: api.lqcaddRole,
    method: "post",
    data: params
  });
}
export function deletelqcperson(params) {
  return axios({
    url: api.deletelqcperson + params.account,
    method: "delete"
  });
}
export function getlqcRoleList(params) {
  return axios({
    url: api.getlqcRoleList,
    method: "get",
    params: params
  });
}
export function updatelqcPerson(params) {
  return axios({
    url: api.updatelqcPerson,
    method: "post",
    data: params
  });
}
export function getshiliaoAmount(data) {
  return axios({
    url: api.getshiliaoAmount,
    params: data
  });
}
export function updateshiliao(params) {
  return axios({
    url: api.updateshiliao,
    method: "post",
    data: params
  });
}
export function getliqingAmount(data) {
  return axios({
    url: api.getliqingAmount,
    params: data
  });
}
export function updateliqing(params) {
  return axios({
    url: api.updateliqing,
    method: "post",
    data: params
  });
}
