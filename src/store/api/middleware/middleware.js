import api from "./middlewareUrl"
import {axios} from "@/utils/requests"
/**
 * 用户登录
 */
export function middleLogin() {
    return axios({
        method: "get",
        url: api.middleLogin
    });
}
/**
 * 获取组织架构树
 */
export function getOrgs(params){
    return axios({
        url: api.getOrgs,
        params: params
    })
}
/**
 * 获取nvr列表
 */
export function getNvrList(data) {
    return axios({
        method: 'get',
        params: data,
        url: api.nvrList
    })
}
/**
 * 获取相机列表
 */
export function getCameraLists(data) {
    let url = api.cameraList, str = ''
    if (data.tags) {
        if (data.tags.length === 1) {
            for (let i = 0; i<data.tags.length;i++ ) {
                str = `?tags=${data.tags[0]}`
            }
            url = url + str
        } else if (data.tags.length > 1) {
            for (let i = 0; i<data.tags.length;i++ ) {
                if (i===0) {
                    str += `?tags=${data.tags[i]}`
                } else {
                    str += `&tags=${data.tags[i]}`
                }
            }
            url = url + str
        }
    }
    return axios({
        method: 'get',
        params: data.obj,
        url: url
    })
}
/**
 * 获取全部点位标签的列表
 */
export function getTagList(data) {
    return axios({
        params: data,
        url: api.oprateTagList
    })
}
/**
 * 添加标签
 */
export function addTags(data) {
    return axios({
        method: 'post',
        data: data,
        url: api.oprateTagList
    })
}
/**
 * 删除标签deleteTag
 */
export function deleteTags(data) {
    return axios({
        method: 'delete',
        data: data,
        url: api.oprateTagList
    })
}
/**
 * 更新标签
 */
export function uptateTags(data) {
    return axios({
        method: 'put',
        data: data,
        url: api.oprateTagList
    })
}
export function setTagForCamera(data) {
    return axios({
        method: 'post',
        data: data,
        url: api.setTagForCamera
    })
}
/**
 * 获取相机的参数
 */
export function getCamaraParams(data) {
    return axios({
        method: 'get',
        url: api.getCameraParams + '/' + data.urn
    })
}
/**
 * 设置相机的参数
 */
export function setCamaraParams(data) {
    return axios({
        method: 'post',
        data: data,
        url: api.setCameraParams
    })
}
/**
 * 云台控制
 */
export function changeViewDirection(data) {
    return axios({
        method: 'post',
        data: data,
        url: api.changeViewDirection + '/' + data.urn
    })
}
export function getVideoUrl(data) {
    return axios({
        method: 'post',
        data: data.operateActions,
        url: api.getVideoUrl + `?action=${data.action}&protocol=${data.protocol}&streamType=${data.streamType}`
    })
}
/**
 * 录像回放
 */
export function videoReplay(params){
    let pam = {
        start:params.start,
        end:params.end
    }
    return axios({
        method:'post',
        url:api.videoReplay+"/"+params.urn,
        data:{
            timeSegment:pam
        }
    })
}

/**
 * 录像回放
 */
export function checkVideo(params){
    return axios({
        method:'get',
        url:api.checkVideo + "/" + params.urn,
    })
    }

/**
 * 修改相机
 */
export function updateCamera(data) {
    return axios({
        method: 'post',
        data: data,
        url: api.updateCamera
    })
}

export function getAllDomTree(data) {
    return axios({
        params: data,
        method: 'get',
        url: api.getAllDomTree
    })
}
