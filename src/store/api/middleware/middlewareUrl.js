//
const api = {
  // 登录
  middleLogin: "/middleware/menu.json",
  //组织架构树
  getOrgs:"/viis/v1/devices/orgs",
  // nvr列表接口
  nvrList: '/viis/v1/devices/nvr',
  // 相机列表
  cameraList: '/viis/v1/devices/camera',
  // 读取全部点位标签
  oprateTagList: '/viis/v1/devices/tags',
  // 设置标签
  setTagForCamera: '/viis/v1/devices/tagging',
  // 获取相机的预览参数
  getCameraParams: '/viis/v1/streaming/previewParamGet',
  // 设置相机的预览参数
  setCameraParams: '/viis/v1/streaming/previewParamSet',
  // 云台控制
  changeViewDirection: '/viis/v1/streaming/realtime/control',
  // 实时视频流操作请求
  getVideoUrl: '/viis/v1/streaming/realtime/operate',
  //录像回放
  videoReplay:"/viis/v1/streaming/record",
  //检查录像回放是否已经准备好
  checkVideo:'/viis/v1/streaming/record/check',
  // 修改相机接口
  updateCamera: '/viis/v1/devices/modify/camera',
  // 获取全部组织结构
  getAllDomTree: '/viis/v1/devices/allOrgs'

};
export default api;
