const api = {
    //基础信息
    getBasicData:'/viis/v1/report/cameras/summary',
    //按视频调用源进行视频调用分类统计的排行榜接口
    getBySource:'/viis/v1/report/streaming/leadboard/bySource',
    //按视频调用量进行视频调用分类统计查询接口
    getByCamera:'/viis/v1/report/streaming/leadboard/byCamera',
    //按视频应用分类进行视频调用分类统计查询接口
    getByTag:'/viis/v1/report/streaming/search/byTag',
    //故障统计接口
    faultClassify:'/viis/v1/report/maintenance/summary'
}
export default api