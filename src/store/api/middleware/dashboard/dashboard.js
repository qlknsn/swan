
import api from "../dashboard/dashboardUrl"
import {axios} from '@/utils/requests'
//基础信息
export function getBasicData(){
    return axios({
        method:'get',
        url:api.getBasicData,
    })
}

//视频接入分析
export function getBySource(params){
    return axios({
        method:'get',
        url:api.getBySource,
        params:params
    })
}

//按视频调用量进行视频调用分类统计查询接口
export function getByCamera(params){
    return axios({
        method:'get',
        url:api.getByCamera,
        params:params,
    })
}

//故障分类、故障分析
export function faultClassify(params){
    return axios({
        method:'get',
        url:api.faultClassify,
        params:params
    })
}

