const api = {
  getlrMenuList: "/lr/lrguanaiMenu.json",
  getAlarmList: "/lrguanai/v1/backend/backendAlarmResource/getAlarmListCx",
  getDeviceList: "/lrguanai/v1/backend/backendDeviceInfo/getDeviceInfoCx",
  unbindUrl: "/lrguanai/v1/backend/backendDeviceInfo/unbind",
  getLrIndexInfo: "/lrguanai/v1/backend/backendDataCount/getAlarmIndex",
  getLrIndexLine: "/lrguanai/v1/backend/backendDataCount/getDataCount",
  deviceEdit: "/lrguanai/v1/backend/backendDeviceInfo/upDateBackendDeviceBind",
  getDistricName: "/lrguanai/v1/district/getDistrictTree"
};
export default api;
