import api from "./lrguanaiUrl";
import { axios } from "@/utils/requests";
export function getlrMenuList(data) {
  return axios({
    url: api.getlrMenuList,
    params: data
  });
}
export function getAlarmList(data) {
  return axios({
    url: api.getAlarmList,
    params: data
  });
}
export function getlrDeviceList(data) {
  return axios({
    url: api.getDeviceList,
    params: data
  });
}
export function unbind(data) {
  return axios({
    url: api.unbindUrl,
    params: data
  });
}
export function getLrIndexInfo(data) {
  return axios({
    url: api.getLrIndexInfo,
    params: data
  });
}
export function getLrIndexLine(data) {
  return axios({
    url: api.getLrIndexLine,
    params: data
  });
}
export function deviceEdit(data) {
  return axios({
    url: api.deviceEdit,
    data: data,
    method: "post"
  });
}
export function getDistricName(data) {
  return axios({
    url: api.getDistricName,
    data: data,
    method: "post"
  });
}
