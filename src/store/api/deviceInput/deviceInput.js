import api from "./deviceInputUrl";
import { axios } from "@/utils/requests";
export function getDImenuList() {
  return axios({
    url: api.getDImenuList
  });
}
export function getVideoList(data) {
  return axios({
    url: api.getVideoList,
    params: data
  });
}
export function deleteVideo(data) {
  return axios({
    url: api.deleteVideo,
    params: data
  });
}
export function brandList(params) {
  return axios({
    url: api.brandList,
    params: params
  });
}
export function deviceInputlogin(params) {
  return axios({
    url: api.deviceInputlogin,
    method: "post",
    data: params
  });
}
export function brandAdd(params) {
  return axios({
    url: api.brandAdd,
    method: "post",
    data: params
  });
}
export function brandDelete(params) {
  return axios({
    url: api.brandDelete,
    params: params
  });
}
export function brandUpdate(params) {
  return axios({
    url: api.brandUpdate,
    method: "post",
    data: params
  });
}
export function districtAdd(params) {
  return axios({
    url: api.districtAdd,
    method: "post",
    data: params
  });
}
export function districtUpdate(params) {
  return axios({
    url: api.districtUpdate,
    method: "post",
    data: params
  });
}
export function districtList(params) {
  return axios({
    url: api.districtList,
    params: params
  });
}
export function districtDelete(params) {
  return axios({
    url: api.districtDelete,
    params: params
  });
}
export function addVideo(params) {
  return axios({
    url: api.addVideo,
    method: "post",
    data: params
  });
}
export function updateVideo(params) {
  return axios({
    url: api.updateVideo,
    method: "post",
    data: params
  });
}
export function getDeviceCameraList(params) {
  return axios({
    url: api.getCameraList,
    params: params
  });
}
export function updatediCamera(params) {
  return axios({
    method: "post",
    url: api.updatediCamera,
    data: params
  });
}
export function getCameraPic(params) {
  return axios({
    url: api.getCameraPic,
    params: params
  });
}
export function getThirdPlatfromList(params) {
  return axios({
    url: api.getThirdPlatfromList,
    params: params
  });
}
export function addThirdPlatfrom(params) {
  return axios({
    method: "post",
    url: api.addThirdPlatfrom,
    data: params
  });
}
export function deleteThirdPlatfrom(params) {
  return axios({
    method: "delete",
    url: api.deleteThirdPlatfrom,
    params: params
  });
}
export function getTreeList(params) {
  return axios({
    url: api.getTreeList,
    params: params
  });
}
export function getThirdVideoList(params) {
  return axios({
    url: api.getThirdVideoList,
    params: params
  });
}
