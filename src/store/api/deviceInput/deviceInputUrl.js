const api = {
  getDImenuList: "/deviceInput/menu.json", // 获取菜单
  getVideoList: "/api/camera/getByDistrictId", // 获取录像机的列表数据
  deleteVideo: "/api/camera/delete", // 删除摄像机
  addVideo: "/api/camera/addV2", // 添加摄像机
  // updateVideo: "/api/camera/updateV2", // 添加摄像机
  brandList: "/api/brand/list", //品牌列表
  deviceInputlogin: "/api/user/login", //登录
  brandAdd: "/api/brand/add", //添加
  brandDelete: "/api/brand/delete", //删除
  brandUpdate: "/api/brand/update", //修改
  districtAdd: "/api/district/addV2", //添加区域
  districtUpdate: "/api/district/updateV2", //更新区域
  districtList: "/api/district/listV2", //区域列表
  districtDelete: "/api/district/delete", //删除区域
  updateVideo: "/api/camera/updateV2", //编辑录相机
  getCameraList: "/api/cam/getByCameraId", // 摄像头列表
  updatediCamera: "/api/cam/updateV2", // 编辑摄像头
  getCameraPic: "/api/cam/snapshot", // 抓拍照片
  getThirdPlatfromList: "/api/openplatform/list", // 获取第三方平台
  addThirdPlatfrom: "/api/openplatform/add",
  deleteThirdPlatfrom: "/api/openplatform/del",
  getTreeList: "/api/openplatform/device/folder", // domtree
  getThirdVideoList: "/api/openplatform/device/list" // 获取列表
};
export default api;
