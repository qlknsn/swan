const api = {
  loginLg: "/linggang/user/loginV3",
  getDistrict2Tree: "/linggang/backend/account/getDistrict2Tree",
  getLgUserList: "/linggang/backend/account/page",
  addOrUpdateUser: "/linggang/backend/account/save",
  getWorkerList: "/url/town/townUser/page",
  updateWorker: "/url/town/townUser/edit",
  // 获取菜单列表，用户菜单创建，角色管理请求的url 
  getLgAllMenus: "/linggang/backend/function/getAllMenus",
  getLgMenu: "/linggang/backend/function/getMenu",
  delLgMenu: "/linggang/backend/function/delMenu",
  addLgMenu: "/linggang/backend/function/addMenu",
  editLgMenu: "/linggang/backend/function/editMenu",
  getLgRoles: "/linggang/backend/account/getRoles",
  getLgAuthority: "/linggang/backend/role/getAuthority",
  saveAuthority: "/linggang/backend/role/saveAuthority",
  getMinAppMenus: "/linggang/backend/function/getMinAppMenus",
  listForBackend: "/linggang/backend/role/listForBackend",
  // 真正的后台管理请求url
  chooseTree:"/linggang/backend/districtTree",
  getTMenus: "/linggang/backend/account/menus",
  lgGetUserList: "/linggang/backend/account/list",
  lgdistrictTree: "/linggang/backend/districtTreeExcludeSelf",
  lgaddDistrict: "/linggang/backend/district/add",
  lgdeleteDistrict: "/linggang/backend/district/del",
  lgDistrictList: "/linggang/backend/org/list",
  lgAddDepart: "/linggang/backend/org/add",
  lgDropDown: "/linggang/backend/dropdown",
  lgDeleteDepart: "/linggang/backend/org/del",
  lgEditDepart: "/linggang/backend/org/edit",
  lgRoles: "/linggang/backend/role/list",
  lgAddAccount: "/linggang/backend/account/add",
  lgDeleteAccount: "/linggang/backend/account/del",
  lgUpdateAccount: "/linggang/backend/account/edit",
  lgVipList: "/linggang/backend/specialPopulation/list",
  lgAddVip: "/linggang/backend/specialPopulation/add",
  lgUpdateVip: "/linggang/backend/specialPopulation/edit",
  lgDeleteVip: "/linggang/backend/specialPopulation/del",
  lggetDistrictList: "/linggang/backend/queryDistrictByParentId",
  getElevatorList: "/linggang/backend/getElevator", //获取电梯列表
  lgyuzhiList: "/linggang/backend/config/list",
  addlgyuzhiList: "/linggang/backend/config/add",
  eventTypes: "/linggang/event/eventTypes",
  deleteyuzhiList: " /linggang/backend/config/del",
  edityuzhiList: "/linggang/backend/config/edit",
  addElevator: "/linggang/backend/addElevator", //获取电梯列表
  lgConfigList: "/linggang/backend/config/list",
  lgConfigAdd: "/linggang/backend/config/add",
  lgConfigEdit: "/linggang/backend/config/edit",
  lgConfigDelete: "/linggang/backend/config/del",
  lggetFireHouse: "/linggang/fireHouse/getFireHouse",
  lgaddFireHouse: "/linggang/fireHouse/add",
  lgdelFireHouse: "/linggang/fireHouse/delete",
  lgUpdateFireHouse: "/linggang/fireHouse/update",
  lgEditElevator: "/linggang/backend/updateElevator", //编辑电梯
  lgDeleteElevator: "/linggang/backend/deleteElevator", //删除电梯
  lgGetNeigh: "/linggang/backend/getNeigh", //获取房屋列表
  lgAddNeighBaseInfo: "/linggang/backend/addNeighBaseInfo", //添加房屋
  lggetWareHouse: "/linggang/backend/getWarehouse",
  lgupdateWareHouse: "/linggang/backend/updateWarehouse",
  lgaddWareHouse: "/linggang/backend/addWarehouse",
  lgdeleteWareHouse: "/linggang/backend/deleteWarehouse",
  lgUpdateNeighBaseInfo: "/linggang/backend/updateNeighBaseInfo", //编辑房屋
  lgDeleteNeighBaseInfo: "/linggang/backend/deleteNeighBaseInfo", //编辑房屋
  getFireHouse: "/linggang/fireHouse/getFireHouse", //微型消防站
  lgaddDevice: "/linggang/fireHouse/add", //添加微型消防站
  lgdeleteDevice: "/linggang/fireHouse/delete", //删除微型消防站
  lgupdateDevice: "/linggang/fireHouse/update", //更新微型消防站
  lggetTodoList: "/linggang/backend/getNoticePerson",
  lgAddorUpdateTodo: "/linggang/backend/addNoticePerson",
  lgdeleteTodo: "/linggang/backend/deleteNoticePerson",
  lgaddqcDevice: "/linggang/fireHouse/addDevice", //添加微型消防站器材
  lgdeleteqcDevice: "/linggang/fireHouse/deleteDevice", //删除微型消防站器材
  lgGetCameraList: "/linggang/backend/camera/cameraList", //获取视频配置相机列表
  lghikTree: "/linggang/backend/camera/hikTree", //获取海康树
  lghikCameraList: "/linggang/backend/camera/hikCameraList", //获取树结构下面的相机
  lgcopyCameraInfo: "/linggang/backend/camera/copyCameraInfo", //复制到村居树结构
  lgCameraEdit: "/linggang/backend/camera/cameraEdit", //编辑相机
  lgCameraDel: "/linggang/backend/camera/cameraDel", //删除相机
  lgGetTagList: "/linggang/backend/tag/list",//获取标签列表
  lgTagEdit: "/linggang/backend/tag/edit",//标签编辑
  lgTagAdd: "/linggang/backend/tag/add",//添加标签
  lgTagDelete: "/linggang/backend/tag/del",//添加标签
  deleteUserProfile: "/linggang/backend/tag/del",//添加标签
  lggetTootipList: "/linggang/noticeContent/list",//常用语列表
  lgAddTootip: "/linggang/noticeContent/add",//添加常用语
  lgEditTootip: "/linggang/noticeContent/add",//添加常用语
  lgDeleteTootip: "/linggang/noticeContent/delete",//删除常用语
  lgGetUserDetail: "/linggang/user/detail",//删除常用语
  lgAddBatch: "/linggang/backend/account/addBatch",//人员批量导入
  getCameraSnap:"/linggang/camera/cameraSnap", //人脸抓拍列表
  lggetConfig: "/linggang/user/config",//获取加载图片前缀
  saveRenovation: "/linggang/backend/saveRenovation",//添加装修
  getRenovationList: "/linggang/backend/getRenovationList",//装修列表
  deleteRenovation: "/linggang/backend/deleteRenovation",//删除装修
  getRenovation: "/linggang/life/getRenovation",//装修详情
  igGetNBaddress:"/linggang/backend/nbAddress", //老人三件套小区列表
  igaddNbAddress:"/linggang/backend/addNbAddress", //老人三件套小区列表
  igeditNbAddress:"/linggang/backend/editNbAddress", //编辑地址
  igdelNbAddress:"/linggang/backend/delNbAddress", //老人三件套小区列表
  ignbDeviceList:"/linggang/backend/nbDevice", //老人三件套设备列表
  ignbDeviceTypeList:"/linggang/backend/nbProduct", //设备类型列表
  igaddNbDevice:"/linggang/backend/addNbDevice", //添加设备
  igeditNbDevice:"/linggang/backend/editNbDevice", //编辑设备
  igdelNbDevice:"/linggang/backend/delNbDevice", //编辑设备
  moduleDown:"/linggang/backend/config", //编辑设备
  exportUser:"/linggang/backend/account/exportUser"
};
export default api;
