import api from "./lgUrls";
import { axios } from "@/utils/requests";

export function exportUser(params) {
   window.open(api.exportUser + "?type=0&token=" + params.token+"&queryToken="+params.queryToken);
}
export function lggetDistrictList(data) {
  return axios({
    url: api.lggetDistrictList + `?parentId=${data.id}`,
    params: data
  });
}
export function lggetTootipList(data) {
  return axios({
    url: api.lggetTootipList,
    data: data,
    method: "post"
  });
}
export function lgGetUserDetail(data) {
  return axios({
    url: api.lgGetUserDetail,
    // params: data,
    method: "get"
  });
}
export function ignbDeviceList(data) {
  return axios({
    url: api.ignbDeviceList,
    data: data,
    method: "post"
  });
}
export function igdelNbDevice(data) {
  return axios({
    url: api.igdelNbDevice,
    params: data,
    method: "delete"
  });
}
export function igeditNbDevice(data) {
  return axios({
    url: api.igeditNbDevice,
    data: data,
    method: "put"
  });
}
export function igeditNbAddress(data) {
  return axios({
    url: api.igeditNbAddress,
    data: data,
    method: "put"
  });
}
export function ignbDeviceTypeList() {
  return axios({
    url: api.ignbDeviceTypeList,
    // data: data,
    method: "get"
  });
}
export function igaddNbDevice(data) {
  return axios({
    url: api.igaddNbDevice,
    data: data,
    method: "post"
  });
}
export function lgAddTootip(data) {
  return axios({
    url: api.lgAddTootip,
    data: data,
    method: "post"
  });
}
export function lgEditTootip(data) {
  return axios({
    url: api.lgEditTootip,
    data: data,
    method: "post"
  });
}
export function lgDeleteTootip(data) {
  return axios({
    url: api.lgDeleteTootip,
    params: data,
    method: "delete"
  });
}
export function lgdeleteTodo(data) {
  return axios({
    url: api.lgdeleteTodo,
    params: data,
    method: "delete"
  });
}
export function lgTagAdd(data) {
  return axios({
    url: api.lgTagAdd,
    data: data,
    method: "post"
  });
}
export function lgTagDelete(data) {
  return axios({
    url: api.lgTagDelete,
    params: data,
    method: "delete"
  });
}
export function lgTagEdit(data) {
  return axios({
    url: api.lgTagEdit,
    data: data,
    method: "put"
  });
}
export function lgAddorUpdateTodo(data) {
  return axios({
    url: api.lgAddorUpdateTodo,
    data,
    method: "post"
  });
}
export function lggetTodoList(data) {
  return axios({
    url: api.lggetTodoList,
    data,
    method: "post"
  });
}
export function lgupdateWareHouse(data) {
  return axios({
    url: api.lgupdateWareHouse,
    data,
    method: "post"
  });
}
export function lgdeleteWareHouse(data) {
  return axios({
    url: api.lgdeleteWareHouse,
    params: data,
    method: "delete"
  });
}
export function lgaddWareHouse(data) {
  return axios({
    url: api.lgaddWareHouse,
    data,
    method: "post"
  });
}
export function getCameraSnap(data) {   //人脸抓拍列表
  return axios({
    url: api.getCameraSnap,
    params:data,
    method: "get"
  });
}
export function lggetConfig() {
  return axios({
    url: api.lggetConfig,
    method:"get"
  });
}
export function igaddNbAddress(params) {
  return axios({
    url: api.igaddNbAddress,
    method:"post",
    data:params
  });
}
export function igdelNbAddress(params) {
  return axios({
    url: api.igdelNbAddress,
    method:"delete",
    params:params
  });
}
export function igGetNBaddress(params) {
  return axios({
    url: api.igGetNBaddress,
    method:"post",
    data:params
  });
}
export function lggetWareHouse(data) {
  return axios({
    url: api.lggetWareHouse,
    data,
    method: "post"
  });
}
export function lgConfigList(data) {
  return axios({
    url: api.lgConfigList,
    params: data
  });
}
export function lgConfigAdd(data) {
  return axios({
    url: api.lgConfigAdd,
    data,
    method: "post"
  });
}
export function lgAddVip(data) {
  return axios({
    url: api.lgAddVip,
    data,
    method: "post"
  });
}
export function lgUpdateVip(data) {
  return axios({
    url: api.lgUpdateVip,
    data,
    method: "put"
  });
}
export function lgVipList(data) {
  return axios({
    url: api.lgVipList,
    data,
    method: "post"
  });
}
export function lgUpdateAccount(data) {
  return axios({
    url: api.lgUpdateAccount,
    data: data,
    method: "put"
  });
}
export function lgDeleteVip(data) {
  return axios({
    url: api.lgDeleteVip + `?id=${data.id}`,
    data: data,
    method: "delete"
  });
}
export function lgDeleteAccount(data) {
  return axios({
    url: api.lgDeleteAccount + `?id=${data.id}`,
    data: data,
    method: "delete"
  });
}
export function lgAddAccount(data) {
  return axios({
    url: api.lgAddAccount,
    data: data,
    method: "post"
  });
}
export function lgRoles(data) {
  return axios({
    url: api.lgRoles,
    params: data
  });
}
export function lgEditDepart(data) {
  return axios({
    url: api.lgEditDepart,
    method: "put",
    data: data
  });
}
export function lgDeleteDepart(data) {
  return axios({
    url: api.lgDeleteDepart + `?id=${data.id}`,
    method: "delete",
    data: data
  });
}
export function lgDropDown(data) {
  return axios({
    url: api.lgDropDown,
    params: data
  });
}
export function lgAddDepart(data) {
  return axios({
    url: api.lgAddDepart,
    data: data,
    method: "post"
  });
}
export function lgDistrictList(data) {
  return axios({
    url: api.lgDistrictList,
    data: data,
    method: "post"
  });
}
export function lgdeleteDistrict(data) {
  return axios({
    url: api.lgdeleteDistrict + `?id=${data.id}`,
    data: data,
    method: "delete"
  });
}
export function lgaddDistrict(data) {
  return axios({
    url: api.lgaddDistrict,
    data: data,
    method: "post"
  });
}
export function lgdistrictTree(data) {
  return axios({
    url: api.lgdistrictTree,
    methodL:'get',
    params: data
  });
}
export function chooseTree(data) {
  return axios({
    url: api.chooseTree,
    methodL:'get',
    params: data
  });
}
export function lgGetUserList(data) {
  return axios({
    url: api.lgGetUserList,
    data: data,
    method: "post"
  });
}
export function getTMenus(data) {
  return axios({
    url: api.getTMenus,
    params: data
  });
}
export function saveAuthority(data) {
  return axios({
    url: api.saveAuthority,
    data: data,
    method: "post"
  });
}
export function listForBackend(data) {
  return axios({
    url: api.listForBackend,
    data: data,
    method: "post"
  });
}
export function getMinAppMenus(data) {
  return axios({
    url: api.getMinAppMenus + `?roleId=${data.id}`,
    params: data
  });
}
export function getLgAllMenus(data) {
  console.log(data);
  return axios({
    url: api.getLgAllMenus + `?roleId=${data.roleId}`,
    params: data
  });
}
export function getLgMenu(data) {
  return axios({
    url: api.getLgMenu,
    params: data
  });
}
export function delLgMenu(data) {
  return axios({
    url: api.delLgMenu,
    method: "delete",
    params: data
  });
}
export function addLgMenu(data) {
  return axios({
    url: api.addLgMenu,
    method: "post",
    data: data
  });
}
export function editLgMenu(data) {
  return axios({
    url: api.editLgMenu,
    method: "post",
    data: data
  });
}
export function getLgRoles(data) {
  return axios({
    url: api.getLgRoles,
    params: data
  });
}
export function getLgAuthority(data) {
  return axios({
    url: api.getLgAuthority + `?roleId=${data.roleId}`,
    params: data
  });
}

export function loginLg(data) {
  return axios({
    url: api.loginLg,
    data: data,
    method: "post"
  });
}

export function getLgUserList(data) {
  return axios({
    url: api.getLgUserList,
    params: data
  });
}
export function addOrUpdateUser(data) {
  return axios({
    url: api.addOrUpdateUser,
    method: "post",
    data: data
  });
}
export function getWorkerList(data) {
  return axios({
    url: api.getWorkerList,
    params: data
  });
}
export function getDistrict2Tree(data) {
  return axios({
    url: api.getDistrict2Tree,
    params: data
  });
}
export function updateWorker(data) {
  return axios({
    url: api.updateWorker,
    params: data
  });
}
// 任务阈值管理lgadmin
export function lgyuzhiList(data) {
  return axios({
    url: api.lgyuzhiList,
    method: "post",
    data: data
  });
}
// 添加任务阈值管理
export function addlgyuzhiList(data) {
  return axios({
    url: api.addlgyuzhiList,
    method: "post",
    data: data
  });
}
// 事件类型
export function eventTypes(data) {
  return axios({
    url: api.eventTypes,
    method: "get",
    params: data
  });
}
// 删处任务阈值
export function deleteyuzhiList(data) {
  return axios({
    url: api.deleteyuzhiList,
    method: "delete",
    params: data
  });
}
// 更新任务阈值
export function edityuzhiList(data) {
  return axios({
    url: api.edityuzhiList,
    method: "put",
    data: data
  });
}
export function getElevatorList(data) {
  //获取电梯列表
  return axios({
    url: api.getElevatorList,
    data: data,
    method: "post"
  });
}
export function addElevator(data) {
  //添加电梯
  return axios({
    url: api.addElevator,
    data: data,
    method: "post"
  });
}
export function getFireHouse(data) {
  //添加电梯
  return axios({
    url: api.getFireHouse,
    data: data,
    method: "post"
  });
}
export function lgEditElevator(data) {
  //添加电梯
  return axios({
    url: api.lgEditElevator,
    data: data,
    method: "post"
  });
}
export function lgaddDevice(data) {
  //添加消防站
  return axios({
    url: api.lgaddDevice,
    data: data,
    method: "post"
  });
}
export function lgDeleteElevator(data) {
  //添加电梯
  return axios({
    url: api.lgDeleteElevator,
    params: data,
    method: "delete"
  });
}
export function lgGetNeigh(data) {
  //房屋列表
  return axios({
    url: api.lgGetNeigh,
    data: data,
    method: "post"
  });
}
export function lgdeleteDevice(data) {
  //删除消防站
  return axios({
    url: api.lgdeleteDevice,
    params: data,
    method: "delete"
  });
}
export function lgupdateDevice(data) {
  //删除消防站
  return axios({
    url: api.lgupdateDevice,
    data: data,
    method: "post"
  });
}
export function lgDeleteNeighBaseInfo(data) {
  //编辑房屋
  return axios({
    url: api.lgDeleteNeighBaseInfo,
    params: data,
    method: "delete"
  });
}
export function lgGetCameraList(data) {
  //编辑房屋
  return axios({
    url: api.lgGetCameraList,
    data: data,
    method: "post"
  });
}
export function lgaddqcDevice(data) {
  //添加消防器材
  return axios({
    url: api.lgaddqcDevice,
    data: data,
    method: "post"
  });
}
export function lgdeleteqcDevice(data) {
  //删除消防器材
  return axios({
    url: api.lgdeleteqcDevice,
    params: data,
    method: "delete"
  });
}
export function lgCameraEdit(data) {  //编辑相机
  return axios({
    url: api.lgCameraEdit,
    data:data,
    method:"post"})
}
export function lghikTree(data) {  //获取海康树
  return axios({
    url: api.lghikTree,
    params: data,
    method: "get"
  });
}
export function lghikCameraList(data) {
  //获取海康树相机列表
  return axios({
    url: api.lghikCameraList,
    data: data,
    method: "post"
  });
}
export function lgGetTagList(data) {  //获取所有标签类型列表
  return axios({
    url: api.lgGetTagList,
    data:data,
    method:"post"})
}
export function lgcopyCameraInfo(data) {  //复制到村居树结构
  return axios({
    url: api.lgcopyCameraInfo,
    data: data,
    method: "post"
  });
}
export function lgCameraDel(data) {  //复制到村居树结构
  return axios({
    url: api.lgCameraDel,
    params: data,
    method:'delete'
  });
}
export function lgAddNeighBaseInfo(data) {  //复制到村居树结构
  return axios({
    url: api.lgAddNeighBaseInfo,
    data: data,
    method:'post'
  });
}
export function lgUpdateNeighBaseInfo(data) {  //复制到村居树结构
  return axios({
    url: api.lgUpdateNeighBaseInfo,
    data: data,
    method:'post'
  });
}
export function saveRenovation(data) {  //添加装修
  return axios({
    url: api.saveRenovation,
    data: data,
    method:'post'
  });
}
export function getRenovationList(data) {  //装修列表
  return axios({
    url: api.getRenovationList,
    data: data,
    method:'post'
  });
}
export function deleteRenovation(data) {  //删除装修
  return axios({
    url: api.deleteRenovation,
    params: data,
    method:'delete'
  });
}
export function getRenovation(data) {  //装修详情
  return axios({
    url: api.getRenovation,
    params: data,
    method:'get'
  });
}

export function moduleDown(data) {  //装修详情
  return axios({
    url: api.moduleDown,
    params: data,
    method:'get'
  });
}
