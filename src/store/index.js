import Vue from "vue";
import Vuex from "vuex";
import login from "./module/rockfish/login";
import addTimeTask from "./module/rockfish/addTimeTask";
import sfLogin from "./module/starfish/login/sfLogin";
import videoPolling from "./module/rockfish/videoPolling";
import people from "./module/starfish/people/people";
import manageFactor from "./module/starfish/manageFactor/manageFactor";
import yaosuManage from "./module/starfish/manageFactor/yaosuManage";
import common from "./module/starfish/common/common";
import company from "./module/starfish/company/company";
import manageFeature from "./module/starfish/manageFactor/manageFeature";
import systemManage from "./module/starfish/systemManage/systemManage";
import loginGateway from "@/store/module/gateway/login/login";
import ogom from "@/store/module/gateway/ogom/ogom";
import gatewaySystemManage from "@/store/module/gateway/systemManage/systemManage";
import lookup from "@/store/module/gateway/lookup/lookup";
import baseData from "./module/starfish/baseData/house";
import deviceManage from "@/store/module/gateway/deviceManage/deviceManage";
import management from "@/store/module/safeUkey/management/management";
import UkeysystemManage from "@/store/module/safeUkey/systemManage/systemManage";
import initMap from "@/store/module/rockfish/initMap";
import logManage from "@/store/module/safeUkey/logManage/logManage";
import Ukeylogin from "@/store/module/safeUkey/login/login";
import mcpLogin from "@/store/module/mcp/login";
import dashboard from "@/store/module/mcp/dashboard";
import office from "@/store/module/office/office";
import deviceInput from "@/store/module/deviceInput/deviceInput";
import trackingLogin from "@/store/module/tracking/login/login";
import trackingData from "@/store/module/tracking/dataCenter/dataCenter";
import workSheet from "@/store/module/workSheet/workSheet";
// 中間件
import middleware from "./module/middleware/middleware";
import deviceOgom from "@/store/module/middleware/ogom";
import radar from "@/store/module/radar/radar";
import middlewareDashboard from "@/store/module/middleware/dashboard/dashboard";
//AR后台管理
import ar from "@/store/module/ar/ar";

import lrguanai from "@/store/module/lrguanai/lrguanai";
import ronghe from "@/store/module/ronghe/ronghe";
import lgadmin from "@/store/module/lingang/lgAdmin";
import oldStreet from "@/store/module/oldStreet/oldStreet";
import lqc from "@/store/module/lqc/lqc";

//三林城运后台
import slcy from "@/store/module/slcy/slcy";
// 垃圾分类后台管理
import wasteSort from "@/store/module/wasteSort/wasteSort";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    wasteSort,
    initMap,
    login,
    videoPolling,
    people,
    addTimeTask,
    sfLogin,
    manageFactor,
    yaosuManage,
    common,
    company,
    manageFeature,
    systemManage,
    //       gateway
    deviceManage,
    loginGateway,
    ogom,
    gatewaySystemManage,
    lookup,
    baseData,
    management,
    UkeysystemManage,
    logManage,
    Ukeylogin,
    mcpLogin,
    dashboard,
    office,
    deviceInput,
    trackingLogin,
    trackingData,
    workSheet,
    // middleware
    middleware,
    deviceOgom,
    radar,
    middlewareDashboard,
    ar,
    lrguanai,
    slcy,
    ronghe,
    lgadmin,
    oldStreet,
    lqc
  }
});
