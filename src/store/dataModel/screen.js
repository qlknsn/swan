export class Screen {
  constructor(params = {}) {
    Object.assign(
      this,
      {
        urn: {},
        descriptor: {
          screenType: "",
          segments: {}
        }
      },
      params
    );
  }
}
