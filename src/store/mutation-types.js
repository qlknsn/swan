// login.js
export const LOGIN_USER = "LOGIN_USER";
export const USER_INFO = "USER_INFO";
export const MENU_LIST = "MENU_LIST";

// rockfish addTimeTask.js
export const SELECT_VIEW = "SELECT_VIEW"; // 选中的撒点
export const SELECT_VIEW_LIST = "SELECT_VIEW_LIST"; // 选中的撒点列表

// sfLogin.js
export const SF_LOGIN_USER = "SF_LOGIN_USER";
export const VIDEO_POLLING_LIST = "VIDEO_POLLING_LIST";
export const DELETE_VIDEO_POLLING = "DELETE_VIDEO_POLLING";
export const RUN_TASK_NOW = "RUN_TASK_NOW";
//starfish相关

// mcp mutation types
export const SET_MICRO_CHECKIN_POINT_MENU = "SET_MICRO_CHECKIN_POINT_MENU";
export const INIT_MCP_MAP = "INIT_MCP_MAP";
export const ADD_MARKER = "ADD_MARKER";
export const ADD_CLUSTER = "ADD_CLUSTER";
export const CONFIGLIST = "CONFIGLIST";
