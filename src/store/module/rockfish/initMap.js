import {
  INIT_AMAP,
  INIT_AMAP_TYPE,
  DRAW_POLYGON,
  STOP_DRAW_POLYGON,
  SHOW_INFO_WINDOWS,
  INIT_AMAPS
} from "@/store/mutation-types/rockfish-mutations";
import Vue from "vue";
import store from "@/store/index";
import { selectdeviceIcon } from "@/utils/deviceIcon";
import router from "../../../router";
const state = {
  satelliteLayer: "",
  globalAMap: "",
  basicControlZoom: "",
  polygonPathList: [],
  globalMouseTool: "",
  // 添加区域
  mouseDrawTool: null,
  polygonList: [],
  maptype: ""
};
const actions = {};
const mutations = {
  [STOP_DRAW_POLYGON](state, data) {
    data;
    state.globalMouseTool.close(false);
  },
  [DRAW_POLYGON](state, data) {
    data;
    state.globalMouseTool = new AMap.MouseTool(state.globalAMap);
    state.globalMouseTool.polygon({
      strokeColor: "#FF33FF",
      // strokeOpacity: 1,
      strokeWeight: 6,
      strokeOpacity: 0.2,
      fillColor: "#1791fc",
      fillOpacity: 0.4,
      // 线样式还支持 'dashed'
      strokeStyle: "solid"
      // strokeStyle是dashed时有效
      // strokeDasharray: [30,10],
    });
    state.globalMouseTool.on("draw", function (event) {
      // event.obj 为绘制出来的覆盖物对象
      let path = event.obj.getPath();
      state.polygonPathList = path.map((item) => {
        return [item.lng, item.lat];
      });
      console.info("覆盖物对象绘制完成");
      store.commit("SEND_POLYGON", state.polygonPathList);
    });
  },
  [INIT_AMAP_TYPE]() {
    state.maptype = "radar";
  },
  [INIT_AMAP]() {
    state.satelliteLayer = new AMap.TileLayer.Satellite();
    const sysName = Vue.ls.get("sysName");
    var centerPosition = "";
    console.log(sysName);
    switch (sysName) {
      case "ROCKFISH-LXZ":
        centerPosition = [121.575189, 31.090247];
        break;
      case "ROCKFISH-SANLIN":
        centerPosition = [121.511594, 31.143172];
        break;
      case "ROCKFISH-ZHOUJIADU":
        centerPosition = [121.496697, 31.175492];
        break;
      case "ROCKFISH-ZHUQIAO":
        centerPosition = [121.759256, 31.103012];
        break;
      default:
        break;
    }
    AMapUI.loadUI(["control/BasicControl"], function (BasicControl) {
      state.globalAMap = new AMap.Map("manage-map-container", {
        // mapStyle: "amap://styles/a2c5b896c39c2bfc85d98d1e8af8229f", //设置地图的显示样式
        zoom: 10, //级别
        resizeEnable: true,
        // center: [121.569542, 31.114507], //中心点坐标(万达)
        center: centerPosition, //中心点坐标(周家渡)
        // pitch: 75, // 地图俯仰角度，有效范围 0 度- 83 度
        viewMode: "3D" //使用3D视图,
      });
      let markerList = [];
      const markers = store.state.radar.radarListAll;
      // let contentstr = '<div style="background-image: url('@/assets/images/radar/radar.png'); height: 24px; width: 24px; border: 1px solid hsl(180, 100%, 40%); border-radius: 12px; box-shadow: hsl(180, 100%, 50%) 0px 0px 1px;"></div>'
      markers.forEach((item) => {
        if (item.position?.latitude) {
          let content = `<div style="background: url('https://bh-frontend.oss-cn-shanghai.aliyuncs.com/radar/radar') no-repeat; background-size: 100%;height: 28px; width: 22px;"></div>`;
          if (item.versions) {
            if (item.versions.platform === "v0.0.0") {
              content = `<div style="background: url('https://bh-frontend.oss-cn-shanghai.aliyuncs.com/radar/radar') no-repeat; background-size: 100%;height: 28px; width: 22px;"></div>`;
            } else {
              content = `<div style="background: url('https://bh-frontend.oss-cn-shanghai.aliyuncs.com/radar/radarNew.png') no-repeat; background-size: 100%;height: 28px; width: 22px;"></div>`;
            }
          }
          let a = new AMap.Marker({
            position: new AMap.LngLat(
              item.position.longitude,
              item.position.latitude
            ),
            content: content,
            itemData: item,
            icon: selectdeviceIcon(item.properties._sys_detectionType),
            title: item.location
          });

          let contentStr = `
            <div style="display: flex;flex-flow: column;">
              <div style="display: flex;flex-flow: row;margin-top: 10px;">
                <div style="flex: 0.3">
                唯一编号
                </div>
                <div style="flex: 0.7">
                ${item.imei}
                </div>
              </div>
              <div style="display: flex;flex-flow: row;margin-top: 10px;">
                <div style="flex: 0.3">
                状态
                </div>
                <div style="flex: 0.7">
                ${item.statusText}
                </div>
              </div> 
              <div style="display: flex;flex-flow: row;margin-top: 10px;">
                <div style="flex: 0.3">
                安装地点
                </div>
                <div style="flex: 0.7">
                ${item.location}
                </div>
              </div>    
            </div>
            `;
          // <div style="display: flex;flex-flow: row;margin-top: 10px;">
          //   <div style="flex: 0.3">
          //   被创建时间
          //   </div>
          //   <div style="flex: 0.7">
          //   ${item.auditHead.createdOn}
          //   </div>
          // </div>
          let info = document.createElement("div");
          info.className = "custom-info input-card content-window-card";
          info.style.width = "400px";
          let middle = document.createElement("div");
          middle.className = "info-middle";
          middle.style.backgroundColor = "white";
          middle.innerHTML = contentStr;
          info.appendChild(middle);
          AMap.event.addListener(a, "click", function () {
            infoWindow.open(state.globalAMap, a.getPosition());
          });
          let infoWindow = new AMap.InfoWindow({
            isCustom: false, //使用自定义窗体
            content: info,
            offset: new AMap.Pixel(16, -45)
          });
          // a.on('click', () => {
          // router.push({
          //   path: '/radar/detail',
          //   query: {
          //     data: item,
          //     type: 'map'
          //   }
          // })
          // infoWindow.open(map, marker.getPosition());
          // })
          markerList.push(a);
        }
      });

      Vue.prototype.globalAMap = state.globalAMap;
      state.globalAMap.plugin(["AMap.MarkerClusterer"], function () {
        // let isClick = false;
        // if (state.maptype === "radar") {
        //   isClick = true;
        // } else {
        //   isClick = false;
        // }
        var cluster = new AMap.MarkerClusterer(state.globalAMap, markerList, {
          gridSize: 20,
          zoomOnClick: false
        });
        // 点击聚合点的回调
        cluster.on("click", function (e) {
          let lnglatLocalList = e.markers;
          let obj = {
            position: lnglatLocalList[0].Ce.position,
            markers: lnglatLocalList
          };
          if (state.maptype !== "radar") {
            store.commit("SHOW_INFO_WINDOW", obj);
          } else {
            store.commit("SHOW_INFO_WINDOWS", obj);
          }
        });
      });

      setTimeout(() => {
        state.basicControlZoom = new BasicControl.Zoom();
        state.basicControlZoom.addTo(state.globalAMap);
      }, 2000);

      // 室内地图
      // state.globalAMap.on('indoor_create', function () {
      //     state.globalAMap.indoorMap.setMap(state.globalAMap)
      //     state.globalAMap.indoorMap.showIndoorMap('B00155QNWL', 1);
      //     state.globalAMap.indoorMap.show()
      //     state.globalAMap.indoorMap.showFloorBar()
      // })
      // AMapUI.loadUI(['ui/misc/PointSimplifier'], function (PointSimplifier) {
      //     if (!PointSimplifier.supportCanvas) {
      //         alert('当前环境不支持 Canvas！');
      //         return;
      //     }

      //     //启动页面
      //     // store.commit('INIT_PAGE', PointSimplifier);
      // });
    });
  },
  SHOW_INFO_WINDOWS(state, data) {
    // let that = this
    console.log("@@@@@", data);
    const contents = Vue.extend({
      template: `
      <div class='infoBox' :style='limitCss'>
      <ul>
      <li style='font-size: 14px;' v-for="item in markerList">IEMI：<span style='color:#00edfb;'>{{item.w.itemData.imei}};</span>状态：<span style='color:#00edfb;'>{{item.w.itemData.statusText}}</span></li>
      </ul>
      </div>
            `,
      data() {
        return {
          markerList: data.markers,
          limitCss: {
            "max-height": "300px"
          }
        };
      },
      mounted() {},
      filters: {
        getImgUrl: function (val) {
          let d = val.w.itemData;
          console.log(val);
          // return selectdeviceIcon(d.properties._sys_detectionType);
          return selectdeviceIcon("GENERAL");
        }
      },
      methods: {
        handleConnectVideo(item) {
          console.log(item);
          let d = item.w.itemData;
          console.log(d);
          // let path = router.currentRoute.fullPath;
          if (d.type == "videoAggregation") {
            // store.commit('SET_VIDEO_MP4_VISIBLE',true)
            // let index = Math.floor(Math.random()*99)
            // let videoId = state.videoIdlist[index]
            // let params = {
            //   cameraId:videoId,
            //   protocol:'hls',
            //   streamType:0
            // }
            // store.dispatch('connectRandomVideo',params)
            store.commit("CONNECT_RANDOMVIDEO", d);
          } else if (d.type == "lingang") {
            // console.log(d);
            store.commit("GET_LOCATION", d);
            store.commit("CAMERA_LINGANG_RANDOMVIDEO", d);
          } else {
            var currentUrn = `urn:${d.urn.organization}:${d.urn.group}:${d.urn.id}`;
            // store.dispatch("connectVideo", [currentUrn]);
            store.dispatch("newConnectVideo", [currentUrn]);
          }
        }
      },
      computed: {
        url: function () {
          return "http://baidu.com";
        }
      }
    });
    const con = new contents().$mount();
    // const contents = `
    //     <div>
    //         ${data.markers.map(item=>`
    //             <div onclick="${openvideo()}">
    //                 <img src="${item.Ce.icon.w.image}">
    //             </div>
    //         `).join(' ')}
    //     </div>
    // `
    var infoWindow = new AMap.InfoWindow({
      // isCustom: true,
      content: con.$el,
      position: data.position,
      offset: new AMap.Pixel(0, -20)
    });
    infoWindow.open(state.globalAMap, data.markers[0].getPosition());
  },
  MOUSE_DRAW_POLYGON() {
    state.mouseDrawTool = new AMap.MouseTool(Vue.prototype.globalAMap);
    state.mouseDrawTool.polygon({
      strokeColor: "#aca8f2",
      fillColor: "#9bcbf2",
      fillOpacity: 0.5
    });
    // state.polygonList = []

    let drawlineItem = document.getElementsByClassName("drawlineItem");
    if (drawlineItem[0].className.includes("pointclick")) {
      state.mouseDrawTool.close();
    } else {
      state.mouseDrawTool.on("draw", function (e) {
        let lnglat = [];
        e.obj.w.path.forEach((item) => {
          lnglat.push([item.lng, item.lat]);
        });
        console.log(lnglat);
        state.polygonList = lnglat;
        console.log(state.polygonList);
        state.mouseDrawTool.close();
        let map = document.getElementById("manage-map-container");
        map.classList.remove("crosshair");
      });
    }
    let map = document.getElementById("manage-map-container");
    map.classList.add("crosshair");
    drawlineItem[0].classList.add("pointclick");
  },
  MOUSE_DRAW_POLYGON_CYCLE(state) {
    state.mouseDrawTool = new AMap.MouseTool(Vue.prototype.globalAMap);
    let polygons = new AMap.Polygon({
      map: Vue.prototype.globalAMap,
      strokeColor: "#aca8f2",
      fillColor: "#9bcbf2",
      fillOpacity: 0.5
    });
    state.mouseDrawTool.polygon({
      strokeColor: "#aca8f2",
      fillColor: "#9bcbf2",
      fillOpacity: 0.5
    });
    let cycleLine = [];
    let cycleolygon = [];
    let pLine = "";
    // if(drawlineItem[0].className.includes('pointclick')){
    //   state.mouseDrawTool.close()
    // }else{
    console.log(state.mouseDrawTool);
    state.mouseDrawTool.on("draw", function (e) {
      // console.log(e.obj.w.path)
      let lnglat = [];
      e.obj.w.path.forEach((item) => {
        lnglat.push([item.lng, item.lat]);
      });
      cycleLine.push(e.obj.w.path);
      pLine += `${JSON.stringify(lnglat)},`;
      cycleolygon.push(e.obj);
      if (cycleLine.length == 2) {
        // console.log(cycleLine)
        let reg = /,$/gi;
        pLine = pLine.replace(reg, "");
        console.log(`[${pLine}]`);
        state.polygonList = JSON.parse(`[${pLine}]`);
        polygons.setPath(cycleLine);
        Vue.prototype.globalAMap.setFitView();

        state.mouseDrawTool.close();

        Vue.prototype.globalAMap.remove(cycleolygon);
        cycleolygon = [];
        let map = document.getElementById("manage-map-container");
        map.classList.remove("crosshair");
      }
    });
    // }
    let map = document.getElementById("manage-map-container");
    map.classList.add("crosshair");
    let drawlineItemCycle =
      document.getElementsByClassName("drawlineItemCycle");
    drawlineItemCycle[0].classList.add("pointclick");
  },
  CLEAR_PLOYGON(state) {
    // console.log(type)
    Vue.prototype.globalAMap.remove(state.polygonList);
    Vue.prototype.globalAMap.clearMap();
    state.polygonList = [];
    if (state.mouseDrawTool) {
      state.mouseDrawTool.close();
    }

    let map = document.getElementById("manage-map-container");
    map.classList.remove("crosshair");

    let drawlineItem = document.getElementsByClassName("drawlineItem");
    drawlineItem[0].classList.remove("pointclick");

    let drawlineItemCycle =
      document.getElementsByClassName("drawlineItemCycle");
    drawlineItemCycle[0].classList.remove("pointclick");
  },
  DRAW_HUAN() {
    var polygon = new AMap.Polygon({
      path: Vue.ls.get("polygonList"),
      // map: Vue.prototype.globalAMap,
      strokeColor: "#aca8f2",
      fillColor: "#9bcbf2",
      fillOpacity: 0.5,
      zIndex: 50
    });
    console.log(Vue.ls.get("polygonList"));
    setTimeout(() => {
      console.log(Vue.prototype.globalAMap);
      Vue.prototype.globalAMap.add(polygon);
      Vue.prototype.globalAMap.setFitView([polygon]);
    }, 100);

    // polygon.setPath(Vue.ls.get('polygonList'));
  }
};
export default {
  state,
  actions,
  mutations
};
