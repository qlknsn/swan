import {
  login,
  getRFmenuList,
  getRFmenuListZhuqiao,
  loginUser,
  getSanlinRFmenuList,
  getusers,
  getroles,
  addsanlinUser,
  userlogin,
  setsanlinRole,
  getAdminmenuList
} from "../../api/rockfish/login";
import {
  GW_MENU_LIST,
  GW_USER_INFO,
  GET_RW_MENULIST,
  GET_SANLIN_RF_MENULIST
} from "../../mutation-types/rockfish-mutations";
import routes from "../../../router/index";
import store from "@/store/index";
import Vue from "vue";
import { Message } from "element-ui";
const state = {
  userInfo: {},
  menuList: [],
  userList: [],//用户列表
  roleList: [],//角色列表
};
const actions = {
  setsanlinRole({ commit }, data) {
    commit;
    let p = setsanlinRole(data);
    return p;
  },
  userlogin({ commit }, data) {
    commit;
    let p = userlogin(data);
    p.then(res => {
      if (res.error) {
        Message({
          type: 'warning',
          message: res.error.message
        })
      } else {
        if (res.result.role[0]?.code == 'BACKEND') {
          Vue.ls.set('sanlintoken', res.result.token)
          store.dispatch("getSanlinRFmenuList")
          setTimeout(() => {
            routes.push("/videoPollingList");
          }, 1000);
          
        } else if (res.result.role[0]?.code == 'ADMIN') {
          Vue.ls.set('sanlintoken', res.result.token)
          store.dispatch("getAdminmenuList")
          setTimeout(() => {
            routes.push("/sanlin/user/userManage");
          }, 1000);
          
        } else{
          Message({
            type: 'warning',
            message: '该用户没有后台管理权限'
          })
        }
      }


    });
  },
  addsanlinUser({ commit }, data) {
    commit;
    let p = addsanlinUser(data);
    return p;
  },
  getroles({ commit }, data) {
    let p = getroles(data);
    p.then(res => {
      commit("GET_ROLES", res);
    });
  },
  getusers({ commit }, data) {
    let p = getusers(data);
    p.then(res => {
      commit("GET_USRES", res);
    });
  },
  getSanlinRFmenuList({ commit }, data) {
    let p = getSanlinRFmenuList(data);
    p.then(res => {
      commit("GET_SANLIN_RF_MENULIST", res);
    });
  },
  getAdminmenuList({ commit }, data) {
    let p = getAdminmenuList(data);
    p.then(res => {
      commit("GET_SANLIN_RF_MENULIST", res);
    });
  },
  getRFmenuList({ commit }, data) {
    let p = getRFmenuList(data);
    p.then(res => {
      commit("GET_RW_MENULIST", res);
    });
  },
  getRFmenuListZhuqiao({ commit }, data) {
    let p = getRFmenuListZhuqiao(data);
    p.then(res => {
      commit("GET_RW_MENULIST", res);
    });
  },
  loginCompute({ commit }, params) {
    let p = login(params);
    p.then(res => {
      commit(GW_USER_INFO, res);
      // commit(GW_MENU_LIST, res.menus);
      // Vue.ls.set('menuList', res.menus)
      routes.push("/videoPollingList");
    });
  },
  loginUser({ commit }, params) {
    commit;
    let p = loginUser(params);
    p.then(res => {
      res;
      Vue.ls.set("userName", params.username);
    });
  }
};

const mutations = {
  GET_USRES(...arg) {
    let [state, { result }] = arg
    state.userList = result
  },
  GET_ROLES(...arg) {
    let [state, { result }] = arg
    state.roleList = result
  },
  [GET_SANLIN_RF_MENULIST](...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
  },
  [GET_RW_MENULIST](...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
  },
  [GW_USER_INFO](state, res) {
    state.userInfo = res;
  },
  [GW_MENU_LIST](state, menuList) {
    state.menuList = menuList;
  }
};

export default {
  state,
  actions,
  mutations
};
