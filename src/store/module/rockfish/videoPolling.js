import { selectdeviceIcon } from "@/utils/deviceIcon";
import videoContainer from "@/components/public/videoContainer.vue";
import hlsVideoContainer from "@/components/public/hlsVideoContainer.vue";

import {
  exportNoise,
  saveNoise,
  getNoiseList,
  getDevices,
  getPrimaryDepart,
  pageBackend,
  getListDept,
  addDept,
  getPerson,
  delDept,
  getPersonList,
  addPerson,
  delPerson,
  configList,
  markInactive,
  handleParking,
  parkingViolationList,
  deleteScreen,
  getScreens,
  createScreen,
  getVideoPollingList,
  deleteVideoPolling,
  runTaskNow,
  getCameraList,
  exportsCameralist,
  setDevicesTag,
  readDevicesTags,
  addDevicesTags,
  deleteDevicesTags,
  connectVideo,
  disconnectVideo,
  listRecord,
  udpateState,
  vehicleRecordListRecord,
  vehicleRecordUpdateState,
  getRockfishSelectOptions,
  getTrashClassifyList,
  getSheetSatisfy,
  getSheetResolved,
  getSheetContact,
  populationList,
  populationEdit,
  resolvedExport,
  satisfyExport,
  contactExport,
  getNoiseDetectionList,
  addNoiseDetectionDevice,
  deleteNoiseDetectionDevice,
  editNoiseDetectionDevice,
  garbageChecklist, //  垃圾分类检查情况列表
  garbageCheckedit, //  垃圾分类检查情况修改
  garbageRecycledList, //  可回收服务点列表
  garbageRecycledEdit, //  可回收服务点列表
  staticConfigList, //垃圾统计静态数据
  staticConfigEdit, //垃圾统计静态修改
  pieStatisticList, //饼图统计列表
  pieStatisticAdd, //饼图统计添加
  pieStatisticEdit, //饼图统计编辑
  pieStatisticDelete,
  addXiaoqu,
  updateXiaoqu,
  searchXiaoqu,
  deleteXiaoqu
} from "../../api/rockfish/videoPolling";
import {
  CONFIGLIST,
  MARK_INACTIVE,
  SHOW_PLAY_VIDEO_WINDOW,
  HANDLE_PARKING,
  GET_ROCKFISH_SELEC_OPTONS,
  GET_CAMERA_LIST_PAGINATION,
  SET_SELECTED_CAMERA_COUNT,
  PARKING_VIOLATION_LIST,
  DELETE_SCREEN,
  GET_SCREENS,
  CLEAR_SELECT_POINT_LIST,
  GET_CAMERA_LIST,
  GET_DEVICE_TOTAL,
  SET_RETURN_CAMERA_LIST,
  VIDEO_POLLING_LIST,
  DELETE_VIDEO_POLLING,
  SHOW_INFO_WINDOW,
  RUN_TASK_NOW,
  CREATE_SCREEN,
  CONNECT_VIDEO,
  CLEAR_MARKERLIST,
  SEND_POLYGON,
  SHOW_PARKING_VIOLATION_POPUP,
  GET_TRASH_CLASSIFY_LIST,
  GET_SHEET_SATISFY,
  GET_SHEET_RESOLVED,
  GET_SHEET_CONTACT,
  CLEAR_MARKER_LIST_ON_MAP
} from "../../mutation-types/rockfish-mutations";
import store from "@/store/index";
import { Message } from "element-ui";
import Vue from "vue";

const state = {
  noiseTotal: null,
  noiseResult: "",
  noiseList: [],
  devices: [],
  personTotal: null,
  persons: [],
  personList: [],
  task: [],
  total: 0,
  departList: [],
  secondryList: [],

  selectList: [],
  imageHost: "",
  sheetSatisfyList: [],
  sheetSatisfyParams: { current: 1, size: 10, total: 0 },
  sheetResolvedList: [],
  sheetResolvedParams: { current: 1, size: 10, total: 0 },
  sheetContactList: [],
  sheetContactParams: { current: 1, size: 10, total: 0 },
  trashClassifyParams: { current: 1, size: 10, total: 0 },
  trashClassifyList: [],
  parkingViolationVisible: false,
  selectOptions: [],
  cameraListPagination: { status: "", count: 10, start: 0, total: 0 },
  wholeCameraList: [],
  parkingViolationList: [],
  parkingViolationParams: {
    size: "10",
    current: 1,
    endTime: "",
    licensePlate: "",
    startTime: "",
    state: "",
    total: 0
  },
  returnCameraList: [],
  selectedCameraCount: 0,
  videoRtsp: [{ metadata: { url: "" } }],
  cameraList: [],
  screenList: [
    { urn: { id: "" }, descriptor: { properties: { screenName: "" } } }
  ],
  selectPointList: [],
  selectMarkerPointList: [],
  markerList: [],
  returnMarkerList: [],
  pollingListParams: {
    start: 0,
    count: 10,
    total: 0,
    page: 1,
    name: ""
  },
  videoPollingList: [],
  deleteResult: {},
  deviceListPagination: {
    start: 0,
    count: 10000,
    page: 0,
    total: 0
  },
  listrecord: [],
  lllegalstopTotal: 0,
  listrecordVeh: [],
  zhatumanageTotal: 0,
  devicesTagsList: [],
  populationLit: [],
  populationParams: { current: 1, size: 10, total: 0 },
  exportcameralist: [],
  noiseDectectionList: {
    pagination: {
      total: 0
    }
  },
  addNoiseDeviceTag: false,
  garbagecheckList: { results: [], pagination: {} },
  garbagerecyclelist: { results: [], pagination: {} },
  garbageStatic: { results: [] },
  pieStatisticList: {
    pagination: {
      total: 0
    }
  },
  xiaoquList: [],
  searchXiaoquPagination: { total: 0, start: 0, count: 10 }
};
const actions = {
  exportNoise({ commit }, params) {
    commit;
    exportNoise(params);
  },
  saveNoise({ commit }, data) {
    let p = saveNoise(data);
    p.then((res) => {
      commit("SAVENOISE", res);
    });
  },
  getNoiseList({ commit }, data) {
    let p = getNoiseList(data);
    p.then((res) => {
      commit("GETNOISELIST", res);
    });
  },
  getDevices({ commit }, data) {
    let p = getDevices(data);
    p.then((res) => {
      commit("GETDEVICES", res);
    });
  },
  delPerson({ commit }, data) {
    let p = delPerson(data);
    p.then((res) => {
      commit("DELPERSON", res);
    });
  },
  addPerson({ commit }, data) {
    let p = addPerson(data);
    p.then((res) => {
      commit("ADDPERSON", res);
    });
  },
  getPersonList({ commit }, data) {
    let p = getPersonList(data);
    p.then((res) => {
      commit("GETPERSONLIST", res);
    });
  },
  delDept({ commit }, data) {
    let p = delDept(data);
    p.then((res) => {
      commit("DELDEPT", res);
    });
  },
  getPerson({ commit }, data) {
    let p = getPerson(data);
    p.then((res) => {
      commit("GETPERSON", res);
    });
  },
  addDept({ commit }, data) {
    let p = addDept(data);
    p.then((res) => {
      commit("ADDDEPT", res);
    });
  },
  getListDept({ commit }, data) {
    let p = getListDept(data);
    p.then((res) => {
      commit("GETLISTDEPT", res);
    });
  },
  pageBackend({ commit }, data) {
    let p = pageBackend(data);
    p.then((res) => {
      commit("GETLIST", res);
    });
  },
  getPrimaryDepart({ commit }, data) {
    let p = getPrimaryDepart(data);
    p.then((res) => {
      commit("GETPRIMARYDEPART", res);
    });
  },

  deleteXiaoqu({ commit }, data) {
    let p = deleteXiaoqu(data);
    p.then((res) => {
      commit("DELETE_XIAOQU", res);
    });
  },
  updateXiaoqu({ commit }, data) {
    let p = updateXiaoqu(data);
    p.then((res) => {
      commit("UPDATE_XIAOQU", res);
    });
  },
  searchXiaoqu({ commit }, data) {
    let p = searchXiaoqu(data);
    p.then((res) => {
      commit("SEARCH_XIAOQU", res);
    });
  },
  addXiaoqu({ commit }, data) {
    let p = addXiaoqu(data);
    p.then((res) => {
      commit("ADD_XIAOQU", res);
    });
  },
  // 垃圾统计数据修改
  staticConfigEdit({ commit }, params) {
    commit;
    params;
    let p = staticConfigEdit(params);
    p.then((res) => {
      if (res.error) {
        Message({
          type: "warning",
          message: "修改失败"
        });
      } else {
        Message({
          type: "success",
          message: "修改成功"
        });
      }
    });
  },
  // 垃圾统计静态数据
  staticConfigList({ commit }, params) {
    commit;
    params;
    let p = staticConfigList(params);
    p.then((res) => {
      commit("GET_STATIC_CONFIG_LIST", res);
    });
  },
  //  垃圾分类检查情况列表修改
  garbageCheckedit({ commit }, params) {
    commit;
    params;
    let p = garbageCheckedit(params);
    p.then((res) => {
      if (res.error) {
        Message({
          type: "warning",
          message: "修改失败"
        });
      } else {
        Message({
          type: "success",
          message: "修改成功"
        });
      }
    });
  },
  //  可回收物列表修改
  garbageRecycledEdit({ commit }, params) {
    commit;
    params;
    let p = garbageRecycledEdit(params);
    p.then((res) => {
      if (res.error) {
        Message({
          type: "warning",
          message: "修改失败"
        });
      } else {
        Message({
          type: "success",
          message: "修改成功"
        });
      }
    });
  },
  //  可回收服务点列表
  garbageRecycledList({ commit }, params) {
    commit;
    params;
    let p = garbageRecycledList(params);
    p.then((res) => {
      commit("GET_GARBAGE_RECYCLE_LIST", res);
    });
  },
  //  垃圾分类检查情况列表
  garbageChecklist({ commit }, params) {
    commit;
    params;
    let p = garbageChecklist(params);
    p.then((res) => {
      commit("GET_GARBAGE_CHECK_LIST", res);
    });
  },
  // 解决率导出列表
  resolvedExport({ commit }, params) {
    commit;
    resolvedExport(params);
    // p.then(res=>{
    //   commit('RESOLVED_EXPORT',res)
    // })
  },
  // 满意率导出列表
  satisfyExport({ commit }, params) {
    commit;
    satisfyExport(params);
    // p.then(res=>{
    //   commit('SATISFY_EXPORT',res)
    // })
  },
  // 联系率导出列表
  contactExport({ commit }, params) {
    commit;
    contactExport(params);
    // p.then(res=>{
    //   commit('CONTACT_EXPORT',res)
    // })
  },
  // 雷达信息列表
  populationList({ commit }, params) {
    let p = populationList(params);
    p.then((res) => {
      commit("POPULATION_LIST", res);
    });
  },
  populationEdit({ commit }, params) {
    let p = populationEdit(params);
    p.then((res) => {
      commit;
      if (res.error == null) {
        Message({
          type: "success",
          message: "修改成功"
        });
      } else {
        Message({
          type: "warning",
          message: res.error.message
        });
      }
    });
  },
  configList({ commit }, params) {
    let p = configList(params);
    p.then((res) => {
      commit("CONFIGLIST", res);
    });
  },
  markInactive({ commit }, data) {
    let p = markInactive(data);
    p.then((res) => {
      commit("MARK_INACTIVE", res);
    });
  },
  getSheetSatisfy({ commit }, data) {
    let p = getSheetSatisfy(data);
    p.then((res) => {
      commit("GET_SHEET_SATISFY", res);
    });
  },
  getSheetResolved({ commit }, data) {
    let p = getSheetResolved(data);
    p.then((res) => {
      commit("GET_SHEET_RESOLVED", res);
    });
  },
  getSheetContact({ commit }, data) {
    let p = getSheetContact(data);
    p.then((res) => {
      commit("GET_SHEET_CONTACT", res);
    });
  },
  getTrashClassifyList({ commit }, data) {
    let p = getTrashClassifyList(data);
    p.then((res) => {
      commit("GET_TRASH_CLASSIFY_LIST", res);
    });
  },
  handleParking({ commit }, data) {
    let p = handleParking(data);
    p.then((res) => {
      commit("HANDLE_PARKING", res);
    });
  },
  getRockfishSelectOptions({ commit }, data) {
    let p = getRockfishSelectOptions(data);
    p.then((res) => {
      commit("GET_ROCKFISH_SELEC_OPTONS", res);
    });
  },
  parkingViolationList({ commit }, data) {
    let p = parkingViolationList(data);
    p.then((res) => {
      commit("PARKING_VIOLATION_LIST", res);
    });
  },
  connectVideo({ commit }, params) {
    let p = connectVideo(params);
    p.then((res) => {
      commit(CONNECT_VIDEO, res);
    });
  },
  disconnectVideo({ commit }, params) {
    commit;
    let p = disconnectVideo(params);
    p.then((res) => {
      res;
    });
  },
  deleteScreen({ commit }, data) {
    let p = deleteScreen(data);
    p.then((res) => {
      commit("DELETE_SCREEN", res);
    });
  },
  getScreens({ commit }, data) {
    let p = getScreens(data);
    p.then((res) => {
      commit("GET_SCREENS", res);
    });
  },
  createScreen({ commit }, data) {
    let p = createScreen(data);
    p.then((res) => {
      commit("CREATE_SCREEN", res);
    });
  },
  getCameraList({ commit }, data) {
    let p = getCameraList(data);
    p.then((res) => {
      commit(GET_CAMERA_LIST, res);
    });
  },
  // 导出列表
  exportsCameralist({ commit }, params) {
    let p = exportsCameralist(params);
    p.then((res) => {
      commit;
      commit("EXPORTS_CAMERALIST", res.results);
    });
  },
  // 设备设定标签
  setDevicesTag({ commit }, data) {
    let p = setDevicesTag(data);
    p.then((res) => {
      res;
      commit;
    });
  },
  // 读取标签列表
  readDevicesTags({ commit }, data) {
    let p = readDevicesTags(data);
    p.then((res) => {
      commit("READ_DEVICES_TAGS", res);
    });
  },
  // 添加标签
  addDevicesTags({ commit }, data) {
    let p = addDevicesTags(data);
    p.then((res) => {
      store.dispatch("readDevicesTags");
      res;
      commit;
    });
  },
  // 删除标签
  deleteDevicesTags({ commit }, data) {
    let p = deleteDevicesTags(data);
    p.then((res) => {
      store.dispatch("readDevicesTags");
      res;
      commit;
    });
  },
  getCameraListPagination({ commit }, data) {
    let p = getCameraList(data);
    p.then((res) => {
      commit("GET_CAMERA_LIST_PAGINATION", res);
      if (data.exportFileType) {
        Message({
          type: "success",
          message: "已经导出到队列，请到队列页面下载"
        });
      }
    });
  },
  getVideoPollingList({ commit }, params) {
    //获取视频轮屏列表
    let p = getVideoPollingList(params);
    p.then((res) => {
      commit(VIDEO_POLLING_LIST, res);
    });
  },

  deleteVideoPolling({ commit }, params) {
    //删除某个轮屏任务
    let p = deleteVideoPolling(params);
    p.then((res) => {
      commit(DELETE_VIDEO_POLLING, res);
    });
  },
  runTaskNow({ commit }, params) {
    //立即执行某个轮屏任务
    let p = runTaskNow(params);
    p.then((res) => {
      if (res.status == 400) {
        Message({
          type: "warning",
          message: "当前任务不在任务执行时间范围内"
        });
      } else {
        commit(RUN_TASK_NOW, res);
      }
    });
  },

  udpateState({ commit }, params) {
    commit;
    //更新违停状态
    let p = udpateState(params);
    p.then((res) => {
      if (res.errcode == 0) {
        store.dispatch("listRecord", { pageNo: 1, pageSize: 10 });
      } else {
        Message({
          type: "warning",
          message: "更新失败"
        });
      }
    });
  },
  vehicleRecordUpdateState({ commit }, params) {
    commit;
    //更新渣土状态
    let p = vehicleRecordUpdateState(params);
    p.then((res) => {
      if (res.errcode == 0) {
        let forms = new FormData();
        forms.append("pageNo", 1);
        forms.append("recordType", "-1");
        forms.append("eventType", "-1");
        forms.append("carNumber", "");
        forms.append("startTime", "");
        forms.append("endTime", "");
        store.dispatch("vehicleRecordListRecord", forms);
      } else {
        Message({
          type: "warning",
          message: "更新失败"
        });
      }
    });
  },
  listRecord({ commit }, params) {
    //c车辆违停列表
    commit;
    let p = listRecord(params);
    p.then((res) => {
      res.data.list.forEach((item) => {
        switch (item.state) {
          case 0:
            item.stateType = "待确认";
            break;
          case 1:
            item.stateType = "违章";
            break;
          case 2:
            item.stateType = "没有违章";
            break;
          case 3:
            item.stateType = "待定";
            break;
        }
      });
      commit("LIST_RECORD", res);
    });
  },
  vehicleRecordListRecord({ commit }, params) {
    //渣土车列表
    commit;
    let p = vehicleRecordListRecord(params);
    p.then((res) => {
      res.data.list.forEach((item) => {
        switch (item.eventType) {
          case 0:
            item.stateType = "待确认";
            break;
          case 1:
            item.stateType = "违章";
            break;
          case 2:
            item.stateType = "没有违章";
            break;
          case 3:
            item.stateType = "待定";
            break;
        }
      });
      commit("LIST_RECORD_VEH", res);
    });
  },

  getNoiseDetectionList({ commit }, params) {
    //获取噪音检测设备列表
    let p = getNoiseDetectionList(params);
    p.then((res) => {
      commit("NOISE_DETECTION_LIST", res);
    });
  },

  addNoiseDetectionDevice({ commit }, params) {
    commit;
    let p = addNoiseDetectionDevice(params);
    return p;
    // p.then(res =>{
    //     if(res.error == null){
    //         Message.success('添加成功')
    //         commit("ADD_NOISE_DEVICE",true);
    //     }else{
    //         Message.warning('添加失败');
    //         commit("ADD_NOISE_DEVICE",false);

    //     }
    // })
  },
  deleteNoiseDetectionDevice({ commit }, params) {
    commit;
    let p = deleteNoiseDetectionDevice(params);
    return p;
  },

  editNoiseDetectionDevice({ commit }, params) {
    commit;
    let p = editNoiseDetectionDevice(params);
    return p;
  },
  pieStatisticList({ commit }, params) {
    let p = pieStatisticList(params);
    p.then((res) => {
      commit("PIE_STATISTIC_LIST", res);
    });
  },
  pieStatisticAdd({ commit }, params) {
    let p = pieStatisticAdd(params);
    return p;
  },
  pieStatisticEdit({ commit }, params) {
    let p = pieStatisticEdit(params);
    return p;
  },
  pieStatisticDelete({ commit }, params) {
    let p = pieStatisticDelete(params);
    return p;
  }
};

const mutations = {
  EXPORTNOISE({ commit }, params) {
    commit;
    EXPORTNOISE(params);
  },
  SAVENOISE(state, res) {
    state.noiseResult = res.result;
  },
  GETNOISELIST(state, res) {
    state.noiseList = res;
    state.noiseTotal = res.pagination.total;
  },
  GETDEVICES(state, res) {
    let arr = [{ address: "全部", deviceUrn: "1" }];
    state.devices = arr.concat(res.result);
  },
  DELPERSON(state, res) {
    if (res.result) {
      Message({
        message: "删除成功",
        type: "success"
      });
    } else {
      Message({
        message: "删除失败",
        type: "error"
      });
    }
  },
  ADDPERSON(state, res) {
    if (res.result) {
      Message({
        message: "操作成功",
        type: "success"
      });
    } else {
      Message({
        message: "操作失败",
        type: "error"
      });
    }
  },
  GETPERSONLIST(state, res) {
    state.personTotal = res.pagination.total;
    state.persons = res.results;
  },
  DELDEPT(state, res) {
    if (res.result) {
      Message({
        message: "删除成功",
        type: "success"
      });
    }
  },
  GETPERSON(state, res) {
    state.personList = res.results;
  },
  ADDDEPT(state, res) {
    state.task = res.result;
    if (res.result) {
      Message({
        message: "操作成功",
        type: "success"
      });
    } else {
      Message({
        message: "操作失败",
        type: "error"
      });
    }
  },

  GETLISTDEPT(state, res) {
    state.task = res.result;
  },
  GETLIST(state, res) {
    let lists = res.results;
    let deptList = lists.map((item) => {
      if (item.status == "0") {
        item.status = "是";
      } else if (item.status == "1") {
        item.status = "否";
      }
    });
    state.departList = lists;
    state.total = res.pagination.total;
  },
  GETPRIMARYDEPART(state, res) {
    state.secondryList = res.result;
  },
  DELETE_XIAOQU(...arg) {
    let [, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ type: "success", message: "删除成功" });
      store.dispatch("searchXiaoqu", { start: 0, count: 10 });
    } else {
      Vue.prototype.$message({ type: "error", message: "删除失败" });
    }
  },
  UPDATE_XIAOQU(...arg) {
    let [, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ type: "success", message: "更新成功" });
      store.dispatch("searchXiaoqu", { start: 0, count: 10 });
    } else {
      Vue.prototype.$message({ type: "error", message: "更新失败" });
    }
  },
  SEARCH_XIAOQU(...arg) {
    let [state, { pagination, results }] = [...arg];
    state.xiaoquList = results;
    state.searchXiaoquPagination = pagination;
  },
  ADD_XIAOQU(...arg) {
    let [, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ type: "success", message: "添加成功" });
      store.dispatch("searchXiaoqu", { start: 0, count: 10 });
    } else {
      Vue.prototype.$message({ type: "error", message: "添加失败" });
    }
  },
  [CONFIGLIST](...arg) {
    let [
      state,
      {
        result: { sanlinScreenImageHost }
      }
    ] = [...arg];
    state.imageHost = sanlinScreenImageHost;
  },
  PIE_STATISTIC_LIST(state, res) {
    state.pieStatisticList = res;
  },
  [MARK_INACTIVE](...arg) {
    let [, { status = 2 }] = [...arg];
    if (status != -2) {
      Vue.prototype.$message({ type: "success", message: "标记成功" });
      store.dispatch("getCameraListPagination", state.cameraListPagination);
    }
  },
  [GET_SHEET_SATISFY](...arg) {
    let [state, { results }] = [...arg];
    let [, { pagination }] = [...arg];
    Object.assign(state.sheetSatisfyParams, pagination);
    state.sheetSatisfyList = results;
  },
  [GET_SHEET_RESOLVED](...arg) {
    let [state, { results }] = [...arg];
    let [, { pagination }] = [...arg];
    Object.assign(state.sheetResolvedParams, pagination);
    state.sheetResolvedList = results;
  },
  [GET_SHEET_CONTACT](...arg) {
    let [state, { results }] = [...arg];
    let [, { pagination }] = [...arg];
    Object.assign(state.sheetContactParams, pagination);
    state.sheetContactList = results;
  },
  [GET_TRASH_CLASSIFY_LIST](...arg) {
    let [state, { results }] = [...arg];
    let [, { pagination }] = [...arg];
    Object.assign(state.trashClassifyParams, pagination);
    state.trashClassifyList = results;
  },
  [SHOW_PARKING_VIOLATION_POPUP](state, data) {
    state.parkingViolationVisible = data;
  },
  [HANDLE_PARKING](...arg) {
    let [, { error, result }] = [...arg];
    if (result) {
      Vue.prototype.$message({ type: "success", message: "执行成功" });
      store.dispatch("parkingViolationList", state.parkingViolationParams);
    } else {
      Vue.prototype.$message({ type: "error", message: error });
    }
  },
  [GET_ROCKFISH_SELEC_OPTONS](...arg) {
    let [state, { result }] = [...arg];
    state.selectOptions = result;
  },
  [GET_CAMERA_LIST_PAGINATION](...arg) {
    let [state, { results, pagination }] = [...arg];

    results.map((item) => {
      if (!item?.position) {
        Object.assign(item, {
          position: {
            longitude: "",
            latitude: ""
          }
        });
      }
    });
    state.wholeCameraList = results;
    // state.wholeCameraList
    state.cameraListPagination.total = pagination.total;
  },
  [SET_SELECTED_CAMERA_COUNT](state, data) {
    state.selectedCameraCount = data;
  },
  [PARKING_VIOLATION_LIST](...arg) {
    let [state, { results, pagination }] = [...arg];
    state.parkingViolationList = results;
    state.parkingViolationParams.total = pagination.total;
  },
  CHANGE_PARAMS(state, res) {
    state.parkingViolationParams.current = res;
  },
  [SET_RETURN_CAMERA_LIST](state, data) {
    state.returnCameraList = data;
  },
  [CLEAR_SELECT_POINT_LIST](state, data) {
    data;
    state.selectedCameraCount = 0;
    state.selectPointList = [];
  },
  [SEND_POLYGON](state, data) {
    state;
    data;
    // state.selectPointList = [];
    state.cameraList.forEach((item) => {
      if (item?.position?.longitude) {
        let po = new AMap.LngLat(
          Number.parseFloat(item.position.longitude),
          Number.parseFloat(item.position.latitude)
        );
        let isInSet = AMap.GeometryUtil.isPointInRing(po, data);
        if (isInSet && item.status != "INACTIVE") {
          Object.assign(item, { checked: true });
          var marker = new AMap.Marker({
            icon: selectdeviceIcon("SELECTED"),
            position: new AMap.LngLat(
              item.position.longitude,
              item.position.latitude
            ),
            itemData: item
          });
          marker.on("rightclick", function (e) {
            console.log(e);
          });
          state.selectPointList.push(item);
          Vue.prototype.globalAMap.add(marker);
          state.selectMarkerPointList.push(marker);
        }
      } else {
        console.log(`没有经纬度：${item.urn.id}`);
      }
    });
    state.selectedCameraCount =
      state.selectedCameraCount + state.selectPointList.length;
  },
  [CLEAR_MARKERLIST](state, data) {
    data;
    state.markerList = [];
  },
  [CONNECT_VIDEO](state, res) {
    let errors = res.errors;
    if (errors.length > 0) {
      alert("视频流拉取失败");
    } else {
      state.videoRtsp = res.results;
      state.videoVisible = true;
    }
  },
  [DELETE_SCREEN](...arg) {
    arg;
    let [, { result }] = [...arg];
    if (result) {
      Vue.prototype.$message({ type: "success", message: "删除成功" });
    } else {
      Vue.prototype.$message({ type: "error", message: "接口报错" });
    }
    store.dispatch("getScreens", { count: 100, start: 0 });
  },
  [GET_SCREENS](...arg) {
    let [, { results }] = [...arg];
    let newScreenList = results.map((screen) => {
      if (screen?.descriptor?.properties?.segConfig) {
        let segObj = screen.descriptor.properties.segConfig;
        let segChildren = [];
        segObj.forEach((seg) => {
          for (let key of Object.keys(seg)) {
            segChildren.push(seg[key]);
          }
        });
        Object.assign(screen, { segChildren: segChildren });
      }
      return screen;
    });
    state.screenList = newScreenList;
    Vue.ls.set("screenList", state.screenList);
  },
  [CREATE_SCREEN](...arg) {
    arg;
    let [, { errors }] = [...arg];
    store.dispatch("getScreens", { start: 0, count: 100 });
    if (errors.length > 0) {
      Vue.prototype.$message({ type: "error", message: errors[0] });
    } else {
      Vue.prototype.$message({ type: "success", message: "添加成功" });
    }
  },
  [SHOW_PLAY_VIDEO_WINDOW](state, data) {
    const contents = Vue.extend({
      template: `
      <div style="height:300px;width:500px;padding:10px;">
      <hlsVideoContainer v-if="showVideo" :reffer="videoContainerID"  :url="url"></hlsVideoContainer>
      </div>
            `,
      data() {
        return {
          url: "",
          showVideo: false,
          videoContainerID: "video-container-id"
        };
      },
      mounted() {
        setTimeout(() => {
          this.showVideo = true;
          this.connectVideo();
        }, 1000);
      },
      components: { hlsVideoContainer },
      filters: {},
      methods: {
        connectVideo: function () {
          let params = [];
          params.push(
            `urn:${data.urn.organization}:${data.urn.group}:${data.urn.id}`
          );
          let p = connectVideo(params);
          p.then((res) => {
            let url = res.results[0].metadata.url;
            this.url = url;
          });
        }
      },
      computed: {}
    });
    const con = new contents().$mount();
    // const contents = `
    //     <div>
    //         ${data.markers.map(item=>`
    //             <div onclick="${openvideo()}">
    //                 <img src="${item.Ce.icon.w.image}">
    //             </div>
    //         `).join(' ')}
    //     </div>
    // `
    var infoWindow = new AMap.InfoWindow({
      // isCustom: true,
      content: con.$el,
      // position: data.position,
      offset: new AMap.Pixel(0, -20)
    });
    infoWindow.open(
      Vue.prototype.globalAMap,
      new AMap.LngLat(data.position.longitude, data.position.latitude)
    );
    infoWindow.on("close", function () {
      let params = [];
      params.push(
        `urn:${data.urn.organization}:${data.urn.group}:${data.urn.id}`
      );
      disconnectVideo(params);
    });
  },
  [SHOW_INFO_WINDOW](state, data) {
    // let that = this
    console.log(data);
    const contents = Vue.extend({
      template: `
      <div>
      <ul>
      <li v-for="item in markerList"><img @contextmenu="handleShowVideo(item)" @click="handleConnectVideo(item)" :src="item.tIcon"></img><span>{{item.w.itemData.location}}</span></li>
      </ul>
      </div>
            `,
      data() {
        return {
          markerList: data.markers.map((item) => {
            let pointUrnList = [];
            state.selectPointList.forEach((point) => {
              pointUrnList.push(point.urn.id);
            });
            if (pointUrnList.indexOf(item.w.itemData.urn.id) > -1) {
              Object.assign(item, { tIcon: selectdeviceIcon("SELECTED") });
            } else {
              Object.assign(item, { tIcon: selectdeviceIcon("GENERAL") });
            }
            return item;
          })
        };
      },
      mounted() {},
      filters: {},
      methods: {
        handleShowVideo: function (val) {
          let d = val.w.itemData;
          console.log(d);
          store.commit("SHOW_PLAY_VIDEO_WINDOW", d);
        },
        handleConnectVideo(item) {
          if (item.tIcon == selectdeviceIcon("SELECTED")) {
            Object.assign(item, { tIcon: selectdeviceIcon("GENERAL") });
            var zindex;
            var pointItem;
            state.selectPointList.forEach((slice, index) => {
              if (slice.urn.id == item.w.itemData.urn.id) {
                zindex = index;
                pointItem = item.w.itemData;
              }
            });
            state.selectPointList.splice(zindex, 1);
            state.returnMarkerList.forEach((item) => {
              if (item.Ce.itemData.urn.id == pointItem.urn.id) {
                item.hide();
              }
            });
          } else {
            // 标记为选中状态
            console.log(item.w.itemData);
            Object.assign(item, { tIcon: selectdeviceIcon("SELECTED") });
            let t = item.w.itemData;
            Object.assign(t, { checked: true });
            state.selectPointList.push(t);
          }
          this.markerList.push(1);
          this.markerList.pop(1);
        }
      },
      computed: {
        url: function () {
          return "http://baidu.com";
        }
      }
    });
    const con = new contents().$mount();
    // const contents = `
    //     <div>
    //         ${data.markers.map(item=>`
    //             <div onclick="${openvideo()}">
    //                 <img src="${item.Ce.icon.w.image}">
    //             </div>
    //         `).join(' ')}
    //     </div>
    // `
    var infoWindow = new AMap.InfoWindow({
      // isCustom: true,
      content: con.$el,
      // position: data.position,
      offset: new AMap.Pixel(0, -20)
    });
    infoWindow.open(Vue.prototype.globalAMap, data.markers[0].getPosition());
  },
  [GET_CAMERA_LIST](...arg) {
    let [state, { pagination, results }] = [...arg];
    state.cameraList = results;
    Object.assign(state.deviceListPagination, pagination);
    let styleObjectList = [
      // DAMAGED,维修状态的摄像头
      {
        url: selectdeviceIcon("DAMAGED"),
        anchor: new AMap.Pixel(0, 1),
        size: new AMap.Size(23, 34)
      },
      // GENERAL,
      {
        url: selectdeviceIcon("GENERAL"),
        anchor: new AMap.Pixel(0, 1),
        size: new AMap.Size(23, 34)
      },
      // VEHICLE,
      {
        url: selectdeviceIcon("VEHICLE"),
        anchor: new AMap.Pixel(0, 1),
        size: new AMap.Size(23, 34)
      },
      // VIOLATION,
      {
        url: selectdeviceIcon("VIOLATION"),
        anchor: new AMap.Pixel(0, 1),
        size: new AMap.Size(23, 34)
      },
      // FACIAL,
      {
        url: selectdeviceIcon("FACIAL"),
        anchor: new AMap.Pixel(0, 1),
        size: new AMap.Size(23, 34)
      }
      //
    ];
    var dataList = [];
    state.selectList = [];
    results.forEach((item) => {
      var data = null;
      // 撒点逻辑
      if (item.status == "DAMAGED" || item.status == "INACTIVE") {
        // data = {
        //   lnglat: [item.position.longitude, item.position.latitude],
        //   name: item.location,
        //   id: item.urn.organization + item.urn.group + item.urn.id,
        //   style: 0
        // };
        // console.log("已损坏...");
        dataList.push(data);
      } else {
        if (item?.position?.longitude) {
          var innerMarker;
          innerMarker = new AMap.Marker({
            position: new AMap.LngLat(
              item.position.longitude,
              item.position.latitude
            ),
            itemData: item,
            icon: selectdeviceIcon("GENERAL")
          });
          innerMarker.on("rightclick", function (e) {
            console.log(e);
            store.commit("SHOW_PLAY_VIDEO_WINDOW", e.target.w.itemData);
          });
          innerMarker.on("click", function (e) {
            let d = e.target.w.itemData;
            state.currentUrn = `urn:${d.urn.organization}:${d.urn.group}:${d.urn.id}`;
            var marker = new AMap.Marker({
              icon: selectdeviceIcon("SELECTED"),
              position: new AMap.LngLat(
                d.position.longitude,
                d.position.latitude
              ),
              itemData: item
            });
            marker.on("rightclick", function (e) {
              store.commit("SHOW_PLAY_VIDEO_WINDOW", e.target.Ce.itemData);
            });
            marker.on("click", function (e) {
              Vue.prototype.globalAMap.remove(e.target);
              var zindex;
              state.selectPointList.forEach((item, index) => {
                if (item.urn.id == e.target.w.itemData.urn.id) {
                  zindex = index;
                }
              });
              state.selectPointList.splice(zindex, 1);
              state.selectedCameraCount -= 1;
            });
            state.selectPointList.push(marker.w.itemData);
            Vue.prototype.globalAMap.add(marker);
            state.selectMarkerPointList.push(marker);
            state.selectedCameraCount += 1;
          });
          state.markerList.push(innerMarker);
          // 在此处根据已经回显的相机做撒点操作
          setTimeout(() => {
            state.returnCameraList.forEach((cameraUrn) => {
              let cameraId = cameraUrn.split(":")[3];
              if (cameraId == item.urn.id) {
                Object.assign(item, { checked: true });
                state.selectList.push(item);
                var marker = new AMap.Marker({
                  icon: selectdeviceIcon("SELECTED"),
                  position: new AMap.LngLat(
                    item.position.longitude,
                    item.position.latitude
                  ),
                  itemData: item
                });
                marker.on("rightclick", function (e) {
                  console.log(e);
                  store.commit("SHOW_PLAY_VIDEO_WINDOW", e.target.w.itemData);
                });
                marker.on("click", function (e) {
                  Vue.prototype.globalAMap.remove(e.target);
                  var zindex;
                  state.selectPointList.forEach((item, index) => {
                    if (item.urn.id == e.target.w.itemData.urn.id) {
                      zindex = index;
                    }
                  });
                  state.selectPointList.splice(zindex, 1);
                  state.selectedCameraCount -= 1;
                });
                state.selectPointList.push(marker.w.itemData);
                state.selectMarkerPointList.push(marker);
                Vue.prototype.globalAMap.add(marker);
                state.returnMarkerList.push(marker);
                state.selectedCameraCount += 1;
              }
            });
          }, 3000);

          //
        } else {
          // 没有点位信息
          // console.log(item)
        }
      }
    });
    // console.log(markerList);
    Vue.prototype.globalAMap.plugin(["AMap.MarkerClusterer"], function () {
      var cluster = new AMap.MarkerClusterer(
        Vue.prototype.globalAMap,
        state.markerList,
        {
          gridSize: 30,
          zoomOnClick: false
        }
      );
      // 点击聚合点的回调
      cluster.on("click", function (e) {
        console.log(e);
        store.commit("SHOW_INFO_WINDOW", e);
      });
    });
    styleObjectList;
  },
  [CLEAR_MARKER_LIST_ON_MAP](state, data) {
    var zindex = null;
    state.selectPointList.forEach((item, index) => {
      if (item.urn.id == data.urn.id) {
        zindex = index;
      }
    });
    state.selectPointList.splice(zindex, 1);
    state.selectedCameraCount -= 1;
    state.selectMarkerPointList.forEach((item) => {
      if (item.Ce.itemData.urn.id == data.urn.id) {
        Vue.prototype.globalAMap.remove(item);
      }
    });
  },
  [GET_DEVICE_TOTAL](state, data) {
    state;
    data;
    let start =
      state.deviceListPagination.start + state.deviceListPagination.count;
    if (start <= state.deviceListPagination.total) {
      setTimeout(() => {
        Object.assign(state.deviceListPagination, {
          start: start,
          count: state.deviceListPagination.count
        });
        store.dispatch("getCameraList", state.deviceListPagination);
      }, 4500);
    } else {
      console.log("大于最大值了.");
    }
  },
  [VIDEO_POLLING_LIST](...res) {
    //获取视频轮屏列表
    let [state, { pagination, results }] = [...res];
    Object.assign(state.pollingListParams, pagination);
    state.videoPollingList = results;
  },

  [DELETE_VIDEO_POLLING](state, res) {
    state.deleteResult = res;
    store.dispatch("getVideoPollingList", {
      start: 0,
      count: 10
    }); //刷新列表
    Message.success("删除成功");
  },

  [RUN_TASK_NOW](...arg) {
    let [, { msg }] = [...arg];
    Message.warning(msg);
  },
  LIST_RECORD(state, res) {
    state.listrecord = res.data.list;
    state.lllegalstopTotal = res.data.totalCount;
  },
  LIST_RECORD_VEH(state, res) {
    state.listrecordVeh = res.data.list;
    state.zhatumanageTotal = res.data.totalCount;
  },
  READ_DEVICES_TAGS(state, res) {
    state.devicesTagsList = res.results;
  },
  // CHANGECAMERATAG(state){
  //   state.currentCameraTag
  // }
  POPULATION_LIST(...arg) {
    console.log("执行完了");
    let [state, { results, pagination }] = [...arg];
    Object.assign(state.populationParams, pagination);
    state.populationLit = results;
  },
  RESOLVED_EXPORT(...arg) {
    console.log("解决率执行", arg);
    // let [state,{results, pagination}] = [...arg]
    // Object.assign(state.resolvedParams, pagination)
    // state.resolvedList = results
  },
  SATISFY_EXPORT(...arg) {
    console.log("满意度", arg);
    // 导出功能
    let blob = new Blob([arg], {
      type: "application/vnd.ms-excel;charset=utf-8"
    });
    let objectUrl = URL.createObjectURL(blob);
    // var objectUrl = '/duty/template/download'
    var filename = "导出数据.xlsx";
    var a = document.createElement("a");
    a.href = objectUrl;
    a.download = filename;
    a.click();
  },
  CONTACT_EXPORT(...arg) {
    console.log("联系率", arg);
  },
  EXPORTS_CAMERALIST(state, data) {
    state.exportcameralist = data.map((item) => {
      console.log(item);
      return item;
    });
  },
  NOISE_DETECTION_LIST(state, data) {
    console.log(data);
    state.noiseDectectionList = data;
  },
  ADD_NOISE_DEVICE(state, data) {
    state.addNoiseDeviceTag = data;
  },

  GET_GARBAGE_CHECK_LIST(state, res) {
    state.garbagecheckList = res;
  },
  GET_GARBAGE_RECYCLE_LIST(state, res) {
    state.garbagerecyclelist = res;
  },
  GET_STATIC_CONFIG_LIST(state, res) {
    state.garbageStatic = res;
  }
};

export default {
  state,
  actions,
  mutations
};
