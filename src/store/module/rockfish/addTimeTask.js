import {
  SELECT_VIEW,
  ADD_TASK,
  SELECT_VIEW_LIST
} from "../../mutation-types/rockfish-mutations";
import { addTask } from "@/store/api/rockfish/addTimeTask";
import Vue from "vue";
import store from "@/store";
const state = {
  selectView: {},
  selectViewList: []
};
const actions = {
  addTask({ commit }, data) {
    let p = addTask(data);
    p.then(res => {
      commit("ADD_TASK", res);
    });
  }
};

const mutations = {
  [ADD_TASK](...arg) {
    let [, { errors }] = [...arg];
    if (errors.length > 0) {
      Vue.prototype.$message({ type: "error", message: errors[0] });
    } else {
      Vue.prototype.$message({ type: "success", message: "添加成功" });
    }
    store.dispatch("getVideoPollingList", this.pollingListParams);
  },
  [SELECT_VIEW](state, res) {
    state.selectView = res;
  },
  [SELECT_VIEW_LIST](state, res) {
    state.selectViewList = res;
  }
};

export default {
  state,
  actions,
  mutations
};
