import {
  exportExcel,
  updateProject,
  createProject,
  updateOrderStatus,
  getOrderList,
  getWorkSheetMenu,
  createOrder,
  getOrderDetail,
  addOrderRemark,
  getOrderRemark,
  workSheetLoginUser,
  getProjectList
} from "@/store/api/workSheet/workSheet";
import routes from "@/router/index";
import Vue from "vue";
import store from "@/store/index";
import {
  CREATE_PROJECT,
  CREATE_ORDER,
  GET_ORDER_DETAIL,
  GET_WORKSHEET_MENU,
  GET_ORDER_LIST,
  GET_ORDER_REMARK,
  ADD_ORDER_REMARK,
  WORK_SHEET_USER_LOGIN,
  GET_PROJECT_LIST,
  UPDATE_ORDER_STATUS,
  UPDATE_PROJECT,
  EXPORT_WORK_SHEET_EXCEL
} from "@/store/mutation-types/worksheet-mutations";
const state = {
  loginLoading: false,
  remarkList: [],
  orderDetail: "",
  orderList: [],
  projectList: [],
  pagination: { size: 10, total: 0, start: 0 },
  remarkPagination: { size: 8, total: 0, start: 0 },
  projectPagination: { size: 10, total: 0, start: 0 }
};
const actions = {
  exportWorkSheetExcel({ commit }, data) {
    let p = exportExcel(data);
    p.then(res => {
      commit("EXPORT_WORK_SHEET_EXCEL", res);
    });
  },
  updateProject({ commit }, data) {
    let p = updateProject(data);
    p.then(res => {
      commit("UPDATE_PROJECT", res);
    });
  },
  createProject({ commit }, data) {
    let p = createProject(data);
    p.then(res => {
      commit("CREATE_PROJECT", res);
    });
  },
  updateOrderStatus({ commit }, data) {
    let p = updateOrderStatus(data);
    p.then(res => {
      commit("UPDATE_ORDER_STATUS", res);
    });
  },
  getProjectList({ commit }, data) {
    let p = getProjectList(data);
    p.then(res => {
      commit("GET_PROJECT_LIST", res);
    });
  },
  workSheetLoginUser({ commit, state }, data) {
    let p = workSheetLoginUser(data);
    state.loginLoading = true;
    p.then(res => {
      commit("WORK_SHEET_USER_LOGIN", res);
    });
  },
  getOrderRemark({ commit }, data) {
    let p = getOrderRemark(data);
    p.then(res => {
      commit("GET_ORDER_REMARK", res);
    });
  },
  addOrderRemark({ commit }, data) {
    let p = addOrderRemark(data);
    p.then(res => {
      res;
      commit("ADD_ORDER_REMARK", res);
    });
  },
  getOrderDetail({ commit }, data) {
    let p = getOrderDetail(data);
    p.then(res => {
      commit("GET_ORDER_DETAIL", res);
    });
  },
  getOrderList({ commit }, data) {
    let p = getOrderList(data);
    p.then(res => {
      commit("GET_ORDER_LIST", res);
    });
  },
  createOrder({ commit }, data) {
    let p = createOrder(data);
    p.then(res => {
      commit("CREATE_ORDER", res);
    });
  },
  getWorkSheetMenu({ commit }, data) {
    let p = getWorkSheetMenu(data);
    p.then(res => {
      commit("GET_WORKSHEET_MENU", res);
    });
  }
};
const mutations = {
  [EXPORT_WORK_SHEET_EXCEL](...arg) {
    let [, res] = [...arg];
    var binaryData = [];
    binaryData.push(res);
    const url = window.URL.createObjectURL(new Blob(binaryData));
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.href = url;
    a.download = "工单列表.xlsx";
    a.click();
    window.URL.revokeObjectURL(url);
    Vue.prototype.$message.success("Excel导出成功");
  },
  [UPDATE_PROJECT](...arg) {
    let [, { code }] = [...arg];
    if (code == 1) {
      Vue.prototype.$message({ type: "success", message: "更新成功" });
      store.dispatch("getProjectList", { size: 10, start: 0 });
    }
  },
  [CREATE_PROJECT](...arg) {
    let [, { code }] = [...arg];
    if (code == 1) {
      Vue.prototype.$message({ type: "success", message: "创建成功" });
      store.dispatch("getProjectList", { size: 10, start: 0 });
    }
  },
  [UPDATE_ORDER_STATUS](...arg) {
    let [, { code }] = [...arg];
    if (code == 1) {
      Vue.prototype.$message({ type: "success", message: "更新成功" });
    }
  },
  [GET_PROJECT_LIST](...arg) {
    let [state, { data, pagination }] = [...arg];
    state.projectList = data;
    state.projectPagination = pagination;
  },
  [WORK_SHEET_USER_LOGIN](...arg) {
    let [state, { code, data }] = [...arg];
    if (code == 1) {
      sessionStorage.setItem("tokens", data.token);
      Vue.prototype.$message({ type: "success", message: "登录成功" });
      state.loginLoading = false;
      setTimeout(() => {
        routes.push("/w/list");
      }, 1500);
    } else {
      Vue.prototype.$message({ type: "error", message: "用户名或密码错误" });
      state.loginLoading = false;
    }
  },
  [ADD_ORDER_REMARK](...arg) {
    let [, { code }] = [...arg];
    if (code == 1) {
      Vue.prototype.$message({ type: "success", message: "备注添加成功" });
    }
  },
  [GET_ORDER_REMARK](...arg) {
    let [state, { data, pagination }] = [...arg];
    state.remarkList = data;
    state.remarkPagination = pagination;
  },
  [GET_ORDER_DETAIL](...arg) {
    let [state, { data }] = [...arg];
    state.orderDetail = data;
  },
  [GET_ORDER_LIST](...arg) {
    let [state, { data, pagination }] = [...arg];
    state.pagination = pagination;
    state.orderList = data.map(order => {
      let t = order.create_time.split(".")[0].split(" ");
      if (order.resolved_time) {
        let w = order.resolved_time.split(".")[0].split(" ");
        Object.assign(order, { resolved_time: `${w[0]} ${w[1]}` });
      }
      Object.assign(order, { create_time: `${t[0]} ${t[1]}` });

      return order;
    });
  },
  [CREATE_ORDER](...arg) {
    let [, { code }] = [...arg];
    if (code == 1) {
      Vue.prototype.$message({ type: "success", message: "提交成功" });
      store.dispatch("getOrderList");
    }
  },
  [GET_WORKSHEET_MENU](...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
    Vue.ls.set("sysName", "WORK-SHEET");
  }
};
export default { state, actions, mutations };
