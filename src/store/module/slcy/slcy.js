import Vue from "vue";
import {
    getSlcyMemuList,
    slcyLogin,
    getPageBackend,
    delDepartent,
    addDepartent,
    getPermissions,
    setPermissions,
    getPeopleList,
    getDepartmentlist,
    slcyAddUser,
    slcyDeleteUser,
    getEventList
} from "../../api/slcy/slcy.js";
import store from "@/store/index";
import router from "../../../router";
import { Message, message } from "element-ui";
const state = {
    test: {},
    getPageBackend: {
        data: {
            pagination: {
                total: 0
            },
            results: []
        }
    },
    permissionlist: {
        data: {
            result: {
                permissions: []
            }
        }
    },
    peopleList: {
        data: {
            pagination: {
                total: 0
            },
            results: []
        }
    },
    departmentList: {
        data: {
            result: []
        }
    },
    eventList: {
        data: {
            result: []
        }
    }
};
const actions = {

    getSlcyMemuList({ commit }, params) {
        let p = getSlcyMemuList(params);
        p.then(res => {
            commit("SLCY_MENU_LIST", res);
        });
    },
    slcyLogin({ commit }, params) {
        let p = slcyLogin(params);
        return p;
    },

    getPageBackend({ commit }, params) {
        let p = getPageBackend(params);
        p.then(res => {
            commit("GET_BACKEND_LIST", res);
        });
    },
    delDepartent({ commit }, params) {
        let p = delDepartent(params);
        return p;
    },
    addDepartent({ commit }, params) {
        let p = addDepartent(params);
        return p;
    },
    getPermissions({ commit }, params) {
        let p = getPermissions(params);
        p.then(res => {
            commit("DEPARTMENT_PERMISSION_LIST", res);
        });
        return p;
    },
    setPermissions({ commit }, params) {
        let p = setPermissions(params);
        return p;
    },
    getPeopleList({ commit }, params) {
        let p = getPeopleList(params);
        p.then(res => {
            commit("PEOPLE_LIST", res);
        });
        return p;
    },
    getDepartmentlist({ commit }, params) {
        let p = getDepartmentlist(params);
        p.then(res => {
            commit("DEPARTMENT_LIST", res);
        });
        return p;
    },
    slcyAddUser({ commit }, params) {
        let p = slcyAddUser(params);
        return p;
    },
    slcyDeleteUser({ commit }, params) {
        let p = slcyDeleteUser(params);
        return p;
    },
    getEventList({ commit }, params) {
        let p = getEventList(params);
        p.then(res => {
            commit("EVENT_LIST", res);
        });
        return p;
    }
};
const mutations = {
    SLCY_MENU_LIST(...arg) {
        let [, { menus }] = [...arg];
        Vue.ls.set("menuList", menus);
        router.push("/sanlin/department/departmentManagement");
    },
    SLCY_LOGIN(state, res) {
        Vue.ls.set("menuList", res);
    },
    GET_BACKEND_LIST(state, res) {
        state.getPageBackend = res;
    },
    DEPARTMENT_PERMISSION_LIST(state, res) {
        state.permissionlist = res;
    },
    PEOPLE_LIST(state, res) {
        state.peopleList = res;
    },
    DEPARTMENT_LIST(state, res) {
        state.departmentList = res;
    },
    EVENT_LIST(state, res) {
        state.eventList = res;
    }
};

export default {
    state,
    actions,
    mutations
};
