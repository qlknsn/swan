import {
  loginRonghe,
  loginRonghe1,
  loginRonghe2,
  account_assembly_setReleaseStatus,
  account_assembly_setTopStatus,
  account_assembly_deleteAssembly,
  account_assembly_findAssemblyByUniqueId,
  account_assembly_getAssembly,
  account_assembly_getAssemblyAll,
  account_assembly_saveAssembly,
  account_assembly_setCommentStatus,
  account_info,
  account_jscode2session,
  account_loginByOpenid,
  account_phone,
  assembly_setCommentStatus,
  attachment_fileUpload,
  booking_deleteByServiceUrnAndAndUserUrn,
  booking_save,
  comment_find,
  comment_delete,
  comment_findTree,
  comment_makeComment,
  comment_page,
  comment_setExamineStatus,
  dictionary_list,
  district_getDistrictTree,
  district_getDistrictGroup,
  function_delete,
  function_info,
  function_page,
  function_save,
  guarantee_deleteGuaranteeObject,
  guarantee_findGuaranteeByUniqueId,
  guarantee_getGuarantee,
  guarantee_getGuaranteeA,
  guarantee_handleGuarantee,
  guarantee_listByType,
  guarantee_saveGuarantee,
  handle_deleteHandleObject,
  handle_findHandleByUniqueId,
  handle_getHandle,
  handle_getHandleA,
  handle_handleRquest,
  handle_listByType,
  handle_saveHandle,
  help_getHelp,
  help_getHelpAll,
  help_saveHelp,
  homePage_deleteHomepageThr,
  homePage_getHomepage,
  homePage_getNotice,
  homePage_listByType,
  homePage_saveHomePage,
  homeService_delete,
  homeService_info,
  homeService_page,
  homeService_save,
  likes_cancel,
  likes_like,
  redident_appeal,
  redident_delay,
  redident_info,
  redident_page,
  redident_register,
  redident_seal,
  redident_unseal,
  shanghaiPerson_saveShanghaiPerson,
  staff_delete,
  staff_info,
  staff_login,
  staff_page,
  staff_save,
  get_repair_list,
  repair_list_alr,
  repair_detail,
  come_hu_list,
  come_hu_detail,
  homeServiceList,
  gethomeServicepage,
  saveHomeServiceType,
  saveServiceTime,
  help_deleteHelp,
  getsomeDatas,
  savecomehuPerson,
  getYuyuePeople,
  voteTableSave,
  setvoteStatus,
  voteDetail
} from "@/store/api/ronghe/ronghe";
import Vue from "vue";
import route from "@/router/index";
import store from "@/store/index";
import { Message } from "element-ui";
const state = {
  districtTree: [],
  commentList: [],
  assemblyObject: "",
  filePaths: "",
  discussList: [],
  pagination: { start: 0, count: 10, total: 0 },
  discusspagination: { start: 0, count: 10, total: 0 },
  homeServiceList: [],
  homeServicepage: [],
  peoplelist: {
    pagination: {
      total: 0
    }
  },
  peopleinfo: {
    districtName: "",
    name: "",
    address: ""
  },
  staffList: {
    pagination: {
      total: 0
    }
  },
  helpList: {
    pagination: {
      total: 0
    },
    results: [{
      createTime: null,
      creater: null,
      id: 0,
      mobile: null,
      typeName: null,
      pictureUrl: null
    }]
  },
  homeServiceInfo: {
    object: {
      
    }
  },
  reportList: [],
  repairList: [],
  repairListPagination: {},
  repairListalr: [],
  repairListalrPagination: {},
  repairDetail: {
    filePathSs: [],
    filePaths: [],
    guaranteeObject: {}
  },
  comehuList: [],
  comehuListPagination: {},
  comehuDetail: {
    filePathSs: [],
    filePaths: [],
    guaranteeObject: {}
  },
  reportDetail: {
    homepageThrObject:{
      title:null
    }
  },
  trafficList: [],
  homepagelist: {
    pagination: {
      total: 0
    }
  },
  yuyuePeopleList: [],
  yuyuePeoplePagination: {}
};
const actions = {
  voteDetail({ commit }, params) {
    let p = voteDetail(params);
    return p;
  },
  setvoteStatus({ commit }, params) {
    let p = setvoteStatus(params);
    return p;
  },
  voteTableSave({ commit }, params) {
    let p = voteTableSave(params);
    return p;
  },

  saveServiceTime({ commit }, data) {
    let p = saveServiceTime(data);
    return p;
  },
  saveHomeServiceType({ commit }, data) {
    let p = saveHomeServiceType(data);
    return p;
  },
  gethomeServicepage({ commit }, data) {
    let p = gethomeServicepage(data);
    p.then(res => {
      commit("GET_HOMESERVICE_PAGE", res);
    });
  },
  homeServiceList({ commit }, data) {
    let p = homeServiceList(data);
    p.then(res => {
      commit("HOMESERVICE_LIST", res);
    });
  },
  loginRonghe({ commit }, data) {
    let p = loginRonghe(data);
    p.then(res => {
      commit("LOGIN_RONGHE", res);
      route.push("/r/systemManage/staffManage");
    });
  },
  loginRonghe1({ commit }, data) {
    let p = loginRonghe1(data);
    p.then(res => {
      commit("LOGIN_RONGHE1", res);
      route.push("/r/discuss");
    });
  },
  loginRonghe2({ commit }, data) {
    let p = loginRonghe2(data);
    p.then(res => {
      commit("LOGIN_RONGHE2", res);
      route.push("/r/repair/repairList");
    });
  },
  getYuyuePeople({ commit }, data) {
    let p = getYuyuePeople(data);
    p.then(res => {
      commit("GET_YUYUE_PEOPLE", res);
    });
  },
  account_assembly_setReleaseStatus({ commit }, data) {
    let p = account_assembly_setReleaseStatus(data);
    p.then(res => {
      commit("ACCOUNT_ASSEMBLY_SETRELEASESTATUS", res);
    });
  },
  account_assembly_setTopStatus({ commit }, data) {
    let p = account_assembly_setTopStatus(data);
    p.then(res => {
      commit("ACCOUNT_ASSEMBLY_SETTOPSTATUS", res);
    });
  },
  account_assembly_deleteAssembly({ commit }, data) {
    let p = account_assembly_deleteAssembly(data);
    p.then(res => {
      commit("ACCOUNT_ASSEMBLY_DELETEASSEMBLY", res);
    });
  },

  account_assembly_findAssemblyByUniqueId({ commit }, data) {
    let p = account_assembly_findAssemblyByUniqueId(data);
    p.then(res => {
      commit("ACCOUNT_ASSEMBLY_FINDASSEMBLYBYUNIQUEID", res);
    });
  },

  account_assembly_getAssembly({ commit }, data) {
    let p = account_assembly_getAssembly(data);
    p.then(res => {
      commit("ACCOUNT_ASSEMBLY_GETASSEMBLY", res);
    });
  },

  account_assembly_getAssemblyAll({ commit }, data) {
    let p = account_assembly_getAssemblyAll(data);
    p.then(res => {
      commit("ACCOUNT_ASSEMBLY_GETASSEMBLYALL", res);
    });
  },

  account_assembly_saveAssembly({ commit }, data) {
    let p = account_assembly_saveAssembly(data);
    p.then(res => {
      commit("ACCOUNT_ASSEMBLY_SAVEASSEMBLY", res);
    });
  },

  account_assembly_setCommentStatus({ commit }, data) {
    let p = account_assembly_setCommentStatus(data);
    p.then(res => {
      commit("ACCOUNT_ASSEMBLY_SETCOMMENTSTATUS", res);
    });
  },

  account_info({ commit }, data) {
    let p = account_info(data);
    p.then(res => {
      commit("ACCOUNT_INFO", res);
    });
  },

  account_jscode2session({ commit }, data) {
    let p = account_jscode2session(data);
    p.then(res => {
      commit("ACCOUNT_JSCODE2SESSION", res);
    });
  },

  account_loginByOpenid({ commit }, data) {
    let p = account_loginByOpenid(data);
    p.then(res => {
      commit("ACCOUNT_LOGINBYOPENID", res);
    });
  },

  account_phone({ commit }, data) {
    let p = account_phone(data);
    p.then(res => {
      commit("ACCOUNT_PHONE", res);
    });
  },

  assembly_setCommentStatus({ commit }, data) {
    let p = assembly_setCommentStatus(data);
    p.then(res => {
      commit("ASSEMBLY_SETCOMMENTSTATUS", res);
    });
  },

  attachment_fileUpload({ commit }, data) {
    let p = attachment_fileUpload(data);
    p.then(res => {
      commit("ATTACHMENT_FILEUPLOAD", res);
    });
  },

  booking_deleteByServiceUrnAndAndUserUrn({ commit }, data) {
    let p = booking_deleteByServiceUrnAndAndUserUrn(data);
    p.then(res => {
      commit("BOOKING_DELETEBYSERVICEURNANDANDUSERURN", res);
    });
  },

  booking_save({ commit }, data) {
    let p = booking_save(data);
    p.then(res => {
      commit("BOOKING_SAVE", res);
    });
  },

  comment_find({ commit }, data) {
    let p = comment_find(data);
    p.then(res => {
      commit("COMMENT_FIND", res);
    });
  },
  comment_delete({ commit }, data) {
    let p = comment_delete(data);
    p.then(res => {
      commit("COMMENT_DELETE", res);
    });
  },

  comment_findTree({ commit }, data) {
    let p = comment_findTree(data);
    p.then(res => {
      commit("COMMENT_FINDTREE", res);
    });
  },

  comment_makeComment({ commit }, data) {
    let p = comment_makeComment(data);
    p.then(res => {
      commit("COMMENT_MAKECOMMENT", res);
    });
  },

  comment_page({ commit }, data) {
    let p = comment_page(data);
    p.then(res => {
      commit("COMMENT_PAGE", res);
    });
  },

  comment_setExamineStatus({ commit }, data) {
    let p = comment_setExamineStatus(data);
    p.then(res => {
      commit("COMMENT_SETEXAMINESTATUS", res);
    });
  },

  dictionary_list({ commit }, data) {
    let p = dictionary_list(data);
    p.then(res => {
      commit("DICTIONARY_LIST", res);
    });
  },

  district_getDistrictTree({ commit }, data) {
    let p = district_getDistrictTree(data);
    p.then(res => {
      commit("DISTRICT_GETDISTRICTTREE", res);
    });
  },

  district_getDistrictGroup({ commit }, data) {
    let p = district_getDistrictGroup(data);
    return p;
  },

  function_delete({ commit }, data) {
    let p = function_delete(data);
    p.then(res => {
      commit("FUNCTION_DELETE", res);
    });
  },

  function_info({ commit }, data) {
    let p = function_info(data);
    p.then(res => {
      commit("FUNCTION_INFO", res);
    });
  },

  function_page({ commit }, data) {
    let p = function_page(data);
    p.then(res => {
      commit("FUNCTION_PAGE", res);
    });
  },

  function_save({ commit }, data) {
    let p = function_save(data);
    p.then(res => {
      commit("FUNCTION_SAVE", res);
    });
  },

  guarantee_deleteGuaranteeObject({ commit }, data) {
    let p = guarantee_deleteGuaranteeObject(data);
    p.then(res => {
      commit("GUARANTEE_DELETEGUARANTEEOBJECT", res);
    });
  },

  guarantee_findGuaranteeByUniqueId({ commit }, data) {
    let p = guarantee_findGuaranteeByUniqueId(data);
    p.then(res => {
      commit("GUARANTEE_FINDGUARANTEEBYUNIQUEID", res);
    });
  },

  guarantee_getGuarantee({ commit }, data) {
    let p = guarantee_getGuarantee(data);
    p.then(res => {
      commit("GUARANTEE_GETGUARANTEE", res);
    });
  },

  guarantee_getGuaranteeA({ commit }, data) {
    let p = guarantee_getGuaranteeA(data);
    p.then(res => {
      commit("GUARANTEE_GETGUARANTEEA", res);
    });
  },

  guarantee_handleGuarantee({ commit }, data) {
    let p = guarantee_handleGuarantee(data);
    p.then(res => {
      commit("GUARANTEE_HANDLEGUARANTEE", res);
    });
  },

  guarantee_listByType({ commit }, data) {
    let p = guarantee_listByType(data);
    p.then(res => {
      commit("GUARANTEE_LISTBYTYPE", res);
    });
  },

  guarantee_saveGuarantee({ commit }, data) {
    let p = guarantee_saveGuarantee(data);
    p.then(res => {
      commit("GUARANTEE_SAVEGUARANTEE", res);
    });
  },

  handle_deleteHandleObject({ commit }, data) {
    let p = handle_deleteHandleObject(data);
    p.then(res => {
      commit("HANDLE_DELETEHANDLEOBJECT", res);
    });
  },

  handle_findHandleByUniqueId({ commit }, data) {
    let p = handle_findHandleByUniqueId(data);
    p.then(res => {
      commit("HANDLE_FINDHANDLEBYUNIQUEID", res);
    });
  },

  handle_getHandle({ commit }, data) {
    let p = handle_getHandle(data);
    p.then(res => {
      commit("HANDLE_GETHANDLE", res);
    });
  },

  handle_getHandleA({ commit }, data) {
    let p = handle_getHandleA(data);
    p.then(res => {
      commit("HANDLE_GETHANDLEA", res);
    });
  },

  handle_handleRquest({ commit }, data) {
    let p = handle_handleRquest(data);
    p.then(res => {
      commit("HANDLE_HANDLERQUEST", res);
    });
  },

  handle_listByType({ commit }, data) {
    let p = handle_listByType(data);
    p.then(res => {
      commit("HANDLE_LISTBYTYPE", res);
    });
  },

  handle_saveHandle({ commit }, data) {
    let p = handle_saveHandle(data);
    p.then(res => {
      commit("HANDLE_SAVEHANDLE", res);
    });
  },

  help_getHelp({ commit }, data) {
    let p = help_getHelp(data);
    p.then(res => {
      commit("HELP_GETHELP", res);
    });
  },

  help_getHelpAll({ commit }, data) {
    let p = help_getHelpAll(data);
    p.then(res => {
      commit("HELP_GETHELPALL", res);
    });
  },

  help_saveHelp({ commit }, data) {
    let p = help_saveHelp(data);
    p.then(res => {
      commit("HELP_SAVEHELP", res);
    });
    return p;
  },
  help_deleteHelp({ commit }, data) {
    let p = help_deleteHelp(data);
    return p;
  },

  homePage_deleteHomepageThr({ commit }, data) {
    let p = homePage_deleteHomepageThr(data);
    return p;
  },

  homePage_getHomepage({ commit }, data) {
    let p = homePage_getHomepage(data);
    p.then(res => {
      commit("HOMEPAGE_GETHOMEPAGE", res);
    });
    return p;
  },

  homePage_getNotice({ commit }, data) {
    let p = homePage_getNotice(data);
    p.then(res => {
      commit("HOMEPAGE_GETNOTICE", res);
    });
    return p;
  },

  homePage_listByType({ commit }, data) {
    let p = homePage_listByType(data);
    p.then(res => {
      commit("HOMEPAGE_LISTBYTYPE", res);
    });
  },

  homePage_saveHomePage({ commit }, data) {
    let p = homePage_saveHomePage(data);
    return p;
  },

  homeService_delete({ commit }, data) {
    let p = homeService_delete(data);
    return p;
  },

  homeService_info({ commit }, data) {
    let p = homeService_info(data);
    p.then(res => {
      commit("HOMESERVICE_INFO", res);
    });
  },

  homeService_page({ commit }, data) {
    let p = homeService_page(data);
    p.then(res => {
      commit("HOMESERVICE_PAGE", res);
    });
  },

  homeService_save({ commit }, data) {
    console.log(data);
    let p = homeService_save(data);
    return p;
  },

  likes_cancel({ commit }, data) {
    let p = likes_cancel(data);
    p.then(res => {
      commit("LIKES_CANCEL", res);
    });
  },

  likes_like({ commit }, data) {
    let p = likes_like(data);
    p.then(res => {
      commit("LIKES_LIKE", res);
    });
  },

  redident_appeal({ commit }, data) {
    let p = redident_appeal(data);
    p.then(res => {
      commit("REDIDENT_APPEAL", res);
    });
  },

  redident_delay({ commit }, data) {
    let p = redident_delay(data);
    p.then(res => {
      commit("REDIDENT_DELAY", res);
    });
  },

  redident_info({ commit }, data) {
    let p = redident_info(data);
    p.then(res => {
      commit("REDIDENT_INFO", res);
    });
    return p;
  },

  redident_page({ commit }, data) {
    let p = redident_page(data);
    p.then(res => {
      commit("REDIDENT_PAGE", res);
    });
    return p;
  },

  redident_register({ commit }, data) {
    let p = redident_register(data);
    p.then(res => {
      commit("REDIDENT_REGISTER", res);
    });
  },

  redident_seal({ commit }, data) {
    let p = redident_seal(data);
    p.then(res => {
      commit("REDIDENT_SEAL", res);
    });
    return p;
  },

  redident_unseal({ commit }, data) {
    let p = redident_unseal(data);
    p.then(res => {
      commit("REDIDENT_UNSEAL", res);
    });
    return p;
  },

  shanghaiPerson_saveShanghaiPerson({ commit }, data) {
    let p = shanghaiPerson_saveShanghaiPerson(data);
    p.then(res => {
      commit("SHANGHAIPERSON_SAVESHANGHAIPERSON", res);
    });
  },

  staff_delete({ commit }, data) {
    let p = staff_delete(data);
    p.then(res => {
      commit("STAFF_DELETE", res);
    });
    return p;
  },

  staff_info({ commit }, data) {
    let p = staff_info(data);
    p.then(res => {
      commit("STAFF_INFO", res);
    });
  },

  staff_login({ commit }, data) {
    let p = staff_login(data);
    p.then(res => {
      if (res.error) {
        Message({
          type: "warning",
          message: res.error.message
        });
      } else {
        commit("STAFF_LOGIN", res);
      }
    });
  },

  staff_page({ commit }, data) {
    let p = staff_page(data);
    p.then(res => {
      commit("STAFF_PAGE", res);
    });
  },

  staff_save({ commit }, data) {
    let p = staff_save(data);
    p.then(res => {
      commit("STAFF_SAVE", res);
    });
    return p;
  },

  get_repair_list({ commit }, data) {
    let p = get_repair_list(data);
    p.then(res => {
      commit("GET_REPAIR_LIST", res);
    });
  },

  repair_list_alr({ commit }, data) {
    let p = repair_list_alr(data);
    p.then(res => {
      commit("GET_REPAIR_LIST_ALR", res);
    });
  },

  repair_detail({ commit }, data) {
    let p = repair_detail(data);
    p.then(res => {
      commit("GET_REPAIR_DETAIL", res);
    });
  },

  come_hu_list({ commit }, data) {
    let p = come_hu_list(data);
    p.then(res => {
      commit("COME_HU_LIST", res);
    });
  },
  come_hu_detail({ commit }, data) {
    let p = come_hu_detail(data);
    p.then(res => {
      commit("COME_HU_DETAIL", res);
    });
  },
  getsomeDatas({ commit }, data) {
    let p = getsomeDatas(data);
    p.then(res => {
      if (data.dictGroup === "TRAFFIC") {
        commit("GET_TRAFFIC_DATAS", res);
      }
    });
  },
  savecomehuPerson({ commit }, data) {
    let p = savecomehuPerson(data);
    p.then(res => {
      commit("SAVE_COMRHU_PERSON", res);
    });
  }
};
const mutations = {
  SAVE_VOTETABLE(state, res) {
    console.log(res);
  },
  GET_HOMESERVICE_PAGE(...arg) {
    let [state, { results,pagination }] = [...arg];
    state.homeServicepage = results;
    state.pagination = pagination;
  },
  CHANGE_HOMESERVICE_PAGE(state, res) {
    state.homeServicepage = res;
  },
  HOMESERVICE_LIST(...arg) {
    let [state, { results }] = [...arg];
    state.homeServiceList = results;
  },
  CHANGE_HOMESERVICE_LIST(state, res) {
    state.homeServiceList = res;
  },
  LOGIN_RONGHE(...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
  },
  LOGIN_RONGHE1(...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
  },
  LOGIN_RONGHE2(...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
  },
  GET_YUYUE_PEOPLE(state, res) {
    state.yuyuePeopleList = res.results;
    state.yuyuePeoplePagination = res.pagination;
  },
  ACCOUNT_ASSEMBLY_SETRELEASESTATUS(...arg) {
    let [, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ type: "success", message: "设置成功" });
      store.dispatch("account_assembly_getAssembly", { start: 0, count: 10 });
    }
  },
  ACCOUNT_ASSEMBLY_SETTOPSTATUS(...arg) {
    arg;
    let [, { error }] = [...arg];
    if (error == null) {
      store.dispatch("account_assembly_getAssembly", { start: 0, count: 10 });
      Vue.prototype.$message({ type: "success", message: "设置成功" });
    }
  },
  ACCOUNT_ASSEMBLY_DELETEASSEMBLY(...arg) {
    arg;
    let [, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ type: "success", message: "删除成功" });
      store.dispatch("account_assembly_getAssembly", { start: 0, count: 10 });
    }
  },

  ACCOUNT_ASSEMBLY_FINDASSEMBLYBYUNIQUEID(...arg) {
    let [
      state,
      {
        result: { assemblyObject, filePaths }
      }
    ] = [...arg];
    state.assemblyObject = assemblyObject;
    state.filePaths = filePaths;
  },

  ACCOUNT_ASSEMBLY_GETASSEMBLY(...arg) {
    arg;
    let [state, { pagination, results }] = [...arg];
    Object.assign(state.pagination, pagination);
    state.discussList = results;
  },

  ACCOUNT_ASSEMBLY_GETASSEMBLYALL(...arg) {
    arg;
  },

  ACCOUNT_ASSEMBLY_SAVEASSEMBLY(...arg) {
    arg;
  },

  ACCOUNT_ASSEMBLY_SETCOMMENTSTATUS(...arg) {
    arg;
  },

  ACCOUNT_INFO(...arg) {
    arg;
  },

  ACCOUNT_JSCODE2SESSION(...arg) {
    arg;
  },

  ACCOUNT_LOGINBYOPENID(...arg) {
    arg;
  },

  ACCOUNT_PHONE(...arg) {
    arg;
  },

  ASSEMBLY_SETCOMMENTSTATUS(...arg) {
    arg;
  },

  ATTACHMENT_FILEUPLOAD(...arg) {
    arg;
  },

  BOOKING_DELETEBYSERVICEURNANDANDUSERURN(...arg) {
    arg;
  },

  BOOKING_SAVE(...arg) {
    arg;
  },

  COMMENT_FIND(...arg) {
    arg;
    let [state, { results, pagination }] = [...arg];
    state.commentList = results;
    state.pagination = pagination;
  },
  COMMENT_DELETE(...arg) {
    arg;
    let [, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ type: "success", message: "删除成功" });
      store.dispatch("comment_find", {
        start: 0,
        count: 10,
        refUrn: sessionStorage.getItem("uniqueId")
      });
    }
  },

  COMMENT_FINDTREE(...arg) {
    arg;
  },

  COMMENT_MAKECOMMENT(...arg) {
    arg;
    let [, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ type: "success", message: "发表成功" });
      store.dispatch("comment_find", {
        start: 0,
        count: 10,
        refUrn: sessionStorage.getItem("uniqueId")
      });
    } else {
      Vue.prototype.$message({ type: "error", message: "该文章暂未开启评论" });
    }
  },

  COMMENT_PAGE(...arg) {
    arg;
  },

  COMMENT_SETEXAMINESTATUS(...arg) {
    arg;
    let [, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ type: "success", message: "设置成功" });
      store.dispatch("account_assembly_findAssemblyByUniqueId", {
        uniqueId: sessionStorage.getItem("uniqueId")
      });
    }
  },

  DICTIONARY_LIST(...arg) {
    arg;
  },

  DISTRICT_GETDISTRICTTREE(...arg) {
    arg;
    let [state, { result }] = [...arg];
    state.districtTree = result;
  },

  FUNCTION_DELETE(...arg) {
    arg;
  },

  FUNCTION_INFO(...arg) {
    arg;
  },

  FUNCTION_PAGE(...arg) {
    arg;
  },

  FUNCTION_SAVE(...arg) {
    arg;
  },

  GUARANTEE_DELETEGUARANTEEOBJECT(...arg) {
    arg;
  },

  GUARANTEE_FINDGUARANTEEBYUNIQUEID(...arg) {
    arg;
  },

  GUARANTEE_GETGUARANTEE(...arg) {
    arg;
  },

  GUARANTEE_GETGUARANTEEA(...arg) {
    arg;
  },

  GUARANTEE_HANDLEGUARANTEE(...arg) {
    arg;
  },

  GUARANTEE_LISTBYTYPE(...arg) {
    arg;
  },

  GUARANTEE_SAVEGUARANTEE(...arg) {
    arg;
  },

  HANDLE_DELETEHANDLEOBJECT(...arg) {
    arg;
  },

  HANDLE_FINDHANDLEBYUNIQUEID(...arg) {
    arg;
  },

  HANDLE_GETHANDLE(...arg) {
    arg;
  },

  HANDLE_GETHANDLEA(...arg) {
    arg;
  },

  HANDLE_HANDLERQUEST(...arg) {
    arg;
  },

  HANDLE_LISTBYTYPE(...arg) {
    arg;
  },

  HANDLE_SAVEHANDLE(...arg) {
    arg;
  },

  HELP_GETHELP(state, res) {
    state.helpList = res;
  },

  HELP_GETHELPALL(...arg) {
    arg;
  },

  HELP_SAVEHELP(...arg) {
    arg;
  },

  HOMEPAGE_DELETEHOMEPAGETHR(...arg) {
    arg;
  },

  HOMEPAGE_GETHOMEPAGE(...arg) {
    let [state, { result }] = [...arg];
    state.reportDetail = result;
  },

  CHANGE_HOMEPAGE_GETNOTICE(state, res) {
    state.reportList = res;
  },
  HOMEPAGE_GETNOTICE(...arg) {
    let [state, { results ,pagination}] = [...arg];
    state.reportList = results;
    state.pagination = pagination;
  },

  HOMEPAGE_LISTBYTYPE(state, res) {
    state.homepagelist = res;
  },

  HOMEPAGE_SAVEHOMEPAGE(...arg) {
    arg;
  },

  HOMESERVICE_DELETE(...arg) {
    arg;
  },

  HOMESERVICE_INFO(...arg) {
    let [state, { result }] = [...arg];
    state.homeServiceInfo = result;
  },

  HOMESERVICE_PAGE(...arg) {
    arg;
  },

  HOMESERVICE_SAVE(...arg) {
    arg;
  },

  LIKES_CANCEL(...arg) {
    arg;
  },

  LIKES_LIKE(...arg) {
    arg;
  },

  REDIDENT_APPEAL(...arg) {
    arg;
  },

  REDIDENT_DELAY(...arg) {
    arg;
  },

  REDIDENT_INFO(state, res) {
    state.peopleinfo = res.result;
  },

  REDIDENT_PAGE(state, res) {
    state.peoplelist = res;
  },

  REDIDENT_REGISTER(...arg) {
    arg;
  },

  REDIDENT_SEAL(...arg) {
    arg;
  },

  REDIDENT_UNSEAL(...arg) {
    arg;
  },

  SHANGHAIPERSON_SAVESHANGHAIPERSON(...arg) {
    arg;
  },

  STAFF_DELETE(...arg) {
    arg;
  },

  STAFF_INFO(...arg) {
    arg;
  },

  STAFF_LOGIN(...arg) {
    arg;
    let [
      ,
      {
        error,
        result: {
          token,
          villages,
          staffObject: { name, workerUrn, districtId, districtName, roles }
        }
      }
    ] = [...arg];
    Vue.ls.set("rongheToken", token);
    Vue.ls.set("name", name);
    sessionStorage.setItem("name", name);
    sessionStorage.setItem("villageDistrictId", districtId);
    sessionStorage.setItem("districtName", districtName);
    sessionStorage.setItem("workerUrn", workerUrn);
    sessionStorage.setItem("role", roles);
    let districtIdList = villages.map(item => {
      return item.districtId;
    });
    sessionStorage.setItem("districtIds", districtIdList);
    // Vue.ls.set("districtIds", districtIdList);

    if (error == null) {
      if (roles == 0) {
        store.dispatch("loginRonghe");
      } else if (roles == 1) {
        store.dispatch("loginRonghe1");
      } else {
        store.dispatch("loginRonghe2");
      }
    }
  },

  STAFF_PAGE(state, res) {
    state.staffList = res;
  },

  STAFF_SAVE(...arg) {
    arg;
  },
  GET_REPAIR_LIST(state, res) {
    state.repairList = res.results;
    state.repairListPagination = res.pagination;
  },
  GET_REPAIR_LIST_ALR(state, res) {
    state.repairListalr = res.results;
    state.repairListalrPagination = res.pagination;
  },
  GET_REPAIR_DETAIL(state, res) {
    state.repairDetail = res.result;
  },
  COME_HU_LIST(state, res) {
    state.comehuList = res.results.map(item => {
      Object.assign(item, {
        stayStatus: item.stayStatus == 0 ? "否" : "是",
        receiverType: item.receiverType == 0 ? "否" : "是",
        coughStatus: item.coughStatus == 0 ? "否" : "是",
        breathStatus: item.breathStatus == 0 ? "否" : "是"
      });
      return item;
    });
    state.comehuListPagination = res.pagination;
  },
  COME_HU_DETAIL(state, res) {
    state.comehuDetail = res.result;
  },
  GET_TRAFFIC_DATAS(state, res) {
    state.trafficList = res.results;
  },
  SAVE_COMRHU_PERSON(...arg) {
    let [, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ type: "success", message: "新增成功" });
      route.push("/r/comeHu/comeHuList");
    }
  }
};
export default {
  state,
  mutations,
  actions
};
