import{
    getWasteList,
    handleWaste,
    deleteEvent,
    uploadImage,
   
} from "../../api/wasteSort/wasteSort"
import Vue from "vue";
import store from "@/store/index";
const state={
    wasteList:[],
    totalPage:null,
    imageInfo:''
};
const actions={
    getWasteList({ commit }, data) {
        let p = getWasteList(data);
        p.then(res => {
          commit("GETWASTELIST", res);
        });
      },
    handleWaste({ commit }, data) {
        let p = handleWaste(data);
        p.then(res => {
          commit("HANDLEWASTE", res);
        });
      },
    deleteEvent({ commit }, data) {
        let p = deleteEvent(data);
        p.then(res => {
          commit("DELETEEVENT", res);
        });
  },
  uploadImage({ commit }, data) {
    let p = uploadImage(data);
    p.then(res => {
      commit("UPLOADIMAGE", res);
    });
  },
};
const mutations = {
    GETWASTELIST(...arg) {
      let [state, { data }] = [...arg];
      state.wasteList = data.list
      state.totalPage=data.totalPage
      console.log(state.wasteList)
      console.log(state.totalPage)
    },
    HANDLEWASTE(...arg) {
      let [, { code }] = [...arg];
      if (code == 200) {
        Vue.prototype.$message({ message: "编辑成功", type: "success" });
        store.dispatch("getWasteList", {page:1,limit:3, timeRangeType: 6});
      } else {
        Vue.prototype.$message({ message: "编辑失败", type: "error" });
      }
    },
      DELETEEVENT(...arg) {
      let [, { code }] = [...arg];
      if (code == 200) {
        Vue.prototype.$message({ message: "删除成功", type: "success" });
        store.dispatch("getWasteList",  {page:1,limit:3, timeRangeType: 6});
      } else {
        Vue.prototype.$message({ message: "删除失败", type: "error" });
      }
  },
  UPLOADIMAGE(...arg) {
    let [state, { data }] = [...arg];
    state.imageInfo = data.prefix + data.filePath
    console.log(state.imageInfo)
  },
};
export default {
    state,
    actions,
    mutations
  };