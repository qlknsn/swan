import {
  roleFind,
  deletePermission,
  savePermission,
  getPermissionList,
  menuDelete,
  getLqcMenus,
  userPermission,
  getLQCProjectList,
  getProjectDetail,
  getProjectPlanList,
  getBlendType,
  menuFind,
  menuSave,
  menuUpdate,
  roleList,
  roleSave,
  addBlendPlan,
  addProductionPlan,
  addLqcProject,
  lqcLogin,
  deleteLqcPlan,
  getlqcPersonList,
  addlqcRole,
  deletelqcperson,
  getProjectBlendType,
  blendPlanApproveList,
  getAuditStatusList,
  blendPlanApprovalType,
  getTorromowPlan,
  roleDelete,
  getlqcRoleList,
  getCheckType,
  addApprove,
  getshiliaoAmount,
  updateshiliao,
  getliqingAmount,
  updateliqing,
  finish
} from "../../api/lqc/lqc";
import Vue from "vue";
import route from "@/router/index";
import store from "@/store/index";
const state = {
  auditPagination: { start: 0, count: 10, total: 0 },
  auditTypeList: [],
  auditStatusList: [],
  auditProjectList: [],
  auditList: [],
  projectList: {
    pagination: {
      total: 0
    },
    results: []
  },
  projectDetail: {},
  blendType: [],
  projectPlanList: {
    results: [
      {
        realNum: 0,
        expectedNum: 10
      }
    ],
    pagination: {
      total: 0
    }
  },
  projectBlendType: [],
  torromowPlan: {
    results: []
  },
  checkType: [
    {
      description: ""
    }
  ],
  infoNumber: 0,
  personList: [],
  personPagination: { total: 0 },
  roleList: [],
  shiliaoList: [
    {
      stock: 0,
      baseStock: 0
    },
    {
      stock: 0,
      baseStock: 0
    },
    {
      stock: 0,
      baseStock: 0
    },
    {
      stock: 0,
      baseStock: 0
    },
    {
      stock: 0,
      baseStock: 0
    },
    {
      stock: 0,
      baseStock: 0
    },
    {
      stock: 0,
      baseStock: 0
    },
    {
      stock: 0,
      baseStock: 0
    },
  ],
  liqingList:[]
};
const actions = {
  finish({ commit }, params) {
    console.log(params)
    let p = finish(params);
    p.then(res => {
      commit("FINISH", res);
    });
  },
  roleFind({ commit }, data) {
    let p = roleFind(data);
    return p;
  },
  deletePermission({ commit }, params) {
    let p = deletePermission(params);
    return p;
  },
  savePermission({ commit }, params) {
    let p = savePermission(params);
    return p;
  },
  getPermissionList({ commit }, params) {
    let p = getPermissionList(params);
    return p;
  },
  menuDelete({ commit }, params) {
    let p = menuDelete(params);
    return p;
  },
  roleDelete({ commit }, params) {
    let p = roleDelete(params);
    return p;
  },
  blendPlanApprovalType({ commit }, params) {
    let p = blendPlanApprovalType(params);
    p.then(res => {
      commit("BLENDPLANAPPROVALTYPE", res);
    });
  },
  roleSave({ commit }, params) {
    let p = roleSave(params);
    return p;
  },
  addProductionPlan({ commit }, params) {
    let p = addProductionPlan(params);
    return p;
  },
  roleList({ commit }, params) {
    let p = roleList(params);
    return p;
  },
  menuUpdate({ commit }, params) {
    let p = menuUpdate(params);
    return p;
  },
  getCheckType({ commit }) {
    let p = getCheckType();
    p.then(res => {
      commit("CHECK_TYPE", res);
    });
    return p;
  },
  menuFind({ commit }, params) {
    let p = menuFind(params);
    return p;
  },
  menuSave({ commit }, params) {
    let p = menuSave(params);
    return p;
  },
  getAuditStatusList({ commit }, params) {
    let p = getAuditStatusList(params);
    p.then(res => {
      commit("GETAUDITSTATUSLIST", res);
    });
  },
  blendPlanApproveList({ commit }, params) {
    let p = blendPlanApproveList(params);
    p.then(res => {
      commit("BLENDPLANAPPROVELIST", res);
    });
  },
  blendPlanApproveListss({ commit }, params) {
    let p = blendPlanApproveList(params);
    p.then(res => {
      commit("GETINFONUMBER", res);
    });
  },
  getLqcMenus() {
    let p = getLqcMenus();
    p.then(res => {
      Vue.ls.set("menuList", res.menus);
      route.push("/lqc/planManage");
    });
  },
  userPermission() {
    let p = userPermission();
    p.then(res => {
      console.log(res);
      Vue.ls.set("menuList", res.menus);
      route.push("/lqc/permission");
    });
  },
  getProjectDetail({ commit }, params) {
    let p = getProjectDetail(params);
    p.then(res => {
      commit("PROJECT_DETAIL", res);
    });
    return p;
  },
  getProjectPlanList({ commit }, params) {
    let p = getProjectPlanList(params);
    p.then(res => {
      commit("PROJECT_PLAN_LIST", res);
    });
    return p;
  },
  getTorromowPlan({ commit }, params) {
    let p = getTorromowPlan(params);
    p.then(res => {
      commit("TOMORROW_PLAN_LIST", res);
    });
    return p;
  },
  getBlendType({ commit }) {
    let p = getBlendType();
    p.then(res => {
      commit("BLEND_TYPE", res);
    });
    return p;
  },
  getLQCProjectList({ commit }, params) {
    let p = getLQCProjectList(params);
    p.then(res => {
      commit("PROJECT_LIST", res);
      commit("PROJECTLIST", res);
    });
    return p;
  },
  addBlendPlan({ commit, state }, params) {
    let p = addBlendPlan(params);
    // let q = blendPlanApproveList(state.auditPagination);
    // q.then(res => {
    //   commit("BLENDPLANAPPROVELIST", res);
    // });
    p.then(res => {
      commit("ADDBLENDPLAN", res);
    });
    return p;
  },
  addLqcProject({ commit }, params) {
    let p = addLqcProject(params);
    return p;
  },
  lqcLogin({ commit }, params) {
    let p = lqcLogin(params);
    p.then(res => {
      commit("LQCLOGIN", res);
    });
    return p;
  },
  deleteLqcPlan({ commit }, params) {
    let p = deleteLqcPlan(params);
    return p;
  },
  getProjectBlendType({ commit }, params) {
    let p = getProjectBlendType(params);
    p.then(res => {
      commit("PROJECT_BLEND_TYPE", res);
    });
    return p;
  },
  getlqcPersonList({ commit }, params) {
    let p = getlqcPersonList(params);
    p.then(res => {
      commit("GET_LQC_PERSON_LIST", res);
    });
    return p;
  },
  addlqcRole({ commit }, params) {
    let p = addlqcRole(params);
    return p;
  },
  deletelqcperson({ commit }, params) {
    let p = deletelqcperson(params);
    return p;
  },
  getlqcRoleList({ commit }, params) {
    let p = getlqcRoleList(params);
    p.then(res => {
      commit("GET_LQC_ROLE_LIST", res);
    });
    return p;
  },
  addApprove({ commit }, params) {
    let p = addApprove(params);
    p.then(res => {
      // commit("GET_LQC_ROLE_LIST", res);
    });
    return p;
  },
  getshiliaoAmount({ commit }, params) {
    let p = getshiliaoAmount(params);
    p.then(res => {
      commit("GET_SHILIAO_LIST", res);
    });
  },
  getliqingAmount({ commit }, params) {
    let p = getliqingAmount(params);
    p.then(res => {
      commit("GET_LIQING_LIST", res);
    });
  },
  updateshiliao({ commit }, params) {
    let p = updateshiliao(params);
    // p.then(res => {
    //   commit("UPDATE_SHILIAO_LIST", res);
    // });
    return p;
  },
  updateliqing({ commit }, params) {
    let p = updateliqing(params);
    return p;
  }
};
const mutations = {
  FINISH(state,data){
    if(data.error==null){
      Vue.prototype.$message({ type: "success", message: "完结成功" });
      // this.$store.commit("PROJECT_LIST")
    }else{
      Vue.prototype.$message({ type: "error", message: data.error.message });
    }
     
  },
  LQCLOGIN(...arg) {
    let [
      ,
      {
        result: { token, realName, menus }
      }
    ] = [...arg];
    Vue.ls.set("name", realName);
  },
  ADDBLENDPLAN(...arg) {
    store.dispatch("getAuditStatusList");
    arg;
    let [, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ type: "success", message: "提交成功" });
    }
  },
  BLENDPLANAPPROVALTYPE(state, data) {
    state.auditTypeList = data;
  },
  GETAUDITSTATUSLIST(state, data) {
    state.auditStatusList = data;
  },
  BLENDPLANAPPROVELIST(...arg) {
    let [state, { results, pagination }] = [...arg];
    state.auditList = [];
    state.auditList = results;
    Object.assign(state.auditPagination, pagination);
  },
  PROJECTLIST(...arg) {
    let [state, { results }] = [...arg];
    state.auditProjectList = results;
  },
  PROJECT_LIST(state, res) {
    res.results.map(item => {
      item.projectPlanList = {
        results: []
      };
    });
    state.projectList = res;
  },
  TOMORROW_PLAN_LIST(state, res) {
    let arr = [];
    res.results.forEach(item => {
      item.dailyProducts.forEach(element => {
        element.projectName = item.projectName;
        arr.push(element);
      });
    });
    state.torromowPlan.results = arr;
    console.log(state.torromowPlan.results)
  },
  PROJECT_DETAIL(state, res) {
    state.projectDetail = res;
  },
  CHECK_TYPE(state, res) {
    state.checkType = res;
  },
  BLEND_TYPE(state, res) {
    state.blendType = res;
  },
  PROJECT_BLEND_TYPE(state, res) {
    state.projectBlendType = res.results;
  },
  PROJECT_PLAN_LIST(state, res) {
    // this.projectPlanList.results = res.results;
    state.projectList.results.forEach((element, index) => {
      state.projectList.results[index].projectPlanList = res;
    });
    // state.projectList.projectPlanList = res;
  },
  GETINFONUMBER(state, res) {
    if (res.pagination) {
      state.infoNumber = res.pagination.total;
    }
  },
  GET_LQC_PERSON_LIST(state, res) {
    state.personList = res.results;
    state.personPagination = res.pagination;
  },
  GET_LQC_ROLE_LIST(state, res) {
    state.roleList = res.results;
  },
  GET_SHILIAO_LIST(state, res) {
    state.shiliaoList = res.results;
    // console.log(state.shiliaoList);
  },
  UPDATE_SHILIAO_LIST(state, res) {
    state.updateshiliao = res.results;
  },
  GET_LIQING_LIST(state, res) {
    state.liqingList = res.results;
    // console.log(state.shiliaoList);
  },
};
export default {
  state,
  actions,
  mutations
};
