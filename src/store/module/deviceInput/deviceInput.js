import {
  getDImenuList,
  getVideoList,
  deleteVideo,
  brandList,
  deviceInputlogin,
  brandAdd,
  brandDelete,
  brandUpdate,
  districtAdd,
  districtList,
  districtDelete,
  districtUpdate,
  addVideo,
  updateVideo,
  getDeviceCameraList,
  updatediCamera,
  getCameraPic,
  getThirdPlatfromList,
  addThirdPlatfrom,
  deleteThirdPlatfrom,
  getTreeList,
  getThirdVideoList
} from "../../api/deviceInput/deviceInput";
import router from "../../../router/index";
import Vue from "vue";
import store from "@/store/index";
const state = {
  menuList: [],
  brandList: [],
  diDialogFormVisible: false,
  districtList: [],
  districtParams: {
    name: "",
    type: "",
    pageNo: 1,
    pageSize: 10
  },
  videoList: [],
  videoListTotal: 0,
  deviceCameraList: [],
  deviceCameraListTotal: 0,
  diLoginLoading: false,
  devicePic: "",
  thirdPlatfromList: [],
  thirdPlatfromTotalCount: 0,
  thirdPlatformTree: [],
  thirdVideoList: [],
  thirdVideoListTotal: 0
};
const actions = {
  getDImenuList({ commit }, data) {
    let p = getDImenuList(data);
    p.then(res => {
      commit("GET_DI_MENULIST", res);
    });
  },
  getVideoList({ commit }, data) {
    let p = getVideoList(data);
    p.then(res => {
      commit("GET_VIDEO_LIST", res);
    });
  },
  deleteVideo({ commit }, data) {
    let p = deleteVideo(data);
    p.then(res => {
      commit;
      if (res.code === 200) {
        Vue.prototype.$message({
          type: "success",
          message: "编辑成功!"
        });
        store.dispatch("getVideoList", {
          districtId: data.districtId,
          pageNo: 1,
          pageSize: 10
        });
      } else {
        Vue.prototype.$message({
          type: "error",
          message: "编辑失败!"
        });
      }
      Vue.prototype.$message({
        type: "success",
        message: "删除成功!"
      });
    });
  },
  getbrandList({ commit }, data) {
    let p = brandList(data);
    p.then(res => {
      commit("GET_BRAND_LIST", res);
    });
  },
  deviceInputlogin({ state, commit }, data) {
    state.deviceInputlogin = true;
    let p = deviceInputlogin(data);
    p.then(res => {
      state.deviceInputlogin = false;
      commit;
      router.push("/di/areaInfo");
      res;
    });
  },
  brandAdd({ commit }, data) {
    let p = brandAdd(data);
    p.then(res => {
      commit;
      store.commit("SET_DIALOG_FORM", false);
      res;
    });
  },
  districtUpdate({ commit }, data) {
    commit;
    let p = districtUpdate(data);
    // p.then(res => {
    //   commit;
    //   // store.commit('SET_DIALOG_FORM',false)
    //   res;
    // });
    return p;
  },
  brandDelete({ commit }, data) {
    let p = brandDelete(data);
    p.then(res => {
      commit;
      res;
      Vue.prototype.$message({
        type: "success",
        message: "删除成功!"
      });
    });
  },
  brandUpdate({ commit }, data) {
    let p = brandUpdate(data);
    p.then(res => {
      commit;
      Vue.prototype.$message({
        type: "success",
        message: "更新成功!"
      });
      store.commit("SET_DIALOG_FORM", false);
      res;
    });
  },
  districtAdd({ commit }, data) {
    let p = districtAdd(data);
    p.then(res => {
      commit;
      res;
      if (res.code == 200) {
        Vue.prototype.$message({
          type: "success",
          message: "添加成功!"
        });
        router.push("/di/areaInfo");
      } else {
        Vue.prototype.$message({
          type: "warning",
          message: res.msg
        });
      }
    });
  },
  diDistrictList({ commit }, data) {
    let p = districtList(data);
    p.then(res => {
      commit;
      res;
      commit("GET_DISTRICT_LIST", res);
    });
  },
  diDistrictDelete({ commit }, data) {
    let p = districtDelete(data);
    p.then(res => {
      commit;
      if (res.code == 200) {
        store.dispatch("diDistrictList", state.districtParams);
        Vue.prototype.$message({
          type: "success",
          message: "删除区域成功"
        });
      }
    });
  },
  async addVideo({ commit }, data) {
    let p = addVideo(data);
    await p.then(res => {
      if (res.code === 200) {
        Vue.prototype.$message({
          type: "success",
          message: "添加成功!"
        });
        store.dispatch("getVideoList", {
          districtId: data.districtId,
          pageNo: 1,
          pageSize: 10
        });
      } else {
        Vue.prototype.$message({
          type: "error",
          message: "添加失败!"
        });
      }
      commit;
    });
  },
  updateVideo({ commit }, data) {
    let p = updateVideo(data);
    p.then(res => {
      commit;
      if (res.code === 200) {
        Vue.prototype.$message({
          type: "success",
          message: "编辑成功!"
        });
        store.dispatch("getVideoList", {
          districtId: data.districtId,
          pageNo: 1,
          pageSize: 10
        });
      } else {
        Vue.prototype.$message({
          type: "error",
          message: "编辑失败!"
        });
      }
    });
  },
  getDeviceCameraList({ commit }, data) {
    let p = getDeviceCameraList(data);
    p.then(res => {
      commit("GET_DEVICE_CAMERA_LIST", res.data);
    });
  },
  updatediCamera({ commit }, data) {
    commit;
    let p = updatediCamera(data);
    p.then(res => {
      if (res.code === 200) {
        Vue.prototype.$message({
          type: "success",
          message: "编辑成功!"
        });
        store.dispatch("getThirdVideoList", {
          pageSize: 10,
          pageNo: 1,
          platformId: data.platformId
        });
      } else {
        Vue.prototype.$message({
          type: "error",
          message: "编辑失败!"
        });
      }
    });
  },
  getCameraPic({ commit, state }, data) {
    state.devicePic = "";
    let p = getCameraPic(data);
    p.then(res => {
      commit("GET_DEVICE_CAMERA_PIC", res.data);
    });
  },
  getThirdPlatfromList({ commit }, data) {
    let p = getThirdPlatfromList(data);
    p.then(res => {
      commit("GET_THIRD_PLATFORM_LIST", res);
    });
  },
  addThirdPlatfrom({ commit }, data) {
    let p = addThirdPlatfrom(data);
    p.then(res => {
      res;
      store.dispatch("getThirdPlatfromList", {
        pageSize: 10,
        pageNo: 1
      });
    });
  },
  deleteThirdPlatfrom({ commit }, data) {
    let p = deleteThirdPlatfrom(data);
    p.then(res => {
      if (res.code == 200) {
        Vue.prototype.$message({
          type: "success",
          message: "删除成功!"
        });
      }
      store.dispatch("getThirdPlatfromList", {
        pageSize: 10,
        pageNo: 1
      });
    });
  },
  getTreeList({ commit }, data) {
    let p = getTreeList(data)
    p.then(res => {
      commit("GET_THIRD_TREE_LIST", res);
    })
  },
  getThirdVideoList( {commit}, data) {
    let p = getThirdVideoList(data)
    p.then(res => {
      commit("GET_THIRD_VIDEO_LIST", res)
    })
  }
};

const mutations = {
  GET_DI_MENULIST(...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
  },
  GET_VIDEO_LIST(state, res) {
    state.videoList = res.data.pageContent;
    state.videoListTotal = res.data.totalCount;
  },
  GET_BRAND_LIST(state, res) {
    state.brandList = res.data;
  },
  SET_DIALOG_FORM(state, res) {
    state.diDialogFormVisible = res;
  },
  GET_DISTRICT_LIST(state, res) {
    state.districtList = res.data;
  },
  DRAW_PLOYGON(state, res) {
    state;
    if (res == "huan") {
      store.commit("DRAW_HUAN");
    } else {
      store.commit("DRAW_HUAN");
    }
  },
  GET_DEVICE_CAMERA_LIST(state, res) {
    res.pageContent.forEach(item => {
      if (item.direction !== null && item.direction.length > 0) {
        item.latlng = item.direction[0].lat + "," + item.direction[0].lng;
      }
    });
    state.deviceCameraList = res.pageContent;

    state.deviceCameraListTotal = res.totalCount;
  },
  GET_DEVICE_CAMERA_PIC(state, res) {
    state.devicePic = res;
  },
  GET_THIRD_PLATFORM_LIST(state, res) {
    state.thirdPlatfromList = res.data.pageContent;
    state.thirdPlatfromTotalCount = res.data.totalCount;
  },
  GET_THIRD_TREE_LIST(state, res) {
    state.thirdPlatformTree = res.data
  },
  GET_THIRD_VIDEO_LIST(state, res) {
    state.thirdVideoList = res.data.pageContent
    state.thirdVideoListTotal = res.data.totalCount
    state.thirdVideoList.forEach(item => {
      item.tag = item.tags.join(',')
    })
  }
};

export default {
  state,
  actions,
  mutations
};
