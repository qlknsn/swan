import {
    getYaosuManageList,
    getYaosuDetail,
    updateYaosuDetail,
    deleteYaosuDetail,
} from '@/store/api/starfish/manageFactor/yaosuManage'
import {
    GET_YAOSU_MANAGE_LIST,
    GET_YAOSU_DETAIL,
    UPDATE_YAOSU_DETAIL,
    DELETE_YAOSU_DETAIL,
} from '@/store/mutation-types/starfish-mutations';
import router from '@/router/index';
import Vue from 'vue';
const state = {
    yaosuList: [],
    yaosuListParams: {
        page: 1,
        limit: 10,
        total: 0,
    },
    yaosuDetail: '',
}
const actions = {
    deleteYaosuDetail({
        commit
    }, data) {
        let p = deleteYaosuDetail(data)
        p.then(res => {
            commit("DELETE_YAOSU_DETAIL", res)
        })
    },
    updateYaosuDetail({
        commit
    }, data) {
        let p = updateYaosuDetail(data)
        p.then(res => {
            commit("UPDATE_YAOSU_DETAIL", res)
        })
    },
    getYaosuDetail({
        commit
    }, data) {
        let p = getYaosuDetail(data)
        p.then(res => {
            commit('GET_YAOSU_DETAIL', res.data)
        })
    },
    getYaosuManageList({
        commit
    }, data) {
        let p = getYaosuManageList(data)
        p.then(res => {
            commit('GET_YAOSU_MANAGE_LIST', res.data)
        })
    }
}
const mutations = {
    [DELETE_YAOSU_DETAIL](...arg) {
        let [, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                type: 'success',
                message: '删除成功'
            })
            setTimeout(() => {
                router.push('/yaosu/manage')
            }, 1500);
        } else {
            Vue.prototype.$message({
                type: 'error',
                message: msg
            })
        }
    },
    [UPDATE_YAOSU_DETAIL](...arg) {
        let [, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                type: 'success',
                message: '更新成功'
            })
            setTimeout(() => {
                router.push('/yaosu/manage')
            }, 1500);
        } else {
            Vue.prototype.$message({
                type: 'error',
                message: msg
            })
        }
    },
    [GET_YAOSU_DETAIL](state, data) {
        state.yaosuDetail = data
    },
    [GET_YAOSU_MANAGE_LIST](...arg) {
        let [state, {
            list,
            page,
            totalCount,
            limit
        }] = [...arg]
        state.yaosuList = list.map((item, index) => {
            Object.assign(item, {
                index: (page - 1) * limit + (index + 1)
            })
            return item
        })
        Object.assign(state.yaosuListParams, {
            page: page,
            total: totalCount,
            limit: limit
        })
    }
}


export default {
    state,
    actions,
    mutations
}