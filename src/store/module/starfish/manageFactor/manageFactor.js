import {
    getAssociationRole,
    getTaskTypeList,
    changeTaskTypeStatus,
    getCompanyTaskObject,
    getFieldTaskObject,
    getHouseTaskObject,
    getPeopleTaskObject,
    getGroupTaskObject,
    addTaskType,
    getYaosuList,
    getTaskTypeDetail,
    deleteTaskType,
    updateTaskType,
    getManageFeatureList,
    getManageFactorDepList,
    addYaosu,
} from '@/store/api/starfish/manageFactor/manageFactor'
import {
    GET_TASK_TYPELIST,
    CLEAR_TASK_TYPELIST_PARAMS,
    GET_COMPANY_TASK_OBJECT,
    GET_FIELD_TASK_OBJECT,
    GET_HOUSE_TASK_OBJECT,
    GET_PEOPLE_TASK_OBJECT,
    GET_GROUP_TASK_OBJECT,
    ADD_TASK_TYPE,
    GET_YAOSU_LIST,
    GET_TASK_TYPE_DETAIL,
    DELETE_TASK_TYPE,
    UPDATE_TASK_TYPE,
    GET_MANAGE_FEATURE_LIST,
    GET_MANAGE_FACTOR_DEP_LIST,
    ADD_YAOSU,
    GET_ASSOCIATION_ROLE,
} from '@/store/mutation-types/starfish-mutations'
import Vue from 'vue';
import router from '@/router/index'
const state = {
    roleList: [],
    depList: [],
    manageFeatureSearchParams: {
        page: 1,
        limit: 10,
        total: 0
    },
    companyTaskObjectParams: {
        page: 1,
        limit: 10,
        total: 0
    },
    fieldTaskObjectParams: {
        page: 1,
        limit: 10,
        total: 0
    },
    houseTaskObjectParams: {
        page: 1,
        limit: 10,
        total: 0
    },
    peopleTaskObjectParams: {
        page: 1,
        limit: 10,
        total: 0
    },
    groupTaskObjectParams: {
        page: 1,
        limit: 10,
        total: 0
    },
    taskTypeListParams: {
        page: 1,
        limit: 10,
        name: '',
        targetName: '',
        total: 0
    },
    taskTypeList: [],
    taskObjectList: [],
    yaosuList: [],
    taskTypeDetail: '',
    manageFeatureList: [],
}
const actions = {
    getAssociationRole({
        commit
    }, data) {
        let p = getAssociationRole(data)
        p.then(res => {
            commit('GET_ASSOCIATION_ROLE', res.data)
        })
    },
    addYaosu({
        commit
    }, data) {
        let p = addYaosu(data)
        p.then(res => {
            commit('ADD_YAOSU', res)
        })
    },
    getManageFactorDepList({
        commit
    }, data) {
        let p = getManageFactorDepList(data)
        p.then(res => {
            commit('GET_MANAGE_FACTOR_DEP_LIST', res.data)
        })
    },
    getManageFeatureList({
        commit
    }, data) {
        let p = getManageFeatureList(data)
        p.then(res => {
            commit("GET_MANAGE_FEATURE_LIST", res.data)
        })
    },
    updateTaskType({
        commit
    }, data) {
        let p = updateTaskType(data)
        p.then(res => {
            commit("UPDATE_TASK_TYPE", res)
        })
    },
    deleteTaskType({
        commit
    }, data) {
        let p = deleteTaskType(data)
        p.then(res => {
            commit("DELETE_TASK_TYPE", res)
        })
    },
    getTaskTypeDetail({
        commit
    }, data) {
        let p = getTaskTypeDetail(data)
        p.then(res => {
            commit('GET_TASK_TYPE_DETAIL', res)
        })
    },
    getYaosuList({
        commit
    }, data) {
        let p = getYaosuList(data)
        p.then(res => {
            commit('GET_YAOSU_LIST', res.data)
        })
    },
    addTaskType({
        commit
    }, data) {
        let p = addTaskType(data)
        p.then(res => {
            commit('ADD_TASK_TYPE', res)
        })
    },
    getCompanyTaskObject({
        commit
    }, data) {
        let p = getCompanyTaskObject(data)
        p.then(res => {
            commit('GET_COMPANY_TASK_OBJECT', res.data)
        })
    },
    getFieldTaskObject({
        commit
    }, data) {
        let p = getFieldTaskObject(data)
        p.then(res => {
            commit('GET_FIELD_TASK_OBJECT', res.data)

        })
    },
    getHouseTaskObject({
        commit
    }, data) {
        let p = getHouseTaskObject(data)
        p.then(res => {
            commit('GET_HOUSE_TASK_OBJECT', res.data)

        })
    },
    getPeopleTaskObject({
        commit
    }, data) {
        let p = getPeopleTaskObject(data)
        p.then(res => {
            commit('GET_PEOPLE_TASK_OBJECT', res.data)

        })
    },
    getGroupTaskObject({
        commit
    }, data) {
        let p = getGroupTaskObject(data)
        p.then(res => {
            commit('GET_GROUP_TASK_OBJECT', res.data)

        })
    },
    // 启用，禁用任务
    changeTaskTypeStatus({
        commit
    }, data) {
        commit;
        let p = changeTaskTypeStatus(data)
        p.then(res => {
            if (res.code == 200) {
                Vue.prototype.$message({
                    message: '操作成功',
                    type: 'success'
                })
            }
        })
    },
    // 获取任务类型列表
    getTaskTypeList({
        commit
    }, data) {
        let p = getTaskTypeList(data)
        p.then(res => {
            commit('GET_TASK_TYPELIST', res.data)
        })
    }
}
const mutations = {
    [GET_ASSOCIATION_ROLE](state, data) {
        state.roleList = data
    },
    [ADD_YAOSU](...arg) {
        let [, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                type: 'success',
                message: '添加成功'
            })
            setTimeout(() => {
                router.push('/yaosu/manage');
            }, 1500);
        } else {
            Vue.prototype.$message({
                type: 'error',
                message: msg
            })
        }
    },
    [GET_MANAGE_FACTOR_DEP_LIST](state, data) {
        state.depList = data
    },
    [GET_MANAGE_FEATURE_LIST](...arg) {
        let [state, {
            list,
            page,
            limit,
            totalCount
        }] = [...arg]
        state.manageFeatureList = list.map((item, index) => {
            Object.assign(item, {
                index: (page - 1) * limit + (index + 1)
            })
            return item;
        })
        Object.assign(state.manageFeatureSearchParams, {
            page: page,
            limit: limit,
            total: totalCount
        })
    },
    [UPDATE_TASK_TYPE](...arg) {
        let [, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                type: 'success',
                message: '更新成功'
            })
            setTimeout(() => {
                // router.push('/task/type/manage')
                window.location.reload();
            }, 1000);
        } else {
            Vue.prototype.$message({
                type: 'error',
                message: msg
            })
        }
    },
    [DELETE_TASK_TYPE](...arg) {
        let [, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                type: 'success',
                message: '删除成功'
            })
            setTimeout(() => {
                router.push('/task/type/manage')
            }, 1500);
        } else {
            Vue.prototype.$message({
                type: 'error',
                message: msg
            })
        }
    },
    [GET_TASK_TYPE_DETAIL](...arg) {
        let [state, {
            data
        }] = [...arg]
        state.taskTypeDetail = data
    },
    [GET_YAOSU_LIST](...arg) {
        let [state, data] = [...arg]
        state.yaosuList = data
    },
    [ADD_TASK_TYPE](...arg) {
        let [, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                message: '添加成功',
                type: 'success'
            })
            setTimeout(() => {
                router.push('/task/type/manage')
            }, 1500);
        } else {
            Vue.prototype.$message.error(msg)
        }
    },
    [GET_COMPANY_TASK_OBJECT](...arg) {
        let [state, {
            list,
            totalCount,
        }] = [...arg]
        list.forEach(item => {
            Object.assign(item, {
                checked: false
            })
        })
        state.taskObjectList = list
        Object.assign(state.companyTaskObjectParams, {
            total: totalCount
        })
    },
    [GET_FIELD_TASK_OBJECT](...arg) {
        let [state, {
            list,
            totalCount,
        }] = [...arg]
        list.forEach(item => {
            Object.assign(item, {
                checked: false
            })
        })
        state.taskObjectList = list
        Object.assign(state.companyTaskObjectParams, {
            total: totalCount
        })
    },
    [GET_HOUSE_TASK_OBJECT](...arg) {
        let [state, {
            list,
            totalCount,
        }] = [...arg]
        list.forEach(item => {
            Object.assign(item, {
                checked: false
            })
        })
        state.taskObjectList = list
        Object.assign(state.companyTaskObjectParams, {
            total: totalCount
        })
    },
    [GET_PEOPLE_TASK_OBJECT](...arg) {
        let [state, {
            list,
            totalCount,
        }] = [...arg]
        list.forEach(item => {
            Object.assign(item, {
                checked: false
            })
        })
        state.taskObjectList = list
        Object.assign(state.companyTaskObjectParams, {
            total: totalCount
        })
    },
    [GET_GROUP_TASK_OBJECT](...arg) {
        let [state, {
            list,
            totalCount,
        }] = [...arg]
        list.forEach(item => {
            Object.assign(item, {
                checked: false
            })
        })
        state.taskObjectList = list
        Object.assign(state.companyTaskObjectParams, {
            total: totalCount
        })
    },
    [GET_TASK_TYPELIST](...arg) {
        let [state, {
            page,
            totalCount,
            list,
            limit
        }] = [...arg]
        state.taskTypeListParams.total = totalCount
        state.taskTypeList = list.map((item, index) => {
            Object.assign(item, {
                index: (page - 1) * limit + (index + 1)
            })
            Object.assign(item, {
                status: item.status == 1 ? true : false
            })
            return item
        })
    },
    [CLEAR_TASK_TYPELIST_PARAMS](state) {
        Object.assign(state.taskTypeListParams, {
            page: 1,
            limit: 10,
            name: '',
            targetName: '',
            total: 0
        })
    },
}

export default {
    state,
    actions,
    mutations
}