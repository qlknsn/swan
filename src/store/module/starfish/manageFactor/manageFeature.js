import {
    addManageFeature,
    getManageFeatureDetail,
    deleteManageFeatureDetail,
    getExistManageFeatureSettings,
    updateManageFeatureDetail,
    setManageFeaturePosition,
} from '@/store/api/starfish/manageFactor/manageFeature'
import {
    ADD_MANAGE_FEATURE,
    GET_MANAGE_FEATURE_DETAIL,
    UPDATE_MANAGE_FEATURE_DETAIL,
    DELETE_MANAGE_FEATURE_DETAIL,
    GET_EXIST_MANAGE_FEATURE_SETTINGS,
    SET_MAANGE_FEATURE_POSITION,
} from '@/store/mutation-types/starfish-mutations'
import store from '@/store/index'
import Vue from 'vue';

const state = {
    manageFeatureSortableList: [],
    manageFeatureDetail: null,
    manageFeatureList: [],
    getManageFeatureParams: {
        page: 1,
        limit: 10,
        total: 0,
    }
}
const actions = {
    setManageFeaturePosition({
        commit
    }, data) {
        let p = setManageFeaturePosition(data)
        p.then(res => {
            commit('SET_MAANGE_FEATURE_POSITION', res)
        })
    },
    getExistManageFeatureSettings({
        commit
    }) {
        let p = getExistManageFeatureSettings()
        p.then(res => {
            commit('GET_EXIST_MANAGE_FEATURE_SETTINGS', res.data)
        })
    },
    updateManageFeatureDetail({
        commit
    }, data) {
        let p = updateManageFeatureDetail(data)
        p.then(res => {
            commit('UPDATE_MANAGE_FEATURE_DETAIL', res)
            if (res.code == 200) {
                store.dispatch('getManageFeatureList', state.getManageFeatureParams)
            }
        })
    },
    deleteManageFeatureDetail({
        commit
    }, data) {
        let p = deleteManageFeatureDetail(data)
        p.then(res => {
            if (res.code == 200) {
                store.dispatch('getManageFeatureList', state.getManageFeatureParams)
            }
            commit('DELETE_MANAGE_FEATURE_DETAIL', res)
        })
    },
    getManageFeatureDetail({
        commit
    }, data) {
        let p = getManageFeatureDetail(data)
        p.then(res => {
            commit('GET_MANAGE_FEATURE_DETAIL', res.data)
        })
    },
    addManageFeature({
        commit,
        state
    }, data) {
        let p = addManageFeature(data)
        p.then(res => {
            commit('ADD_MANAGE_FEATURE', res)
            if (res.code == 200) {
                store.dispatch('getManageFeatureList', state.getManageFeatureParams)
            }
        })
    }
}
const mutations = {
    [SET_MAANGE_FEATURE_POSITION](...arg) {
        let [, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                type: 'success',
                message: '修改成功'
            })
        } else {
            Vue.prototype.$message({
                type: 'error',
                message: msg
            })
        }
    },
    [GET_EXIST_MANAGE_FEATURE_SETTINGS](state, data) {
        state.manageFeatureSortableList = data
    },
    [DELETE_MANAGE_FEATURE_DETAIL](...arg) {
        let [, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                type: 'success',
                message: '删除成功'
            })
        } else {
            Vue.prototype.$message({
                type: 'error',
                message: msg
            })
        }
    },
    [UPDATE_MANAGE_FEATURE_DETAIL](...arg) {
        let [, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                type: 'success',
                message: '更新成功'
            })
        } else {
            Vue.prototype.$message({
                type: 'error',
                message: msg
            })
        }
    },
    [GET_MANAGE_FEATURE_DETAIL](state, data) {
        state.manageFeatureDetail = data
    },
    [ADD_MANAGE_FEATURE](...arg) {
        let [, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                type: 'success',
                message: '添加成功'
            })
        } else {
            Vue.prototype.$message({
                type: 'error',
                message: msg
            })
        }
    },
}


export default {
    state,
    actions,
    mutations,
}