import {
    getVisitRecordList,
    getVisitRecordDetail
} from '@/store/api/starfish/visitRecord/visitRecord'



import {
    GET_VISIT_RECORD_LIST,
    GET_VISIT_RECORD_DETAIL
} from '@/store/mutation-types/starfish-mutations'

const state = {
    visitRecordList: [],
    visitRecordDetail: '',
    visitRecordSearchParams: {
        page: 1,
        limit: 10,
        total: 0,
    }
}
const actions = {
    getVisitRecordList({
        commit
    }, data) {
        let p = getVisitRecordList(data)
        p.then(res => {
            commit('GET_VISIT_RECORD_LIST', res.data)
        })
    },
    getVisitRecordDetail({
        commit
    }, data) {
        let p = getVisitRecordDetail(data)
        p.then(res => {
            commit("GET_VISIT_RECORD_DETAIL", res.data)
        })
    },
}
const mutations = {
    [GET_VISIT_RECORD_LIST](...arg) {
        let [state, {
            list
        }] = [...arg]
        state.visitRecordList = list
    },
    [GET_VISIT_RECORD_DETAIL](...arg) {
        arg
    },
}


export default {
    state,
    actions,
    mutations,
}