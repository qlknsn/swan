import {
    districtCorporationList,
    corporationDelete,
    findRooms,
    addCompany,
    singleCompany,
    updateCompany,
    shopList,
    deleteShopList,
    addShopList,
    updateShopList,
    singleShop,
    lvguanList,
    deleteLvguanList,
    singleLvguan,
    addLvguanList,
    updateLvguanList,
    minsuList,
    deleteMinsuList,
    singleMinsu,
    addMinsuList,
    updateMinsuList,
    corporationExport,
    shopExport,
    hotelExport,
    homestayExport
} from '@/store/api/starfish/company/company'
import {
    GET_COMPANY_LIST,
    GET_ROOMS_DATA,
    GET_SINGLE_COMPANY_INFO,
    GET_SHOP_LIST,
    DELETE_SHOP_LIST,
    GET_SINGLE_SHOP_INFO,
    GET_LVGUAN_LIST,
    DELETE_LVGUAN_LIST,
    GET_SINGLE_LVGUAN_INFO,
    GET_MINSU_LIST,
    DELETE_MINSU_LIST,
    GET_SINGLE_MINSU_INFO
} from '@/store/mutation-types/starfish-mutations'
// import Vue from 'vue';
import router from '@/router/index'

const state = {
    companyList: [],
    companyTotalcount: 0,
    roomInfo: [],
    singleCompanyInfo: {},
    shopList:[],
    shopTotalcount:0,
    singleShopInfo:{},
    lvguanTotalcount:0,
    lvguanList:[],
    minsuList:[],
    minsuTotalcount:0,
    singleMinsuInfo:{}
}
const actions = {
    districtCorporationList({
        commit
    }, data) {
        let p = districtCorporationList(data)
        p.then(res => {
            commit(GET_COMPANY_LIST, res)
        })
    },
    corporationDelete({
        commit
    }, data,index) {
        let p = corporationDelete(data)
        p.then(res => {
            if(res.code==200){
              commit('DELETE_COMPANY_LIST',index)  
            }
        })
    },
    findRooms({
        commit
    }, params) {
        let p = findRooms(params)
        p.then(res => {
            commit('GET_ROOMS_DATA', res.data)
        })
    },
    addCompany({
        commit
    }, params) {
        let p = addCompany(params)
        p.then(res => {
            commit;
            if (res.code == 200) {
                router.push('baseCompany')
            }
        })
    },
    singleCompany({
        commit
    }, params) {
        let p = singleCompany(params)
        p.then(res => {
            commit(GET_SINGLE_COMPANY_INFO, res.data)
        })
    },
    updateCompany({
        commit
    }, params) {
        let p = updateCompany(params)
        p.then(res => {
            commit;
            if (res.code == 200) {
                router.push('baseCompany')
            }
        })
    },
    shopList({commit},params){
        let p = shopList(params)
        p.then(res=>{
            commit(GET_SHOP_LIST,res.data)
        })
    },
    deleteShopList({commit},params,index){
        let p = deleteShopList(params)
        p.then(res=>{
            if(res.code==200){
                commit(DELETE_SHOP_LIST,index)  
            }
        })
    },
    addShopList({
        commit
    }, params) {
        let p = addShopList(params)
        p.then(res => {
            commit;
            if (res.code == 200) {
                router.push('baseCompany')
            }
        })
    },
    updateShopList({
        commit
    }, params) {
        let p = updateShopList(params)
        p.then(res => {
            commit;
            if (res.code == 200) {
                router.push('baseCompany')
            }
        })
    },
    singleShop({
        commit
    }, params) {
        let p = singleShop(params)
        p.then(res => {
            commit(GET_SINGLE_SHOP_INFO, res.data)
        })
    },
    lvguanList({commit},params){
        let p = lvguanList(params)
        p.then(res=>{
            commit(GET_LVGUAN_LIST,res.data)
        })
    },
    deleteLvguanList({commit},params,index){
        let p = deleteLvguanList(params)
        p.then(res=>{
            if(res.code==200){
                commit(DELETE_LVGUAN_LIST,index)  
            }
        })
    },
    singleLvguan({
        commit
    }, params) {
        let p = singleLvguan(params)
        p.then(res => {
            commit(GET_SINGLE_LVGUAN_INFO, res.data)
        })
    },
    addLvguanList({
        commit
    }, params) {
        let p = addLvguanList(params)
        p.then(res => {
            commit;
            if (res.code == 200) {
                router.push('baseCompany')
            }
        })
    },
    updateLvguanList({
        commit
    }, params) {
        let p = updateLvguanList(params)
        p.then(res => {
            commit;
            if (res.code == 200) {
                router.push('baseCompany')
            }
        })
    },
    minsuList({commit},params){
        let p = minsuList(params)
        p.then(res=>{
            commit(GET_MINSU_LIST,res.data)
        })
    },
    deleteMinsuList({commit},params,index){
        let p = deleteMinsuList(params)
        p.then(res=>{
            if(res.code==200){
                commit(DELETE_MINSU_LIST,index)  
            }
        })
    },
    singleMinsu({
        commit
    }, params) {
        let p = singleMinsu(params)
        p.then(res => {
            commit(GET_SINGLE_MINSU_INFO, res.data)
        })
    },
    addMinsuList({
        commit
    }, params) {
        let p = addMinsuList(params)
        p.then(res => {
            commit;
            if (res.code == 200) {
                router.push('baseCompany')
            }
        })
    },
    updateMinsuList({
        commit
    }, params) {
        let p = updateMinsuList(params)
        p.then(res => {
            commit;
            if (res.code == 200) {
                router.push('baseCompany')
            }
        })
    },
    corporationExport(params){
        let p = corporationExport(params)
        p.then(res=>{
            const url = window.URL.createObjectURL(new Blob([res], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}))
            const link = document.createElement('a')
            link.style.display = 'none'
            link.href = url
            link.setAttribute('download', 'excel.xlsx')
            document.body.appendChild(link)
            link.click()
            document.body.removeChild(link)
        })
    },
    shopExport(params){
        let p = shopExport(params)
        p.then(res=>{
            const url = window.URL.createObjectURL(new Blob([res], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}))
            const link = document.createElement('a')
            link.style.display = 'none'
            link.href = url
            link.setAttribute('download', 'excel.xlsx')
            document.body.appendChild(link)
            link.click()
            document.body.removeChild(link)
        })
    },
    hotelExport(params){
        let p = hotelExport(params)
        p.then(res=>{
            const url = window.URL.createObjectURL(new Blob([res], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}))
            const link = document.createElement('a')
            link.style.display = 'none'
            link.href = url
            link.setAttribute('download', 'excel.xlsx')
            document.body.appendChild(link)
            link.click()
            document.body.removeChild(link)
        })
    },
    homestayExport(params){
        let p = homestayExport(params)
        p.then(res=>{
            const url = window.URL.createObjectURL(new Blob([res], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}))
            const link = document.createElement('a')
            link.style.display = 'none'
            link.href = url
            link.setAttribute('download', 'excel.xlsx')
            document.body.appendChild(link)
            link.click()
            document.body.removeChild(link)
        })
    },

}
const mutations = {
    [GET_SINGLE_MINSU_INFO](state,res){
        state.singleMinsuInfo = res
    },
    [DELETE_MINSU_LIST](state,index){
        state.minsuList.splice(index, 1);
    },
    [GET_MINSU_LIST](state,res){
        state.minsuTotalcount = res.totalCount
        state.minsuList = res.list
    },
    [GET_SINGLE_LVGUAN_INFO](state,res){
        state.singleLvguanInfo = res
    },
    [DELETE_LVGUAN_LIST](state,index){
        state.lvguanList.splice(index, 1);
    },
    [GET_LVGUAN_LIST](state,res){
        state.lvguanTotalcount = res.totalCount
        state.lvguanList = res.list
    },
    [GET_SINGLE_SHOP_INFO](state,res){
        state.singleShopInfo = res
    },
    [DELETE_SHOP_LIST](state,index){
        state.shopList.splice(index, 1);
    },
    [GET_SHOP_LIST](state,res){
        state.shopTotalcount = res.totalCount
        state.shopList = res.list
    },
    [GET_COMPANY_LIST](state, res) {
        state.companyTotalcount = res.data.totalCount
        state.companyList = res.data.list
    },
    [GET_ROOMS_DATA](state, res) {
        state.roomInfo = res
    },
    [GET_SINGLE_COMPANY_INFO](state, res) {
        state.singleCompanyInfo = res
    },
    DELETE_COMPANY_LIST(state,index){
        state.companyList.splice(index, 1);
    }
}


export default {
    state,
    actions,
    mutations
}