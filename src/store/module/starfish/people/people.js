import {
    GET_BASIC_PEOPLE,GET_ALL_DICTS,ADD_BASIC_PEOPLE,
    DELETE_BASIC_PEOPLE,BASIC_PEOPLE_DETAIL,EDIT_PEOPLE_DETAIL,
    GET_IMPORTANT_PEOPLE,DELETE_IMPORTANT_PEOPLE,ADD_IMPORTANT_PEOPLE,
    GET_IMPORTANT_PEOPLE_DETAIL,EDIT_IMPORTANT_PEOPLE,GET_CARE_PEOPLE_LIST,
    DELETE_CARE_PEOPLE,ADD_CARE_PEOPLE,EDIT_CARE_PEOPLE,CARE_PEOPLE_DETAIL,
    GET_PARTY_PEOPLE_LIST,ADD_PARTY_PEOPLE,DELETE_PARTY_PEOPLE,EDIT_PARTY_PEOPLE,
    GET_PARTY_PEOPLE_DETAIL,GET_PARTY_ORGANIZATION

} from '../../../mutation-types/starfish-mutations'
import {
    getBasicPeople,getAllDicts,addBasicPeople,deleteBasicPeople,
    basicPeopleDetail,editPeopleDetail,getImportantPeopleList,
    deleteImportantPeople,addImportantPeople,getImportantPeopleDetail,
    editImportantPeople,getCarePeoppleList,deleteCarePeople,addCarePeople,
    editCarePeople,getCarePeopleDetail,getPartyPeopleList,addPartyPeople,
    deletePartyPeople,editPartyPeople,getPartyPeopleDetail,getPartyorganization
} from '../../../api/starfish/people/people'
import store from '@/store/index'
import {Message} from 'element-ui'
import routes from '@/router/index'
const state = {
    getBasicPeople:{},           //实有人口列表
    getImportantPeople:{},       //重点人员列表
    getCarePeoppleList:{},       //关爱人员列表
    getPartyPeopleList:{},       //党员列表

    getAllDicts:{               //全部的字典项查询
        ADMIN_LEVEL :[],    
        BUILDING_TYPE  :[],   
        RESIDENT_TYPE :[],
        GENDER :[],
        ID_TYPE :[],
        EDUCATION :[],
        RELATIONSHIP :[],
        PERSON_TYPE :[],
        NATION :[],
        SPECIAL_TYPE :[],
        CARE_TYPE :[],
        PART_TPYE :[],
        SITUATION :[],
        INSTALLATION :[],
    },

    basicPeopleDetail:{}        ,     //实有人口详情
    importantPeopleDetail:{},          //重点人员详情
    carePeopleDetail:{},               //关爱人员详情
    partyPeopleDetail:{} ,            //党员详情
    partyOrganization:{},             //党支部(下拉框)
}
const actions = {
    getBasicPeople({commit}, params) { //获取实有人口数据列表
        let p = getBasicPeople(params);
        p.then(res => {
            commit(GET_BASIC_PEOPLE, res)
        })
    },

    getAllDicts({commit},params){                         //获取全部字典项查询
        let p = getAllDicts(params);
        p.then(res =>{
            commit(GET_ALL_DICTS,res)
        })
    },

    addBasicPeople({commit},params){             //添加实有人口
        let p = addBasicPeople(params);
        p.then(res => {
            commit(ADD_BASIC_PEOPLE,res)
        })
    },

    deleteBasicPeople({commit},params){           //删除某一条实有人口
        let p = deleteBasicPeople(params);
        p.then(res =>{
            commit(DELETE_BASIC_PEOPLE,res);
        })
    },

    basicPeopleDetail({commit},params){         //获取单个实有人口详情
        let p = basicPeopleDetail(params);
        p.then(res =>{
            commit(BASIC_PEOPLE_DETAIL,res)
        })
    },

    editPeopleDetail({commit},params){         //编辑实有人口
        let p = editPeopleDetail(params);
        p.then(res =>{
            commit(EDIT_PEOPLE_DETAIL,res)
        })
    },

    getImportantPeople({commit},params){            //获取重点人员列表
        let p = getImportantPeopleList(params);
        p.then(res =>{
            commit(GET_IMPORTANT_PEOPLE,res);
        })
    },

    deleteImportantPeople({commit},params){             //删除重点人员
        let p = deleteImportantPeople(params);
        p.then(res =>{
            commit(DELETE_IMPORTANT_PEOPLE,res);
        })
    },

    addImportantPeople({commit},params){                     //添加重点人人员
        let p = addImportantPeople(params);
        p.then(res =>{
            commit(ADD_IMPORTANT_PEOPLE,res)
        })
    },


    getImportantPeopleDetail({commit},params){                //获取重点人员详情
        let p = getImportantPeopleDetail(params);
        p.then(res =>{
            commit(GET_IMPORTANT_PEOPLE_DETAIL,res)
        })
    },


    editImportantPeople({commit},params){                            //编辑重点人员
        let p = editImportantPeople(params);
        p.then(res => {
            commit(EDIT_IMPORTANT_PEOPLE,res)
        })
    },

    getCarePeopleList({commit},params){                  //获取关爱人员列表
        let p = getCarePeoppleList(params);
        p.then(res =>{
            commit(GET_CARE_PEOPLE_LIST,res)
        })
    },

    deleteCarePeople({commit},params){                             //删除关爱人员
        let p = deleteCarePeople(params);
        p.then(res =>{
            commit(DELETE_CARE_PEOPLE,res)
        })
    },

    addCarePeople({commit},params){                              //添加关爱人员
        let p = addCarePeople(params);
        p.then(res =>{
            commit(ADD_CARE_PEOPLE,res)
        })
    },

    editCarePeople({commit},params){                           //编辑关爱人员
        let p = editCarePeople(params);
        p.then(res =>{
            commit(EDIT_CARE_PEOPLE,res)
        })
    },

    getCarePeopleDetail({commit},params){                         //关爱人员详情
        let p = getCarePeopleDetail(params);
        p.then(res =>{
            commit(CARE_PEOPLE_DETAIL,res)
        })
    },


    getPartyPeopleList({commit},params){                     //获取党员列表
        let p = getPartyPeopleList(params); 
        p.then(res =>{
            commit(GET_PARTY_PEOPLE_LIST,res);
        })
    },


    addPartyPeople({commit},params){                         //添加党员
        let p = addPartyPeople(params);
        p.then(res =>{
            commit(ADD_PARTY_PEOPLE,res);
        })
    },

    deletePartyPeople({commit},params){                   //删除党员
        let p = deletePartyPeople(params);
        p.then(res =>{
            commit(DELETE_PARTY_PEOPLE,res);
        })
    },

    editPartyPeople({commit},params){                     //编辑党员
        let p = editPartyPeople(params);
        p.then(res =>{
            commit(EDIT_PARTY_PEOPLE,res)
        })
    },

    getPartyPeopleDetail({commit},params){                 //获取党员详情
        let p = getPartyPeopleDetail(params);
        p.then(res =>{
            commit(GET_PARTY_PEOPLE_DETAIL,res)
        })
    },

    getPartyorganization({commit},params){
        let p = getPartyorganization(params);
        p.then(res =>{
            commit(GET_PARTY_ORGANIZATION,res);
        })
    }






}
const mutations = {
    [GET_BASIC_PEOPLE](state, res) {
        state.getBasicPeople = res.data; //获取实有人口数据列表
    },

    [GET_ALL_DICTS](state,res){              //全部的字典项查询
        state.getAllDicts = res.data;  
    },

    [ADD_BASIC_PEOPLE](state,res){         //添加实有人口
        if(res.code == 200){
            Message.success('添加成功');
            routes.push('/tabs?tab=first');
        }else{
            Message.warning(res.message)
        }
    },

    [DELETE_BASIC_PEOPLE](state,res){     //删除某一条实有人口
        if(res.data == true){
            Message.success('删除成功');
            store.dispatch('getBasicPeople',{page:1,limit:7})
        }else{
            Message.warning(res.message);
        }
    },

    [BASIC_PEOPLE_DETAIL](state,res){     //获取实有人口详情
        state.basicPeopleDetail = res;
    },

    [EDIT_PEOPLE_DETAIL](state,res){     //编辑实有人口
        if(res.data == true){
            Message.success('更新成功');
            routes.push('/tabs?tab=first');
        }else{
            Message.warning(res.message)
        }
    },


    [GET_IMPORTANT_PEOPLE](state,res){     //获取重点人员列表
        state.getImportantPeople = res.data;
    },

    [DELETE_IMPORTANT_PEOPLE](state,res){      //删除重点人员
        if(res.data == true){
            Message.success('删除成功');
            store.dispatch('getImportantPeople',{page:1,limit:7})
        }else{
            Message.warning(res.message)
        }
    },

    [ADD_IMPORTANT_PEOPLE](state,res){        //添加重点人员
        if(res.data == true){
            Message.success('添加成功');
            routes.push({path:'/tabs?tab=second'});
        }else{
            Message.warning(res.message)
        }
    },

    [GET_IMPORTANT_PEOPLE_DETAIL](state,res){    //重点人员详情
        state.importantPeopleDetail = res;
    },


    [EDIT_IMPORTANT_PEOPLE](state,res){            //编辑重点人员
        if(res.data == true){
            Message.success('更新成功');
            routes.push('/tabs?tab=second');
        }else{
            Message.warning(res.message)
        }
    },

    [GET_CARE_PEOPLE_LIST](state,res){            //获取关爱人员列表
        state.getCarePeoppleList = res.data;
    },

    [DELETE_CARE_PEOPLE](state,res){                      //删除关爱人员
        if(res.data == true){
            Message.success('删除成功');
            store.dispatch('getCarePeopleList',{page:1,limit:7})
        }else{
            Message.warning(res.message)
        }
    },

    [ADD_CARE_PEOPLE](state,res){                      //添加关爱人员
        if(res.data == true){
            Message.success('添加成功');
            routes.push('/tabs?tab=third');
        }else{
            Message.warning(res.message)
        }
    },

    [EDIT_CARE_PEOPLE](state,res){                     //编辑关爱人员
        if(res.data == true){
            Message.success('更新成功');
            routes.push('/tabs?tab=third');
        }else{
            Message.warning(res.message)
        }
    },

    [CARE_PEOPLE_DETAIL](state,res){                  //关爱人员详情
        state.carePeopleDetail = res
    },


    [GET_PARTY_PEOPLE_LIST](state,res){              //获取党员列表
        state.getPartyPeopleList = res.data;
    },


    [ADD_PARTY_PEOPLE](state,res){                      //添加党员
        if(res.data == true){
            Message.success('添加成功');
            routes.push('/tabs?tab=fourth');
        }else{
            Message.warning(res.message)
        }
    },

    [DELETE_PARTY_PEOPLE](state,res){                   //删除党员
        if(res.data == true){
            Message.success('删除成功');
            store.dispatch('getPartyPeopleList',{page:1,limit:7})
        }else{
            Message.warning(res.message)
        }
    },

    [EDIT_PARTY_PEOPLE](state,res){                 //编辑党员
        if(res.data == true){
            Message.success('更新成功');
            routes.push('/tabs?tab=fourth');
        }else{
            Message.warning(res.message)
        }
    },
 
    [GET_PARTY_PEOPLE_DETAIL](state,res){     //党员详情
        state.partyPeopleDetail = res
    },

    [GET_PARTY_ORGANIZATION](state,res){      //获取党支部(下拉框)
        state.partyOrganization = res
    }

}

export default {
    state,
    actions,
    mutations
}