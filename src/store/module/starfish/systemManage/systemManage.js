import Vue from 'vue';
import {
    SAVE_ROLE_MENUS,
    GET_ACCOUNT_LIST,
    GET_DEPTS_LIST,
    GET_ROLES,
    GET_DISTRICT_LIST,
    GET_DISTRICT_INFO,
    GET_DEPT_LIST,
    GET_GROUP_LIST,
    GET_CHARACTER_LIST,
    GET_USER_DETAIL,
    GET_USER_LIST,
    GET_ROLE_MENUS,
    UPDATE_ROLE,
} from '../../../mutation-types/starfish-mutations'
import {
    accountList,
    accountGetDepts,
    getRoles,
    addUser,   
    districtList,
    districtSave,
    districtDetail,
    getDept,
    getGroup,
    getCharacter,
    getUserDetail,
    editUserDetail,
    getUserList,
    deleteUser,
    changePassword,
    getRoleMenus,
    saveRoleMenus,
    updateRole,
    districtEdit,
    districtDel,
    saveRole
} from '@/store/api/starfish/systemManage/systemManage'
import router from '@/router/index'
import store from '@/store/index'
import {Message} from 'element-ui'
const state = {
    getAccountlist: [], //用户列表
    AccountTotalpage: 0,
    roleList: [],
    shequliebiaoList: [],
    shequlistTotal: 0,
    getDeptlist: [],
    deptList: [],
    groupList: [],
    characterList: [],
    characterParams: {
        limit: 10,
        page: 1,
        total: 0
    },
    accountParams: {
        limit: 10,
        page: 1,
        total: 0
    },
    userParams: {
        limit: 10,
        page: 1,
    },
    userDetail: {},
    userList: [],
    districtInfo:{},
    roleMenuList: [],
    roleDefaultMenuList: [],
    roleTreeVisible: false,
    isShowLoading: false
}

const actions = {
    updateRole({
        commit
    }, data) {
        let p = updateRole(data)
        p.then(res => {
            commit('UPDATE_ROLE', res)
        })
    },
    saveRoleMenus({
        commit
    }, data) {
        let p = saveRoleMenus(data)
        p.then(res => {
            commit('SAVE_ROLE_MENUS', res)
        })
    },
    getRoleMenus({
        commit
    }, data) {
        let p = getRoleMenus(data)
        p.then(res => {
            commit("GET_ROLE_MENUS", res.data)
        })
    },
    accountList({
        commit
    }, params) {
        let p = accountList(params)
        p.then(res => {
            commit(GET_ACCOUNT_LIST, res.data)
        })
    },
    accountGetDepts({
        commit
    }, params) {
        let p = accountGetDepts(params)
        p.then(res => {
            commit(GET_DEPTS_LIST, res)
        })
    },
    getRoles({
        commit
    }, params) {
        let p = getRoles(params)
        p.then(res => {
            commit(GET_ROLES, res)
        })
    },
    addUser({
        commit
    }, params) {
        let p = addUser(params)
        p.then(res => {
            commit;
            if (res.code == 200) {
                Message.success('添加成功');
                if(params.position == '两委成员'){
                    router.push('/system/twoCommittees?position=两委成员')
                }else if(params.position == '自有力量'){
                    router.push('/system/ownPower?position=自由力量')
                }else if(params.position == '协同力量'){
                    router.push('/system/friendPower?position=协同力量')
                }
            }else{
                Message.error(res.msg);
            }
        })
    },
    districtList({
        commit
    }, params) {
        let p = districtList(params)
        p.then(res => {
            commit('GET_DISTRICT_LIST', res.data)
        })
    },
    districtSave({
        commit
    }, params) {
        commit;
        let p = districtSave(params)
        p.then(res => {
            res;
        })
    },
    districtDetail({
        commit
    }, params) {
        let p = districtDetail(params)
        p.then(res => {
            commit(GET_DISTRICT_INFO, res.data)
        })
    },
    getDept({
        commit
    }, params) {
        let p = getDept(params)
        p.then(res => {
            commit(GET_DEPT_LIST, res)
        })
    },
    getGroup({
        commit
    }, params) {
        let p = getGroup(params)
        p.then(res => {
            commit(GET_GROUP_LIST, res)
        })
    },
    getCharacter({
        commit
    }, params) {
        let p = getCharacter(params)
        p.then(res => {
            commit(GET_CHARACTER_LIST, res.data)
        })
    },
    getUserDetail({commit}, params) {
        state.isShowLoading = true
        let p = getUserDetail(params)
        p.then(res => {
            commit(GET_USER_DETAIL, res)
        })
    },
    editUserDetail({commit}, params) {
        let p = editUserDetail(params)
        p.then(res => {
            commit;
            if (res.code == 200) {
                Message.success('修改成功');
                if(params.position == '两委成员'){
                    router.push('/system/twoCommittees?position=两委成员')
                }else if(params.position == '自有力量'){
                    router.push('/system/ownPower?position=自有力量')
                }else if(params.position == '协同力量'){
                    router.push('/system/friendPower?position=协同力量')
                }
            } else{
                Message.error(res.msg);
            }
        })
    },
    getUserList({commit}, params, type) {
        state.isShowLoading = true
        let p = getUserList(params, type)
        p.then(res => {
            commit(GET_USER_LIST, res)
        })
    },
    deleteUser({commit}, params) {
        let p = deleteUser(params)
        p.then(res => {
            res;
            commit;
            state.userParams.position = params.type
            store.dispatch('getUserList',state.userParams)
        })
    },
    changePassword({commit}, params) {
        let p = changePassword(params)
        p.then(res => {
            res;
            commit;
        })
    },
    districtEdit({commit},params){
        commit;
        let p = districtEdit(params)
        p.then(res=>{
            res;
        })
    },
    districtDel({commit},params){
        commit;
        let p = districtDel(params)
        p.then(res=>{
            res;
        })
    },
    saveRole({commit},params){
        
        let p = saveRole(params)
        p.then(res=>{
            if(res.code ==200){
                commit('SAVE_ROLE')
            }
            
        })
    }
}

const mutations = {
    SAVE_ROLE(state,res){
        res;
        store.dispatch('getCharacter',{page:state.characterParams.page,limit:state.characterParams.limit})
    },
    [UPDATE_ROLE](...arg) {
        let [, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                type: 'success',
                message: '修改成功'
            })
            store.dispatch('getCharacter', {
                limit: 10,
                page: 1
            })
        } else {
            Vue.prototype.$message({
                type: 'error',
                message: msg
            })
        }
    },
    [SAVE_ROLE_MENUS](...arg) {
        let [state, {
            code,
            msg
        }] = [...arg]
        if (code == 200) {
            Vue.prototype.$message({
                type: 'success',
                message: '修改成功'
            })
            state.roleTreeVisible = false
        } else {
            Vue.prototype.$message({
                type: 'error',
                message: msg
            })
        }
    },
    [GET_ROLE_MENUS](state, data) {
        state.roleDefaultMenuList = data.authority
        state.roleMenuList = data.menus

    },
    [GET_ROLES](state, res) {
        state.roleList = res.data
    },
    [GET_ACCOUNT_LIST](...arg) {
        let [state, {
            list,
            page,
            totalCount
        }] = [...arg]
        Object.assign(state.accountParams, {
            page: page,
            total: totalCount
        })
        state.getAccountlist = list
        state.AccountTotalpage = totalCount
    },
    [GET_DEPTS_LIST](state, res) {
        state.getDeptlist = res.data
    },
    [GET_DISTRICT_LIST](state, res) {
        state.shequliebiaoList = res.list
        state.shequlistTotal = res.totalCount
    },
    [GET_DISTRICT_INFO](state, res) {
        state.districtInfo = res
    },
    [GET_DEPT_LIST](state, res) {
        state.deptList = res.data
    },
    [GET_GROUP_LIST](state, res) {
        state.groupList = res.data
    },
    [GET_CHARACTER_LIST](...arg) {
        let [state, {
            currPage,
            list,
            totalCount,
        }] = [...arg]
        state.characterParams.page = currPage
        state.characterParams.total = totalCount
        state.characterList = list
    },
    [GET_USER_DETAIL](state, res) {
        state.userDetail = res.data
        state.isShowLoading = false
    },
    [GET_USER_LIST](state, res) {
        state.userList = res.data
        state.isShowLoading = false
    }
}

export default {
    state,
    actions,
    mutations
}