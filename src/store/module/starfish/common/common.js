// starfish 登录模块API

import {
    getDicts
} from '@/store/api/starfish/common/common'
import {
    GET_DICTS
} from '@/store/mutation-types/starfish-mutations'
// import Vue from 'vue';
// import router from '@/router/index'

const state = {
    dictList: [],
}
const actions = {
    getDicts({
        commit
    }, data) {
        let p = getDicts(data)
        p.then(res => {
            commit('GET_DICTS', res.data)
        })
    }
}
const mutations = {
    [GET_DICTS](state, data) {
        state.dictList = data
    }
}


export default {
    state,
    actions,
    mutations
}