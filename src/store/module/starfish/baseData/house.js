// import Vue from 'vue';
import {
    GET_STREET_INFO,
} from '../../../mutation-types/starfish-mutations'
import {
    getXiaquInfo,
} from '@/store/api/starfish/baseData/house/house.js'
// import router from '@/router/index'
// import store from '@/store/index'
// import {Message} from 'element-ui'
const state = {
    streetInfo: {},
}

const actions = {
    getXiaquInfo({ commit }) {
        let p = getXiaquInfo()
        p.then(res => {
            commit('GET_STREET_INFO', res)
        })
    }
}

const mutations = {
    [GET_STREET_INFO](state, res) {
        state.streetInfo = res.data
    },
    // [GET_CHARACTER_LIST](...arg) {
    //     let [state, {
    //         currPage,
    //         list,
    //         totalCount,
    //     }] = [...arg]
    //     state.characterParams.page = currPage
    //     state.characterParams.total = totalCount
    //     state.characterList = list
    // },
}

export default {
    state,
    actions,
    mutations
}