// starfish 登录模块API

import {
    sfLoginUser,
    getUserName
} from '@/store/api/starfish/login/login'
import {
    SF_LOGIN_USER,
    USER_INFO
} from '@/store/mutation-types/starfish-mutations'
import Vue from 'vue';
import router from '@/router/index'
import store from '@/store/index'
const state = {
    userInfo: {}
}
const actions = {
    sfLoginUser({
        commit
    }, data) {
        let p = sfLoginUser(data)
        p.then(res => {
            store.dispatch('getUserName', res.auth.token)
            commit('SF_LOGIN_USER', res)
        })
    },
    getUserName({
        commit
    }, data) {
        let p = getUserName(data)
        p.then(res => {
            if (res.code === 200) {
                commit('USER_INFO', res)
            }
        })
    }
}
const mutations = {
    [SF_LOGIN_USER](state, data) {
        state;
        let d = data
        Vue.ls.set('token', d.auth.token)
    },
    [USER_INFO](state, data) {
        state;
        let d = data
        state.userInfo = d.data
        Vue.ls.set('menuList', d.data.uiConfiguration.menus)
        Vue.ls.set('userName', d.data.name)
        router.push('/sf/homepage')
        setTimeout(() => {
            window.location.reload()
        }, 1500);
    }
}


export default {
    state,
    actions,
    mutations
}