import {
    teambitionMenu,
  } from "../../api/teambition/teambition";
//   import routes from "../../../router/index";
  import Vue from "vue";
  const state = {
    menuList: []
  };
  const actions = {
    teambitionMenu({ commit }, data) {
      let p = teambitionMenu(data);
      p.then(res => {
        commit("GET_TB_MENULIST", res);
      });
    },
  };
  
  const mutations = {
    GET_TB_MENULIST(...arg) {
      let [, { menus }] = [...arg];
      Vue.ls.set("menuList", menus);
    }
  };
  
  export default {
    state,
    actions,
    mutations
  };