// import Vue from 'vue';
import {
    listPageByName,
    insertServer,
    dbTypeList,
    deleteById,
    serverUpdate,
    userInsert,//添加用户
    listDetailsPageByKey,
    userDeleteById,
    listDetailsPageByRoleName,
    userUpdate,//更新
    roleDeleteById,
    listVO,
    roleInsert,
    roleGetDetailsById,
    roleUpdate,
    deviceBindUpdateStatus
} from '@/store/api/safeUkey/systemManage/systemManage'
import {
    SERVER_LIST,
    DBTYPE_LIST
    // ADD_SERVER_LIST
} from '@/store/mutation-types/safeUkey-mutations'
import store from '@/store'
import {Message} from 'element-ui'

const state = {
    getServerLists:[],
    ServerTotalCount:0,
    dbtypelist:[],
    getukeyUserList:[],
    userTotalCount:0,
    getRoleList:[],
    roleTotal:0,
    volist:[],
    voname:'',
    checkStatus:[]
}
const actions = {
    listPageByName({commit},params){
        let p = listPageByName(params)
        p.then(res=>{
            commit(SERVER_LIST,res)
        })
    },
    insertServer({commit},params){
        commit;
        let p = insertServer(params)
        p.then(res=>{
            // commit(ADD_SERVER_LIST,res)
            if(res.error==null){
                Message({
                    message:'添加成功',
                    type:'success'
                })
                store.dispatch('listPageByName',{start:0,count:10})
            }else{
                Message({
                    message:res.error.message,
                    type:'warning'
                })
            }
        })
    },
    dbTypeList({commit},params){
        let p = dbTypeList(params)
        p.then(res=>{
            commit(DBTYPE_LIST,res)
        })
    },
    deleteById({commit},params){
        commit;
        let p = deleteById(params)
        p.then(res=>{
            // commit(DELETE_DBTYPE,res)
            if(res.error==null){
                Message({
                    message:'删除成功',
                    type:'success'
                })
                store.dispatch('listPageByName',{start:0,count:10})
            }else{
                Message({
                    message:res.error.message,
                    type:'warning'
                })
            }
        })
    },
    serverUpdate({commit},params){
        commit;
        let p = serverUpdate(params)
        p.then(res=>{
            // commit(DELETE_DBTYPE,res)
            console.log(res)
            if(res.error==null){
                Message({
                    message:'修改成功',
                    type:'success'
                })
                store.dispatch('listPageByName',{start:0,count:10})
            }else{
                Message({
                    message:res.error.message,
                    type:'warning'
                })
            }
        })
    },
    userInsert({commit},params){
        commit
        let p = userInsert(params)
        p.then(res=>{
            if(res.error){
                Message({
                    message:res.error.message,
                    type:'warning'
                })    
            }else{
                Message({
                    message:'添加成功',
                    type:'success'
                })
            }
        })
    },
    listDetailsPageByKey({commit},params){
        let p = listDetailsPageByKey(params)
        p.then(res=>{
            console.log(res)
            commit('GET_USER_LISTS',res);
        })
    },
    userDeleteById({commit},params){
        commit;
        let p = userDeleteById(params)
        p.then(res=>{
            if(res.error){
                Message({
                    message:res.error.message,
                    type:'warning'
                })
            }else{
                Message({
                    message:'删除成功',
                    type:'success'
                })
            }
        })
    },
    listDetailsPageByRoleName({commit},params){
        let p = listDetailsPageByRoleName(params)
        p.then(res=>{
            commit('GET_ROLE_LIST',res)
        })
    },
    userUpdate({commit},params){
        commit;
        let p = userUpdate(params)
        p.then(res=>{
            if(res.error){
                Message({
                    message:res.error.message,
                    type:'warning'
                })
            }else{
                Message({
                    message:'修改成功',
                    type:'success'
                })
            }
        })
    },
    roleDeleteById({commit},params){
        commit;
        let p = roleDeleteById(params)
        p.then(res=>{
            if(res.result==1){
                Message({
                    message:'删除成功',
                    type:'success'
                })
            }else{
                Message({
                    message:'删除失败',
                    type:'warning'
                })
            } 
        })
    },
    listVO({commit},params){
        let p = listVO(params)
        p.then(res=>{
            console.log(res)
            commit('LIST_VO',res.result)
        })
    },
    roleInsert({commit},params){
        commit;
        let p = roleInsert(params)
        p.then(res=>{
            if(res.result==1){
                Message({
                    message:'添加角色成功',
                    type:'success'
                })
            }else{
                Message({
                    message:'添加失败',
                    type:'warning'
                })
            } 
        })
    },
    roleGetDetailsById({commit},params){
        commit;
        let p = roleGetDetailsById(params)
        p.then(res=>{
            commit('LIST_VO',res.result.permissionTreeList)
            commit('NULL_NAME')
            commit('FILTER_NAME',res.result.permissionTreeList)
            commit('NAME_VO',res.result.roleName)  
        })
    },
    roleUpdate({commit},params){
        commit;
        let p = roleUpdate(params)
        p.then(res=>{
            if(res.result==1){
                Message({
                    message:'修改角色成功',
                    type:'success'
                })
            }else{
                Message({
                    message:'修改失败',
                    type:'warning'
                })
            }
        })
    },
    deviceBindUpdateStatus({commit},params){
        commit;
        let p = deviceBindUpdateStatus(params)
        p.then(res=>{
            if(res.result==1){
                Message({
                    message:'修改状态成功',
                    type:'success'
                })
            }else{
                Message({
                    message:'修改失败',
                    type:'warning'
                })
            }
        })
    },
}
const mutations = {
    SERVER_LIST(state,res){
        state.getServerLists = res.results
        state.ServerTotalCount = res.pagination.total
    },
    DBTYPE_LIST(state,res){
        state.dbtypelist = res.results
    },
    GET_USER_LISTS(state,res){
        state.getukeyUserList = res.results
        state.userTotalCount = res.pagination.total
    },
    GET_ROLE_LIST(state,res){
        state.getRoleList = res.results
        state.roleTotal = res.pagination.total
    },
    LIST_VO(state,res){
        state.volist = res
    },
    NAME_VO(state,res){
        state.voname = res
    },
    NULL_NAME(state){
        state.checkStatus = []
    },
    GET_CHECK_NAME(state,res){
        state.checkStatus = res
    },
    FILTER_NAME(state,data){
        data.forEach(item => {
            if(item.status&&item.parentId!==0){
                state.checkStatus.push(item.id) ;
            }
            if(item.childTreeList && item.childTreeList.length > 0){
                store.commit('FILTER_NAME',item.childTreeList);
            }
        });
    }
}

export default {
    state,
    actions,
    mutations
}