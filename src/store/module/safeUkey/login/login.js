import {
    userLogin,
    userRandom,
    userUkLogin,
    userLogout
} from '@/store/api/safeUkey/login/login'
// import store from '@/store'
import router from '@/router/index'
import {Message} from 'element-ui'
import Vue from "vue";
const state = {
    randomdata:{}
}
const actions = {
    userLogin({commit},params){
        commit;
        let p = userLogin(params)
        p.then(res=>{
            if(res.error==null){
                router.push('/homepages')
                Vue.ls.set('menuList',res.result.role.menus)
                Vue.ls.set("sysName",'USBKEY');
                Vue.ls.set("userName", res.result.userName);
            }else{
                Message({
                    message:res.error.message,
                    type:'error'
                })
            }
        })
    },
    userRandom({commit},params){
        commit;
        let p = userRandom(params)
        p.then(res=>{
            // console.log(res)
            commit('USER_RAMDOM',res)
        })
    },
    userUkLogin({commit},params){
        let p = userUkLogin(params)
        p.then(res=>{
            commit;
            if(res.error==null){
                router.push('/homepages')
                Vue.ls.set('menuList',res.result.role.menus)
                Vue.ls.set("sysName",'USBKEY');
                Vue.ls.set("userName", res.result.userName);
            }else{
                Message({
                    message:res.error.message,
                    type:'error'
                })
            }
        })
    },
    userLogout(){
        let p = userLogout()
        p.then(res=>{
            if(res.error == null){
                router.push('/usbk/login')
                Vue.ls.remove('sysName')
                Vue.ls.remove('menuList')
                Vue.ls.remove('userName')
            }
        })
    }
}
const mutations = {
    USER_RAMDOM(state,res){
        state.randomdata = res.result
    }
}

export default {
    state,
    actions,
    mutations
}