// import Vue from 'vue';
import {
    listPageByIdOrUuid,
    generateUUIDAndKey,
    deviceExistsById,
    deviceExistsByUuid,
    BindlistPageByIdOrUuid,
    BindDeleteById,
    deviceDeleteById,
    deviceBindInsertList,
    insertIfNotExists,
    deviceBindChange,
    deviceBindListPageUserNameByServerId
} from '@/store/api/safeUkey/management/management'
import {
    MANAGEMENT_LIST
} from '@/store/mutation-types/safeUkey-mutations'
import store from '@/store'
import {Message} from 'element-ui'

const state = {
    managementList:[],//设备管理列表
    managementTotalcount:0,
    ArmCtrl:null,
    ArmInfo:{
        ArmWebsock:false,
        ArmNum:0,
        ArmProductID:null,
        ArmUserID : null,
        ArmHardWareID : null,
        ArmHandle : null 
    },
    ArmWriteStatus:false,
    generateuuidAndkey:{},
    getidstatus:false,
    getuuidstatus:false,
    bindManagementlsit:[],
    bindtotalManagement:0,
    addShebei:false,
    serverIdUserList:[],
    bindManageFormVisible:false,
    ArmReadDataData:null,
    ArmReadStatus:false
}
const actions = {
    listPageByIdOrUuid({commit},params){
        let p = listPageByIdOrUuid(params)
        p.then(res=>{
            console.log(res)
            commit(MANAGEMENT_LIST,res)
        })
    },
    generateUUIDAndKey({commit}){
        let p = generateUUIDAndKey()
        p.then(res=>{
            commit('GENERATE_UUIDANDKEY',res)
        }) 
    },
    deviceExistsById({commit},params){
        commit;
        let p = deviceExistsById(params)
        p.then(res=>{
            res;
            commit('ID_STATUS',res.result)
        }) 
    },
    deviceExistsByUuid({commit},params){
        commit;
        let p = deviceExistsByUuid(params)
        p.then(res=>{
            commit('UUID_STATUS',res.result)
        }) 
    },
    BindlistPageByIdOrUuid({commit},params){
        commit;
        let p = BindlistPageByIdOrUuid(params)
        p.then(res=>{
            commit('BIND_LIST_PAGEBYIDORUUID',res)
        }) 
    },
    BindDeleteById({commit},params){
        commit;
        let p = BindDeleteById(params)
        p.then(res=>{
            console.log(res)
            if(res.result ==1){
                Message({
                    message:'解绑成功',
                    type:'success'
                })
            }else{
                Message({
                    message:res.error.message,
                    type:'warning'
                })
            }
            // if(res.error){
            //     Message({
            //         message:res.error.message,
            //         type:'warning'
            //     })
            // }else{
            //     Message({
            //         message:'删除成功',
            //         type:'success'
            //     })
            // }
        }) 
    },
    deviceDeleteById({commit},params){
        commit;
        let p = deviceDeleteById(params)
        p.then(res=>{
            if(res.result==1){
                Message({
                    message:'删除成功',
                    type:'success'
                })
            }else{
                Message({
                    message:res.error.message,
                    type:'warning'
                })
            }
        }) 
    },
    deviceBindInsertList({commit},params){
        commit;
        let p = deviceBindInsertList(params)
        p.then(res=>{
            console.log(res)
            if(res.errors.length>0){
                let str =''
                res.errors.forEach(item=>{
                    str += `${item.message};`
                })
                Message({
                    message:str,
                    type:'warning'
                })
            }else{
                Message({
                    message:'绑定成功',
                    type:'success'
                })
                commit('SET_BIND_MANAGE_VISIBLE',false)
            }
        }) 
    },
    insertIfNotExists({commit},params){
        commit;
        let p = insertIfNotExists(params)
        p.then(res=>{
            if(res.result ==1){
                commit('INSERT_IFNOTEXISTS_TYPE',true)
            }
        }) 
    },
    deviceBindChange({commit},params){
        commit;
        let p = deviceBindChange(params)
        p.then(res=>{
            if(res.error.result == '成功'){
                Message({
                    message:'置换成功',
                    type:'success'
                })
            }
        }) 
    },
    deviceBindListPageUserNameByServerId({commit},params){
        let p = deviceBindListPageUserNameByServerId(params)
        p.then(res=>{
            // serverIdUserList
            commit('SERVERID_USERLIST',res)
        }) 
    },
}

const mutations = {
    [MANAGEMENT_LIST](state,res){
        state.managementList = res.results
        state.managementTotalcount = res.pagination.total
    },
    SET_BIND_MANAGE_VISIBLE(state,res){
        state.bindManageFormVisible = res
    },
    SERVERID_USERLIST(state,res){
        state.serverIdUserList = res.results
    },
    INSERT_IFNOTEXISTS_TYPE(state,res){
        state.addShebei = res
    },
    BIND_LIST_PAGEBYIDORUUID(state,res){
        res.results.forEach(item=>{
            if(item.status==1){
                item.switch = true
            }else{
                item.switch = false
            }
        })
        state.bindManagementlsit = res.results
        state.bindtotalManagement = res.pagination.total
    },
    GENERATE_UUIDANDKEY(state,res){
        state.generateuuidAndkey = res.result
    },
    ID_STATUS(state,res){
        state.getidstatus = res
    },
    UUID_STATUS(state,res){
        state.getuuidstatus = res
    },
    ARM_LOAD(state){
        if(state.ArmCtrl == null || state.ArmInfo.ArmWebsock == false){
            
            try {
                state.ArmCtrl = new AtlCtrlForRockeyArm("{33020048-3E6B-40BE-A1D4-35577F57BF14}");
                console.log(state.ArmCtrl)
                state.ArmInfo.ArmWebsock = true;
                // console.log(`ArmLoad() success`);
            } catch (e) {
                state.ArmCtrl = null;
                state.ArmInfo.ArmWebsock = false;
                // console.log(`ArmLoad() error`);
            }
        }     
    },
    ARM_ENUM(state){
        if (!state.ArmInfo.ArmWebsock) {
            console.log(`该浏览器不支持此插件！`);
        } else {
            state.ArmCtrl.Arm_Enum(function (result, response) {
                if (!result) {
                    alert("ArmEnum() error. " + response);
                } else {
                    console.log(3)
                    state.ArmInfo.ArmNum = response;
                }
            })
        }     
    },
    ARM_GET(state,[index,index2]){
        Index =index
        DongleInfoNum = index2;
        if (state.ArmCtrl != null && state.ArmInfo.ArmNum > 0) {
            state.ArmCtrl.Arm_GetDongleInfo(function (result, response) {
                if (!result) {
                    state.ArmInfo.ArmNum = 0;
                } else {
                    if (index2 == 0) {
                        state.ArmInfo.ArmProductID = response;
                    
                    } else if (index2 == 1) {
                        state.ArmInfo.ArmUserID = response;
                    
                    } else if (index2 == 2) {
                        state.ArmInfo.ArmHardWareID = response;

                    }
                }
            })
        }
    },
    ARM_OPEN(state,index){
        if (state.ArmCtrl != null && state.ArmInfo.ArmNum > 0) {
            Index = index;
            state.ArmCtrl.Arm_Open(function (result, response) {
                console.log(`****************${result}${response}*************************`)
                if (!result) {
                    state.ArmInfo.ArmHandle = null;
                } else {
                    state.ArmInfo.ArmHandle = response;
                }
            })
        }     
    },
    ARM_CLOSE(){
        state.ArmCtrl.Arm_Close(function (result, response) {
            if (!result){
                alert("Arm_Close error. " + response);
            }else{
                // rtn = response;
                if(rtn==0){
                    console.log("关闭锁成功");
                }else{
                    console.log(response);
                }
            }
        })

    },
    ARM_WRITEDATA(state,res){
        state;
        res;
        Handle =  state.ArmInfo.ArmHandle;
        Offset = 0;
        state.ArmWriteStatus = false;
        console.log(state.ArmCtrl,state.ArmInfo.ArmNum,state.ArmInfo.ArmHandle)
        if (state.ArmCtrl != null && state.ArmInfo.ArmNum > 0 && state.ArmInfo.ArmHandle > 0) {
            //此处写入的数据是经过base64编码后直接传入，也可使用base64.js文件中encode和decode接口进行编码解码传递数据
            let jsonData = JSON.stringify(res)
            if(jsonData.length > 128){
                throw('写入的数据不能超过128位')
            }else{
                jsonData.padEnd(128-jsonData.length,'')
            }
            let base64 = new Base64()
            let base64Data = base64.encode(base64.encode(jsonData));
            DataInput = base64Data;
            state.ArmCtrl.Arm_WriteData(function(result,response){
                console.log(result)
                if(!result){
                    this.$message({
                        message:'数据写入失败',
                        type:'warning'
                    })
                }else{
                    if(response==0){
                        state.ArmWriteStatus = true;
                        console.log('写入数据成功')
                    } 
                }
            })
        }
    },
    ARM_READDATA(){
        ArmReadStatus = false;//将读数据的状态设置为失败
        if(state.ArmCtrl !==null && state.ArmInfo.ArmNum > 0 && state.ArmInfo.ArmHandle > 0){
            Handle = state.ArmInfo.ArmHandle;
            Offset = 0;
            ReadLength = 128;
            state.ArmCtrl.Arm_ReadData(function (result, response) {
                if(!result){
                    console.log(response)
                }else{
                    var base64 = new Base64()
                    var jsonData = base64.decode(base64.decode(response));
                    jsonData = jsonData.replace(/ /g,"");
                    state.ArmReadDataData = JSON.parse(jsonData);
                    state.ArmReadStatus = true
                }
            })
        }
    },
    ARM_LOOP(){
        setTimeout(() => {
            store.commit('ARM_LOAD')
        }, 0);
        setTimeout(() => {
            store.commit('ARM_ENUM')
        }, 50);
        setTimeout(() => {
            store.commit('ARM_GET',[0,0])
        }, 400);
        setTimeout(() => {
            store.commit('ARM_GET',[0,1])
        }, 400);
        setTimeout(() => {
            store.commit('ARM_GET',[0,2])
        }, 400);
        setTimeout(() => {
            store.commit('ARM_OPEN',0)
        }, 400);
        // store.commit('ARM_LOAD').then(()=>{
        //     store.commit('ARM_ENUM')
        // }).then(()=>{
        //     store.commit('ARM_GET',0)
        // }).then(()=>{
        //     store.commit('ARM_GET',1)
        // }).then(()=>{
        //     store.commit('ARM_GET',2)
        // }).then(()=>{
        //     store.commit('ARM_OPEN',0)
        // })
    },
    CLEAR_ARMINFO(state){
        state.ArmInfo = {
            ArmWebsock:false,
            ArmNum:0,
            ArmProductID:null,
            ArmUserID : null,
            ArmHardWareID : null,
            ArmHandle : null 
        }
    }
}

export default {
    state,
    actions,
    mutations
}