import {
    listPageByCreateTimeAndIdOrUUID,
} from '@/store/api/safeUkey/logManage/logManage'
// import store from '@/store'
const state = {
    logList:[],
    logTotal:0
}
const actions = {
    listPageByCreateTimeAndIdOrUUID({commit},params){
        let p = listPageByCreateTimeAndIdOrUUID(params)
        p.then(res=>{
            commit('LOG_MANAGE',res)
        })
    }
}
const mutations = {
    LOG_MANAGE(state,res){
        state.logList = res.results
        state.logTotal = res.pagination.total
    }
}

export default {
    state,
    actions,
    mutations
}