import {
  laojieSetRoles,
  laojieRoles,
  lajieUsers,
  laojieAddUser,
  login2admin,
  login2normal,
  login2log,
  administratorLogin,
  getOldStreet,
  getkeliuInfo,
  getkeliuCount,
  getpersonInfo,
  deletepersonInfo,
  addpersonInfo,
  getpersonWork,
  getOldStreetJingdian,
  getJingdianType,
  deleteJingdian,
  addJingdian,
  zhinengbaojing,
  wentitousu,
  xiaochengxuUse,
  osLogin,
  getTourNotes,
  saveTourNotes,
  osDelToourNote,
  getCivilizationBack,
  saveCivilization,
  deleteCivilization,
  getConvenientBack,
  saveConvenient,
  deleteConvenient,
  getQuestionBack,
  deleteQuestion,
  getOldStreetBack,
  saveOldStreet,
  getXiaochi,
  addXiaochi,
  deleteXiaochi,
  updateXiaochi,
  deleteOldStreet,
  getCanyin,
  addCanyin,
  deleteCanyin,
  updateCanyin,
  getCultureBack,
  saveCulture,
  deleteCulture,
  getPersonBack,
  savePerson,
  deletePerson,
  getRestaurantBack,
  saveRestaurant,
  deleteRestaurant,
  getQuestionInfo,
  addQuestionInfo,
  getLogList,
} from "../../api/oldStreet/oldStreet";
import router from "../../../router";
import { Message } from "element-ui";
import Vue from "vue";
import store from "@/store/index";
const state = {
  xiaochiList: [],
  commonPagination: { start: 0, count: 10, total: 0, currentPage: 1 },
  oldstreetMenus: {},
  noticeToVistorList: {
    pagination: {
      total: 0
    },
    results: []
  },
  civilizedRulesList: {
    pagination: {
      total: 0
    },
    results: []
  },
  bianminxinxiList: {
    pagination: {
      total: 0
    },
    results: []
  },
  questionList: {
    pagination: {
      total: 0
    },
    results: []
  },
  wenbaoList: {
    pagination: {
      total: 0
    },
    results: []
  },
  wenhuachangguanList: {
    pagination: {
      total: 0
    },
    results: []
  },
  renwuList: {
    pagination: {
      total: 0
    },
    results: []
  },
  canyinList: {
    pagination: {
      total: 0
    },
    results: []
  },
  kwliuInfo: {},
  keliuCount: {},
  personInfo: [],
  personInfoPagination: {},
  personWork: [],
  jingdianList: [],
  jingdianInfoPagination: {},
  jingdianType: [],
  xiaochengxuList: [],
  zhinengbaojingList: [],
  wentitousuList: [],
  getLogList: {
    pagination:{
      total:0
    }
  },
};
const actions = {
  laojieSetRoles({ commit }, data) {
    let p = laojieSetRoles(data);
    return p;
  },
  laojieRoles({ commit }, data) {
    let p = laojieRoles(data);
    return p;
  },
  lajieUsers({ commit }, data) {
    let p = lajieUsers(data);
    return p;
  },
  laojieAddUser({ commit }, params) {
    let p = laojieAddUser(params);
    return p;
  },
  administratorLogin({ commit }, params) {
    let p = administratorLogin(params);
    return p;
  },
  login2admin({ commit }, params) {
    let p = login2admin(params);
    return p;
  },
  login2normal({ commit }, params) {
    let p = login2normal(params);
    return p;
  },
  login2log({ commit }, params){
    let p = login2log(params);
    return p;
  },
  getXiaochi({ commit }, params) {
    let p = getXiaochi(params);
    p.then(res => {
      commit("GET_XIAO_CHI", res);
    });
  },
  addXiaochi({ commit }, params) {
    let p = addXiaochi(params);
    p.then(res => {
      commit("ADD_XIAO_CHI", res);
    });
  },
  deleteXiaochi({ commit }, params) {
    let p = deleteXiaochi(params);
    p.then(res => {
      commit("DELETE_XIAO_CHI", res);
    });
  },
  updateXiaochi({ commit }, params) {
    let p = updateXiaochi(params);
    p.then(res => {
      commit("UPDAET_XIAO_CHI", res);
    });
  },
  getCanyin({ commit }, params) {
    let p = getCanyin(params);
    p.then(res => {
      commit("GET_CANYIN", res);
    });
  },
  addCanyin({ commit }, params) {
    let p = addCanyin(params);
    p.then(res => {
      commit("ADD_CANYIN", res);
    });
  },
  deleteCanyin({ commit }, params) {
    let p = deleteCanyin(params);
    p.then(res => {
      commit("DELETE_CANYIN", res);
    });
  },
  updateCanyin({ commit }, params) {
    let p = updateCanyin(params);
    p.then(res => {
      commit("UPDATE_CANYIN", res);
    });
  },
  getkeliuInfo({ commit }, params) {
    let p = getkeliuInfo(params);
    p.then(res => {
      commit("GET_KELIU_INFO", res);
    });
  },
  getkeliuCount({ commit }, params) {
    let p = getkeliuCount(params);
    p.then(res => {
      commit("GET_KELIU_COUNT", res);
    });
  },
  getPersonBack({ commit }, params) {
    let p = getPersonBack(params);
    p.then(res => {
      commit("REN_WU_LIST", res);
    });
  },
  getQuestionInfo({ commit }, params) {
    let p = getQuestionInfo(params);
    return p;
  },
  addQuestionInfo({ commit }, params) {
    let p = addQuestionInfo(params);
    return p;
  },
  getpersonInfo({ commit }, params) {
    let p = getpersonInfo(params);
    p.then(res => {
      commit("GET_PERSON_INFO", res);
    });
  },
  deletepersonInfo({ commit }, params) {
    let p = deletepersonInfo(params);
    p.then(res => {
      if (res.result) {
        Message({
          type: "success",
          message: "删除成功！"
        });
        store.dispatch("getpersonInfo", { current: 0, count: 10 });
      } else {
        Message({
          type: "error",
          message: "删除失败！"
        });
      }
    });
  },
  addpersonInfo({ commit }, params) {
    let p = addpersonInfo(params);
    p.then(res => {
      if (res.result) {
        Message({
          type: "success",
          message: "添加成功！"
        });
        store.dispatch("getpersonInfo", { current: 0, count: 10 });
      } else {
        Message({
          type: "error",
          message: "添加失败！"
        });
      }
    });
  },
  getpersonWork({ commit }, params) {
    let p = getpersonWork();
    p.then(res => {
      commit("GET_PERSON_WORK", res);
    });
  },
  getOldStreetJingdian({ commit }, params) {
    let p = getOldStreetJingdian(params);
    p.then(res => {
      commit("GET_JINGDIAN_INFO", res);
    });
  },
  getJingdianType({ commit }, params) {
    let p = getJingdianType(params);
    p.then(res => {
      commit("GET_JINGDIAN_TYPE", res);
    });
  },
  deleteJingdian({ commit }, params) {
    let p = deleteJingdian(params);
    p.then(res => {
      if (res.result) {
        Message({
          type: "success",
          message: "删除成功！"
        });
        store.dispatch("getOldStreetJingdian", { current: 0, count: 10 });
      } else {
        Message({
          type: "error",
          message: "删除失败！"
        });
      }
    });
  },
  addJingdian({ commit }, params) {
    let p = addJingdian(params);
    p.then(res => {
      if (res.result) {
        Message({
          type: "success",
          message: "添加成功！"
        });
        store.dispatch("getOldStreetJingdian", { current: 0, count: 10 });
      } else {
        Message({
          type: "error",
          message: "添加失败！"
        });
      }
    });
  },
  zhinengbaojing({ commit }, params) {
    let p = zhinengbaojing(params);
    p.then(res => {
      commit("GET_ZHINENGBAOJING_LIST", res);
    });
  },
  wentitousu({ commit }, params) {
    let p = wentitousu(params);
    p.then(res => {
      commit("GET_WENTITOUSU_LIST", res);
    });
  },
  xiaochengxuUse({ commit }, params) {
    let p = xiaochengxuUse(params);
    p.then(res => {
      commit("GET_XIAOCEHNGXU_LIST", res);
    });
  },
  getOldStreet({ commit }, params) {
    let p = getOldStreet();
    p.then(res => {
      commit("OLD_STREET_MENUS", res.menus);
    });
  },
  getRestaurantBack({ commit }, params) {
    let p = getRestaurantBack(params);
    p.then(res => {
      commit("CAN_YIN_LIST", res);
    });
  },
  osLogin({ commit }, params) {
    let p = osLogin(params);
    p.then(res => {
      if (res.result) {
        Vue.ls.set("osToken", res.result.token);
        Vue.ls.set("name", res.result.accountEntity.name);
      } else {
        Message.success(res.error.message);
      }
    });
  },
  getTourNotes({ commit }, params) {
    let p = getTourNotes(params);
    p.then(res => {
      commit("NOTICE_TO_VISTOR_LIST", res);
    });
  },
  getCivilizationBack({ commit }, params) {
    let p = getCivilizationBack(params);
    p.then(res => {
      commit("CIVILIZED_RULES_LIST", res);
    });
  },
  getCultureBack({ commit }, params) {
    let p = getCultureBack(params);
    p.then(res => {
      commit("WEN_HUA_CHANG_GUAN_LIST", res);
    });
  },
  getConvenientBack({ commit }, params) {
    let p = getConvenientBack(params);
    p.then(res => {
      commit("BIANMIN_LIST", res);
    });
  },
  getQuestionBack({ commit }, params) {
    let p = getQuestionBack(params);
    p.then(res => {
      commit("QUESTION_LIST", res);
    });
  },
  getOldStreetBack({ commit }, params) {
    let p = getOldStreetBack(params);
    p.then(res => {
      commit("WENBAO_LIST", res);
    });
  },
  saveTourNotes({ commit }, params) {
    let p = saveTourNotes(params);
    p.then(res => {});
    return p;
  },
  osDelToourNote({ commit }, params) {
    let p = osDelToourNote(params);
    p.then(res => {});
    return p;
  },
  saveCivilization({ commit }, params) {
    let p = saveCivilization(params);
    p.then(res => {});
    return p;
  },
  deleteCivilization({ commit }, params) {
    let p = deleteCivilization(params);
    p.then(res => {});
    return p;
  },
  saveConvenient({ commit }, params) {
    let p = saveConvenient(params);
    p.then(res => {});
    return p;
  },
  deleteConvenient({ commit }, params) {
    let p = deleteConvenient(params);
    p.then(res => {});
    return p;
  },
  deleteQuestion({ commit }, params) {
    let p = deleteQuestion(params);
    p.then(res => {});
    return p;
  },
  saveOldStreet({ commit }, params) {
    let p = saveOldStreet(params);
    p.then(res => {});
    return p;
  },
  deleteOldStreet({ commit }, params) {
    let p = deleteOldStreet(params);
    p.then(res => {});
    return p;
  },
  saveCulture({ commit }, params) {
    let p = saveCulture(params);
    p.then(res => {});
    return p;
  },
  deleteCulture({ commit }, params) {
    let p = deleteCulture(params);
    p.then(res => {});
    return p;
  },
  savePerson({ commit }, params) {
    let p = savePerson(params);
    p.then(res => {});
    return p;
  },
  deletePerson({ commit }, params) {
    let p = deletePerson(params);
    p.then(res => {});
    return p;
  },
  saveRestaurant({ commit }, params) {
    let p = saveRestaurant(params);
    p.then(res => {});
    return p;
  },
  deleteRestaurant({ commit }, params) {
    let p = deleteRestaurant(params);
    p.then(res => {});
    return p;
  },
  getLogList({ commit }, params) {
    let p = getLogList(params);
    p.then(res => {
      commit("GET_LOG_LIST", res);
    });
  }
};
const mutations = {
  ADMINISTRATORLOGIN(...arg) {
    console.log(arg);
  },
  GET_CANYIN(...arg) {
    let [state, { results, pagination }] = [...arg];
    state.canyinList = results;
    Object.assign(state.commonPagination, pagination);
  },
  GET_XIAO_CHI(...arg) {
    let [state, { results, pagination }] = [...arg];
    state.xiaochiList = results;
    Object.assign(state.commonPagination, pagination);
  },
  ADD_XIAO_CHI(...arg) {
    let [, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ message: "添加成功", type: "success" });
      store.dispatch("getXiaochi");
    } else {
      Vue.prototype.$message({ message: "接口报错", type: "error" });
    }
  },
  DELETE_XIAO_CHI(...arg) {
    let [state, { error }] = [...arg];
    if (error == null) {
      Object.assign(state.commonPagination, { currentPage: 1 });
      Vue.prototype.$message({ message: "删除成功", type: "success" });
      store.dispatch("getXiaochi");
    }
  },
  UPDAET_XIAO_CHI(...arg) {},
  GET_KELIU_INFO(state, res) {
    state.kwliuInfo = res.results;
  },
  GET_KELIU_COUNT(state, res) {
    if (res.result) {
      state.keliuCount = res.result;
    } else {
      // Message({
      //   type:'error',
      //   message:'网络出错！'
      // })
    }
  },
  GET_PERSON_INFO(state, res) {
    state.personInfo = res.results;
    state.personInfo.forEach(item => {
      if (item.gender == 0) {
        item.sex = "男";
      } else {
        item.sex = "女";
      }
      let juese = "";
      switch (item.workType) {
        case "1":
          juese = "总支书记";
          break;
        case "2":
          juese = "城管";
          break;
        case "3":
          juese = "民警";
          break;
        case "4":
          juese = "村居干部";
          break;
        case "5":
          juese = "值班主任";
          break;
        case "6":
          juese = "值班人员";
          break;
        case "7":
          juese = "信息员";
          break;
      }
      item.juese = juese;
    });
    state.personInfoPagination = res.pagination;
  },
  GET_PERSON_WORK(state, res) {
    state.personWork = res.results;
  },
  GET_JINGDIAN_INFO(state, res) {
    state.jingdianList = res.results;
    state.jingdianInfoPagination = res.pagination;
    let leixing = "";
    state.jingdianList.forEach(item => {
      let jwd = `${item.lon},${item.lat}`;
      item.jingweidu = jwd;
      item.imgUrl = `http://bh-sanlin.oss-cn-shanghai.aliyuncs.com/${item.pictureUrl}`;
      switch (item.type) {
        case "1":
          leixing = "老街文保";
          break;
        case "2":
          leixing = "文化场馆";
          break;
        case "3":
          leixing = "人物介绍";
          break;
        case "4":
          leixing = "特色小吃";
          break;
        case "5":
          leixing = "餐馆信息";
          break;
        case "6":
          leixing = "文化活动点";
          break;
        case "7":
          leixing = "体育健身点";
          break;
      }
      item.leixing = leixing;
    });
  },
  GET_JINGDIAN_TYPE(state, res) {
    state.jingdianType = res.results;
  },
  GET_XIAOCEHNGXU_LIST(state, res) {
    state.xiaochengxuList = res.results;
  },
  GET_ZHINENGBAOJING_LIST(state, res) {
    state.zhinengbaojingList = res.results;
  },
  GET_WENTITOUSU_LIST(state, res) {
    state.wentitousuList = res.results;
  },
  OLD_STREET_MENUS(state, res) {
    state.oldstreetMenus = res;
    Vue.ls.set("menuList", res);
  },
  NOTICE_TO_VISTOR_LIST(state, res) {
    state.noticeToVistorList = res;
  },
  CIVILIZED_RULES_LIST(state, res) {
    state.civilizedRulesList = res;
  },
  BIANMIN_LIST(state, res) {
    state.bianminxinxiList = res;
  },
  QUESTION_LIST(state, res) {
    state.questionList = res;
  },
  WENBAO_LIST(state, res) {
    state.wenbaoList = res;
  },
  WEN_HUA_CHANG_GUAN_LIST(state, res) {
    state.wenhuachangguanList = res;
  },
  REN_WU_LIST(state, res) {
    state.renwuList = res;
  },
  CAN_YIN_LIST(state, res) {
    state.canyinList = res;
  },
  GET_LOG_LIST(state, res) {
    state.getLogList = res;
  }
};
export default {
  state,
  actions,
  mutations
};
