import { getTrackingMenuList } from "@/store/api/tracking/login/login";
import routes from "@/router/index";
import Vue from "vue";
import { GET_TRACKING_MENU_LIST } from "@/store/mutation-types/tracking-mutations";
const state = {};
const actions = {
  getTrackingMenuList({ commit }, data) {
    let p = getTrackingMenuList(data);
    p.then(res => {
      commit("GET_TRACKING_MENU_LIST", res);
    });
  }
};
const mutations = {
  [GET_TRACKING_MENU_LIST](...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
    Vue.ls.set("sysName", "TRACKING");
    routes.push("/t/dashboard");
  }
};
export default { state, actions, mutations };
