import {getUserBehavior} from '@/store/api/tracking/dataCenter/dataCenter'
import { GET_USER_BEHAVIOR } from '@/store/mutation-types/tracking-mutations'
const state = {
    behaviorsList: [],
    pagination: {}
}
const actions = {
    getUserBehavior({commit},data) {
        let p = getUserBehavior(data)
        p.then(res => {
            commit('GET_USER_BEHAVIOR', res)
        })
    }
}
const mutations = {
    [GET_USER_BEHAVIOR] (...arg) {
        let [,{data,pagination}]= [...arg]
        state.behaviorsList = data
        state.pagination = pagination
    }
}
export default { state, actions, mutations };