import {
  getlrMenuList,
  getAlarmList,
  getlrDeviceList,
  unbind,
  getLrIndexInfo,
  getLrIndexLine,
  deviceEdit,
  getDistricName
} from "../../api/lrguanai/lrguanai";
import Vue from "vue";
import router from "../../../router/index";
const state = {
  alarmList: [],
  alarmListPagination: {},
  lrDeviceList: [],
  lrDeviceListPagination: {},
  lrIndexInfo: {
    alarmCount: {},
    backendCare: {},
    deviceCount: {},
    deviceDismantle: {}
  },
  lrIndexLine: [],
  districtName: []
};
const actions = {
  getlrMenuList({ commit }, data) {
    let p = getlrMenuList(data);
    p.then(res => {
      commit("GET_LR_MENULIST", res);
    });
  },
  getAlarmList({ commit }, data) {
    let p = getAlarmList(data);
    p.then(res => {
      commit("GET_ALARM_LIST", res);
    });
  },
  getlrDeviceList({ commit }, data) {
    let p = getlrDeviceList(data);
    p.then(res => {
      commit("GET_LRDEVICE_LIST", res);
    });
  },
  unbind({ commit }, data) {
    let p = unbind(data);
    p.then(res => {
      res;
      Vue.prototype.$message({
        type: "success",
        message: "解绑成功!"
      });
    });
  },
  getLrIndexInfo({ commit }, data) {
    let p = getLrIndexInfo(data);
    p.then(res => {
      commit("GET_LRINDEX_INFO", res);
    });
  },
  getLrIndexLine({ commit }, data) {
    let p = getLrIndexLine(data);
    p.then(res => {
      commit("GET_LRINDEX_LINE", res);
    });
  },
  deviceEdit({ commit }, data) {
    let p = deviceEdit(data);
    p.then(res => {
      res;
      Vue.prototype.$message({
        type: "success",
        message: "编辑成功!"
      });
    });
  },
  getDistricName({commit},data) {
    let p = getDistricName(data)
    p.then(res => {
      commit("GET_DISTRICT_NAME", res)
    })
  }
};
const mutations = {
  GET_LR_MENULIST(...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
    router.push("/lrga/homepage");
  },
  GET_ALARM_LIST(state, res) {
    state.alarmList = res.results;
    state.alarmListPagination = res.pagination;
  },
  //
  GET_LRDEVICE_LIST(state, res) {
    res.results.forEach(item => {
      item.longitudelatitude = item.longitude + "," + item.latitude;
    });
    state.lrDeviceList = res.results;
    state.lrDeviceListPagination = res.pagination;
  },
  GET_LRINDEX_INFO(state, res) {
    state.lrIndexInfo = res.result;
  },
  GET_LRINDEX_LINE(state, res) {
    state.lrIndexLine = res.result;
  },
  GET_DISTRICT_NAME(state, res) {
    state.districtName = res.result
  }
};
export default {
  state,
  actions,
  mutations
};
