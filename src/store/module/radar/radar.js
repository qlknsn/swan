import {
  inputRfid,
  getRadarmenuList,
  getRadarList,
  getRadarStatus,
  changeRadarDetail,
  getAreaList,
  getBicycleList,
  getBicycle,
  getUserList,
  getBicycleTrack,
  getRadarDetail,
  districtSearch,
  rfidTracksearch,
  getOnlineList,
  getOnlineRate,
  getAllRadar,
  getRadarPath,
  getRadarAll,
  search,
  getSimpleColours,
  theftcondition,
  workflowStatus,
  alldistrictSearch,
  reportCase,
  getBikeInfo,
  getUserInfo,
  theftUpdate,
  legalCaseType
} from "../../api/radar/radar";
import { RADAR_LOGIN } from "../../mutation-types/radar-mutations";
import routes from "../../../router/index";
import Vue from "vue";
import store from "@/store";
import router from "../../../router/index";
const state = {
  radarList: [],
  radarListAll: [],
  radarListPagination: {},
  radarStatusList: [],
  radarDetail: {
    properties: {},
    auditHead: {}
  },
  areaList: [],
  isShowMapLoading: true,
  radarBicycleList: [],
  radarBicyclePagination: {},
  userList: [],
  radarUserPagination: {},
  bicycleTrackList: [],
  isShowTrackMap: false,
  districtList: [],
  loading: false,
  // 区域id和name
  deviceZone: [],
  devicezoneName: [],
  radarBicycle: [],
  suoList: [],
  TrackList: [],
  onlineList: [],
  onlineRate: [],
  onlinePagination: {},
  allRadarList: [],
  radarTotal: 0,
  searchList:[],
  threfList:[],
  threftotal:0,
  workstatusList:[],//案件状态列表
  allDistrictlist:[],
  singleBikeInfo:{
    productModel:{brandName:''},
    rfIdList:[{urn:''}],
    snapshots:{
      FRONT:[{url:''}],
      BACK:[{url:''}],
      LICENSE:[{url:''}],
      BRAND:[{url:''}],
      ENGINE:[{url:''}],
      LEFT:[{url:''}],
      RIGHT:[{url:''}],
      SIN_BACK:[{url:''}],
      SIN_FRONT:[{url:''}],
      VIN:[{url:''}],
    }
  },
  singleUserInfo:{
    member:{
      phones:[]
    },
    idCards:[]
  },
  legalCaseTypelist:[],
};
const actions = {
  legalCaseType({ commit }, data) {
    let p = legalCaseType(data);
    p.then(res => {
      commit("LEGAL_CASE_TYPE", res);
    });
  },
  theftUpdate({ commit }, data) {
    let p = theftUpdate(data);
    p.then(res => {
      commit;
      res;
      console.log(res)
      if(res.errors.length){
        Vue.prototype.$message({
          type: "warning",
          message: "请重试!"
        });
      }else{
        Vue.prototype.$message({
          type: "success",
          message: "结案成功!"
        });
      }
      // commit("THEFT_UPDATE", res);
    });
  },
  getUserInfo({ commit }, data) {
    let p = getUserInfo(data);
    p.then(res => {
      commit("GETUSER_INFO", res);
    });
  },
  getBikeInfo({ commit }, data) {
    let p = getBikeInfo(data);
    p.then(res => {
      commit("GETBIKE_INFO", res);
    });
  },
  workflowStatus({ commit }, data) {
    let p = workflowStatus(data);
    p.then(res => {
      commit("WORK_FLOWSTATUS", res);
    });
  },
  theftcondition({ commit }, data) {
    let p = theftcondition(data);
    p.then(res => {
      commit("THEFT_CONDITION", res);
    });
  },
  getRadarmenuList({ commit }, data) {
    let p = getRadarmenuList(data);
    p.then(res => {
      commit("RADAR_LOGIN", res);
    });
  },
  inputRfid({ commit }, data) {
    let p = inputRfid(data);
    p.then(res => {
      commit("INPUT_RFID", res);
    });
  },
  getRadarStatus({ commit }, data) {
    let p = getRadarStatus(data);
    p.then(res => {
      commit("RADAR_STATUS", res);
    });
  },
  getRadarList({ commit }, data) {
    state.isShowMapLoading = true;
    let p = getRadarList(data);
    p.then(res => {
      if (data.isAll) {
        commit("RADAR_LIST_ALL", res);
      } else {
        commit("RADAR_LIST", res);
      }
    });
  },
  getRadarDetail({ commit }, data) {
    let p = getRadarDetail(data);
    p.then(res => {
      commit("RADAR_DETAIL", res);
    });
  },
  changeRadarDetail({ commit }, data) {
    let p = changeRadarDetail(data);
    p.then(res => {
      if (res.errors.lengtth) {
        Vue.prototype.$message({
          type: "error",
          message: "编辑失败!"
        });
      } else {
        Vue.prototype.$message({
          type: "success",
          message: "编辑成功!"
        });
        router.go(-1);
      }
    });
  },
  getAreaList({ commit }, data) {
    let p = getAreaList(data);
    p.then(res => {
      commit("RADAR_AREALIST", res);
    });
  },
  getBicycleList({ commit }, data) {
    let p = getBicycleList(data);
    p.then(res => {
      commit("RADAR_BICYCLELIST", res);
    });
  },
  getBicycle({ commit }, data) {
    let p = getBicycle(data);
    p.then(res => {
      commit("RADAR_BICYCLE", res);
      if (res.results.length === 0) {
        Vue.prototype.$message({
          type: "error",
          message: "没有匹配到对应的车牌号!"
        });
      }
    });
  },
  getRadarUserList({ commit }, data) {
    let p = getUserList(data);
    p.then(res => {
      commit("RADAR_USERLIST", res);
    });
  },
  search({ commit }, data) {
    let p = search(data);
    return p;
  },
  getBicycleTrack({ commit }, data) {
    let p = getBicycleTrack(data);
    return p;
  },
  alldistrictSearch({ commit }, data) {
    let p = alldistrictSearch(data);
    p.then(res => {
      commit("GET_ALL_DISTRICT", res.results);
    });
  },
  districtSearch({ commit }, data) {
    let p = districtSearch(data);
    store.commit("CHANGE_LOADING", true);
    p.then(res => {
      setTimeout(() => {
        store.commit("CHANGE_LOADING", false);
      }, 1500);
      commit("GET_DISTRICT", res.results);
    });
  },
  suoSearch({ commit }, data) {
    let p = districtSearch(data);
    store.commit("CHANGE_LOADING", true);
    p.then(res => {
      setTimeout(() => {
        store.commit("CHANGE_LOADING", false);
      }, 1500);

      commit("GET_SUO", res.results);
    });
  },
  rfidTracksearch({ commit }, data) {
    let p = rfidTracksearch(data);
    p.then(res => {
      commit("GET_TRACK", res.results);
    });
    return p;
  },
  getOnlineList({ commit }, data) {
    let p = getOnlineList(data);
    p.then(res => {
      commit("GET_ONLINE_LIST", res);
    });
  },
  getOnlineRate({ commit }, data) {
    let p = getOnlineRate(data);
    p.then(res => {
      commit("GET_ONLINE_RATE", res);
    });
  },
  getAllRadar({ commit }, data) {
    let p = getAllRadar(data);
    p.then(res => {
      commit("GET_All_RADAR", res);
    });
  },
  getRadarPath({ commit }, data) {
    let p = getRadarPath(data);
    p.then(res => {
      commit("GET_RADAR_PATH", res);
    });
  },
  getRadarAll({ commit }, data) {
    let p = getRadarAll(data);
    p.then(res => {
      commit("GET_RADAR_ALL", res);
    });
  },
  getSimpleColours({ commit }, data) {
    let p = getSimpleColours();
    return p;
  },
  reportCase({ commit }, data) {
    let p = reportCase(data);
    return p;
  }
};

const mutations = {
  LEGAL_CASE_TYPE(state, res){
    state.legalCaseTypelist = res;
  },
  GETUSER_INFO(state, res){
    state.singleUserInfo = res.result;
  },
  GETBIKE_INFO(state, res) {
    state.singleBikeInfo = res.result;
  },
  GET_ALL_DISTRICT(state, res) {
    state.allDistrictlist = res;
  },
  WORK_FLOWSTATUS(state, res) {
    state.workstatusList = [];
    res.forEach(item=>{
      // if(item.name=='OPEN'){
      //   state.workstatusList.push(item)
      // }
      if(item.name=='PROCESSING'){
        state.workstatusList.push(item)
      }
      if(item.name=='CLOSE'){
        state.workstatusList.push(item)
      }
    })
    
  },
  THEFT_CONDITION(state, res) {
    console.log(res);
    // res.results.forEach(item=>{
    //   if(item.caseConclusionType){
    //     item.caseConclusionTypestr
    //   }
    // })
    state.threfList = res.results;
    state.threftotal = res.pagination.total
  },
  GET_All_RADAR(state, res) {
    console.log(res);
    state.allRadarList = res.results;
  },
  INPUT_RFID(...arg) {
    let [, { errors }] = [...arg];
    if (errors.length == 0) {
      Vue.prototype.$message({ message: "添加成功", type: "success" });
    } else {
      Vue.prototype.$message({ message: "添加失败", type: "error" });
    }
  },
  GET_TRACK(state, res) {
    state.TrackList = res;
  },
  GET_SUO(state, res) {
    state.suoList = res;
  },
  CHANGE_LOADING(state, res) {
    state.loading = res;
  },
  GET_DISTRICT(state, res) {
    state.districtList = res;
  },
  [RADAR_LOGIN](...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
    routes.push("/radar/radarList");
  },
  RADAR_STATUS(state, res) {
    state.radarStatusList = res;
  },
  RADAR_LIST(state, res) {
    state.radarList = res.results;
    state.radarList.forEach(item => {
      state.radarStatusList.forEach(i => {
        if (i.name === item.status) {
          Object.assign(item, { statusText: i.text });
        }
      });
    });
    state.radarListPagination = res.pagination;
  },
  RADAR_LIST_ALL(state, res) {
    state.radarListAll = res.results;
    state.isShowMapLoading = false;
    state.radarListAll.forEach(item => {
      state.radarStatusList.forEach(i => {
        if (i.name === item.status) {
          Object.assign(item, { statusText: i.text });
        }
      });
    });
  },
  RADAR_DETAIL(state, res) {
    if (res.result.zones) {
      let a = res.result.zones.replace(/\[|]/g, "");
      state.deviceZone = a.split(",");
      let i = state.deviceZone.findIndex(val => val == "");
      if (i !== -1) {
        state.deviceZone.pop();
      }
    }
    state.radarDetail = res.result;
  },
  RADAR_AREALIST(state, res) {
    const arr = [];
    arr.push(res.result);
    state.areaList = arr;
  },
  RADAR_BICYCLE(state, res) {
    state.radarBicycle = res.results;
  },
  RADAR_BICYCLELIST(state, res) {
    state.radarBicyclePagination = res.pagination;
    state.radarBicycleList = res.results;
  },
  RADAR_USERLIST(state, res) {
    state.radarUserPagination = res.pagination;
    state.userList = res.results;
  },
  RADAR_BICYCLETRACK(state, res) {
    state.bicycleTrackList = res.results;
  },
  GET_ONLINE_LIST(state, res) {
    state.onlineList = res;
  },
  GET_ONLINE_RATE(state, res) {
    state.onlinePagination = res.pagination;
    state.onlineRate = res.results;
  },
  GET_RADAR_PATH(state, res) {
    res;
  },
  GET_RADAR_ALL(state, res) {
    state.radarTotal = res.result;
  }
};

export default {
  state,
  actions,
  mutations
};
