import {
    officeLogin,
    getOfficemenuList,
    addNews,
    getNewsList,
    deleteNews,
    singleNews,
    editNews
  } from "../../api/office/office/office";
  import {
 
  } from "../../mutation-types/rockfish-mutations";
  import routes from "../../../router/index";
  import Vue from "vue";
  const state = {
    newstotal:0,
    newsList:[],
    singleNews:{}
  };
  const actions = {
    officeLogin({commit},params){
        let p = officeLogin(params)
        p.then(res=>{
          commit;
          if(res.code=='1'){
            routes.push('/office/newsCenter')
            // console
            Vue.ls.set('token',res.data.token)
          }
        })
    },
    getOfficemenuList({commit},params){
        let p = getOfficemenuList(params)
        p.then(res=>{
            commit("GET_OFFICE_MENULIST", res)
        })
    },
    addNews({commit},params){
        let p = addNews(params)
        p.then(res=>{
          commit;
            if(res.code=='1'){
              routes.push('/office/newsCenter')
            }
        })
    },
    editNews({commit},params){
        let p = editNews(params)
        p.then(res=>{
          commit;
            if(res.code=='1'){
              routes.push('/office/newsCenter')
            }
        })
    },
    getNewsList({commit},params){
        let p = getNewsList(params)
        p.then(res=>{
          commit('GET_NEWS_LIST',res)
        })
    },
    deleteNews({commit},params){
        let p = deleteNews(params)
        p.then(res=>{
          if(res.code==1){
            this.$message({
              type:'success',
              message:'删除成功！'
            })
          }
          commit;
        })
    },
    singleNews({commit},params){
        let p = singleNews(params)
        p.then(res=>{
          routes.push('/office/editNews')
          commit('GET_SINGLE_NEWS',res)
        })
    },
  };
  
  const mutations = {
    GET_OFFICE_MENULIST(...arg){
        let [,{menus}] = [...arg]
        Vue.ls.set('menuList',menus)
    },
    GET_NEWS_LIST(...arg){
      let [state,{data,pagination:{total}}] = [...arg]
      state.newsList = data
      state.newstotal = total
    },
    GET_SINGLE_NEWS(...arg){
      let [state,{data}] = [...arg]
      state.singleNews = data
    }
  };
  
  export default {
    state,
    actions,
    mutations
  };
  