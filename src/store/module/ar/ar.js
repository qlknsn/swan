import {
  getHighPoint,
  setHighPoint,
  searchHighPoint,
  resetPassWord,
  editUserInf,
  addUserInf,
  queryAllUser,
  getArMemuList,
  arlogin,
  hightPointList,
  getListEnum,
  getLabelList,
  exportHCamera,
  addHCamera,
  delHCamera,
  getHCameraTmpl,
  loginout,
  delLabel,
  moreDelLabel,
  modifyLable,
  queryLableType,
  addLableType,
  delLableType,
  queryLableIcon,
  delLableIcon,
  queryLableLayer,
  addLableLayer,
  delLableLayer,
  queryLableTmpl,
  addLableTmpl,
  modifyLableTmpl,
  delLableTmpl,
  queryPOIFromGD,
  addPOI,
  queryPOI,
  queryPoiResType,
  queryMarkTemplate,
  deletePOI,
  queryPoiTypeMap,
  addPoiTypeMap,
  delPoiTypeMap,
  getConfig,
  modConfig,
  queryOptLog,
  exportOptLog,
  getSystemConfigurationmenus,
  setMarkShow,
  setMenus,
  searchTree,
  editTree,
  addTree,
  delTree,
  getArEventList,
  getEventDetail,
  getEventStatus,
  editAttributes,
  editEventStatus,
  getMapConfig,
  saveMapConfig,
  searchLowPoint,
  queryLowPoint,
  addCamera,
  getHKTree,
  getHKCamera,
  copyHkCamera,
  deleteCamera,
  getTemplate,
  importCamera,
  editCamera,
} from "../../api/ar/ar";
import Vue from "vue";
import routes from "../../../router";
import { Message } from "element-ui";
import store from "@/store/index";

const state = {
  highPoint: [],
  totalAllPage: null,
  userHigPoint: [],
  hightPoint: [],
  newPassWord: '',
  allUserList: [],
  districtList: [],
  hightPointList: {},
  listEnum: {},
  getLabelList: {},
  labelTypeList: {},
  labelIconList: {},
  labelLayerList: {},
  labelTmplList: {},
  config: {},
  queryOptLog: {},
  queryPOI: {},
  queryPoiResType: {},
  queryMarkTemplate: {},
  queryPoiTypeMap: {},
  queryPOIFromGD: {
    data: {
      pois: []
    }
  },
  getArEventList: {
    data: [{
      nickName: null,
      rank: null,
      isVisible: false
    }]
  },
  getEventDetail: {
    data: [{
      nickName: null,
      rank: null,
      isVisible: false
    }]
  },
  getEventStatus: {
    data: [{
      nickName: null,
      isVisible: false
    }]
  },
  getMapConfig: {
    data: {

    }
  },
  searchLowPoint: [],
  lowPointList:{
    data:[{}]
  },
};
const actions = {
  getHighPoint({ commit }, data) {
    let p = getHighPoint(data);
    p.then(res => {
      commit("GETHIGHPOINT", res);
    });
  },
  setHighPoint({ commit }, data) {
    let p = setHighPoint(data)
    p.then(res => {
      commit("SETHIGHPOINT", res);
    });
  },
  searchHighPoint({ commit }, data) {
    let p = searchHighPoint(data);
    p.then(res => {
      commit("SEARCHHIGHPOINT", res);
    });
  },
  resetPassWord({ commit }, data) {
    let p = resetPassWord(data)
    p.then(res => {
      commit("RESETPASSWORD", res);
    });
  },
  editUserInf({ commit }, data) {
    let p = editUserInf(data)
    p.then(res => {
      commit("EDITUSERINF", res);
    });
  },
  addUserInf({ commit }, data) {
    let p = addUserInf(data);
    p.then(res => {
      commit("ADDUSERINF", res);
    });
  },
  queryAllUser({ commit }, data) {
    let p = queryAllUser(data);
    p.then(res => {
      commit("QUERYALLUSER", res);
    });
  },
  delTree({ commit }, data) {
    let p = delTree(data);
    p.then(res => {
      commit("DELTREE", res);
    });
  },
  addTree({ commit }, data) {
    let p = addTree(data);
    p.then(res => {
      commit("ADDTREE", res);
    });
  },
  editTree({ commit }, data) {
    let p = editTree(data);
    p.then(res => {
      commit("EDITTREE", res);
    });
  },
  searchTree({ commit }, data) {
    let p = searchTree(data);
    p.then(res => {
      commit("SEARCHTREE", res);
    });
  },
  setMarkShow({ commit }, data) {
    let p = setMarkShow(data);
    p.then(res => {
      commit("SETMARKSHOW", res);
    });
  },
  getArMemuList({ commit }, data) {
    let p = getArMemuList(data);
    p.then(res => {
      commit("GET_AR_MENULIST", res);
    });
  },
  arlogin({ commit }, data) {
    let p = arlogin(data);
    p.then(res => {
      commit("AR_LOGIN", res);
    });
  },
  hightPointList({ commit }, data) {
    let p = hightPointList(data);
    p.then(res => {
      commit("HIGHT_POINT_LIST", res);
    });
    return p;
  },
  addHCamera({ commit }, data) {
    let p = addHCamera(data);
    return p;
  },

  delHCamera({ commit }, data) {
    let p = delHCamera(data);
    return p;
  },

  getHCameraTmpl({ commit }, data) {
    let p = getHCameraTmpl(data);
    return p;
  },
  exportHCamera({ commit }, data) {
    let p = exportHCamera(data);
    return p;
  },
  loginout({ commit }, data) {
    let p = loginout(data);
    return p;
  },
  getListEnum({ commit }, data) {
    let p = getListEnum(data);
    p.then(res => {
      commit("LIST_ENUM", res);
    });
  },
  getLabelList({ commit }, data) {
    let p = getLabelList(data);
    p.then(res => {
      commit("LABEL_LIST", res);
    });
  },
  delLabel({ commit }, data) {
    let p = delLabel(data);
    return p;
  },
  moreDelLabel({ commit }, data) {
    let p = moreDelLabel(data);
    return p;
  },
  modifyLable({ commit }, data) {
    let p = modifyLable(data);
    return p;
  },
  queryLableType({ commit }, data) {
    let p = queryLableType(data);
    p.then(res => {
      commit("LABEL_TYPE_LIST", res);
    });
  },
  addLableType({ commit }, data) {
    let p = addLableType(data);
    return p;
  },
  delLableType({ commit }, data) {
    let p = delLableType(data);
    return p;
  },
  delLableIcon({ commit }, data) {
    let p = delLableIcon(data);
    return p;
  },
  queryLableIcon({ commit }, data) {
    let p = queryLableIcon(data);
    p.then(res => {
      commit("LABEL_ICON_LIST", res);
    });
  },
  queryLableLayer({ commit }, data) {
    let p = queryLableLayer(data);
    p.then(res => {
      commit("LABEL_LAYER_LIST", res);
    });
  },
  addLableLayer({ commit }, data) {
    let p = addLableLayer(data);
    return p;
  },
  delLableLayer({ commit }, data) {
    let p = delLableLayer(data);
    return p;
  },
  queryLableTmpl({ commit }, data) {
    let p = queryLableTmpl(data);
    p.then(res => {
      commit("LABEL_TMPL_LIST", res);
    });
  },
  addLableTmpl({ commit }, data) {
    let p = addLableTmpl(data);
    return p;
  },
  modifyLableTmpl({ commit }, data) {
    let p = modifyLableTmpl(data);
    return p;
  },
  delLableTmpl({ commit }, data) {
    let p = delLableTmpl(data);
    return p;
  },
  queryPOIFromGD({ commit }, data) {
    let p = queryPOIFromGD(data);
    p.then(res => {
      commit("QUERY_POI_FROM_GD", res);
    });
  },
  addPOI({ commit }, data) {
    let p = addPOI(data);
    return p;
  },
  queryPOI({ commit }, data) {
    let p = queryPOI(data);
    p.then(res => {
      commit("SYSTEM_POI_LIST", res);
    });
  },
  queryPoiResType({ commit }, data) {
    let p = queryPoiResType(data);
    p.then(res => {
      commit("QUERY_POI_TYPE", res);
    });
  },
  queryMarkTemplate({ commit }, data) {
    let p = queryMarkTemplate(data);
    p.then(res => {
      commit("QUERY_TEMP_TYPE", res);
    });
  },
  deletePOI({ commit }, data) {
    let p = deletePOI(data);
    return p;
  },
  queryPoiTypeMap({ commit }, data) {
    let p = queryPoiTypeMap(data);
    p.then(res => {
      commit("QUERY_POI_TYPE_MAP", res);
    });
  },
  delPoiTypeMap({ commit }, data) {
    let p = delPoiTypeMap(data);
    return p;
  },
  addPoiTypeMap({ commit }, data) {
    let p = addPoiTypeMap(data);
    return p;
  },
  getConfig({ commit }, data) {
    let p = getConfig(data);
    return p;
  },
  modConfig({ commit }, data) {
    let p = modConfig(data);
    return p;
  },
  queryOptLog({ commit }, data) {
    let p = queryOptLog(data);
    p.then(res => {
      commit("LOG_LIST", res);
    });
    // return p;
  },
  exportOptLog({ commit }, data) {
    let p = exportOptLog(data);
    return p;
  },
  getSystemConfigurationmenus({ commit }, data) {
    let p = getSystemConfigurationmenus(data);
    return p;
  },
  setMenus({ commit }, data) {
    let p = setMenus(data);
    return p;
  },
  getArEventList({ commit }, data) {
    let p = getArEventList(data);
    p.then(res => {
      commit("GET_AR_EVENT_LIST", res);
    });
    // return p;
  },
  getEventDetail({ commit }, data) {
    let p = getEventDetail(data);
    p.then(res => {
      commit("GET_EVENT_DETAIL", res);
    });
    // return p;
  },
  getEventStatus({ commit }, data) {
    let p = getEventStatus(data);
    p.then(res => {
      commit("GET_EVENT_STATUS", res);
    });
    // return p;
  },
  editAttributes({ commit }, data) {
    let p = editAttributes(data);
    return p;
  },
  editEventStatus({ commit }, data) {
    let p = editEventStatus(data);
    return p;
  },
  getMapConfig({ commit }, data) {
    let p = getMapConfig(data);
    // p.then(res => {
    //   commit("GET_MAP_CONFIG", res);
    // });
    return p;
  },
  saveMapConfig({ commit }, data) {
    let p = saveMapConfig(data);
    return p;
  },
  searchLowPoint({ commit }, data) {
    let p = searchLowPoint(data);
    p.then(res => {
      commit("SEARCH_LOWPOINT_TREE", res);
    });
  },
  queryLowPoint({ commit }, data) {
    let p = queryLowPoint(data);
    p.then(res => {
      // console.log(res);
      for(let val of res.data){
        // console.log(val);
        if(val.ptz == 0){
          val.ptz = "不支持";
        } else if(val.ptz == 1){
          val.ptz = "支持";
        }
      }
      commit("QUERY_LOW_POINT", res);
    });
  },
  addCamera({ commit }, data) {
    let p = addCamera(data);
    return p;
  },
  getHKTree({ commit }, data) {
    let p = getHKTree(data);
    return p;
  },
  getHKCamera({ commit }, data) {
    let p = getHKCamera(data);
    return p;
  },
  copyHkCamera({ commit }, data) {
    let p = copyHkCamera(data);
    return p;
  },
  deleteCamera({ commit }, data) {
    let p = deleteCamera(data);
    return p;
  },
  getTemplate({ commit }, data) {
    let p = getTemplate(data);
    return p;
  },
  importCamera({ commit }, data) {
    let p = importCamera(data);
    return p;
  },
  editCamera({ commit }, data) {
    let p = editCamera(data);
    return p;
  },
};
const mutations = {
  GETHIGHPOINT(...arg) {

    let [state, { data }] = [...arg];
    state.userHigPoint = data.map(item => {
      return item;
    });
    state.highPoint = data.map(item => {
      return item.name;
    });

  },
  SETHIGHPOINT(...arg) {
    arg;
    let [, { code }] = [...arg];
    if (code == 0) {
      Vue.prototype.$message({ message: "添加成功", type: "success" });
      store.dispatch("searchHighPoint", {});
    } else {
      Vue.prototype.$message({ message: "添加失败", type: "error" });
    }
  },

  SEARCHHIGHPOINT(...arg) {
    let [state, { data }] = [...arg];
    state.hightPoint = data.map(item => {
      return item;
    });
  },
  RESETPASSWORD(...arg) {
    let [state, { data }] = [...arg];
    state.newPassWord = data
  },

  EDITUSERINF(...arg) {
    let [, { code }] = [...arg];
    if (code == 0) {
      Vue.prototype.$message({ message: "编辑成功", type: "success" });
      store.dispatch("queryAllUser", { page: 1, limit: 10 });
    } else {
      Vue.prototype.$message({ message: "编辑失败", type: "error" });
    }
  },
  ADDUSERINF(...arg) {
    let [, { code }] = [...arg];
    if (code == 0) {
      Vue.prototype.$message({ message: "添加成功", type: "success" });
      store.dispatch("queryAllUser", { page: 1, limit: 10 });
    } else {
      Vue.prototype.$message({ message: "用户已存在", type: "error" });
    }
  },
  QUERYALLUSER(...arg) {
    let [, { totalAll }] = [...arg];
    let [state, { data }] = [...arg];
    state.totalAllPage = totalAll
    state.allUserList = data.map(item => {
      return item;
    });
  },
  EDITTREE(...arg) {
    let [, { code }] = [...arg];
    if (code == 0) {
      Vue.prototype.$message({ message: "编辑成功", type: "success" });
      store.dispatch("searchTree", {});
    } else {
      Vue.prototype.$message({ message: "编辑失败", type: "error" });
    }
  },
  DELTREE(...arg) {
    let [, { code }] = [...arg];
    if (code == 0) {
      Vue.prototype.$message({ message: "删除成功", type: "success" });
      store.dispatch("searchTree", {});
    } else {
      Vue.prototype.$message({ message: "删除失败", type: "error" });
    }
  },
  ADDTREE(...arg) {
    arg;
    let [, { code }] = [...arg];
    if (code == 0) {
      Vue.prototype.$message({ message: "添加成功", type: "success" });
      store.dispatch("searchTree", {});
    } else {
      Vue.prototype.$message({ message: "添加失败", type: "error" });
    }
  },
  SEARCHTREE(...arg) {
    let [state, { data }] = [...arg];
    state.districtList = data.map(item => {
      return item;
    });
  },
  SETMARKSHOW(...arg) {
    arg;
    let [, { code }] = [...arg];
    if (code == 0) {
      Vue.prototype.$message({ message: "修改成功", type: "success" });
    } else {
      Vue.prototype.$message({ message: "修改失败", type: "error" });
    }
  },
  GET_AR_MENULIST(...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
  },
  AR_LOGIN(state, res) {
    if (res.msg == "成功") {
      Vue.ls.set("artoken", res.token);
      routes.push("/ar/highPoint");
      store.dispatch("getListEnum", {});
    } else {
      Message.error(res.msg);
    }
  },
  HIGHT_POINT_LIST(state, res) {
    state.hightPointList = res;

  },
  LIST_ENUM(state, res) {
    state.listEnum = res;
  },
  LABEL_LIST(state, res) {
    state.getLabelList = res;
  },
  LABEL_TYPE_LIST(state, res) {
    state.labelTypeList = res;
    state.labelTypeList.data = state.labelTypeList.data.map(item => {
      if (item.isShow == 0) {
        Object.assign(item, { isShow: true });
      } else {
        Object.assign(item, { isShow: false });
      }
      return item;
    });
  },
  LABEL_ICON_LIST(state, res) {
    state.labelIconList = res;
  },
  LABEL_LAYER_LIST(state, res) {
    state.labelLayerList = res;
  },
  LABEL_TMPL_LIST(state, res) {
    state.labelTmplList = res;
  },
  CONFIG(state, res) {
    state.config = res;
  },
  LOG_LIST(state, res) {
    state.queryOptLog = res;
  },
  SYSTEM_POI_LIST(state, res) {
    state.queryPOI = res;
  },
  QUERY_POI_TYPE(state, res) {
    state.queryPoiResType = res;
  },
  QUERY_TEMP_TYPE(state, res) {
    state.queryMarkTemplate = res;
  },
  QUERY_POI_TYPE_MAP(state, res) {
    state.queryPoiTypeMap = res;
  },
  QUERY_POI_FROM_GD(state, res) {
    state.queryPOIFromGD = res;
  },
  GET_AR_EVENT_LIST(state, res) {
    state.getArEventList = res;
  },
  GET_EVENT_DETAIL(state, res) {
    state.getEventDetail = res;
  },
  GET_EVENT_STATUS(state, res) {
    state.getEventStatus = res;
  },
  // GET_MAP_CONFIG(state, res){
  //   state.getMapConfig = res;
  // }
  SEARCH_LOWPOINT_TREE(state, res) {
    state.searchLowPoint = res.data;
  },
  QUERY_LOW_POINT(state, res) {
    state.lowPointList = res;
    // console.log(state.lowPointList);
  },
};
export default {
  state,
  actions,
  mutations
};
