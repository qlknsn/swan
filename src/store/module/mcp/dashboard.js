import { INIT_MCP_MAP, ADD_MARKER, ADD_CLUSTER } from "@/store/mutation-types";
import Vue from "vue";
/**
 * 有自己的作用域
 */
const namespaced = true;
const state = {
  mapLoadCompleted: false,
  addMarkerDone: false,
  mcpGlobalMap: "",
  centerPosition: [121.511594, 31.143173],
  markerList: [],
  cluster: ""
};
const actions = {};
const mutations = {
  /**
   * 初始化地图
   * @param  {...any} arg
   */
  [INIT_MCP_MAP](...arg) {
    let [state] = [...arg];
    state.mapLoadCompleted = false;
    let centerPosition = state.centerPosition;
    state.mcpGlobalMap = new AMap.Map("mcp-map-id", {
      mapStyle: "amap://styles/a2c5b896c39c2bfc85d98d1e8af8229f", //设置地图的显示样式
      zoom: 14,
      center: centerPosition,
      viewMode: "3D"
    });
    Vue.prototype.mcpGlobalMap = state.mcpGlobalMap;
    state.mcpGlobalMap.on("complete", function() {
      state.mapLoadCompleted = true;
      state.addMarkerDone = false;
    });
  },
  /**
   * 添加撒点
   * @param  {...any} arg
   */
  [ADD_MARKER](...arg) {
    let [state] = [...arg];
    let marker = new AMap.Marker({
      icon: require("@/assets/images/mcp/lan@2x.png"),
      position: state.centerPosition
    });
    state.markerList.push(marker);
    marker.setMap(state.mcpGlobalMap);
    state.addMarkerDone = true;
  },
  /**
   * 添加点聚合
   * @param  {...any} arg
   */
  [ADD_CLUSTER](...arg) {
    let [state] = [...arg];
    Vue.prototype.mcpGlobalMap.plugin(["AMap.MarkerClusterer"], function() {
      let cluster = new AMap.MarkerClusterer(
        state.mcpGlobalMap,
        state.markerList,
        {
          gridSize: 20
        }
      );
      state.cluster = cluster;
    });
  }
};

export default { namespaced, state, actions, mutations };
