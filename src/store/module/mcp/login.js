import { login } from "@/store/api/mcp/login/login";
import { SET_MICRO_CHECKIN_POINT_MENU } from "@/store/mutation-types";
import routes from "@/router/index";
import Vue from "vue";
const namespaced = true;
const state = {};
const actions = {
  login({ commit }, data) {
    let p = login(data);
    p.then(res => {
      commit("SET_MICRO_CHECKIN_POINT_MENU", res);
    });
  }
};
const mutations = {
  [SET_MICRO_CHECKIN_POINT_MENU](...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
    Vue.ls.set("sysName", "MCP");
    routes.push("/mcp/dashboard");
  }
};
export default { namespaced, state, actions, mutations };
