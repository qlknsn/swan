import {
  getDNS,
  saveDNS,
  resetPwd,
} from "@/store/api/gateway/systemManage/systemManage";
import {
  GET_DNS,
  SAVE_DNS,
  RESET_PWD,
} from "@/store/mutation-types/gateway-mutations";
import Vue from "vue";
import route from "@/router/index";

const state = {
  dnsObj: {
    dns1: "",
    dns2: "",
  },
};
const actions = {
  resetPwd({ commit }, data) {
    let p = resetPwd(data);
    p.then((res) => {
      commit("RESET_PWD", res);
    });
  },
  saveDNS({ commit }, data) {
    let p = saveDNS(data);
    p.then((res) => {
      commit("SAVE_DNS", res);
    });
  },
  getDNS({ commit }, data) {
    let p = getDNS(data);
    p.then((res) => {
      commit("GET_DNS", res.results);
    });
  },
};
const mutations = {
  [RESET_PWD](...arg) {
    let [, { code, errors }] = [...arg];
    code;
    errors;
    if (code === 200) {
      Vue.prototype.$message({
        type: "success",
        message: errors,
      });
      route.push("/gw/login");
    } else {
      Vue.prototype.$message({
        type: "error",
        message: errors,
      });
    }
  },
  [GET_DNS](state, data) {
    Object.assign(state.dnsObj, data);
  },
  [SAVE_DNS](...arg) {
    let [, { errors, code }] = [...arg];
    if (code == "0") {
      Vue.prototype.$message({
        message: errors,
        type: "success",
      });
    } else {
      Vue.prototype.$message({
        message: errors,
        type: "error",
      });
    }
  },
};

export default {
  state,
  actions,
  mutations,
};
