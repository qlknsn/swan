import {
  getInterfaceList,
  updataInterfaceList,
  getHostInfo,
  updataHostInfo
} from "@/store/api/gateway/lookup/lookup";
import {
  UPDATE_HOST_INFO,
  GET_INTERFACE_LIST,
  GAT_HOST_INFO,
  CHANGE_CURRENTINTERFACE
} from "@/store/mutation-types/gateway-mutations";
import Vue from "vue";
import { Loading } from "element-ui";
const state = {
  interfaceList: [],
  hostInfo: {},
  policeList: [],
  connectivity: {},
  currentInterface: {
    metrics: {}
  }
};
const actions = {
  getInterfaceList({ commit }, data) {
    let p = getInterfaceList(data);
    p.then(res => {
      commit("GET_INTERFACE_LIST", res.results);
    });
    // commit;
    // return getInterfaceList(data)
  },
  updataInterfaceList({ commit }, data) {
    let options = {
      text: "网络正在重启"
    };
    let loadingInstance = Loading.service(options);
    let p = updataInterfaceList(data);

    p.then(res => {
      commit;
      loadingInstance.close();
      if (res.code === 200) {
        Vue.prototype.$message({
          type: "success",
          message: res.msg
        });
        let p = getInterfaceList({ name: "" });
        p.then(res => {
          commit("GET_INTERFACE_LIST", res.results);
        });
      } else {
        Vue.prototype.$message({
          type: "error",
          message: res.msg
        });
      }
    });
  },
  // 获取主机信息
  getHostInfo({ commit }, data) {
    let p = getHostInfo(data);
    p.then(res => {
      commit("GAT_HOST_INFO", res);
    });
  },
  // 修改主机信息
  updataHostInfo({ commit }, data) {
    let p = updataHostInfo(data);
    p.then(res => {
      commit("UPDATE_HOST_INFO", res);
    });
  }
};
const mutations = {
  [UPDATE_HOST_INFO](...arg) {
    let [, { msg, code }] = [...arg];
    if (code == 200) {
      Vue.prototype.$message({
        type: "success",
        message: "更新成功"
      });
    } else {
      Vue.prototype.$message({
        type: "error",
        message: msg
      });
    }
  },
  [GET_INTERFACE_LIST](state, data) {
    state.interfaceList = data;
    state.interfaceList.inet = [
      {
        id: 0,
        value: "static",
        name: "static | 固定"
      },
      {
        id: 1,
        value: "dhcp",
        name: "dhcp | 动态"
      }
    ];
    // state.currentInterface = data.netInterfaces[0];
  },
  [CHANGE_CURRENTINTERFACE](state, data) {
    state.currentInterface = data;
  },
  [GAT_HOST_INFO](...arg) {
    let [state, { msg, code, results }] = [...arg];
    if (code == 200) {
      state.hostInfo = results.hostInfo;
      state.policeList = results.police_division_list;
      state.connectivity = results.connectivity;
    } else {
      Vue.prototype.$message({
        type: "error",
        message: msg
      });
    }
  }
};

export default {
  state,
  actions,
  mutations
};
