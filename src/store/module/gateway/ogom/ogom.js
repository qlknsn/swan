import {
    SET_OGOM_CURRENT_STEP
} from "@/store/mutation-types/gateway-mutations"
const state = {
    ogomCurrentStepObj: {
        index: 0,
        name: 'ogomBasic'
    },
}
const actions = {}
const mutations = {
    [SET_OGOM_CURRENT_STEP](state, data) {
        state.ogomCurrentStepObj = data
    }
}

export default {
    state,
    actions,
    mutations,
}