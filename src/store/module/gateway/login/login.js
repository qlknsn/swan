import {
  getHomepageInfo,
  login,
  getDefaultDicts
} from "@/store/api/gateway/login/login";
import {
  GET_HOMEPAGE_INFO,
  LOGIN_MENU,
  GET_DEFAULT_DICTS
} from "@/store/mutation-types/gateway-mutations";
import Vue from "vue";
// import router from ''
import router from "@/router";
const state = {
  hostInfo: {
    infoBoard: {
      placeName: "",
      dataService: { versionString: "", versionPublishDay: "" },
      webService: { versionString: "" },
      uptime: { name: "", uptime: "" }
    }
  },
  loginMe: {},
  defaultDicts: {
    VENDOR_LIST: []
  }
};

const actions = {
  getHomepageInfo({ commit }, data) {
    let p = getHomepageInfo(data);
    p.then(res => {
      commit("GET_HOMEPAGE_INFO", res);
    });
  },
  getDefaultDicts({ commit }, data) {
    let p = getDefaultDicts(data);
    p.then(res => {
      commit("GET_DEFAULT_DICTS", res);
    });
  },
  login({ commit }, params) {
    let p = login(params);
    p.then(res => {
      commit(LOGIN_MENU, res);
    });
  }
};

const mutations = {
  [GET_HOMEPAGE_INFO](...arg) {
    let [state, { results }] = [...arg];
    if (typeof results == "object") {
      state.hostInfo = results;
    } else {
      Vue.prototype.$message({
        type: "error",
        message: "API数据返回类型不正确"
      });
    }
  },
  [GET_DEFAULT_DICTS](...arg) {
    let [state, { results }] = [...arg];
    state.defaultDicts = results;
  },
  [LOGIN_MENU](...arg) {
    let [, { code, msg, results }] = [...arg];
    if (code == 200) {
      Vue.ls.set("token", results.token);
      Vue.ls.set("menuList", results.menus);
      router.push("/gw/homePage");
    } else {
      Vue.prototype.$message({
        type: "error",
        message: msg
      });
    }
  }
};

export default {
  state,
  actions,
  mutations
};
