import Vue from "vue";
import {
  capturePic,
  addDevice,
  updateDevice,
  getDeviceList,
  getDeviceDetail,
  deleteDevice,
  scanCamera,
} from "@/store/api/gateway/deviceManage/deviceManage";
import {
  UPDATE_DEVICE,
  ADD_DEVICE,
  GET_DEVICE_LIST,
  GET_DEVICE_DETAIL,
  SET_ADD_DEVICE_POP,
  DELETE_DEVICE,
  FINAL_UPDATE_DEVICE,
  SCAN_CAMERA,
  CAPTURE_PIC,
} from "@/store/mutation-types/gateway-mutations";
// import Vue from 'vue'
import router from "@/router";
import store from "@/store";
const state = {
  capturePicList: [],
  deviceList: [],
  isShowPop: false,
  deviceListParams: {
    page: 1,
    size: 10,
    total: 0,
    current: 1,
    name: "",
    ipv4: "",
  },
  deviceDetail: "",
  scanCameraLoading: false,
  scanCameraParams: {
    name: "",
    page: 1,
    size: 10,
    total: 0,
    current: 1,
  },
  scanDeviceList: [],
  capturePicParams: {
    startTime: "",
    endTime: "",
    page: 1,
    size: 10,
    total: 0,
    current: 1,
    name: "",
    ipv4: "",
  },
};

const actions = {
  capturePic({ commit }, data) {
    let p = capturePic(data);
    p.then((res) => {
      commit("CAPTURE_PIC", res);
    });
  },
  scanCamera({ commit, state }, data) {
    state.scanCameraLoading = true;
    let p = scanCamera(data);
    p.then((res) => {
      commit("SCAN_CAMERA", res);
    });
  },
  finalUpdateDevice({ commit }, data) {
    let p = updateDevice(data);
    p.then((res) => {
      commit("FINAL_UPDATE_DEVICE", res);
    });
  },
  updateDevice({ commit }, data) {
    let p = updateDevice(data);
    p.then((res) => {
      commit("UPDATE_DEVICE", res);
    });
  },
  deleteDevice({ commit }, data) {
    let p = deleteDevice(data);
    p.then((res) => {
      commit("DELETE_DEVICE", res);
    });
  },
  addDevice({ commit }, data) {
    let p = addDevice(data);
    p.then((res) => {
      commit("ADD_DEVICE", res);
    });
  },
  getDeviceDetail({ commit }, data) {
    let p = getDeviceDetail(data);
    p.then((res) => {
      commit("GET_DEVICE_DETAIL", res);
    });
  },
  getDeviceList({ commit }, data) {
    let p = getDeviceList(data);
    p.then((res) => {
      commit("GET_DEVICE_LIST", res);
    });
  },
};

const mutations = {
  [CAPTURE_PIC](...arg) {
    let [state, { code, msg, results, count, current_num, size }] = [...arg];
    if (code == 200) {
      state.capturePicList = results;
      Object.assign(state.capturePicParams, {
        page: current_num,
        size: size,
        total: count,
      });
    } else {
      Vue.prototype.$message({
        type: "error",
        message: msg,
      });
    }
  },
  [SCAN_CAMERA](...arg) {
    let [state, { code, results, count, current_num }] = [...arg];
    state.scanCameraLoading = false;
    if (code == 200) {
      state.scanDeviceList = results;
      state.scanCameraParams.total = count;
      state.scanCameraParams.page = current_num;
    }
  },
  [FINAL_UPDATE_DEVICE](...arg) {
    let [, { code, msg }] = [...arg];
    if (code == 200) {
      Vue.prototype.$message({
        type: "success",
        message: "提交成功",
      });

      setTimeout(() => {
        router.push("/gw/ogom/list");
      }, 1500);
    } else {
      Vue.prototype.$message({
        type: "error",
        message: msg,
      });
    }
  },
  [UPDATE_DEVICE](...arg) {
    let [, { code, msg }] = [...arg];
    if (code == 200) {
      Vue.prototype.$message({
        type: "success",
        message: "更新成功",
      });
      store.dispatch("getDeviceList");
    } else {
      Vue.prototype.$message({
        type: "error",
        message: msg,
      });
    }
  },
  [DELETE_DEVICE](...arg) {
    let [, { code, msg }] = [...arg];
    if (code == 200) {
      Vue.prototype.$message({
        type: "success",
        message: "删除成功",
      });
      store.dispatch("getDeviceList");
    } else {
      Vue.prototype.$message({
        type: "error",
        message: msg,
      });
    }
  },
  [SET_ADD_DEVICE_POP](state, data) {
    state.isShowPop = data;
  },
  [ADD_DEVICE](...arg) {
    let [state, { code, msg }] = [...arg];
    if (code == 200 || code == 201) {
      Vue.prototype.$message({
        type: "success",
        message: "添加成功",
      });
      state.isShowPop = false;
      store.dispatch("getDeviceList");
    } else {
      Vue.prototype.$message({
        type: "error",
        message: msg,
      });
      store.dispatch("getDeviceList");
    }
  },
  [GET_DEVICE_DETAIL](...arg) {
    let [state, { results }] = [...arg];
    state.deviceDetail = results;
  },
  [GET_DEVICE_LIST](...arg) {
    let [state, { results, count }] = [...arg];
    state.deviceList = results;
    Object.assign(state.deviceListParams, {
      total: count,
    });
  },
};

export default {
  state,
  actions,
  mutations,
};
