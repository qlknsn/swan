import {
  lgdeleteTodo,
  lgAddorUpdateTodo,
  lggetTodoList,
  lgdeleteWareHouse,
  lgupdateWareHouse,
  lggetWareHouse,
  lgaddWareHouse,
  lgConfigList,
  lgUpdateVip,
  lgDeleteVip,
  lgAddVip,
  lggetDistrictList,
  lgAddDepart,
  lgDistrictList,
  lgdeleteDistrict,
  lgaddDistrict,
  lgdistrictTree,
  chooseTree,
  lgGetUserList,
  saveAuthority,
  getTMenus,
  listForBackend,
  getMinAppMenus,
  getWorkerList,
  updateWorker,
  getDistrict2Tree,
  getLgUserList,
  loginLg,
  addOrUpdateUser,
  getLgAllMenus,
  getLgMenu,
  delLgMenu,
  addLgMenu,
  editLgMenu,
  getLgRoles,
  getLgAuthority,
  lgDropDown,
  lgDeleteDepart,
  lgEditDepart,
  lgRoles,
  lgAddAccount,
  lgDeleteAccount,
  lgyuzhiList,
  addlgyuzhiList,
  eventTypes,
  deleteyuzhiList,
  edityuzhiList,
  lgUpdateAccount,
  lgVipList,
  addElevator,
  getElevatorList,
  getFireHouse,
  lgEditElevator,
  lgaddDevice,
  lgdeleteDevice,
  lgupdateDevice,
  lgDeleteElevator,
  lgGetNeigh,
  lgAddNeighBaseInfo,
  lgUpdateNeighBaseInfo,
  lgDeleteNeighBaseInfo,
  lgaddqcDevice,
  lgdeleteqcDevice,
  lghikTree,
  lghikCameraList,
  lgcopyCameraInfo,
  lgGetCameraList,
  lgCameraEdit,
  lgGetTagList,
  lgCameraDel,
  ignbDeviceTypeList,
  lgTagAdd,
  lgTagEdit,
  lgTagDelete,
  lggetTootipList,
  lgAddTootip,
  lgDeleteTootip,
  lgEditTootip,
  lgGetUserDetail,
  getCameraSnap,
  lggetConfig,
  saveRenovation,
  getRenovationList,
  deleteRenovation,
  getRenovation,
  igGetNBaddress,
  igaddNbAddress,
  igdelNbAddress,
  ignbDeviceList,
  igaddNbDevice,
  igdelNbDevice,
  igeditNbDevice,
  igeditNbAddress,
  moduleDown,
   exportUser
} from "../../api/lingang/lgAdmin";
import Vue from "vue";
import router from "../../../router/index";
import store from "../../index";
const state = {
  districtList: [],
  lgUserList: [],
  pagination: { count: 8, start: 0, total: 0 },

  getElevatorList: {},
  getlgyuzhilist: {
    list: [],
    totalCount: 0,
    limit: 0
  },
  eventLists: [],
  fireHouseList: {
    list: [],
    limit: 0,
    totalCount: 0
  },
  lgGetNeighList: {},
  lgGetCameraList:{
    list:[{
      tags:[{
        name:""
      }]
    }]
  },
  lgCameraTagslist:{},
  getTootipList:{},
  RenovationList:{
    list:[]
  },
  renovationParams:{
    limit:10,
    page:1,
    name:''
  },
  igGetNBaddress:{
    data:{
      list:[
        {projectPlanList:{
          data:{
            list:[
              {
                address:""
              }
            ],
            totalCount:0,
          },
        }
      }

      ]
    }
  },
  ignbDeviceList:{}
};
const actions = {
  exportUser({ commit }, params) {
    commit;
    exportUser(params);
  },
  // 人员信息采集上传模板
  moduleDown(){
    let p = moduleDown()
    return p;
  },
  //删除装修信息
  deleteRenovation({ commit }, data) {
    let p = deleteRenovation(data);
    return p;
  },
  //添加装修信息
  saveRenovation({ commit }, data) {
    let p = saveRenovation(data);
    return p;
    // p.then(res =>{
    //   commit("SAVE_RENOVATION",res);
    // })
  },
  //装修列表
  getRenovationList({ commit }, data) {
    let p = getRenovationList(data);
    p.then(res =>{
      commit("GET_RENOVATION_LIST",res);
    })
  },
  //老人三件套以及小区列表
  igGetNBaddress({ commit }, data) {
    let p = igGetNBaddress(data);
    p.then(res =>{
      commit("GET_OLDMAN_ADDRESS_LIST",res);
    })
  },
  //老人三件套设备列表
  ignbDeviceList({ commit }, data) {
    let p = ignbDeviceList(data);
    p.then(res =>{
      commit("GET_OLDMAN_DEVICE_LIST",res);
    })
  },
  igeditNbAddress({ commit }, data) {
    let p = igeditNbAddress(data);
    return p;
  },
  //
  igaddNbAddress({ commit }, data) {
    let p = igaddNbAddress(data);
    return p;
  },
  igaddNbDevice({ commit }, data) {
    let p = igaddNbDevice(data);
    return p;
  },
  igeditNbDevice({ commit }, data) {
    let p = igeditNbDevice(data);
    return p;
  },
  igdelNbDevice({ commit }, data) {
    let p = igdelNbDevice(data);
    return p;
  },
  igdelNbAddress({ commit }, data) {
    let p = igdelNbAddress(data);
    return p;
  },
  lggetTootipList({ commit }, data) {
    let p = lggetTootipList(data);
    p.then(res =>{
      commit("GET_TOOTIP_LIST",res.data);
    })
    return p;
  },
  lgDeleteTootip({ commit }, data) {
    let p = lgDeleteTootip(data);
    // p.then(res =>{
    //   commit("GET_TOOTIP_LIST",res.data);
    // })
    return p;
  },
  lggetConfig({ commit }, ) {
    let p = lggetConfig()
    return p;
  },
  lgcopyCameraInfo({ commit }, data) {
    let p = lgcopyCameraInfo(data);
    return p;
  },
  getCameraSnap({ commit }, data) {
    let p = getCameraSnap(data);
    return p;
  },

  lgGetUserDetail({ commit }, data) {
    let p = lgGetUserDetail();
    return p;
  },
  lgAddTootip({ commit }, data) {
    let p = lgAddTootip(data);
    return p;
  },
  lgEditTootip({ commit }, data) {
    let p = lgEditTootip(data);
    return p;
  },
  lghikCameraList({ commit }, data) {
    let p = lghikCameraList(data);
    return p;
  },
  lghikTree({ commit }, data) {
    let p = lghikTree(data);
    return p;
  },
  lgTagAdd({ commit }, data) {
    let p = lgTagAdd(data);
    return p;
  },
  lgTagDelete({ commit }, data) {
    let p = lgTagDelete(data);
    return p;
  },
  lgTagEdit({ commit }, data) {
    let p = lgTagEdit(data);
    return p;
  },
  lgdeleteTodo({ commit }, data) {
    let p = lgdeleteTodo(data);
    return p;
  },
  lgAddorUpdateTodo({ commit }, data) {
    let p = lgAddorUpdateTodo(data);
    return p;
  },
  lggetTodoList({ commit }, data) {
    let p = lggetTodoList(data);
    return p;
  },
  lgCameraDel({ commit }, data) {
    let p = lgCameraDel(data);
    return p;
  },
  lgdeleteqcDevice({ commit }, data) {
    let p = lgdeleteqcDevice(data);
    return p;
  },
  lgaddqcDevice({ commit }, data) {
    let p = lgaddqcDevice(data);
    return p;
  },
  lgdeleteWareHouse({ commit }, data) {
    let p = lgdeleteWareHouse(data);
    return p;
  },
  lgGetTagList({ commit }, data) {
    let p = lgGetTagList(data);
    p.then(res =>{
      commit("LG_CAMERA_TAG_LIST",res);
    })
    return p;
  },
  lgupdateWareHouse({ commit }, data) {
    let p = lgupdateWareHouse(data);
    return p;
  },
  lgaddWareHouse({ commit }, data) {
    let p = lgaddWareHouse(data);
    return p;
  },
  lggetWareHouse({ commit }, data) {
    let p = lggetWareHouse(data);
    return p;
  },
  lgupdateDevice({ commit }, data) {
    let p = lgupdateDevice(data);
    return p;
  },
  lgGetCameraList({ commit }, data) {
    let p = lgGetCameraList(data);
    p.then(res => {
      commit("GET_LG_CAMERA_LIST", res);
    });
    return p;
  },
  lgdeleteDevice({ commit }, data) {
    let p = lgdeleteDevice(data);
    return p;
  },
  lgaddDevice({ commit }, data) {
    let p = lgaddDevice(data);
    return p;
  },
  lgConfigList({ commit }, data) {
    let p = lgConfigList(data);
    return p;
  },
  lgUpdateVip({ commit }, data) {
    let p = lgUpdateVip(data);
    return p;
  },
  lgDeleteNeighBaseInfo({ commit }, data) {
    let p = lgDeleteNeighBaseInfo(data);
    return p;
  },
  lgGetNeigh({ commit }, data) {
    let p = lgGetNeigh(data);
    p.then(res => {
      commit("GET_NEIGH_LIST", res);
    });
  },
  lgDeleteVip({ commit }, data) {
    let p = lgDeleteVip(data);
    return p;
  },
  lgAddVip({ commit }, data) {
    let p = lgAddVip(data);
    return p;
  },
  getFireHouse({ commit }, data) {
    let p = getFireHouse(data);
    p.then(res => {
      commit("GET_FIREhOUSE", res);
    });
  },
  lgAddNeighBaseInfo({ commit }, data) {
    let p = lgAddNeighBaseInfo(data);
    return p;
  },
  lgUpdateNeighBaseInfo({ commit }, data) {
    let p = lgUpdateNeighBaseInfo(data);
    return p;
  },
  lgEditElevator({ commit }, data) {
    let p = lgEditElevator(data);
    return p;
  },
  edityuzhiList({ commit }, data) {
    let p = edityuzhiList(data);
    return p;
  },
  deleteyuzhiList({ commit }, data) {
    let p = deleteyuzhiList(data);
    return p;
  },
  eventTypes({ commit }, data) {
    let p = eventTypes(data);
    p.then(res => {
      commit("GET_EVENT_LIST", res);
    });
  },
  addlgyuzhiList({ commit }, data) {
    let p = addlgyuzhiList(data);
    return p;
  },
  lgDeleteElevator({ commit }, data) {
    let p = lgDeleteElevator(data);
    return p;
  },
  lgCameraEdit({ commit }, data) {
    let p = lgCameraEdit(data);
    return p;
  },
  lgyuzhiList({ commit }, data) {
    let p = lgyuzhiList(data);
    p.then(res => {
      // console.log(res)
      commit("GET_LGYUZHI_LIST", res);
    });
  },
  lggetDistrictList({ commit }, data) {
    let p = lggetDistrictList(data);
    return p;
  },
  lgVipList({ commit }, data) {
    let p = lgVipList(data);
    return p;
  },
  lgUpdateAccount({ commit }, data) {
    let p = lgUpdateAccount(data);
    return p;
  },
  getElevatorList({ commit }, data) {
    let p = getElevatorList(data);
    p.then(res => {
      commit("GET_ELEVATOR_LIST", res);
    });
    return p;
  },
  lgDeleteAccount({ commit }, data) {
    let p = lgDeleteAccount(data);
    return p;
  },
  lgAddAccount({ commit }, data) {
    let p = lgAddAccount(data);
    return p;
  },
  lgRoles({ commit }, data) {
    let p = lgRoles(data);
    return p;
  },
  lgEditDepart({ commit }, data) {
    let p = lgEditDepart(data);
    return p;
  },
  lgDeleteDepart({ commit }, data) {
    let p = lgDeleteDepart(data);
    return p;
  },
  lgDropDown({ commit }, data) {
    let p = lgDropDown(data);
    return p;
  },
  lgAddDepart({ commit }, data) {
    let p = lgAddDepart(data);
    return p;
  },
  lgDistrictList({ commit }, data) {
    let p = lgDistrictList(data);
    return p;
  },
  lgdeleteDistrict({ commit }, data) {
    let p = lgdeleteDistrict(data);
    return p;
  },
  lgaddDistrict({ commit }, data) {
    let p = lgaddDistrict(data);
    return p;
  },
  lgdistrictTree({ commit }, data) {
    let p = lgdistrictTree(data);
    return p;
  },
  chooseTree({ commit }, data) {
    let p = chooseTree(data);
    return p;
  },
  lgGetUserList({ commit }, data) {
    let p = lgGetUserList(data);
    return p;
  },
  saveAuthority({ commit }, data) {
    let p = saveAuthority(data);
    return p;
  },

  getTMenus({ commit }, data) {
    let p = getTMenus(data);
    return p;
  },
  listForBackend({ commit }, data) {
    let p = listForBackend(data);
    return p;
  },
  getMinAppMenus({ commit }, data) {
    let p = getMinAppMenus(data);
    return p;
  },
  getLgAuthority({ commit }, data) {
    let p = getLgAuthority(data);
    return p;
  },
  getLgRoles({ commit }, data) {
    let p = getLgRoles(data);
    return p;
  },
  editLgMenu({ commit }, data) {
    let p = editLgMenu(data);
    return p;
  },
  addLgMenu({ commit }, data) {
    let p = addLgMenu(data);
    return p;
  },
  delLgMenu({ commit }, data) {
    let p = delLgMenu(data);
    return p;
  },
  getLgMenu({ commit }, data) {
    let p = getLgMenu(data);
    return p;
  },
  getLgAllMenus({ commit }, data) {
    let p = getLgAllMenus(data);
    return p;
  },
  updateWorker({ commit }, data) {
    let p = updateWorker(data);
    p.then(res => {
      commit("UPDATE_WORKER", res);
    });
  },
  getDistrict2Tree({ commit }, data) {
    let p = getDistrict2Tree(data);
    p.then(res => {
      commit("GET_DISTRICT_2_TREE", res);
    });
  },
  getWorkerList({ commit }, data) {
    let p = getWorkerList(data);
    p.then(res => {
      commit("GET_WORKER_LIST", res);
    });
  },
  loginLg({ commit }, data) {
    let p = loginLg(data);
    return p;
  },
  getLgUserList({ commit }, data) {
    let p = getLgUserList(data);
    p.then(res => {
      commit("GET_LG_USER_LIST", res);
    });
  },
  addOrUpdateUser({ commit }, data) {
    let p = addOrUpdateUser(data);
    p.then(res => {
      commit("ADD_OR_UPDATE_USER", res);
    });
  },
  addElevator({ commit }, data) {
    let p = addElevator(data);
    return p;
  },
  ignbDeviceTypeList({ commit }, data) {
    let p = ignbDeviceTypeList(data);
    return p;
  }
};
const mutations = {
  EXPORTUSER({ commit }, params) {
    commit;
    EXPORTUSER(params);
  },
  GET_RENOVATION_LIST(state, res){
    console.log(res)
    state.RenovationList = res.data;
  },
  GET_FIREhOUSE(state, res) {
    state.fireHouseList = res.data;
  },
  GET_OLDMAN_ADDRESS_LIST(state,res){
    
    res.data.list.map(item => {
      item.projectPlanList = {
        data: {
          list:[],
          totalCount:0
        }
      };
    });
    state.igGetNBaddress = res
  },
  GET_OLDMAN_DEVICE_LIST(state,res){
    state.igGetNBaddress.data.list.forEach((element,index) =>{
      state.igGetNBaddress.data.list[index].projectPlanList = res;
    })

    
  },
  GET_TOOTIP_LIST(state,res){
    state.getTootipList = res
  },
  GET_EVENT_LIST(state, res) {
    state.eventLists = res.data;
  },
  GET_LG_CAMERA_LIST(state, res) {
    state.lgGetCameraList = res.data;
  },
  LG_CAMERA_TAG_LIST(state, res) {
    state.lgCameraTagslist = res.data;
  },
  GET_LGYUZHI_LIST(state, res) {
    state.getlgyuzhilist = res.data;
  },

  UPDATE_WORKER(...arg) {
    let [state, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ message: "修改成功", type: "success" });
    } else {
      Vue.prototype.$message({ message: "修改失败", type: "error" });
    }
    store.dispatch("getWorkerList", state.pagination);
  },
  GET_DISTRICT_2_TREE(...arg) {
    let [state, { data }] = [...arg];
    state.districtList = data;
  },
  GET_NEIGH_LIST(state, res) {
    state.lgGetNeighList = res.data;
  },
  GET_WORKER_LIST(...arg) {
    let [state, { results, pagination }] = [...arg];
    results.forEach(item => {
      let roleIdArray = item.roleIds.split(",");
      let ownRole = "";
      if (roleIdArray.length == 2) {
        ownRole = "物业，建设者";
      } else if (roleIdArray.length == 1) {
        if (roleIdArray[0] == "1") {
          ownRole = "建设者";
        } else if (roleIdArray[0] == "2") {
          ownRole = "物业";
        }
      }
      Object.assign(item, { ownRole });
    });
    state.lgUserList = results;
    Object.assign(state.pagination, pagination);
  },
  ADD_OR_UPDATE_USER(...arg) {
    let [state, { error }] = [...arg];
    if (error == null) {
      Vue.prototype.$message({ message: "修改成功", type: "success" });
    } else {
      Vue.prototype.$message({ message: "修改失败", type: "error" });
    }

    store.dispatch("getLgUserList", state.pagination);
  },
  GET_LG_USER_LIST(...arg) {
    let [state, { pagination, results }] = [...arg];
    state.lgUserList = results.map(item => {
      Object.assign(item, { ownRole: "临港城运管理人员" });
      return item;
    });
    Object.assign(state.pagination, pagination);
  },
  LOGIN_LG(...arg) {
    let [, { menus }] = [...arg];
    Vue.ls.set("menuList", menus);
    router.push("/lg/users");
  },
  GET_ELEVATOR_LIST(state, res) {
    state.getElevatorList = res.data;
  }
};
export default {
  state,
  actions,
  mutations
};
