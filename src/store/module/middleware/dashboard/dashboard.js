
import {getBasicData,getBySource,getByCamera,faultClassify} from '../../../api/middleware/dashboard/dashboard'

const state = {
    basicData:{},
     //根据调用方次数排名
    getByTime:{
        current:{
            details:[]
        }
    },  
     //根据调用方时长
    getByDurationfourth:[], 
    getByDurationOther:[],
    getByCamera:[],   //根据探头被调用次数 
    faultClassifyType:'M',
    faultStatistics:"M"
}
const actions = {
    //基础数据
    getBasicData({commit},params){
        commit;
        let p = getBasicData(params);
        return p;
        // p.then(res =>{
        //     console.log(res);
        // })
    },

    //按视频调用源进行视频调用分类统计的排行榜接口
    getBySource({commit},params){
        commit;
        let p = getBySource(params);
        p.then(res =>{
            if(params.sort == 'HITS'){
                commit("GET_BY_TIME",res);
            }else{
                commit("GET_BY_DURATION",res);
            }
        })
    },
    //按视频调用量进行视频调用分类统计查询接口
    getByCamera({commit},params){
        commit;
        let p = getByCamera(params);
        p.then(res =>{
            commit("GET_BY_CAMERA",res)
            
        })
    },
    //故障分析、故障分类
    faultClassify({commit},params){
        commit;
        let p = faultClassify(params);
        return p;
    }
}
const mutations = {
    GET_BASICDATA(state,res){
        state.basicData = res;
    },
    GET_BY_TIME(state,res){
        state.getByTime = res.result;
    },
    GET_BY_DURATION(state,res){
        if(res.result.current.details.length > 4){
            state.getByDurationfourth = res.result.current.details.slice(0,3);
            state.getByDurationOther = res.result.current.details.slice(4);
        }else{
            state.getByDurationfourth = res.result.current.details;
        }
    },
    GET_BY_CAMERA(state,res){
        state.getByCamera = res.result.current.details;
    },
    DATE_CHANGE_FAULT_CLASSIFY(state,res){
        state.faultClassifyType = res;
    } ,
    DATE_CHANGE_FAULT_STATISTICS(state,res){
        state.faultStatistics =res
    }  
}

export default {
    state,actions,mutations
}