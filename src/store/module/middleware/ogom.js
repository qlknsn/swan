import {
    SET_OGOM_CURRENT_TAB
} from "@/store/mutation-types/middleware-mutations"
const state = {
    ogomCurrentTabObj: {
        index: 0,
        name: 'ogomBasic'
    },
}
const actions = {}
const mutations = {
    [SET_OGOM_CURRENT_TAB](state, data) {
        state.ogomCurrentTabObj = data
    }
}

export default {
    state,
    actions,
    mutations,
}