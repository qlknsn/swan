import {
    getNvrList,
    middleLogin,
    getOrgs,
    getCameraLists,
    getTagList,
    addTags,
    deleteTags,
    uptateTags,
    setTagForCamera,
    getCamaraParams,
    setCamaraParams,
    changeViewDirection,
    getVideoUrl,
    videoReplay,
    checkVideo,
    updateCamera,
    getAllDomTree} from '../../api/middleware/middleware'
import {MIDDLE_LOGIN, SET_HEADER_STATUS} from "../../mutation-types/middleware-mutations";
import Vue from "vue";
import {Message} from 'element-ui'
import routes from "../../../router";
// import brandInfo from "../../../views/deviceInput/brandInfo/brandInfo";
const state ={
    hideHeader: false,
    nvrList: [],
    orgsTree:[],
    firstNode:[],
    pagination: {},
    cameraList: [],
    paginationOfCamera: {},
    cameraOfDeviceList: [],
    paginationOfCameraDevice: {},
    tagsList: [],
    tagListPagination: {},
    paramsList: {
        brightness: 10,
        contrast: 10,
        saturation: 10,
        hue: 10
    },
    isShowVideo: false,
    videoInfoList: [0,0,0,0],
    preOrgsTree: [],
    isShowTree: false,
    videoReplay:{},  //录像回放
    allTree: [],
    activeTagsList: []
}
const actions = {
    /**
     * 用户登录
     */
    middleLogin({ commit }, data) {
        let p = middleLogin(data);
        p.then(res => {
            commit("MIDDLE_LOGIN", res);
        });
    },
    /**
     * 获取组织结构树
     */
    getOrgs({commit},params){
        let p = getOrgs(params);
        p.then(res=>{
            if(params.parentUrn =='urn:hikvision:folder:28459205:-1'){
                commit("FIRST_GET_ORGS",res)
            }else{
                if (params.type === 'camera') {
                    const params0 = {
                        type: 'add',
                        parents: params.parentUrn,
                        count: 1000,
                        start: 0
                    }
                    this.dispatch('getCameraLists', params0)
                    commit("GET_ORGS",res)
                } else {
                    commit("GET_ORGS",res)
                }
            }
        })
    },
    /**
     * 获取nvr列表
     */
    getNvrList({commit},params){
        let p = getNvrList(params);
        p.then(res=>{
            commit("GET_NVRLIST",res)
        })
    },
    /**
     * 获取相机列表
     */
    getCameraLists ({commit}, params) {
        let p = getCameraLists(params)
        p.then(res=> {
            if (params.type === 'add') {
                let cameraList = res.results
                let orgCameraTree = []
                cameraList.forEach(item => {
                    let obj = {}
                    obj.name = item.nickname
                    obj.parent = [params.parents]
                    obj.urn = 'urn:' + item.urn.organization + ':' + item.urn.group + ':' + item.urn.id
                    obj.leaf = true
                    orgCameraTree.push(obj)
                })
                state.cameraList = [...orgCameraTree]
            } else {
                commit("GET_CAMERALIST",res)
            }

        })
    },
    /**
     * 获取点位标签列表
     */
    getTagList ({commit},params) {
        let p = getTagList(params)
        p.then(res => {
            commit("GET_TAGLIST",res)
        })
    },
    addTags ({commit},params) {
        let p = addTags(params)
        p.then(res => {
            commit;
            if (res.errors.length === 0) {
                Message({
                    type:'success',
                    message:'添加成功！'
                })
                this.dispatch('getTagList',{start: 0,count: 15,tag: ''})
            } else {
                Message({
                    type:'error',
                    message:res.errors[0].message
                })
            }

        })
    },
    deleteTags ({commit}, params) {
        let p = deleteTags(params)
        p.then(res => {
            res;
            commit;
            Message({
                type:'success',
                message:'删除成功！'
            })
            this.dispatch('getTagList',{start: 0,count: 15,tag: ''})
        })
    },
    updateTags ({commit},params) {
        let p = uptateTags(params)
        p.then(res => {
            commit;
            res;
            Message({
                type:'success',
                message:'更新成功！'
            })
            this.dispatch('getTagList',{start: 0,count: 15,tag: ''})
        })
    },
    setTagForCamera ({commit}, params) {
        let p = setTagForCamera(params)
        p.then(res => {
            commit;
            res;
            Message({
                type:'success',
                message:'配置成功！'
            })
        })
    },
    getCamaraParams ({commit}, params) {
        let p = getCamaraParams(params)
        p.then(res=> {
            commit("GET_CAMERA_PARAMSLIST",res)
        })
    },
    setCamaraParams ({commit}, params) {
        let p = setCamaraParams(params)
        p.then(res=> {
            commit;
            res;
        })
    },
    changeViewDirection ({commit}, params) {
        let p = changeViewDirection(params)
        p.then(res=> {
            commit;
            res;
        })
    },
    getVideoUrl ({commit}, params) {
        let p = getVideoUrl(params)
        p.then(res=> {
            if (params.action === 'CONNECT') {
                res.action = 'CONNECT'
            } else {
                res.action = 'DISCONNECT'
            }
            res.id = params.selectId
            commit("GET_VIDEO_INFO",res)
        })
    },
    videoReplay({commit},params){
        commit;
        let p = videoReplay(params);
        return p;
        // p.then(res=>{
        //     commit("VIDEO_REPLAY",res);
        // })
    },
    checkVideo({commit},params){
        commit;
        let p = checkVideo(params);
        return p;
    },

    updateCamera({commit},params) {
        let p = updateCamera(params)
        commit;
        p.then(res=>{
            res;
        })
    },
    getAllDomTree({commit}, params) {
        let p = getAllDomTree(params)
        p.then(res=>{
            commit("GET_ALL_TREE",res);
        })
    }
}
const mutations = {
    [MIDDLE_LOGIN](...arg) {
        let [, { menus }] = [...arg];
        Vue.ls.set("menuList", menus);
        routes.push("/m/dashboard");
    },
    [SET_HEADER_STATUS](state, data) {
        state.hideHeader = data;
    },
    FIRST_GET_ORGS(state,res){
        state.firstNode = res.results
    },

    GET_ORGS(state,res){
        state.orgsTree = res.results
    },
    GET_NVRLIST(state,res){
        let {pagination,results} = {...res};
        state.nvrList = results
        state.nvrList.forEach(item => {
            item.deviceNumber = 'urn:' + item.urn.organization + ':' + item.urn.group + ':' + item.urn.id
            item.ipPort = item.nap.ipAddress + ':' +  item.nap.port
            switch (item.properties.treatyType) {
                case 'gb_reg':
                    item.treatyType = '国标接入'
                    break;
                case 'onvif_net':
                    item.treatyType = '地标接入'
                    break;
                case 'url':
                    item.treatyType = 'URL流地址接入'
                    break;
                default:
                    item.treatyType = '其他接入'
                    break;
            }
            if (item.status === 'ACTIVE') {
                item.isOpen = '在线'
            } else {
                item.isOpen = '离线'
            }
        })
        state.pagination = pagination
    },
    // 获取相机的列表
    GET_CAMERALIST(state,res) {
        let {pagination,results} = {...res};
        state.cameraList = results
        state.cameraList.forEach(item => {
            item.deviceNumber = 'urn:' + item.urn.organization + ':' + item.urn.group + ':' + item.urn.id
            if (item.nap) {
                item.ipPort = item.nap.ipAddress + ':' +  item.nap.port
            }
            switch (item.properties.treatyType) {
                case 'gb_reg':
                    item.treatyType = '国标接入'
                    break;
                case 'onvif_net':
                    item.treatyType = '地标接入'
                    break;
                case 'url':
                    item.treatyType = 'URL流地址接入'
                    break;
                default:
                    item.treatyType = '其他接入'
                    break;
            }
            if (item.status === 'ACTIVE') {
                item.isOpen = '在线'
            } else {
                item.isOpen = '离线'
            }
            item.auditSearchHead.parents.forEach(items => {
                if (items.indexOf('nvr') > 0) {
                    item.parentUrn = items
                }
            })
        })

        state.paginationOfCamera = pagination
    },
    // 详情的相机列表
    GET_CAMERAOFDEVICELIST(state,res) {
        let {pagination,results} = {...res};
        state.cameraOfDeviceList = results
        state.cameraOfDeviceList.forEach(item => {
            item.deviceNumber = 'urn:' + item.urn.organization + ':' + item.urn.group + ':' + item.urn.id
            if (item.nap) {
                item.ipPort = item.nap.ipAddress + ':' +  item.nap.port
            }
        })
        state.paginationOfCameraDevice = pagination
    },
    GET_TAGLIST(state,res){
        state.activeTagsList = []
        state.tagsList = res.results
        state.tagListPagination = res.pagination
        state.tagsList.forEach(item => {
            if (item.visible) {
                item.isOpen = '启用'
                state.activeTagsList.push(item)
            } else {
                item.isOpen = '停用'
            }
        })
    },
    GET_CAMERA_PARAMSLIST (state,res) {
        state.paramsList = res.result
    },
    GET_VIDEO_INFO (state, res) {
        let obj = {}
        let index = res.id
        let urn = res.results[0].streamNode.urn
        obj.urn = 'urn:' + urn.organization + ':' + urn.group + ':' + urn.id
        obj.url = res.results[0].metadata.url
        obj.playToken = res.results[0].metadata.playToken
        state.videoInfoList[index] = obj

        state.isShowVideo = true
    },
    CLOSE_VIDEO_INFO (state,res) {
        state.videoInfoList = res
    },
    VIDEO_REPLAY(state,res){
        state.videoReplay = res;
    },
    GET_ALL_TREE(state, res) {
        res;
        state.allTree = res.results
    }
}
export default{
    state,
    actions,
    mutations
}