//登录
export const LOGIN = "LOGIN";
export const GET_DEFAULT_DICTS = "GET_DEFAULT_DICTS";
export const GET_HOMEPAGE_INFO = "GET_HOMEPAGE_INFO";
// 一机一档
export const SET_OGOM_CURRENT_STEP = "SET_OGOM_CURRENT_STEP";
export const LOGIN_MENU = "LOGIN_MENU";
// =======================  设备管理  =======================
export const CAPTURE_PIC = "CAPTURE_PIC";
export const SET_ADD_DEVICE_POP = "SET_ADD_DEVICE_POP";
export const GET_DNS = "GET_DNS";
export const SAVE_DNS = "SAVE_DNS";
export const RESET_PWD = "RESET_PWD";
export const GET_DEVICE_LIST = "GET_DEVICE_LIST";
export const GET_DEVICE_DETAIL = "GET_DEVICE_DETAIL";
export const ADD_DEVICE = "ADD_DEVICE";
export const SCAN_CAMERA = "SCAN_CAMERA";
export const DELETE_DEVICE = "DELETE_DEVICE";
export const UPDATE_DEVICE = "UPDATE_DEVICE";
export const FINAL_UPDATE_DEVICE = "FINAL_UPDATE_DEVICE";
// =======================  查看lookup  =======================
export const GET_INTERFACE_LIST = "GET_INTERFACE_LIST";
export const GAT_HOST_INFO = "GAT_HOST_INFO";
export const UPDATE_HOST_INFO = "UPDATE_HOST_INFO";
export const CHANGE_CURRENTINTERFACE = "CHANGE_CURRENTINTERFACE";
