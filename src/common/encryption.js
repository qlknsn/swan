import CryptoJS from 'crypto-js';
// 加密

export const encryptBy = (message, key, iv) => {
    let keyHex = CryptoJS.enc.Utf8.parse(key)
    let ivHex = CryptoJS.enc.Utf8.parse(key)
    let option = { iv: ivHex, mode: CryptoJS.mode.CBC, padding:CryptoJS.pad.Pkcs7 }
    let encrypted = CryptoJS.DES.encrypt(message, keyHex, option)
    return encrypted.ciphertext.toString()
}

// 解密
export const decryptBy = (ciphertext, key, iv) => {
    let keyHex = CryptoJS.enc.Utf8.parse(key)
    let ivHex = CryptoJS.enc.Utf8.parse(iv)
    let decrypted = CryptoJS.DES.decrypt(
        {
            ciphertext: CryptoJS.enc.Hex.parse(message)
        },
        keyHex,
        {
            iv: ivHex,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }
    )
    return decrypted.toString(CryptoJS.enc.Utf8)
}
