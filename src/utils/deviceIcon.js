const deviceIcons = {
  GENERAL: require("../assets/images/screen/office.png"),
  VEHICLE: require("../assets/images/screen/road.png"),
  VIOLATION: require("../assets/images/screen/car.png"),
  FACIAL: require("../assets/images/screen/face.png"),
  DAMAGED: require("../assets/images/screen/errIcon.png"),
  SELECTED: require("../assets/images/screen/selected.png")
};

export function selectdeviceIcon(statu) {
  return deviceIcons[statu];
}
export function getIcons() {
  return deviceIcons;
}
