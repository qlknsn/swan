import Vue from "vue";
import axios from "axios";
// import store from '@/store'
import router from "../router/index.js";
import { VueAxios } from "./axios";
import { config as configuration } from "./config.js";
const CancelToken = axios.CancelToken;
let requestList = [];
// 创建 axios 实例
const service = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL, // api base_url
  timeout: 600000, // 请求超时时间
  // onUploadProgress: function (progressEvent) {
  //     progressEvent;
  //     // Do whatever you want with the native progress event
  //     // console.log('total: ' + progressEvent.total + ',loaded: ' + progressEvent.loaded)
  // },
  cancelToken: new CancelToken(function(e) {
    requestList.push(e);
  })
});

const err = error => {
  var eventMap = new Map([
    [
      "lqcLogin",
      () => {
        router.push("/lqc/login");
      }
    ],
    [
      "MIDDLEWARE",
      () => {
        router.push("/m/login");
      }
    ],
    [
      "WORK-SHEET",
      () => {
        router.push("/w/login");
      }
    ],
    [
      "ROCKFISH-LXZ",
      () => {
        router.push("/rf/liexiongzuo/login");
      }
    ],
    [
      "ROCKFISH-SANLIN",
      () => {
        router.push("/rf/sanlin/login");
      }
    ],
    [
      "ROCKFISH-ZHOUJIADU",
      () => {
        router.push("/rf/zhoujaidu/login");
      }
    ],
    [
      "STARFISH",
      () => {
        router.push("/sf/login");
      }
    ],
    [
      "GATEWAY",
      () => {
        router.push("/gw/login");
      }
    ],
    [
      "USBKEY",
      () => {
        router.push("/usbk/login");
      }
    ],
    [
      "DEVICE-INPUT",
      () => {
        router.push("/di/login");
      }
    ],
    [
      "RADAR-ADMIN",
      () => {
        router.push("/radar/login");
      }
    ],
  ]);
  if (error.response) {
    // todo: 需要对503状态吗进行处理
    if (error.response.status === 401) {
      Vue.prototype.$message({
        type: "error",
        message: "身份认证过期，请重新登录"
      });
      setTimeout(() => {
        const sysName = Vue.ls.get("sysName");
        eventMap.get(sysName)();
      }, 1500);
    }
    if (error.response.status == 404) {
      Vue.prototype.$message({
        type: "error",
        message: "API未找到"
      });
    }
    if (error.response.status === 400) {
      // console.log(router.currentRoute.name)
      Vue.prototype.$message({
        type: "error",
        message: error.response.data.message
      });
    }
    if (error.response.status === 403) {
      // notification.error({
      //     message: 'Forbidden',
      //     description: data.message
      // })
      alert("forbidden");
    }
    if (error.response.status === 503) {
      // console.log(error.response)
      // console.log.error("Bad GateWay")
    }
    if (error.response.status === 500) {
      Vue.prototype.$message({
        type: "error",
        // message: error.response.data.message
        message: "系统错误"
      });
    }
  }
  return Promise.reject(error);
};

// request interceptor
service.interceptors.request.use(config => {
  const token = Vue.ls.get("token");

  let route = router.currentRoute;
  let path = route.fullPath;
  if (path.indexOf("sanlin") > -1) {
    config.headers["identifier"] = configuration.identifier.sanlin.token;
    config.headers["token"] = configuration.identifier.sanlin.tokens;
  } else if (path.indexOf("zhoujiadu") > -1) {
    config.headers["identifier"] = configuration.identifier.zhoujiadu.token;
    config.headers["token"] = configuration.identifier.zhoujiadu.tokens;
  } else if (path.indexOf("liexiongzuo") > -1) {
    config.headers["identifier"] = configuration.identifier.lxz.token;
    config.headers["token"] = configuration.identifier.lxz.tokens;
  } else if (path.indexOf("videoAggregation") > -1) {
    config.headers["identifier"] = configuration.identifier.lxz.token;
    config.headers["token"] = configuration.identifier.lxz.tokens;
  } else if (path.indexOf("zhuqiao") > -1) {
    config.headers["identifier"] = configuration.identifier.zhuqiao.token;
    config.headers["token"] = configuration.identifier.zhuqiao.tokens;
  } else if (path.indexOf("nanhui") > -1) {
    config.headers["identifier"] = configuration.identifier.nanhui.token;
    config.headers["token"] = configuration.identifier.nanhui.tokens;
  } else if (path.indexOf("kangqiao") > -1) {
    config.headers["identifier"] = configuration.identifier.kangqiao.token;
    config.headers["token"] = configuration.identifier.kangqiao.tokens;
  }

  const sysName = Vue.ls.get("sysName");
  switch (sysName) {
    case "LINGANG":
      config.headers["token"] = Vue.ls.get("token");
      break;
    case "lqcLogin":
      config.headers["token"] = Vue.ls.get("token");
      break;
    case "ROCKFISH-LXZ":
      config.headers["identifier"] = configuration.identifier.lxz.token;
      break;
    case "ROCKFISH-SANLIN":
      config.headers["identifier"] = configuration.identifier.sanlin.token;
      config.headers["tokenv2"] = Vue.ls.get("sanlintoken");
      break;
    case "ROCKFISH-ZHOUJIADU":
      config.headers["identifier"] = configuration.identifier.zhoujiadu.token;
      break;
    case "ROCKFISH-ZHUQIAO":
      config.headers["identifier"] = configuration.identifier.zhuqiao.token;
      break;
    case "MIDDLEWARE":
      config.headers["identifier"] = configuration.identifier.lxz.token;
      config.headers["token"] = configuration.identifier.lxz.tokens;
      break;
    case "SLCYLogin":
      config.headers["token"] = Vue.ls.get("slcytoken");
      break;
    case "rongheLogin":
      config.headers["token"] = Vue.ls.get("rongheToken");
      break;
    case "osLogin":
      config.headers["token"] = Vue.ls.get("osLogToken");
      break;
    case "RADAR-ADMIN":
      config.headers["Authorization"] = Vue.ls.get("radarloginToken");
      break;
    default:
      config.headers["token"] = "null";
      break;
  }
  let systemList = ["ROCKFISH-SANLIN"];
  if (systemList.indexOf(sysName) > -1) {
    config.headers["Authorization"] = "Basic YWRtaW46YWRtaW4=";
  }
  config.headers["screen"] = sessionStorage.getItem("screen");
  let workSheetToken = sessionStorage.getItem("tokens");
  config.headers["tokens"] = workSheetToken;
  if (token) {
    config.headers["token"] = token;
    config.headers["Authorization"] = "JWT " + token;
    config.headers["Authorization"] = Vue.ls.get("token");
  }

  return config;
}, err);

// response interceptor
service.interceptors.response.use(response => {
  return response.data;
}, err);

const installer = {
  vm: {},
  install(Vue) {
    Vue.use(VueAxios, service);
  }
};

export { installer as VueAxios, service as axios };
