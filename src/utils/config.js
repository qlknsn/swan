export const config = {
  identifier: {
    sanlin: {
      zone: "urn:sh:district:310115130",
      token: "urn:sh:district:310115130",
      leftScreenUrl: "",
      rightScreenUrl: "",
      tokens: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTQ5MzgyMTksImV4cCI6MjYxNDkzODIxOX0.290JaEOh-BptESwCHSG6mg2VuEnCPUU2qI4yUEkTTzA'
    },
    zhoujiadu: {
      zone: "urn:sh:district:310115007",
      token: "urn:sh:district:310115007",
      leftScreenUrl: "",
      rightScreenUrl: "",
      tokens: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTQ5MzgyNDcsImV4cCI6MjYxNDkzODI0N30.4gswZgPU9JC1_5YZx98IguvFZAeW6B3vXgoZ9QpHK6g'
    },
    lxz: {
      zone: "urn:sh:district:310115000",
      token: "urn:sh:district:310115000",
      leftScreenUrl: "",
      rightScreenUrl: "",
      tokens: 'eyJhbGciOiJIUzI1NiJ9.eyJ1cm4iOiJ1cm46c2g6dXNlcjowMDEiLCJuYW1lIjoi5p2R5bGF5b6u5bmz5Y-wIiwiYXZhaWxhYmxlIjoiMTAwMCIsImNvbmN1cnJlbnQiOiIyMDAiLCJleHAiOjI2MTQ3NjA4Njl9.-cAQS-8DEry2QRlNfuKYv1gVtACfm-wbr2jry6Az45w'
    },
    zhuqiao: {
      zone: "urn:sh:district:310115139",
      token: "urn:sh:district:310115139",
      leftScreenUrl: "",
      rightScreenUrl: "",
      tokens: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTQ5MzgyNjgsImV4cCI6MjYxNDkzODI2OH0.I_88WNHqXcnbR71sVL_HCyJefyBf8v733JKiAvpZYaA'
    },
    kangqiao: {
      zone: "urn:sh:district:310115136",
      token: "urn:sh:district:310115136",
      leftScreenUrl: "",
      rightScreenUrl: "",
      tokens: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTQ5MzgxODUsImV4cCI6MjYxNDkzODE4NX0.mQSrwoY0UUDenyjnNblmW87mwU_KKZo_UwQeWl-0tag'
    },
    nanhui: {
      zone: "urn:sh:district:310115145",
      token: "urn:sh:district:310115145",
      leftScreenUrl: "",
      rightScreenUrl: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTQ5MzgxODUsImV4cCI6MjYxNDkzODE4NX0.mQSrwoY0UUDenyjnNblmW87mwU_KKZo_UwQeWl-0tagd"
    }
  }
};
