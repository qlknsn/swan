# 后台管理系统

## 项目简介

猎熊座基本上所有的后台管理系统都在这个项目中，通过不同的路由进行区分登录，现有系统如下所示。

| 系统名称                     | 登录链接         |
| ---------------------------- | :--------------- |
| starfish系统                 | /sf/login        |
| 三林石斑鱼                   | /rf/sanlin/login |
| 网关管理系统                 | /gw/login        |
| 石斑鱼（摄像头）设备录入系统 | /di/login        |
| 微卡口管理系统               | /mcp/login       |
| 公司官网管理系统             | /office/login    |
| 工单报修系统                 | /w/login         |
| 埋点管理系统                 | /t/login         |
| 视频汇聚中间件系统           | /m/login         |
| 雷达管理系统                 | /radar/login     |
| AR管理系统                   | /ar/login        |
| 三林城运后台管理系统         | /slcy/slcylogin  |
| 老人关爱管理系统             | /lrguanai/login  |
| 周浦微电子社区管理系统       | /r/login         |
| 三林老街后台管理系统         | /os/login        |
| 临港社区综合管理平台         | /lg/login        |

## 代码部署（静态资源部署）

- 你需要一个nodejs版本在V12.10.0以上。

- clone项目，成功后进入到项目目录，运行命令`npm run install` ，你将得到一个目录为 `swan` 的目录，你可以把这个文件夹传输到服务器，以备部署。

- 配置Nginx代理配置转发规则，如下所示。

  | 系统名称               | 源地址                  | 目标地址                                                     |
  | ---------------------- | ----------------------- | ------------------------------------------------------------ |
  | starfish系统           | /seaweed                | http://server:port/public/api/seaweed                        |
  | rockfish系统           | /vis<br />/seaweed      | http://server:port/public/api/vis<br />http://server:port/public/api/seaweed |
  | 网关管理系统           | /gateway                | http://server:port                                           |
  | 石斑鱼设备录入系统     |                         |                                                              |
  | 微卡口管理系统         |                         |                                                              |
  | 公司官网管理系统       | /website                | http://server:port                                           |
  | 工单报修系统           | /flamingo               | http://server:port                                           |
  | 埋点管理系统           | /tracking               | http://server:port                                           |
  | 视频汇聚中间件系统     | /middleware             | http://server:port                                           |
  | 雷达管理系统           | /sphyrnidae             | http://server:port/public/api/sphyrnidae                     |
  | ar管理系统             | /arapi                  | http://server:port/api                                       |
  | 三林城运后台管理系统   | /slcyapi                | http://server:port/public/api/seaweed                        |
  | 老人关爱管理系统       | /lrguanai               | http://server:port/public/api/vis                            |
  | 周浦微电子社区管理系统 |                         |                                                              |
  | 三林老街后台管理系统   | /oldstreet<br />/osback | http://server:port/public/api/seaweed/v1<br />http://server:port/public/api/oldStreet/v1 |
  | 沥青厂系统             | /asp                    | http://server:port/public/api/asp                            |
  | 临港社区综合管理平台   | /lingang                | http://server:port/lingang                                   |
  | 周浦融合               | /hip                    | http://server:port/public/api/hip                            |

## 代码部署（docker部署）

- 首先login到公司的docker私服仓库
- 运行命令 `docker build -t registry.cn-shanghai.aliyuncs.com/bearhunting-fe/swan:{version.number} .` 
- 成功后再运行命令`docker push registry.cn-shanghai.aliyuncs.com/bearhunting-fe/swan:{version.number}`
- 把其中的 `{version.number}` 替换成你自己的版本号

## 自动化部署

采用teambition中提供的flow功能进行自动化部署，默认master分支代码触发，部署结果将通过钉钉发送。

## 技术架构

项目采用 `Vue`作为前端开发框架，使用 `elementUI`作为样式库。


# 三林石斑鱼
## 项目主要结构
  1. view视图文件： `/src/views/rockfish`
  2. 组件文件： `/src/components/rockfish`
  3. 路由模块: `/src/router/rockfishRouter`
    所有路由跳转都在 `/src/router/index.js` 文件内
  4. vuex数据：
     接口地址文件： `/src/store/api/rockfishUrl.js`
     ajax方法文件： `/src/store/api/addTimeTask.js`、`/src/store/api/login`、`/src/store/api/videoPolling`
     ajax方法请求：`/src/store/module/rockfish/addTimeTask.js`、`/src/store/module/rockfish/login.js`、`/src/store/module/rockfish/videoPolling.js`

## 测试环境地址
## 正式环境地址
`http://10.9.0.61:58101/rf/sanlin/login`
 用户名：admin
 密码：admin123

## 项目仓库说明
地址：http://gitlab.bearhunting.cn/frontend/swan.git

`master`主分支为线上部署分支


# 三林老街大屏
## 项目主要结构
  1. view视图文件： `/src/views/oldStreet`
  2. 路由模块: `/src/router/osRouter`
    所有路由跳转都在 `/src/router/index.js` 文件内
  3. vuex数据：
     接口地址文件： `/src/store/api/oldStreet/oldStreetUrl.js`
     ajax方法文件： `/src/store/api/oldStreet.js`
     ajax方法请求：`/src/store/module/oldStreet/oldStreet.js`

## 测试环境地址
`http://frontend.bearhunting.cn:58101/os/login`
 账号：18621509310密码：admin123
## 公网地址
`http://sanlinsmart.bearhunting.cn:58110/os/login`
 用户名：admin
 密码：admin123
## 内网地址
`http://10.5.200.254:58101/os/login`
 用户名：admin
 密码：admin123

## 项目仓库说明
地址：http://gitlab.bearhunting.cn/frontend/swan.git

`master`主分支为线上部署分支