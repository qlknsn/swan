const path = require("path");
require("events").EventEmitter.defaultMaxListeners = 0;
function resolve(dir) {
  return path.join(__dirname, dir);
}
module.exports = {
  outputDir: process.env.NODE_ENV === "production" ? `swan` : "swan-test",
  chainWebpack: (config) => {
    config.resolve.alias
      .set("@", resolve("src"))
      .set("assets", resolve("src/assets"))
      .set("components", resolve("src/components"))
      .set("public", resolve("public"));
  },
  configureWebpack: {
    externals: {
      AMap: "AMap"
    },
    resolve: {
      alias: {
        vue$: "vue/dist/vue.esm.js"
      }
    },
    module: {
      rules: [
        {
          test: /\.(swf|ttf|eot|svg|woff(2))(\?[a-z0-9]+)?$/,
          loader: "file-loader"
        }
      ]
    }
  },
  devServer: {
    proxy: {
      "/url": {
        // target: "http://192.168.199.181:5889",
        // target: "http://47.100.65.32:39001/url", // 临港后台管理代理
        target: "http://cjtest.bearhunting.cn:39002/url", // 临港后台管理代理
        changeOrigin: true,
        pathRewrite: {
          "^/url": ""
        }
      },

      "/flamingo": {
        // target: "http://192.168.199.181:5889",
        target: "http://localhost:5889",
        changeOrigin: true,
        pathRewrite: {
          "^/flamingo": ""
        }
      },
      "/website": {
        // target: 'http://192.168.63.60:58118',
        target: "http://www.bearhunting.cn:58118",
        changeOrigin: true,
        pathRewrite: {
          "^/website": ""
        }
      },
      "/lxz": {
        // target: 'http://192.168.250.97:19001',
        target: "http://192.168.25.54:19001",
        changeOrigin: true,
        pathRewrite: {
          "^/lxz": ""
        }
      },
      "/Ukey": {
        target: "http://192.168.199.164:8040",
        // target: "http://192.168.63.125:8040",
        changeOrigin: true,
        pathRewrite: {
          "^/Ukey": ""
        }
      },
      "/gateway": {
        target: "http://192.168.63.97:8000",
        changeOrigin: true,
        pathRewrite: {
          "^/gateway": ""
        }
      },
      "/seaweed": {
        // target: "http://starfish-backend-test.bearhunting.cn:9001", // /public/api/seaweed/v1/face/countInfo
        // 微卡口模块统计信息接口
        target: "http://10.5.200.254:9001/public/api/seaweed", // http://192.168.63.14:9001/
        // target: "http://192.168.63.195:9001/public/api/seaweed", // http://192.168.63.14:9001/
        changeOrigin: true,
        pathRewrite: {
          "^/seaweed": ""
        }
      },
      "/vcs": {
        // target: "http://192.168.25.54:8081/public/api/vis", //   猎熊座视频轮巡后台地址、
        // target: "http://10.2.0.14:8081", //   祝桥视频轮巡后台地址、
        // target: "http://10.3.200.10:8081", //   周家渡视频轮巡后台地址、
        target: "http://10.5.200.254:8086/public/api/vis", //   三林视频轮巡后台地址、
        // target: "http://192.168.25.31:8081/public/api/vis", //   三林视频轮巡后台地址、
        // target:'http://192.168.25.110:28076',
        changeOrigin: true,
        pathRewrite: { "^/vcs": "" }
      },
      "/vis": {
        // target: "http://192.168.25.54:8081/public/api/vis", //   猎熊座视频轮巡后台地址、
        // target: "http://10.2.0.14:8081", //   祝桥频轮巡后台地址、
        // target: "http://192.168.63.32:8081/public/api/vis", //   周家渡视频轮巡后台地址、
        target: "http://10.5.200.254:8081/public/api/vis", //   三林视频轮巡后台地址、
        // target: "http://10.224.106.38:8081/public/api/vis", //   三林视频轮巡后台地址、
        changeOrigin: true,
        pathRewrite: {
          "^/vis": ""
        }
      },
      "/zq": {
        // target: 'http://10.2.0.11:8089',
        // target: 'http://10.5.200.254:8086',
        target: "http://192.168.25.54:18888",
        changeOrigin: true,
        pathRewrite: {
          "^/zq": ""
        }
      },
      "/picture": {
        target: "http://10.2.0.11:8089",
        changeOrigin: true,
        pathRewrite: {
          "^/picture": "/picture"
        }
      },
      "/wasteTruck": {
        target: "http://10.2.0.11:8089",
        changeOrigin: true,
        pathRewrite: {
          "^/wasteTruck": "/wasteTruck"
        }
      },
      "/tracking": {
        target: "http://192.168.199.181:3344",
        changeOrigin: true,
        pathRewrite: {
          "^/tracking": ""
        }
      },
      "/middleware": {
        target: "http://192.168.25.54:9999",
        changeOrigin: true,
        pathRewrite: {
          "^/middleware": ""
        }
      },
      "/viis": {
        target: "http://192.168.25.39:9001/public/api/viis",
        changeOrigin: true,
        pathRewrite: {
          "^/viis": ""
        }
      },
      // "/api": {
      //   // target:"http://192.168.123.232:8032/api",
      //   // target:"http://192.168.63.162:8031/api",
      //   // target: "http://192.168.25.100:8031/api",
      //   target: "http://10.5.200.254:7031/api",
      //   changeOrigin: true,
      //   pathRewrite: {
      //     "^/api": ""
      //   }
      // },
      "/sphyrnidae": {
        // target: "http://47.100.19.229:9001/public/api/sphyrnidae",
        target: "https://radar.bearhunting.cn/sphyrnidae",
        // target: "http://192.168.10.129:8888/public/api/sphyrnidae",
        changeOrigin: true,
        pathRewrite: {
          "^/sphyrnidae": ""
        }
      },
      "/arapi": {
        // target: "http://192.168.63.73:8080/api",
        target: "http://192.168.25.26:8088/api",
        changeOrigin: true,
        pathRewrite: {
          "^/arapi": ""
        }
      },
      "/arxz": {
        target: "http://192.168.25.43:38081/public/api/robin/v1",
        changeOrigin: true,
        pathRewrite: {
          "^/arxz": ""
        }
      },
      "/arxapi": {
        target: "http://192.168.25.43:38081/api",
        changeOrigin: true,
        pathRewrite: {
          "^/arxapi": ""
        }
      },
      "/slcyapi": {
        target: "http://47.100.65.32:33591/public/api/seaweed", //三林城运后台正式环境
        changeOrigin: true,
        pathRewrite: {
          "^/slcyapi": ""
        }
      },
      "/lrguanai": {
        target: "http://47.100.65.32:17901/public/api/vis",
        // target: "https://homecare-backend.bearhunting.cn/public/api/vis",
        changeOrigin: true,
        pathRewrite: {
          "^/lrguanai": ""
        }
      },
      "/hip": {
        // 这是
        // target: "http://homecare-backend.bearhunting.cn:19001/public/api/hip",
        target: "http://47.100.65.32:19001/public/api/hip",
        changeOrigin: true,
        pathRewrite: {
          "^/hip": ""
        }
      },
      "/oldstreet": {
        target: "http://47.100.65.32:33591/public/api/seaweed/v1",
        // target: "http://sanlinsmart.bearhunting.cn/public/api/seaweed/v1",
        changeOrigin: true,
        pathRewrite: {
          "^/oldstreet": ""
        }
      },
      "/osback": {
        target: "http://47.100.65.32:33591/public/api/oldStreet/v1",
        // target: "http://sanlinsmart.bearhunting.cn/public/api/oldStreet/v1",
        changeOrigin: true,
        pathRewrite: {
          "^/osback": ""
        }
      },

      "/asp": {
        target: "http://47.100.65.32:17902/public/api/asp",
        changeOrigin: true,
        pathRewrite: {
          "^/asp": ""
        }
      },
      "/linggang": {
        target: "http://192.168.25.2:39001/linggang",
        //target: "http://10.242.212.152:39001/linggang",
        changeOrigin: true,
        pathRewrite: {
          "^/linggang": ""
        }
      }
    }
  }
};
